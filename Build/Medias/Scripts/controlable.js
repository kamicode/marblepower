var camera = engine.getElement("Camera");

if(! soundEngine.musicFinished("MainMusic")) {
    soundEngine.stopMusic("MainMusic");
}

if(soundEngine.musicFinished("Music")) {
    soundEngine.playMusic("Music", "Musics/IrrlichtTheme.ogg");
    soundEngine.setMusicLoop("Music", true);
}

function update(time) {
    pos = camera.getPosition();
    dir = self.getPosition();
    soundEngine.setSoundListenerProperties(1, [pos.X, pos.Y, pos.Z], [0, 0, 0], [dir.X, dir.Y, dir.Z], [0, 1, 0]);

    if(eventEngine.isKeyDown("KEY_ESCAPE")) {
        if(! soundEngine.musicFinished("Music")) {
            soundEngine.stopMusic("Music");
        }
        engine.setNextMap("Map0.dat");
        engine.setEtat("changeMap");
    }
    else if(self.isInCollisionWithMap()) {
        if(eventEngine.isKeyDown("KEY_UP") || eventEngine.isKeyDown("KEY_KEY_Z")) {
            dir2 = dir;
            dir2.X -= pos.X;
            dir2.Y = 0;
            dir2.Z -= pos.Z;
            dir2 = engine.normalizeVector(dir2);
            dir2.X *= self.getMasse() * 50;
            dir2.Z *= self.getMasse() * 50;
            self.addForce(dir2.X, dir2.Y, dir2.Z);
        }
        else if (eventEngine.isKeyDown("KEY_DOWN") || eventEngine.isKeyDown("KEY_KEY_S")) {
            dir2 = dir;
            dir2.X -= pos.X;
            dir2.Y = 0;
            dir2.Z -= pos.Z;
            dir2 = engine.normalizeVector(dir2);
            dir2.X *= self.getMasse() * -50;
            dir2.Z *= self.getMasse() * -50;
            self.addForce(dir2.X, dir2.Y, dir2.Z);
        }

        if (eventEngine.isKeyDown("KEY_LEFT") || eventEngine.isKeyDown("KEY_KEY_Q")) {
            dir2 = dir;
            dir2.X -= pos.X;
            dir2.Y = 0;
            dir2.Z -= pos.Z;
            dir3 = new Object();
            dir3.X = 0;
            dir3.Y = 1;
            dir3.Z = 0;
            dir2 = engine.crossProductVector(dir2, dir3);
            dir2 = engine.normalizeVector(dir2);
            dir2.X *= self.getMasse() * 50;
            dir2.Z *= self.getMasse() * 50;
            self.addForce(dir2.X, dir2.Y, dir2.Z);
        }
        else if (eventEngine.isKeyDown("KEY_RIGHT") || eventEngine.isKeyDown("KEY_KEY_D")) {
            dir2 = dir;
            dir2.X -= pos.X;
            dir2.Y = 0;
            dir2.Z -= pos.Z;
            dir3 = new Object();
            dir3.X = 0;
            dir3.Y = -1;
            dir3.Z = 0;
            dir2 = engine.crossProductVector(dir2, dir3);
            dir2 = engine.normalizeVector(dir2);
            dir2.X *= self.getMasse() * 50;
            dir2.Z *= self.getMasse() * 50;
            self.addForce(dir2.X, dir2.Y, dir2.Z);
        }
    }
}
