dir = {"X": 0, "Y": 0, "Z": 0};

timeBuf = 0;

function update(time) {
    timeBuf += time;

    if(timeBuf > 10) {
        timeBuf = 0;
        updateDir();
    }
}

function updateDir() {
    pos = self.getPosition();

    dir.X += engine.frandom(-0.1, 0.1);
    if(dir.X > 1)
        dir.X = 1;
    if(dir.X < -1)
        dir.X = -1;
    if(pos.X < -25)
        dir.X = 1;
    if(pos.X > 25)
        dir.X = -1;

    dir.Y += engine.frandom(-0.1, 0.1);
    if(dir.Y > 1)
        dir.Y = 1;
    if(dir.Y < -1)
        dir.Y = -1;
    if(pos.Y < -25)
        dir.Y = 1;
    if(pos.Y > 25)
        dir.Y = -1;

    dir.Z += engine.frandom(-0.1, 0.1);
    if(dir.Z > 1)
        dir.Z = 1;
    if(dir.Z < -1)
        dir.Z = -1;
    if(pos.Z < -25)
        dir.Z = 1;
    if(pos.Z > 25)
        dir.Z = -1;

    engine.setSharedPool("dirX", dir.X);
    engine.setSharedPool("dirY", dir.Y);
    engine.setSharedPool("dirZ", dir.Z);
}
