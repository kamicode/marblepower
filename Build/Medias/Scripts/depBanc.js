dir = {"X": 0, "Y": 0, "Z": 0};
timeBuf = 0;

function update(time) {
    timeBuf += time;
    if(timeBuf > 10) {
        timeBuf = 0;
        dir.X = parseFloat(engine.getSharedPoolVariable("dirX"));
        dir.Y = parseFloat(engine.getSharedPoolVariable("dirY"));
        dir.Z = parseFloat(engine.getSharedPoolVariable("dirZ"));
    }
    pos = self.getPosition();
    if(pos.X + dir.X > -30 && pos.X + dir.X < 30 && pos.Y + dir.Y > -30 && pos.Y + dir.Y < 30 && pos.Z + dir.Z > -30 && pos.Z + dir.Z < 30) {
        pos.X += dir.X;
        pos.Y += dir.Y;
        pos.Z += dir.Z;
        self.setTarget(pos);
        rot = self.getRotation();
        rot.Y += 90;
        self.setRotation(rot);
        self.setPosition(pos);
    }
}
