var objName = $$;
obj = engine.getElement(objName);

var rotationX = $$;
var rotationY = $$;

posSphere = engine.cartesianToSpheric(self.getPosition());
phy = posSphere.phy;
theta = posSphere.theta;
rho = posSphere.rho;

eventEngine.setMouseVisible(false);
oldPos = eventEngine.getMousePos();

function update(time) {
    wheel = eventEngine.getWheel();
    if(wheel != 0) {
        eventEngine.setWheel(0);
        rho -= 2 * wheel;
        if (rho < 10) {
            rho = 10;
        }
        else if (rho > 100) {
            rho = 100;
        }
    }

    newPos = eventEngine.getMousePos();
    eventEngine.setMousePos(oldPos);

    deltaX = newPos.X - oldPos.X;
    deltaY = newPos.Y - oldPos.Y;

    rotationX += deltaX * 10;
    rotationY += deltaY * 10;
    if (rotationY >= -0.01) {
        rotationY = -0.01;
    }
    else if (rotationY <= -3.14) {
        rotationY = -3.14;
    }

    phy = rho * Math.cos(rotationY);
    theta = rho * Math.sin(rotationY);

    camPos = new Object();
    camPos.X = 0;
    camPos.Y = phy;
    camPos.Z = theta;

    posObj = obj.getPosition();
    camPos.X += posObj.X;
    camPos.Y += posObj.Y;
    camPos.Z += posObj.Z;
    camPos = engine.rotateXZBy(-rotationX * 40, camPos, posObj);

    self.setPosition(camPos);
    self.setTarget(posObj);

    up = self.getUpVector();
    posObj.X -= camPos.X;
    posObj.Y -= camPos.Y;
    posObj.Z -= camPos.Z;
    soundEngine.setSoundListenerProperties(1, [camPos.X, camPos.Y, camPos.Z], [0, 0, 0], [posObj.X, posObj.Y, posObj.Z], [up.X, up.Y, up.Z]);
}
