var temps = 0;
var arrive = false;
var nextMap = $$;
var minPos = $$;
var elt = engine.getElement("Boule");

function update(time) {
    if(self.nbCollisions("Boule") > 0) {
        arrive = true;
        temps += time;
    }
    else {
        arrive = false;
        temps = 0;
    }
    if(arrive == true && temps > 1000) {
        engine.setNextMap(nextMap);
        engine.setEtat("changeMap");
    }

    if(elt.getPosition().Y < minPos) {
        engine.setEtat("changeMap");
    }
}
