dir = {"X": engine.frandom(-1, 1),
        "Y": engine.frandom(-1, 1),
        "Z": engine.frandom(-1, 1)};
timeBuf = 0;

function update(time) {

    timeBuf += time;

    if(timeBuf > 1000) {
        timeBuf = 0;
        dir.X = engine.frandom(-1, 1);
        dir.Y = engine.frandom(-1, 1);
        dir.Z = engine.frandom(-1, 1);
    }
    pos = self.getPosition();
    pos.X += dir.X;
    pos.Y += dir.Y;
    pos.Z += dir.Z;

    if(pos.X < -30)
        pos.X = -30;
    else if(pos.X > 30)
        pos.X = 30;

    if(pos.Y < 5)
        pos.Y = 5;
    else if(pos.Y > 20)
        pos.Y = 20;

    if(pos.Z < -30)
        pos.Z = -30;
    else if(pos.Z > 30)
        pos.Z = 30;

    self.setTarget(pos);
    rot = self.getRotation();
    rot.Y += 90;
    self.setRotation(rot);

    self.setPosition(pos);
}
