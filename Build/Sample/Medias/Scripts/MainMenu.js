var nextMap = $$;

{
    eventEngine.setMouseVisible(true);
    eventEngine.setMousePos({"X": 0.5, "Y": 0.5});
}

if(! soundEngine.musicFinished("Music")) {
    soundEngine.stopMusic("Music");
}

if(soundEngine.musicFinished("MainMusic")) {
    soundEngine.playMusic("MainMusic", "Musics/MusicMono.ogg");
    soundEngine.setMusicLoop("MainMusic", true);
}

soundEngine.setSoundListenerProperties(1, [0, 0, -1], [0, 0, 0], [0, 0, 1], [0, 1, 0]);

function update(time) {
    if(eventEngine.isKeyDown("KEY_ESCAPE")) {
        engine.setEtat("quit");
    }
}

function QuitPushButton() {
    engine.setEtat("quit");
}

function LaunchPushButton() {
    soundEngine.stopMusic("MainMusic");
    engine.setNextMap(nextMap);
    engine.setEtat("changeMap");
}

function OptionPushButton() {
    engine.setNextMap("Option");
    engine.setEtat("changeMap");
}
