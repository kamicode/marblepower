
readProjectFile();
//PrintJoystick();

function update(time) {
    if(eventEngine.isKeyDown("KEY_ESCAPE")) {
        engine.setNextMap("Map0");
        engine.setEtat("changeMap");
    }
}

function ReturnPushButton() {
    engine.setNextMap("Map0");
    engine.setEtat("changeMap");
}

function ValidPushButton() {
    guiEngine.GuiSetVisible("Valid", true);
    guiEngine.GuiSetVisible("Main", false);
}

function PrintJoystick() {
    num = eventEngine.numJoysticks();
    list = eventEngine.listJoysticks();
    engine.log("Error", "***************************************************");
    engine.log("Error", "Number of joysticks : " + num);
    engine.log("Error", "listJoysticks");
    for(var i in list)
        engine.log("Error", i + " : " + list[i]);
    engine.log("Error", "***************************************************");
}

function readProjectFile() {
    content = engine.readProjectFile();

    width = content.match(/width=\"(\w*)\"/)[1];
    height = content.match(/height=\"(\w*)\"/)[1];
    resolution = width + "*" + height;
    guiEngine.GuiComboAddItem("Resolution", "800*600", "OgreTrayImages", "Shade");
    guiEngine.GuiComboAddItem("Resolution", "1280*1024", "OgreTrayImages", "Shade");
    guiEngine.GuiComboAddItem("Resolution", resolution, "OgreTrayImages", "Shade");
    guiEngine.GuiComboSetSelected("Resolution", resolution);

    driverType = content.match(/driverType=\"(\w*)\"/)[1];
    guiEngine.GuiComboAddItem("Driver", "EDT_DIRECT3D9");
    guiEngine.GuiComboAddItem("Driver", "EDT_OPENGL");
    guiEngine.GuiComboSetSelected("Driver", driverType);

    //autoSize = content.match(/autoSize=\"(\w*)\"/)[1];

    bits = content.match(/bits=\"(\w*)\"/)[1];
    guiEngine.GuiSpinnerSetValue("Bits", bits);

    zBufferBits = content.match(/zBufferBits=\"(\w*)\"/)[1];
    guiEngine.GuiSpinnerSetValue("zBufferBits", zBufferBits);

    antiAlias = content.match(/antiAlias=\"(\w*)\"/)[1];
    guiEngine.GuiSpinnerSetValue("AntiAlias", antiAlias);

    fullscreen = content.match(/fullscreen=\"(\w*)\"/)[1];
    guiEngine.setCheckBoxSelect("FullScreen", fullscreen == "1");

    stencilbuffer = content.match(/stencilbuffer=\"(\w*)\"/)[1];
    guiEngine.setCheckBoxSelect("Stencilbuffer", stencilbuffer == "1");

    vsync = content.match(/vsync=\"(\w*)\"/)[1];
    guiEngine.setCheckBoxSelect("VSync", vsync == "1");

    withAlphaChannel = content.match(/withAlphaChannel=\"(\w*)\"/)[1];
    guiEngine.setCheckBoxSelect("WithAlphaChannel", withAlphaChannel == "1");

    doublebuffer = content.match(/doublebuffer=\"(\w*)\"/)[1];
    guiEngine.setCheckBoxSelect("Doublebuffer", doublebuffer == "1");

    stereobuffer = content.match(/stereobuffer=\"(\w*)\"/)[1];
    guiEngine.setCheckBoxSelect("Stereobuffer", stereobuffer == "1");

    highPrecisionFPU = content.match(/highPrecisionFPU=\"(\w*)\"/)[1];
    guiEngine.setCheckBoxSelect("HighPrecisionFPU", highPrecisionFPU == "1");

    driverMultithreaded = content.match(/driverMultithreaded=\"(\w*)\"/)[1];
    guiEngine.setCheckBoxSelect("DriverMultithreaded", driverMultithreaded == "1");

    usePerformanceTimer = content.match(/usePerformanceTimer=\"(\w*)\"/)[1];
    guiEngine.setCheckBoxSelect("UsePerformanceTimer", usePerformanceTimer == "1");
}

function OkPushButton() {
    resolution = guiEngine.GuiComboGetSelected("Resolution");
    width = resolution.match(/(\w*)\*(\w*)/)[1];
    height = resolution.match(/(\w*)\*(\w*)/)[2];
    content = content.replace(/width=\"(\w*)\"/, "width=\"" + width + "\"");
    content = content.replace(/height=\"(\w*)\"/, "height=\"" + height + "\"");

    content = content.replace(/driverType=\"(\w*)\"/, "driverType=\"" + guiEngine.GuiComboGetSelected("Driver") + "\"");

    content = content.replace(/bits=\"(\w*)\"/, "bits=\"" + guiEngine.GuiSpinnerGetValue("Bits") + "\"");
    content = content.replace(/zBufferBits=\"(\w*)\"/, "zBufferBits=\"" + guiEngine.GuiSpinnerGetValue("zBufferBits") + "\"");
    content = content.replace(/antiAlias=\"(\w*)\"/, "antiAlias=\"" + guiEngine.GuiSpinnerGetValue("AntiAlias") + "\"");
    content = content.replace(/fullscreen=\"(\w*)\"/, "fullscreen=\"" + (guiEngine.isCheckBoxSelected("FullScreen") ? 1 : 0) + "\"");
    content = content.replace(/stencilbuffer=\"(\w*)\"/, "stencilbuffer=\"" + (guiEngine.isCheckBoxSelected("Stencilbuffer") ? 1 : 0) + "\"");
    content = content.replace(/vsync=\"(\w*)\"/, "vsync=\"" + (guiEngine.isCheckBoxSelected("VSync") ? 1 : 0) + "\"");
    content = content.replace(/doublebuffer=\"(\w*)\"/, "doublebuffer=\"" + (guiEngine.isCheckBoxSelected("Doublebuffer") ? 1 : 0) + "\"");
    content = content.replace(/stereobuffer=\"(\w*)\"/, "stereobuffer=\"" + (guiEngine.isCheckBoxSelected("Stereobuffer") ? 1 : 0) + "\"");
    content = content.replace(/highPrecisionFPU=\"(\w*)\"/, "highPrecisionFPU=\"" + (guiEngine.isCheckBoxSelected("HighPrecisionFPU") ? 1 : 0) + "\"");
    content = content.replace(/withAlphaChannel=\"(\w*)\"/, "withAlphaChannel=\"" + (guiEngine.isCheckBoxSelected("WithAlphaChannel") ? 1 : 0) + "\"");
    content = content.replace(/driverMultithreaded=\"(\w*)\"/, "driverMultithreaded=\"" + (guiEngine.isCheckBoxSelected("DriverMultithreaded") ? 1 : 0) + "\"");
    content = content.replace(/usePerformanceTimer=\"(\w*)\"/, "usePerformanceTimer=\"" + (guiEngine.isCheckBoxSelected("UsePerformanceTimer") ? 1 : 0) + "\"");

    engine.writeProjectFile(content);

    ReturnPushButton();
}
