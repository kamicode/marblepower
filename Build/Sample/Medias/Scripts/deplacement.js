dir = {"X": -1, "Y": 0, "Z": 0};

function update(time) {
    pos = self.getPosition();
    pos.X += dir.X;
    pos.Y += dir.Y;
    pos.Z += dir.Z;

    if(pos.X <= 0) {
        dir.X = 1;
    }
    if(pos.X >= 50) {
        dir.X = -1;
    }

    self.setPosition(pos);
}
