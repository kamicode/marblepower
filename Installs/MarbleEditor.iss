﻿#define MyAppName "MarbleEditor"
#define MyAppVersion "1.0"
#define MyAppPublisher "Haiecapique"
#define MyAppURL "http://www.haiecapique.fr/marble.html"
#define MyFireWallExept "{app}\MarbleEditor.exe"
#define MyAppExeName "MarbleEditor.exe"

[Setup]
AppID={{AE523124-1DCC-4CCC-84CA-51B126E758B5}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} - {#MyAppVersion}
AppCopyright=Copyright (C) 2012 Haiecapique
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\Haiecapique\{#MyAppName}
DefaultGroupName={#MyAppPublisher}\{#MyAppName}
OutputDir=.
OutputBaseFilename=Setup {#MyAppName}
AllowNoIcons=True
LicenseFile=lgpl.txt
ShowLanguageDialog=no
SetupIconFile=Icon.ico
UninstallDisplayIcon={app}\Icon.ico
UninstallDisplayName={#MyAppName} - {#MyAppVersion}
VersionInfoVersion=1.0.0.0
PrivilegesRequired=none
UsePreviousSetupType=False
UsePreviousAppDir=False
UsePreviousTasks=False
UsePreviousLanguage=False
Compression=none
SolidCompression=False
InternalCompressLevel=none
AllowUNCPath=True
ShowTasksTreeLines=True
AlwaysShowGroupOnReadyPage=True
AlwaysShowDirOnReadyPage=True
AlwaysUsePersonalGroup=True
AppendDefaultGroupName=False

[Languages]
Name: French; MessagesFile: compiler:Languages\French.isl;

[CustomMessages]
thirdParty="Installation de logiciels tiers"
directX="Installation de DirectX"
vcredist="Installation des redistribuables visual studio"
openAL="Installation de OpenAL
firewall="Ajout d'exception par-feu"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.0

[Files]
Source: "..\Build\{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion restartreplace; Permissions: everyone-full
Source: "..\Build\*"; DestDir: "{app}"; Flags: ignoreversion restartreplace recursesubdirs createallsubdirs; Permissions: everyone-full
Source: "Icon.ico"; DestDir: "{app}"; Flags: ignoreversion restartreplace; Permissions: everyone-full
Source: "Support\*"; DestDir: "{app}\Support\"; Flags: ignoreversion restartreplace recursesubdirs createallsubdirs; Permissions: everyone-full

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; WorkingDir: "{app}"; IconFilename: "{app}\Icon.ico"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"; IconFilename: "{app}\Icon.ico"
Name: "{group}\{cm:UninstallProgram, {#MyAppName}}"; Filename: "{uninstallexe}"; IconFilename: "{app}\Icon.ico"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; WorkingDir: "{app}"; IconFilename: "{app}\Icon.ico"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; WorkingDir: "{app}"; IconFilename: "{app}\Icon.ico"; Tasks: quicklaunchicon

[Run]
Filename: {app}\{#MyAppExeName}; Description: {cm:LaunchProgram,{#StringChange(MyAppName, "&", "&&")}}; Flags: nowait postinstall skipifsilent;

[UninstallDelete]
Type: filesandordirs; Name: {app}\*;

[code]

#ifdef UNICODE
  #define uni "W"
#else
  #define uni "A"
#endif

function AddApplicationToExceptionList(path: String; name: String): Boolean;
  external 'AddApplicationToExceptionList{#uni}@files:FirewallInstallHelper.dll stdcall setuponly';
function RemoveApplicationFromExceptionList(path: String): Boolean;
  external 'RemoveApplicationFromExceptionList{#uni}@{app}\Support\FireWallInstall\FirewallInstallHelper.dll stdcall uninstallonly';

function InitializeSetup(): Boolean;
var
  name : String;
  check: Boolean;
begin
  Result := True;
  check := not CheckForMutexes('MarbleEditorSetupMutex');
  name := FmtMessage(SetupMessage(msgSetupAppRunningError), [ExpandConstant('{srcexe}')]);

  while not check do begin
    check := MsgBox(name, mbConfirmation, MB_YESNO) = IDYES;
    if check then begin
      check := not CheckForMutexes('MarbleEditorSetupMutex');
      Result := True;
    end else begin
      check := True;
      Result := False;
    end;
  end;
  CreateMutex('MarbleEditorSetupMutex');
end;

procedure CurStepChanged(CurStep: TSetupStep);
var
  ProgressPage: TOutputProgressWizardPage;
  ResultCode: Integer;
begin
  if CurStep = ssPostInstall then begin
    try
      ProgressPage := CreateOutputProgressPage(ExpandConstant('{cm:thirdParty}'), '');
      progressPage.ProgressBar.Style := npbstMarquee
      ProgressPage.SetProgress(0, 100);
      ProgressPage.Show;

      ProgressPage.SetText(ExpandConstant('{cm:thirdParty}'), ExpandConstant('{cm:vcredist}'));
      Exec(ExpandConstant('{app}\Support\VCRedist\vcredist_x86.exe'),
      '/Q', ExpandConstant('{app}\Support\VCRedist'),
      SW_HIDE, ewWaitUntilTerminated, ResultCode);

      ProgressPage.SetProgress(40, 100);

      ProgressPage.SetText(ExpandConstant('{cm:thirdParty}'), ExpandConstant('{cm:directX}'));
      Exec(ExpandConstant('{app}\Support\DirectX\DXSETUP.exe'),
      '/Silent', ExpandConstant('{app}\Support\DirectX'),
      SW_HIDE, ewWaitUntilTerminated, ResultCode);

      ProgressPage.SetProgress(80, 100);

      ProgressPage.SetText(ExpandConstant('{cm:thirdParty}'), ExpandConstant('{cm:openAL}'));
      Exec(ExpandConstant('{app}\Support\OpenAL\oalinst.exe'),
      '/S', ExpandConstant('{app}\Support\OpenAL'),
      SW_HIDE, ewWaitUntilTerminated, ResultCode);

      ProgressPage.SetProgress(90, 100);

      ProgressPage.SetText(ExpandConstant('{cm:thirdParty}'), ExpandConstant('{cm:firewall}'));
      AddApplicationToExceptionList(ExpandConstant('{#MyFireWallExept}'), ExpandConstant('{#MyAppName}'));
      UnloadDll(ExpandConstant('{app}\Support\FireWallInstall\FirewallInstallHelper.dll'));

      ProgressPage.SetProgress(100, 100);
    finally
      ProgressPage.Hide;
    end;
  end;
end;

procedure CurUninstallStepChanged (CurUninstallStep: TUninstallStep);
begin
  case CurUninstallStep of
    usUninstall:
      begin
        RemoveApplicationFromExceptionList(ExpandConstant('{{#MyFireWallExept}'));
        UnloadDll(ExpandConstant('{app}\Support\FireWallInstall\FirewallInstallHelper.dll'));
      end;
  end;
end;
