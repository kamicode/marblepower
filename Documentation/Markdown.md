@mainpage MarblePower

![marble](http://haiecapique.fr/Public/images/miniMarble.jpg)

# Ce projet est un moteur de jeu 3D écrit en C++. Il sous licence LGPL.

## Les installeurs sont disponibles [ici](http://haiecapique.fr/Applications/Marbles) (Pas forcement à jour).

## Le projet est disponible [ici](https://bitbucket.org/haiecapique/marblepower) (Gestionnaire de version Bitbucket basé sur Git).

### Commande : 

	$ git clone https://bitbucket.org/haiecapique/marblepower.git
	
## Des images sont disponibles [ici](https://bitbucket.org/haiecapique/marblepower/wiki/Images).


## Il est réalisé avec les bibliothèques :

* Irrlicht : Pour l'affichage 3D
* CEGUI : Pour la GUI des jeux
* Bullet : Pour la physique
* OpenAL : Pour le son
* TinyXML2 : Pour la persistance
* Qt : Pour l'IHM de l'éditeur et le Scripting via le module QScript

## Les Outils utilisés :

* QtCreator
* Git (Console et Tortoise Git)
* MSVC 10 (Windows SDK 7.1)
* Doxygen
* Graphviz
* InnoSetup


## Travail en cours :

Je travaille à un meilleur support de Bullet Physics.

Je commente aussi le code pour pouvoir générer une documentation Doxygen correcte.

Enfin, je fais de l'optimisation de code et j'ajoute des fonctions de Scripting.

## Les commandes pour le jeu "Sample" sont :

On se déplace avec les touches "Z, S, Q, D" (ou "haut, bas, gauche, droite" et bientôt avec un joystick).

La souris sert à tourner autour de la boule.
