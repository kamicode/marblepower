#Copyright(C) 2012 Tristan Kahn
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU Lesser General Public
#License as published by the Free Software Foundation; either
#version 2.1 of the License, or(at your option) any later version.
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#Lesser General Public License for more details.
#You should have received a copy of the GNU Lesser General Public
#License along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

TEMPLATE = app
LANGUAGE = C++
CONFIG -= console app_bundle
CONFIG *= ordered windows embed_manifest_exe
QT *= script
QT -= gui

greaterThan(QT_MAJOR_VERSION, 4) {
    CONFIG *= c++11
} else {
}

# BugFix QtCreator
debug_and_release {
    CONFIG -= debug_and_release
}

QMAKE_CXXFLAGS *= -Zc:wchar_t
QMAKE_CFLAGS_RELEASE *= -O2 -GL
QMAKE_LFLAGS_RELEASE *= /LTCG

# Target
DEPENDPATH *= "$$_PRO_FILE_PWD_"
INCLUDEPATH *= "$$_PRO_FILE_PWD_"
DESTDIR = "$$_PRO_FILE_PWD_/../../Build"

CONFIG(debug, debug|release) {
    message("Marble : Debug compilation")
    TARGET = Marble_d
    DEFINES *= _DEBUG _WIN32 ENABLE_LOGGER
    OBJECTS_DIR = "$$_PRO_FILE_PWD_/../../Obj/Debug/Marble/Obj"
    MOC_DIR = "$$_PRO_FILE_PWD_/../../Obj/Debug/Marble/Moc"
    RCC_DIR = "$$_PRO_FILE_PWD_/../../Obj/Debug/Marble/Rcc"
    UI_DIR = "$$_PRO_FILE_PWD_/../../Obj/Debug/Marble/Ui"
} else {
    message("Marble : Release compilation")
    TARGET = Marble
    DEFINES *= RELEASE NDEBUG _WIN32
    OBJECTS_DIR = "$$_PRO_FILE_PWD_/../../Obj/Release/Marble/Obj"
    MOC_DIR = "$$_PRO_FILE_PWD_/../../Obj/Release/Marble/Moc"
    RCC_DIR = "$$_PRO_FILE_PWD_/../../Obj/Release/Marble/Rcc"
    UI_DIR = "$$_PRO_FILE_PWD_/../../Obj/Release/Marble/Ui"
}

# Precompilation
PRECOMPILED_HEADER = "$$_PRO_FILE_PWD_/Marble/Precompiled.h"
DEFINES *= USING_PCH
CONFIG *= precompile_header

# Win32 Libs
LIBS *= User32.lib Advapi32.lib Gdi32.lib

# DirectX
LIBS *= -L"$$system(echo %DXSDK_DIR%)Lib/x86"

# Cg
LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/Cg-3.1/Lib"
LIBS *= cg.lib

# Irrlicht
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Include"
DEFINES *= _IRR_STATIC_LIB_
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Lib/Debug" -lIrrlicht
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Lib/Debug/Irrlicht.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Lib/Release" -lIrrlicht
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Lib/Release/Irrlicht.lib"
}

# Bullet
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Include"
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Debug"
    LIBS *= -lBulletCollision_vs2010_debug -lBulletDynamics_vs2010_debug -lLinearMath_vs2010_debug
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Debug/BulletCollision_vs2010_debug.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Debug/BulletDynamics_vs2010_debug.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Debug/LinearMath_vs2010_debug.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Release"
    LIBS *= -lBulletCollision_vs2010 -lBulletDynamics_vs2010 -lLinearMath_vs2010
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Release/BulletCollision_vs2010.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Release/BulletDynamics_vs2010.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Release/LinearMath_vs2010.lib"
}

# TinyXml2
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Include"
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Lib/Debug"
    LIBS *= -ltinyxml2
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Lib/Debug/tinyxml2.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Lib/Release"
    LIBS *= -ltinyxml2
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Lib/Release/tinyxml2.lib"
}

# CEGUI
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Include"
DEFINES *= CEGUI_STATIC
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug"
    LIBS *= -lCEGUIBase_Static_d -lCEGUIIrrlichtRenderer_Static_d -lCEGUIExpatParser_Static_d -lCEGUICoronaImageCodec_Static_d -lCEGUIDevILImageCodec_Static_d -lCEGUIFreeImageImageCodec_Static_d -lCEGUISILLYImageCodec_Static_d -lCEGUISTBImageCodec_Static_d -lCEGUITGAImageCodec_Static_d -lCEGUIFalagardWRBase_Static_d
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIBase_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIIrrlichtRenderer_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIExpatParser_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUICoronaImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIDevILImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIFreeImageImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUISILLYImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUISTBImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUITGAImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIFalagardWRBase_Static_d.lib"
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug"
    LIBS *= -lfreetype_d -lpcre_d -lexpat_d -lcorona_d
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug/freetype_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug/pcre_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug/expat_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug/corona_d.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release"
    LIBS *= -lCEGUIBase_Static -lCEGUIIrrlichtRenderer_Static -lCEGUIExpatParser_Static -lCEGUICoronaImageCodec_Static -lCEGUIDevILImageCodec_Static -lCEGUIFreeImageImageCodec_Static -lCEGUISILLYImageCodec_Static -lCEGUISTBImageCodec_Static -lCEGUITGAImageCodec_Static -lCEGUIFalagardWRBase_Static
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIBase_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIIrrlichtRenderer_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIExpatParser_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUICoronaImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIDevILImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIFreeImageImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUISILLYImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUISTBImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUITGAImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIFalagardWRBase_Static.lib"
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release"
    LIBS *= -lfreetype -lpcre -lexpat -lcorona
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release/freetype.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release/pcre.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release/expat.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release/corona.lib"
}

# Theoraplayer
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Include"
DEFINES *= THEORAVIDEO_STATIC
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Debug"
    LIBS *= -llibogg -llibvorbis -llibtheora -llibtheoraplayer
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Debug/libogg.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Debug/libvorbis.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Debug/libtheora.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Debug/libtheoraplayer.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Release"
    LIBS *= -llibogg -llibvorbis -llibtheora -llibtheoraplayer
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Release/libogg.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Release/libvorbis.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Release/libtheora.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/libtheoraplayer-Rev189/Lib/Release/libtheoraplayer.lib"
}

# OpenAL
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/OpenAL-1.1/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/OpenAL-1.1/Include"
LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/OpenAL-1.1/Lib"
LIBS *= -lOpenAL32
PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/OpenAL-1.1/Lib/OpenAL32.lib"

# libsndfile
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/libsndfile/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/libsndfile/Include"
LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/libsndfile/Lib"
LIBS *= -llibsndfile-1
PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/libsndfile/Lib/libsndfile-1.lib"

SOURCES *= \
    "$$_PRO_FILE_PWD_/Marble/Main.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Divers.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Engines/Engine.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Engines/Logger.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Engines/GraphicsEngine.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Engines/PhysicsEngine.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Engines/GuiEngine.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Engines/SoundEngine.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Engines/EventEngine.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Engines/VideoEngine.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Engines/VideoAudioInterface.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Materials/ShaderCallBack.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Materials/Material3D.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Materials/MaterialPhys.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Element.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Script.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Mesh.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Sound.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Light.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Constraint.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Particules.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Terrain.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Camera.cpp" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Video2D.cpp" \

HEADERS *= \
    "$$_PRO_FILE_PWD_/Marble/Precompiled.h" \
    "$$_PRO_FILE_PWD_/Marble/Divers.h" \
    "$$_PRO_FILE_PWD_/Marble/Engines/Engine.h" \
    "$$_PRO_FILE_PWD_/Marble/Engines/Logger.h" \
    "$$_PRO_FILE_PWD_/Marble/Engines/GraphicsEngine.h" \
    "$$_PRO_FILE_PWD_/Marble/Engines/PhysicsEngine.h" \
    "$$_PRO_FILE_PWD_/Marble/Engines/GuiEngine.h" \
    "$$_PRO_FILE_PWD_/Marble/Engines/SoundEngine.h" \
    "$$_PRO_FILE_PWD_/Marble/Engines/EventEngine.h" \
    "$$_PRO_FILE_PWD_/Marble/Engines/VideoAudioInterface.h" \
    "$$_PRO_FILE_PWD_/Marble/Engines/VideoEngine.h" \
    "$$_PRO_FILE_PWD_/Marble/Materials/MaterialPhys.h" \
    "$$_PRO_FILE_PWD_/Marble/Materials/ShaderCallBack.h" \
    "$$_PRO_FILE_PWD_/Marble/Materials/Material3D.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Particules.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Mesh.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Sound.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Terrain.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Light.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Constraint.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Camera.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Element.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Script.h" \
    "$$_PRO_FILE_PWD_/Marble/Elements/Video2D.h" \
