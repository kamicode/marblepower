/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Mesh.h"

#include <map>

#include "Marble/Materials/Material3D.h"
#include "Marble/Materials/MaterialPhys.h"
#include "Marble/Engines/Engine.h"
#include "Marble/Engines/PhysicsEngine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Elements/Script.h"

namespace MarbleEngine {

Mesh::Mesh(QString name) : Element(name) {
    mesh = NULL;
    node = NULL;
    masse = 0;
    typeBody = None;
    mat = irr::core::matrix4();
    forces = irr::core::vector3df();
    torques = irr::core::vector3df();
    m_rigidBody = NULL;
    m_collisionShape = NULL;
    m_motionState = NULL;
}

Mesh::~Mesh() {
    if (node != NULL) {
        node->remove();
        node = NULL;
    }

    if (m_rigidBody != NULL) {
        Engine::getPhysicsEngine()->getWorld()->removeRigidBody(m_rigidBody);
        delete m_motionState;
        delete m_collisionShape;
        delete m_rigidBody;
        m_rigidBody = NULL;
    }
}

bool Mesh::isInCollisionWithMap() {
    if (m_rigidBody != NULL) {
        btDispatcher* dispatcher =
                Engine::getPhysicsEngine()->getWorld()->getDispatcher();

        int numManifolds = dispatcher->getNumManifolds();
        for (int i = 0; i < numManifolds; ++i) {
            btPersistentManifold* contactManifold =
                    dispatcher->getManifoldByIndexInternal(i);
            const btCollisionObject* obA =
                    static_cast<const btCollisionObject*>(
                        contactManifold->getBody0());
            const btCollisionObject* obB =
                    static_cast<const btCollisionObject*>(
                        contactManifold->getBody1());

            if (obA->getCollisionShape()->getUserPointer() == this
                    || obB->getCollisionShape()->getUserPointer() == this) {
                if (contactManifold->getNumContacts() > 0) {
                    return true;
                }
            }
        }
    }
    return false;
}

int Mesh::nbCollisions(Element* obj) {
    if (m_rigidBody != NULL) {
        btDispatcher* dispatcher =
                Engine::getPhysicsEngine()->getWorld()->getDispatcher();

        int numManifolds = dispatcher->getNumManifolds();
        for (int i = 0; i < numManifolds; ++i) {
            btPersistentManifold* contactManifold =
                    dispatcher->getManifoldByIndexInternal(i);
            const btCollisionObject* obA =
                    static_cast<const btCollisionObject*>(
                        contactManifold->getBody0());
            const btCollisionObject* obB =
                    static_cast<const btCollisionObject*>(
                        contactManifold->getBody1());

            if (obA->getCollisionShape()->getUserPointer() == this
                    && obB->getCollisionShape()->getUserPointer() == obj) {
                return contactManifold->getNumContacts();
            }
        }
    }
    return 0;
}

int Mesh::nbCollisions(QString name) {
    std::map<QString, Element*> elements = Engine::getElements();
    std::map<QString, Element*>::const_iterator it = elements.find(name);
    if (it != elements.end()) {
        if (it->second != this) {
            return nbCollisions(it->second);
        }
    }
    return 0;
}

void Mesh::showPhysic(bool value) {
    if (m_rigidBody != NULL) {
        if (value) {
            m_rigidBody->setCollisionFlags(
                        m_rigidBody->getCollisionFlags()
                        & ~btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);
        } else {
            m_rigidBody->setCollisionFlags(
                        m_rigidBody->getCollisionFlags()
                        | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);
        }
    }
}

bool Mesh::loadMesh(irr::core::vector3df size, irr::core::vector3df pos,
                    irr::core::vector3df rot, irr::io::path ficMesh) {
    bool ret = false;
    irr::scene::ISceneManager* scene =
            Engine::getGraphicsEngine()->getSceneManager();
    mesh = scene->getMesh(ficMesh);
    if (mesh) {
        mesh->setHardwareMappingHint(irr::scene::EHM_STATIC);
        node = scene->addAnimatedMeshSceneNode(
                    (irr::scene::IAnimatedMesh*) mesh);

        node->setScale(size);
        node->setPosition(pos);
        node->setRotation(rot);
        ret = true;
    }
    return ret;
}

bool Mesh::loadBody(irr::core::vector3df size, QString material) {
    btDiscreteDynamicsWorld* nWorld = Engine::getPhysicsEngine()->getWorld();
    if (nWorld) {
        if (m_rigidBody != NULL) {
            nWorld->removeRigidBody(m_rigidBody);
            delete m_motionState;
            delete m_collisionShape;
            delete m_rigidBody;
            m_rigidBody = NULL;
        }


        switch (typeBody) {
        case Box: {
            m_collisionShape = new btBoxShape(btVector3(size.X * 0.5,
                                                        size.Y * 0.5,
                                                        size.Z * 0.5));
            break;
        }
        case Sphere: {
            m_collisionShape = new btSphereShape(size.X);
            break;
        }
        case Cone: {
            m_collisionShape = new btConeShape(size.X, size.Y);
            break;
        }
        case Capsule: {
            m_collisionShape = new btCapsuleShape(size.X, size.Y);
            break;
        }
        case Cylinder: {
            m_collisionShape = new btCylinderShape(btVector3(size.X * 0.5,
                                                             size.Y * 0.5,
                                                             size.Z * 0.5));
            break;
        }
        case ConvexHull: {
            m_collisionShape = Convert::meshToBulletConvexHullShape(mesh, size);
            break;
        }
        case TerrainTree: {
            btTriangleMesh* m = Convert::meshToBulletTriangleMesh(mesh, size);
            m_collisionShape = new btBvhTriangleMeshShape(m, true);
            break;
        }
        default:
            break;
        }

        btTransform trans;
        trans.setIdentity();
        trans.setOrigin(btVector3(node->getPosition().X,
                                  node->getPosition().Y,
                                  node->getPosition().Z));

        btQuaternion btq = Convert::EulerToQuaternion(
                    btVector3(node->getRotation().X * 0.0174532925f,
                              node->getRotation().Y * 0.0174532925f,
                              node->getRotation().Z * 0.0174532925f));

        trans.setRotation(btq);

        m_motionState = new MotionState(trans, node);
        btVector3 inertia(0, 0, 0);
        m_collisionShape->calculateLocalInertia(masse, inertia);
        btRigidBody::btRigidBodyConstructionInfo rigidBodyInfo(masse,
                                                               m_motionState,
                                                               m_collisionShape,
                                                               inertia);
        m_rigidBody = new btRigidBody(rigidBodyInfo);
        m_collisionShape->setUserPointer(this);
        m_rigidBody->setUserPointer(this);
        setMaterialPhys(material);

        showPhysic(false);

        Engine::getPhysicsEngine()->getWorld()->addRigidBody(m_rigidBody);
        m_rigidBody->setCollisionFlags(
                    m_rigidBody->getCollisionFlags()
                    | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
    }
    return true;
}

void Mesh::setMaterialPhys(QString material) {
    if (m_rigidBody != NULL) {
        Material* mat = Engine::getMaterialPhysEngine()->getMaterial(material);
        if (mat) {
            m_rigidBody->setDamping(mat->lin_damping, mat->ang_damping);
            m_rigidBody->setFriction(mat->m_friction);
            m_rigidBody->setRestitution(mat->m_restitution);

            materialPhys = material;
        }
    }
}

void Mesh::addShadow(const irr::scene::IMesh* shadowMesh,
                     bool zfailmethod, irr::f32 infinity) {
    ((irr::scene::IAnimatedMeshSceneNode*)node)->addShadowVolumeSceneNode(
                shadowMesh, -1, zfailmethod, infinity);
}

bool Mesh::create(irr::core::vector3df pos, irr::core::vector3df rot,
                  irr::core::vector3df scale, QString mesh, QString type,
                  irr::core::vector3df size, QString material, float masse) {
    Element::init();

    bool ret = loadMesh(scale, pos, rot,
                        (Engine::getDirMedia() + mesh).toUtf8().data());
    if (ret && type != "None") {
        typeBody = Convert::stringBody(type);
        this->masse = masse;
        ret = loadBody(size, material);
    }
    return ret;
}

bool Mesh::readXML(tinyxml2::XMLElement* elem) {
    bool ret = false;

    irr::core::vector3df scale(1, 1, 1);
    irr::core::vector3df pos(0, 0, 0);
    irr::core::vector3df rot(0, 0, 0);

    if (elem->Attribute("scaleX") && elem->Attribute("scaleY")
            && elem->Attribute("scaleZ")) {
        scale.X = elem->FloatAttribute("scaleX");
        scale.Y = elem->FloatAttribute("scaleY");
        scale.Z = elem->FloatAttribute("scaleZ");
    }

    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
    }

    if (elem->Attribute("rotX") && elem->Attribute("rotY")
            && elem->Attribute("rotZ")) {
        rot.X = elem->FloatAttribute("rotX");
        rot.Y = elem->FloatAttribute("rotY");
        rot.Z = elem->FloatAttribute("rotZ");
    }

    if (elem->Attribute("mesh")) {
        QString path = Engine::getDirMedia() + elem->Attribute("mesh");
        irr::io::path mesh = path.toUtf8().data();
        ret = loadMesh(scale, pos, rot, mesh);
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (ret) {
            if (QString(elem->Value()) == "Material3D") {
                int num = elem->IntAttribute("num");
                Material3D* m = new Material3D(&(node->getMaterial(num)));
                m->readXML(elem);
                delete m;
                m = NULL;
            } else if (QString(elem->Value()) == "Animation") {
                animation(elem);
            } else if (QString(elem->Value()) == "Physic") {
                physic(elem);
            } else if (QString(elem->Value()) == "Shadow") {
                shadow(elem);
            } else if (QString(elem->Value()) == "Script") {
                Element::readXML(elem);
            }
        }

        elem = elem->NextSiblingElement();
    }

    return ret;
}

void Mesh::physic(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("type")) {
        typeBody = Convert::stringBody(elem->Attribute("type"));
    }

    if (typeBody != None) {
        irr::core::vector3df size;
        if (elem->Attribute("sizeX") && elem->Attribute("sizeY")
                && elem->Attribute("sizeZ")) {
            size.X = elem->FloatAttribute("sizeX");
            size.Y = elem->FloatAttribute("sizeY");
            size.Z = elem->FloatAttribute("sizeZ");
        } else {
            size = node->getScale();
        }

        if (elem->Attribute("masse")) {
            masse = elem->FloatAttribute("masse");
        }

        loadBody(size, elem->Attribute("material"));
    }
}

void Mesh::animation(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("speed")) {
        int speed = elem->IntAttribute("speed");
        ((irr::scene::IAnimatedMeshSceneNode*)node)->setAnimationSpeed(speed);
    }

    if (elem->Attribute("frameLoopBegin") && elem->Attribute("frameLoopEnd")) {
        int begin = elem->IntAttribute("frameLoopBegin");
        int end = elem->IntAttribute("frameLoopEnd");
        ((irr::scene::IAnimatedMeshSceneNode*)node)->setFrameLoop(begin, end);
    }

    if (elem->Attribute("currentFrame")) {
        int currentFrame = elem->IntAttribute("currentFrame");
        ((irr::scene::IAnimatedMeshSceneNode*)node)->setCurrentFrame(
                    currentFrame);
    }

    if (elem->Attribute("loopMode")) {
        bool loopMode = elem->BoolAttribute("loopMode");
        ((irr::scene::IAnimatedMeshSceneNode*)node)->setLoopMode(loopMode);
    }
}

void Mesh::shadow(tinyxml2::XMLElement* elem) {
    irr::scene::IMesh* shadowMesh = 0;
    bool zfailmethod = true;
    irr::f32 infinity = 10000.0f;

    if (elem->Attribute("mesh")) {
        QString path = Engine::getDirMedia() + elem->Attribute("mesh");
        irr::io::path mesh = path.toUtf8().data();
        shadowMesh =
                Engine::getGraphicsEngine()->getSceneManager()->getMesh(mesh);
    }

    if (elem->Attribute("zfailmethod")) {
        zfailmethod = elem->BoolAttribute("zfailmethod");
    }

    if (elem->Attribute("infinity")) {
        infinity = elem->FloatAttribute("infinity");
    }

    addShadow(shadowMesh, zfailmethod, infinity);
}

float Mesh::getMasse() const {
    return masse;
}

void Mesh::setMasse(float masse) {
    if (m_rigidBody != NULL) {
        this->masse = masse;
        btVector3 inertia(0, 0, 0);
        m_collisionShape->calculateLocalInertia(masse, inertia);
        m_rigidBody->setMassProps(masse, inertia);
    }
}

void Mesh::setAnimationSpeed(int speed) {
    ((irr::scene::IAnimatedMeshSceneNode*)node)->setAnimationSpeed(speed);
}

void Mesh::setAnimationFrameLoop(int begin, int end) {
    ((irr::scene::IAnimatedMeshSceneNode*)node)->setFrameLoop(begin, end);
}

void Mesh::setAnimationCurrentFrame(int current) {
    ((irr::scene::IAnimatedMeshSceneNode*)node)->setCurrentFrame(current);
}

void Mesh::setAnimationLoopMode(bool loop) {
    ((irr::scene::IAnimatedMeshSceneNode*)node)->setLoopMode(loop);
}

void Mesh::setShadow(QString mesh, bool zfailmethod, float infinity) {
    irr::io::path meshbuf = (Engine::getDirMedia() + mesh).toUtf8().data();
    irr::scene::IMesh* shadowMesh =
            Engine::getGraphicsEngine()->getSceneManager()->getMesh(meshbuf);
    addShadow(shadowMesh, zfailmethod, infinity);
}

QScriptValue Mesh::getPosition() {
    irr::core::vector3df pos = node->getPosition();
    QScriptValue ret = Engine::getScriptEngine()->newObject();

    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Mesh::setPosition(const QScriptValue &pos) {
    irr::core::vector3df vect;
    vect.X = pos.property("X").toNumber();
    vect.Y = pos.property("Y").toNumber();
    vect.Z = pos.property("Z").toNumber();

    setPosition(vect, true);
}

void Mesh::setPosition(const irr::core::vector3df &vect, bool updatePhysic) {
    node->setPosition(vect);
    node->updateAbsolutePosition();

    if (updatePhysic && m_rigidBody != NULL && typeBody != None) {
        btTransform trans;
        m_motionState->getWorldTransform(trans);
        trans.setOrigin(btVector3(vect.X, vect.Y, vect.Z));
        m_motionState->setWorldTransform(trans);
    }
}

QScriptValue Mesh::getRotation() {
    irr::core::vector3df pos = node->getRotation();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Mesh::setRotation(const QScriptValue &pos) {
    irr::core::vector3df vect;
    vect.X = pos.property("X").toNumber();
    vect.Y = pos.property("Y").toNumber();
    vect.Z = pos.property("Z").toNumber();

    setRotation(vect, true);
}

void Mesh::setRotation(const irr::core::vector3df &vect, bool updatePhysic) {
    node->setRotation(vect);

    if (updatePhysic && m_rigidBody != NULL && typeBody != None) {
        btTransform trans;
        m_motionState->getWorldTransform(trans);
        btQuaternion btq = Convert::EulerToQuaternion(
                    btVector3(node->getRotation().X * 0.0174532925f,
                              node->getRotation().Y * 0.0174532925f,
                              node->getRotation().Z * 0.0174532925f));
        trans.setRotation(btq);
        m_motionState->setWorldTransform(trans);
    }
}

QScriptValue Mesh::getScale() {
    irr::core::vector3df pos = node->getScale();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Mesh::setScale(const QScriptValue &scale) {
    irr::core::vector3df vect;
    vect.X = scale.property("X").toNumber();
    vect.Y = scale.property("Y").toNumber();
    vect.Z = scale.property("Z").toNumber();

    node->setScale(vect);
}

void Mesh::setPhysicScale(const QScriptValue &scale) {
    irr::core::vector3df vect;
    vect.X = scale.property("X").toNumber();
    vect.Y = scale.property("Y").toNumber();
    vect.Z = scale.property("Z").toNumber();

    loadBody(vect, materialPhys);
}

void Mesh::setTarget(const QScriptValue &target) {
    irr::core::vector3df pos;
    pos.X = target.property("X").toNumber();
    pos.Y = target.property("Y").toNumber();
    pos.Z = target.property("Z").toNumber();

    irr::core::vector3df nodePos = node->getPosition();
    irr::core::vector3df posDiff = pos - nodePos;
    posDiff.normalize();
    setRotation(posDiff.getHorizontalAngle(), true);
}

void Mesh::setMaterial3D(int num, bool wireframe, bool pointCloud,
                         bool gouraudShading, bool lighting, bool zwriteEnable,
                         bool backfaceCulling, bool frontfaceCulling,
                         bool fogEnable, bool normalizeNormals, float shininess,
                         QString materialTypeParam, QString materialTypeParam2,
                         QString materialType, QString zBuffer,
                         QString antiAliasing, QString colorMask,
                         QString colorMaterial, QString texture,
                         int ambiantAlpha, int ambiantRed, int ambiantGreen,
                         int ambiantBlue, int diffuseAlpha, int diffuseRed,
                         int diffuseGreen, int diffuseBlue, int emissiveAlpha,
                         int emissiveRed, int emissiveGreen, int emissiveBlue,
                         int specularAlpha, int specularRed, int specularGreen,
                         int specularBlue) {
    irr::video::SMaterial* material = &(node->getMaterial(num));
    material->Wireframe = wireframe;
    material->PointCloud = pointCloud;
    material->GouraudShading = gouraudShading;
    material->Lighting = lighting;
    material->ZWriteEnable = zwriteEnable;
    material->BackfaceCulling = backfaceCulling;
    material->FrontfaceCulling = frontfaceCulling;
    material->FogEnable = fogEnable;
    material->NormalizeNormals = normalizeNormals;
    material->Shininess = shininess;
    material->MaterialTypeParam =
            Convert::stringMaterialType(materialTypeParam);
    material->MaterialTypeParam2 =
            Convert::stringMaterialType(materialTypeParam2);
    material->MaterialType = Convert::stringMaterialType(materialType);
    material->ZBuffer = Convert::stringZBuffer(zBuffer);
    material->AntiAliasing = Convert::stringAntiAliasing(antiAliasing);
    material->ColorMask = Convert::stringColorMask(colorMask);
    material->ColorMaterial = Convert::stringColorMaterial(colorMaterial);

    material->setTexture(
                0,
                Engine::getGraphicsEngine()->getDriver()->getTexture(
                    (Engine::getDirMedia() + texture).toUtf8().data()));

    irr::video::SColor ambiantColor(ambiantAlpha, ambiantRed,
                                    ambiantGreen, ambiantBlue);
    material->AmbientColor = ambiantColor;
    irr::video::SColor diffuseColor(diffuseAlpha, diffuseRed,
                                    diffuseGreen, diffuseBlue);
    material->DiffuseColor = diffuseColor;
    irr::video::SColor emissiveColor(emissiveAlpha, emissiveRed,
                                     emissiveGreen, emissiveBlue);
    material->EmissiveColor = emissiveColor;
    irr::video::SColor specularColor(specularAlpha, specularRed,
                                     specularGreen, specularBlue);
    material->SpecularColor = specularColor;
}

irr::scene::IMesh* Mesh::getMesh() const {
    return mesh;
}

irr::scene::ISceneNode* Mesh::getNode() const {
    return node;
}

btRigidBody* Mesh::getRigidBody() const {
    return m_rigidBody;
}

btCollisionShape* Mesh::getCollisionShape() const {
    return m_collisionShape;
}

QString Mesh::getMaterialPhys() const {
    return materialPhys;
}

void Mesh::addForce(float x, float y, float z) {
    if (m_rigidBody  != NULL) {
        m_rigidBody->applyCentralForce(btVector3(x, y, z));
        m_rigidBody->activate();
    }
}

void Mesh::addForce(const QScriptValue &pos) {
    if (m_rigidBody  != NULL) {
        m_rigidBody->applyCentralForce(btVector3(pos.property("X").toNumber(),
                                                 pos.property("Y").toNumber(),
                                                 pos.property("Z").toNumber()));
        m_rigidBody->activate();
    }
}

void Mesh::addTorque(float x, float y, float z) {
    if (m_rigidBody  != NULL) {
        m_rigidBody->applyTorque(btVector3(x, y, z));
        m_rigidBody->activate();
    }
}

void Mesh::addTorque(const QScriptValue &pos) {
    if (m_rigidBody  != NULL) {
        m_rigidBody->applyTorque(btVector3(pos.property("X").toNumber(),
                                           pos.property("Y").toNumber(),
                                           pos.property("Z").toNumber()));
        m_rigidBody->activate();
    }
}

void Mesh::addImpulse(const irr::core::vector3df & pointDeltaVeloc,
                      const irr::core::vector3df & pointPosit) {
    if (m_rigidBody  != NULL) {
        m_rigidBody->applyImpulse(btVector3(pointDeltaVeloc.X,
                                            pointDeltaVeloc.Y,
                                            pointDeltaVeloc.Z),
                                  btVector3(pointPosit.X,
                                            pointPosit.Y,
                                            pointPosit.Z));
        m_rigidBody->activate();
    }
}

void Mesh::addImpulse(float pointDeltaVelocX, float pointDeltaVelocY,
                      float pointDeltaVelocZ, float pointPositX,
                      float pointPositY, float pointPositZ) {
    if (m_rigidBody  != NULL) {
        m_rigidBody->applyImpulse(btVector3(pointDeltaVelocX, pointDeltaVelocY,
                                            pointDeltaVelocZ),
                                  btVector3(pointPositX, pointPositY,
                                            pointPositZ));
        m_rigidBody->activate();
    }
}

void Mesh::addImpulse(const QScriptValue &pointDeltaVeloc,
                      const QScriptValue &pointPosit) {
    if (m_rigidBody  != NULL) {
        irr::core::vector3df vec1;
        vec1.X = pointDeltaVeloc.property("X").toNumber();
        vec1.Y = pointDeltaVeloc.property("Y").toNumber();
        vec1.Z = pointDeltaVeloc.property("Z").toNumber();

        irr::core::vector3df vec2;
        vec2.X = pointPosit.property("X").toNumber();
        vec2.Y = pointPosit.property("Y").toNumber();
        vec2.Z = pointPosit.property("Z").toNumber();

        m_rigidBody->applyImpulse(btVector3(vec1.X, vec1.Y, vec1.Z),
                                  btVector3(vec2.X, vec2.Y, vec2.Z));
        m_rigidBody->activate();
    }
}

}  // namespace MarbleEngine
