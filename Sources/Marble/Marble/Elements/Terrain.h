/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Terrain.h
 * \brief Element of type "Terrain"
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_TERRAIN_H_
#define MARBLE_ELEMENTS_TERRAIN_H_

#include "Marble/Elements/Mesh.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Terrain
 * \brief
 *
 */
class Terrain: public Mesh {
    Q_OBJECT

    public:
        explicit Terrain(QString name);
        virtual ~Terrain();
        bool create(QString heightMapFile, QString patchSize,
                    irr::core::vector3df pos, irr::core::vector3df rot,
                    irr::core::vector3df scale, int maxLOD, int smoothFactor,
                    bool addAlsoIfHeightmapEmpty, int alpha, int red, int green,
                    int blue, irr::core::vector3df physiqueSize,
                    QString materialPhysic);
        bool readXML(tinyxml2::XMLElement* elem);
        bool loadBody(irr::core::vector3df size, QString material);

    public:
        Q_INVOKABLE QString getTypeName() { return "Terrain"; }
        Q_INVOKABLE void setScaleTexture(float scaleTexture1,
                                         float scaleTexture2);
        Q_INVOKABLE void setLODOfPatch(int patchX, int patchZ, int lod);

    private:
        int maxLod;

    private:
        DISALLOW_COPY_AND_ASSIGN(Terrain);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_TERRAIN_H_
