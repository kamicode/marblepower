/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Light.h
 * \brief Element of type "Light"
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_LIGHT_H_
#define MARBLE_ELEMENTS_LIGHT_H_

#include "Marble/Elements/Element.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Light
 * \brief
 *
 */
class Light: public Element {
    Q_OBJECT

    public:
        explicit Light(QString name);
        virtual ~Light();
        bool readXML(tinyxml2::XMLElement* elem);
        bool create(QString type, bool shadow, irr::core::vector3df pos,
                    irr::core::vector3df rot, float outerCone, float innerCone,
                    float falloff, int ambiantA, int ambiantR, int ambiantG,
                    int ambiantB, int diffuseA, int diffuseR, int diffuseG,
                    int diffuseB, int specularA, int specularR, int specularG,
                    int specularB, float attenuationConst, float attenuationLin,
                    float attenuationQuad);

    public:
        Q_INVOKABLE QString getTypeName() { return "Light"; }
        Q_INVOKABLE QScriptValue getPosition();
        Q_INVOKABLE void setPosition(const QScriptValue &pos);
        Q_INVOKABLE QScriptValue getRotation();
        Q_INVOKABLE void setRotation(const QScriptValue &pos);
        Q_INVOKABLE void setTarget(const QScriptValue &target);
        Q_INVOKABLE void setType(const QString &type);
        Q_INVOKABLE void enableCastShadow(bool enable);
        Q_INVOKABLE void setCone(float outerCone,
                                 float innerCone,
                                 float falloff);
        Q_INVOKABLE void setAttenuation(float attenuationConst,
                                        float attenuationLin,
                                        float attenuationQuad);
        Q_INVOKABLE void setAmbiantColor(int alpha, int red,
                                         int green, int blue);
        Q_INVOKABLE void setDiffuseColor(int alpha, int red,
                                         int green, int blue);
        Q_INVOKABLE void setSpecularColor(int alpha, int red,
                                          int green, int blue);

    private:
        irr::scene::ILightSceneNode* lumiere;

    private:
        DISALLOW_COPY_AND_ASSIGN(Light);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_LIGHT_H_
