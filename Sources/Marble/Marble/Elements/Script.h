/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Script.h
 * \brief Element of type "Script"
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_SCRIPT_H_
#define MARBLE_ELEMENTS_SCRIPT_H_

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

class Element;

/*! \class Script
 * \brief
 *
 */
class Script {
    public:
        explicit Script(Element* s);
        virtual ~Script();
        bool readXML(tinyxml2::XMLElement* elem);
        bool init();
        void update(float time);
        void execute(QString code);
        void replaceVariable(tinyxml2::XMLElement* elem);
        QScriptEngine* getScriptEngine();

    private:
        explicit Script();

    private:
        QScriptEngine* scriptEngine;
        QString scriptFile;
        QString content;
        Element* element;

    private:
        DISALLOW_COPY_AND_ASSIGN(Script);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_SCRIPT_H_
