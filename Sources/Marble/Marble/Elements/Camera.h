/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Camera.h
 * \brief Element of type Camera
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_CAMERA_H_
#define MARBLE_ELEMENTS_CAMERA_H_

#include "Marble/Elements/Element.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Camera
 * \brief This class represents an Element of type Camera
 */
class Camera: public Element {
    Q_OBJECT

    public:
        /*!
         *  \brief Camera Constructor
         *  \param name : Identifiant of the Camera
         */
        explicit Camera(QString name);

        /*!
         *  \brief Destructor
         */
        virtual ~Camera();

        /*!
         *  \brief Return the Irrlicht node of the Camera
         *  \return Irrlicht node of the Camera
         */
        irr::scene::ICameraSceneNode* getCamera() const;

        /*!
         *  \brief Read the XML node for define the Camera
         *  \param xmlNode : XML node
         *  \return true if initialization was successful, false otherwise
         */
        bool readXML(tinyxml2::XMLElement* xmlNode);

        /*!
         *  \brief Return the Camera Viewport (Relative to the size of the window)
         *  \return The Camera Viewport
         */
        irr::core::rect<irr::f32> getViewport();

        /*!
         *  \brief Return the Camera Viewport (in pixel)
         *  \return The Camera Viewport
         */
        irr::core::rect<irr::s32> getAbsoluteViewport();

    public:
        /*!
         *  \brief Scripting method to get the dynamic type of the Element (Camera)
         *  \return The dynamic type of the Element (Camera)
         */
        Q_INVOKABLE QString getTypeName() { return "Camera"; }

        /*!
         *  \brief Scripting method to get the Camera position
         *  \return The Camera position ( Format: [X, Y, Z] )
         */
        Q_INVOKABLE QScriptValue getPosition();

        /*!
         *  \brief Scripting method to Set the Camera position
         *  \param position : The new Camera position ( Format: [X, Y, Z] )
         */
        Q_INVOKABLE void setPosition(const QScriptValue &position);

        /*!
         *  \brief Scripting method to get the Camera target
         *  \return The Camera target ( Format: [X, Y, Z] )
         */
        Q_INVOKABLE QScriptValue getTarget();

        /*!
         *  \brief Scripting method to set the Camera target
         *  \param target : The new Camera target ( Format: [X, Y, Z] )
         */
        Q_INVOKABLE void setTarget(const QScriptValue &target);

        /*!
         *  \brief Scripting method to get the Camera rotation
         *  \return The Camera rotation ( Format: [X, Y, Z] )
         */
        Q_INVOKABLE QScriptValue getRotation();

        /*!
         *  \brief Scripting method to set the Camera rotation
         *  \param rotation : The new Camera rotation ( Format: [X, Y, Z] )
         */
        Q_INVOKABLE void setRotation(const QScriptValue &rotation);

        /*!
         *  \brief Scripting method to get the Camera Up Vector
         *  \return The Camera Up Vector ( Format: [X, Y, Z] )
         */
        Q_INVOKABLE QScriptValue getUpVector();

        /*!
         *  \brief Scripting method to set the Camera Up Vector
         *  \param rotation : The new Camera Up Vector ( Format: [X, Y, Z] )
         */
        Q_INVOKABLE void setUpVector(const QScriptValue &upVector);

        /*!
         *  \brief Scripting method to define the far limit of what can see the Camera
         *  \param farValue : The new far limit of what can see the Camera
         */
        Q_INVOKABLE void setFarValue(float farValue);

        /*!
         *  \brief Scripting method to define the near limit of what can see the Camera
         *  \param nearValue : The new near limit of what can see the Camera
         */
        Q_INVOKABLE void setNearValue(float nearValue);

        /*!
         *  \brief Scripting method to set the Camera FOV
         *  \param fov : The new Camera FOV
         */
        Q_INVOKABLE void setFOV(float fov);

        /*!
         *  \brief Scripting method to set the Camera Viewport
         *  \param upperLeftX : The abscissa of the upper left corner (Relative to the size of the window)
         *  \param upperLeftY : The ordinate of the upper left corner (Relative to the size of the window)
         *  \param lowerRightX : The abscissa of the lower right corner (Relative to the size of the window)
         *  \param lowerRightY : The ordinate of the lower right corner (Relative to the size of the window)
         */
        Q_INVOKABLE void setCameraViewport(float upperLeftX,
                                           float upperLeftY,
                                           float lowerRightX,
                                           float lowerRightY);

        /*!
         *  \brief Scripting method to get the Camera Viewport (Relative to the size of the window)
         *  \return The Camera Viewport ( Format: [UpperLeftX, UpperLeftY, LowerRightX, LowerRightY] )
         */
        Q_INVOKABLE QScriptValue getCameraViewport();

    private:
        /*!
         *  \brief Read the XML node for define the Camera Viewport
         *  \param xmlNode : XML node
         */
        void setViewport(tinyxml2::XMLElement* xmlNode);

    private:
        irr::scene::ICameraSceneNode* camera;
        irr::core::rect<irr::f32> viewport;
        irr::core::rect<irr::s32> absoluteViewport;

    private:
        DISALLOW_COPY_AND_ASSIGN(Camera);
    };

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_CAMERA_H_
