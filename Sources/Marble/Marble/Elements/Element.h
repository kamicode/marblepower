/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Element.h
 * \brief Element base
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_ELEMENT_H_
#define MARBLE_ELEMENTS_ELEMENT_H_

#include <map>

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

class Script;

/*! \class Element
 * \brief
 *
 */
class Element: public QObject {
    Q_OBJECT

    public:
        explicit Element(QString name);
        virtual ~Element();
        virtual void destroy();
        virtual bool readXML(tinyxml2::XMLElement* elem);
        virtual void update(float time);
        virtual void init();
        virtual void lineRender() { }
        virtual btRigidBody* getRigidBody() const { return NULL; }
        bool readScript(tinyxml2::XMLElement* elem);
        bool eventGUI(const CEGUI::EventArgs& e);

    public:
        Q_INVOKABLE virtual QString getTypeName() { return "Element"; }
        Q_INVOKABLE void execute(QString code, QString script = "");
        Q_INVOKABLE QString getName() const { return name; }

    protected:
        QString name;

    private:
        std::map<QString, Script*> scripts;

    private:
        DISALLOW_COPY_AND_ASSIGN(Element);
    };

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_ELEMENT_H_
