/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Constraint.h
 * \brief Element of type Constraint
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_CONSTRAINT_H_
#define MARBLE_ELEMENTS_CONSTRAINT_H_

#include "Marble/Elements/Element.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Constraint
 * \brief
 *
 */
class Constraint: public Element {
    Q_OBJECT

    public:
        explicit Constraint(QString name);

        virtual ~Constraint();
        void init();

        bool createPoint2PointConstraint(
                QString nameA,
                const btVector3 &pivotInA,
                QString nameB = "",
                const btVector3 &pivotInB = btVector3());

        bool createHingeConstraint(QString nameA, const btVector3 &pivotInA,
                                   const btVector3 &axisInA, QString nameB = "",
                                   const btVector3 &pivotInB = btVector3(),
                                   const btVector3 &axisInB = btVector3());

        bool readXML(tinyxml2::XMLElement* elem);

    public:
        Q_INVOKABLE QString getTypeName() { return "Constraint"; }

    private:
        btRigidBody* bodyA;
        btRigidBody* bodyB;
        btTypedConstraint* constaint;

        QString constaintType;
        QString nameA;
        QString nameB;
        btVector3 pivotInA;
        btVector3 pivotInB;
        btVector3 axisInA;
        btVector3 axisInB;

    private:
        DISALLOW_COPY_AND_ASSIGN(Constraint);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_CONSTRAINT_H_
