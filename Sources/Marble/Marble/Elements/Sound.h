/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Sound.h
 * \brief Element of type "Sound"
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_SOUND_H_
#define MARBLE_ELEMENTS_SOUND_H_

#include "Marble/Elements/Element.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Sound
 * \brief
 *
 */
class Sound: public Element {
    Q_OBJECT

    public:
        explicit Sound(QString name);
        virtual ~Sound();
        bool readXML(tinyxml2::XMLElement* elem);

    public:
        Q_INVOKABLE QString getTypeName() { return "Sound"; }
        Q_INVOKABLE QScriptValue getPosition();
        Q_INVOKABLE QScriptValue getVelocity();
        Q_INVOKABLE float getGain();
        Q_INVOKABLE float getPitch();
        Q_INVOKABLE bool getLoop();
        Q_INVOKABLE void setPosition(const QScriptValue &position);
        Q_INVOKABLE void setVelocity(const QScriptValue &vel);
        Q_INVOKABLE void setGain(float value);
        Q_INVOKABLE void setPitch(float value);
        Q_INVOKABLE void setLoop(bool value);

    private:
        int mSource;
        QString file;
        float pitch, gain;
        float pos[3], velocity[3];
        bool loop;

    private:
        DISALLOW_COPY_AND_ASSIGN(Sound);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_SOUND_H_
