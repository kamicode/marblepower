/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Light.h"

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Elements/Script.h"

namespace MarbleEngine {

Light::Light(QString name)
    : Element(name) {
    irr::scene::ISceneManager* m =
            Engine::getGraphicsEngine()->getSceneManager();
    lumiere = m->addLightSceneNode(0);
}

bool Light::create(QString type, bool shadow, irr::core::vector3df pos,
                   irr::core::vector3df rot, float outerCone, float innerCone,
                   float falloff, int ambiantA, int ambiantR, int ambiantG,
                   int ambiantB, int diffuseA, int diffuseR, int diffuseG,
                   int diffuseB, int specularA, int specularR, int specularG,
                   int specularB, float attenuationConst, float attenuationLin,
                   float attenuationQuad) {
    Element::init();
    lumiere->setLightType(Convert::stringLightType(type));
    lumiere->enableCastShadow(shadow);
    lumiere->setPosition(pos);
    lumiere->setRotation(rot);

    irr::video::SLight light = lumiere->getLightData();

    light.OuterCone = outerCone;
    light.InnerCone = innerCone;
    light.Falloff = falloff;
    irr::video::SColor ambiant(ambiantA, ambiantR, ambiantG, ambiantB);
    light.AmbientColor = irr::video::SColorf(ambiant);
    irr::video::SColor diffuse(diffuseA, diffuseR, diffuseG, diffuseB);
    light.DiffuseColor = irr::video::SColorf(diffuse);
    irr::video::SColor specular(specularA, specularR, specularG, specularB);
    light.SpecularColor = irr::video::SColorf(specular);
    irr::core::vector3df attenuation(attenuationConst,
                                     attenuationLin,
                                     attenuationQuad);
    light.Attenuation = attenuation;

    lumiere->setLightData(light);

    return true;
}

Light::~Light() {
}

bool Light::readXML(tinyxml2::XMLElement* elem) {
    irr::video::SLight light;

    if (elem->Attribute("type")) {
        lumiere->setLightType(
                    Convert::stringLightType(elem->Attribute("type")));
    }

    if (elem->Attribute("castShadow")) {
        lumiere->enableCastShadow(elem->BoolAttribute("castShadow"));
    }

    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        irr::core::vector3df pos;
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
        lumiere->setPosition(pos);
    }

    if (elem->Attribute("rotX") && elem->Attribute("rotY")
            && elem->Attribute("rotZ")) {
        irr::core::vector3df rot;
        rot.X = elem->FloatAttribute("rotX");
        rot.Y = elem->FloatAttribute("rotY");
        rot.Z = elem->FloatAttribute("rotZ");
        lumiere->setRotation(rot);
    }

    light = lumiere->getLightData();

    if (elem->Attribute("outerCone")) {
        light.OuterCone = elem->FloatAttribute("outerCone");
    }

    if (elem->Attribute("innerCone")) {
        light.InnerCone = elem->FloatAttribute("innerCone");
    }

    if (elem->Attribute("falloff")) {
        light.Falloff = elem->FloatAttribute("falloff");
    }

    if (elem->Attribute("ambiantR") && elem->Attribute("ambiantG")
            && elem->Attribute("ambiantB") && elem->Attribute("ambiantA")) {
        irr::video::SColor color(0, 0, 0, 0);
        color.setRed(elem->IntAttribute("ambiantR"));
        color.setGreen(elem->IntAttribute("ambiantG"));
        color.setBlue(elem->IntAttribute("ambiantB"));
        color.setAlpha(elem->IntAttribute("ambiantA"));
        irr::video::SColorf col(color);
        light.AmbientColor = col;
    }

    if (elem->Attribute("diffuseR") && elem->Attribute("diffuseG")
            && elem->Attribute("diffuseB") && elem->Attribute("diffuseA")) {
        irr::video::SColor color(0, 0, 0, 0);
        color.setRed(elem->IntAttribute("diffuseR"));
        color.setGreen(elem->IntAttribute("diffuseG"));
        color.setBlue(elem->IntAttribute("diffuseB"));
        color.setAlpha(elem->IntAttribute("diffuseA"));
        irr::video::SColorf col(color);
        light.DiffuseColor = col;
    }

    if (elem->Attribute("specularR") && elem->Attribute("specularG")
            && elem->Attribute("specularB") && elem->Attribute("specularA")) {
        irr::video::SColor color(0, 0, 0, 0);
        color.setRed(elem->IntAttribute("specularR"));
        color.setGreen(elem->IntAttribute("specularG"));
        color.setBlue(elem->IntAttribute("specularB"));
        color.setAlpha(elem->IntAttribute("specularA"));
        irr::video::SColorf col(color);
        light.SpecularColor = col;
    }

    if (elem->Attribute("attenuationConst") && elem->Attribute("attenuationLin")
            && elem->Attribute("attenuationQuad")) {
        irr::core::vector3df attenuation;
        attenuation.X = elem->FloatAttribute("attenuationConst");
        attenuation.Y = elem->FloatAttribute("attenuationLin");
        attenuation.Z = elem->FloatAttribute("attenuationQuad");
        light.Attenuation = attenuation;
    }

    lumiere->setLightData(light);

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            Element::readXML(elem);
        }
        elem = elem->NextSiblingElement();
    }

    return true;
}

void Light::setType(const QString &type) {
    lumiere->setLightType(Convert::stringLightType(type));
}

void Light::enableCastShadow(bool enable) {
    lumiere->enableCastShadow(enable);
}

void Light::setCone(float outerCone, float innerCone, float falloff) {
    irr::video::SLight light = lumiere->getLightData();
    light.OuterCone = outerCone;
    light.InnerCone = innerCone;
    light.Falloff = falloff;
    lumiere->setLightData(light);
}

void Light::setAttenuation(float attenuationConst, float attenuationLin,
                           float attenuationQuad)  {
    irr::video::SLight light = lumiere->getLightData();
    irr::core::vector3df attenuation;
    attenuation.X = attenuationConst;
    attenuation.Y = attenuationLin;
    attenuation.Z = attenuationQuad;
    light.Attenuation = attenuation;
    lumiere->setLightData(light);
}

void Light::setAmbiantColor(int alpha, int red, int green, int blue)  {
    irr::video::SLight light = lumiere->getLightData();
    irr::video::SColor color(alpha, red, green, blue);
    irr::video::SColorf col(color);
    light.AmbientColor = col;
    lumiere->setLightData(light);
}

void Light::setDiffuseColor(int alpha, int red, int green, int blue)  {
    irr::video::SLight light = lumiere->getLightData();
    irr::video::SColor color(alpha, red, green, blue);
    irr::video::SColorf col(color);
    light.DiffuseColor = col;
    lumiere->setLightData(light);
}

void Light::setSpecularColor(int alpha, int red, int green, int blue)  {
    irr::video::SLight light = lumiere->getLightData();
    irr::video::SColor color(alpha, red, green, blue);
    irr::video::SColorf col(color);
    light.SpecularColor = col;
    lumiere->setLightData(light);
}

QScriptValue Light::getPosition() {
    irr::core::vector3df pos = lumiere->getPosition();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Light::setPosition(const QScriptValue &pos) {
    irr::core::vector3df vect;
    vect.X = pos.property("X").toNumber();
    vect.Y = pos.property("Y").toNumber();
    vect.Z = pos.property("Z").toNumber();

    lumiere->setPosition(vect);
    lumiere->updateAbsolutePosition();
}

QScriptValue Light::getRotation() {
    irr::core::vector3df pos = lumiere->getRotation();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Light::setRotation(const QScriptValue &pos) {
    irr::core::vector3df vect;
    vect.X = pos.property("X").toNumber();
    vect.Y = pos.property("Y").toNumber();
    vect.Z = pos.property("Z").toNumber();

    lumiere->setRotation(vect);
}

void Light::setTarget(const QScriptValue &target) {
    irr::core::vector3df pos;
    pos.X = target.property("X").toNumber();
    pos.Y = target.property("Y").toNumber();
    pos.Z = target.property("Z").toNumber();

    irr::core::vector3df nodePos = lumiere->getPosition();
    irr::core::vector3df posDiff = pos - nodePos;
    posDiff.normalize();
    lumiere->setRotation(posDiff.getHorizontalAngle());
}

}  // namespace MarbleEngine
