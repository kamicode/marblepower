/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Particules.h
 * \brief Element of type "Particules"
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_PARTICULES_H_
#define MARBLE_ELEMENTS_PARTICULES_H_

#include "Marble/Elements/Element.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Particules
 * \brief
 *
 */
class Particules: public Element {
    Q_OBJECT

    public:
        explicit Particules(QString name);
        virtual ~Particules();
        bool readXML(tinyxml2::XMLElement* elem);
        void boxEmetteur(tinyxml2::XMLElement* elem);
        bool create(irr::core::vector3df pos, irr::core::vector3df rot,
                    irr::core::vector3df scale, irr::core::vector3df boxMin,
                    irr::core::vector3df boxMax, irr::core::vector3df dir,
                    int minParticlesPerSecond, int maxParticlesPerSecond,
                    int lifeTimeMin, int lifeTimeMax, int maxAngleDegrees,
                    int minStartColorA, int minStartColorR, int minStartColorG,
                    int minStartColorB, int maxStartColorA, int maxStartColorR,
                    int maxStartColorG, int maxStartColorB, float minStartWidth,
                    float minStartHeight, float maxStartWidth,
                    float maxStartHeight);

    public:
        Q_INVOKABLE QString getTypeName() { return "Particules"; }
        Q_INVOKABLE QScriptValue getPosition();
        Q_INVOKABLE void setPosition(const QScriptValue &pos);
        Q_INVOKABLE QScriptValue getRotation();
        Q_INVOKABLE void setRotation(const QScriptValue &pos);
        Q_INVOKABLE QScriptValue getScale();
        Q_INVOKABLE void setScale(const QScriptValue &pos);
        Q_INVOKABLE void setTarget(const QScriptValue &target);
        Q_INVOKABLE void removeAllAffectors();
        Q_INVOKABLE void addFadeOutAffector(int alpha, int red, int green,
                                            int blue, int timeNeededToFadeOut);
        Q_INVOKABLE void addAttractionAffector(float posX, float posY,
                                               float posZ, float speed,
                                               bool attract, bool affectX,
                                               bool affectY, bool affectZ);
        Q_INVOKABLE void addGravityAffector(float posX, float posY,
                                            float posZ, int timeForceLost);
        Q_INVOKABLE void addRotationAffector(float pivotX, float pivotY,
                                             float pivotZ, float speedX,
                                             float speedY, float speedZ);
        Q_INVOKABLE void setMaterial3D(int num, bool wireframe, bool pointCloud,
                                       bool gouraudShading, bool lighting,
                                       bool zwriteEnable, bool backfaceCulling,
                                       bool frontfaceCulling, bool fogEnable,
                                       bool normalizeNormals, float shininess,
                                       QString materialTypeParam,
                                       QString materialTypeParam2,
                                       QString materialType, QString zBuffer,
                                       QString antiAliasing, QString colorMask,
                                       QString colorMaterial, QString texture,
                                       int ambiantAlpha, int ambiantRed,
                                       int ambiantGreen, int ambiantBlue,
                                       int diffuseAlpha, int diffuseRed,
                                       int diffuseGreen, int diffuseBlue,
                                       int emissiveAlpha, int emissiveRed,
                                       int emissiveGreen, int emissiveBlue,
                                       int specularAlpha, int specularRed,
                                       int specularGreen, int specularBlue);

    private:
        irr::scene::IParticleSystemSceneNode* particleSystem;

    private:
        DISALLOW_COPY_AND_ASSIGN(Particules);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_PARTICULES_H_
