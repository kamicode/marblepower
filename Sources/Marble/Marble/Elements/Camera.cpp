/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Camera.h"

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Elements/Mesh.h"
#include "Marble/Elements/Script.h"

namespace MarbleEngine {

Camera::Camera(QString name) : Element(name) {
    GraphicsEngine* engine = Engine::getGraphicsEngine();
    camera = engine->getSceneManager()->addCameraSceneNode();
    camera->bindTargetAndRotation(true);
    setCameraViewport(0, 0, 1, 1);

    irr::f32 width = absoluteViewport.getWidth();
    irr::f32 height = absoluteViewport.getHeight();

    camera->setAspectRatio(width / height);
}

Camera::~Camera() {
}

bool Camera::readXML(tinyxml2::XMLElement* xmlNode) {
    if (xmlNode->Attribute("posX") && xmlNode->Attribute("posY")
            && xmlNode->Attribute("posZ")) {
        irr::core::vector3df pos;
        pos.X = xmlNode->FloatAttribute("posX");
        pos.Y = xmlNode->FloatAttribute("posY");
        pos.Z = xmlNode->FloatAttribute("posZ");
        camera->setPosition(pos);
    }

    if (xmlNode->Attribute("targetX") && xmlNode->Attribute("targetY")
            && xmlNode->Attribute("targetZ")) {
        irr::core::vector3df target;
        target.X = xmlNode->FloatAttribute("targetX");
        target.Y = xmlNode->FloatAttribute("targetY");
        target.Z = xmlNode->FloatAttribute("targetZ");
        camera->setTarget(target);
    }

    if (xmlNode->Attribute("farValue")) {
        camera->setFarValue(xmlNode->FloatAttribute("farValue"));
    }

    if (xmlNode->Attribute("nearValue")) {
        camera->setNearValue(xmlNode->FloatAttribute("nearValue"));
    }

    if (xmlNode->Attribute("fov")) {
        camera->setFOV(xmlNode->FloatAttribute("fov"));
    }

    xmlNode = xmlNode->FirstChildElement();
    while (xmlNode) {
        if (QString(xmlNode->Value()) == "Script") {
            Element::readXML(xmlNode);
        } else if (QString(xmlNode->Value()) == "Viewport") {
            setViewport(xmlNode);
        }

        xmlNode = xmlNode->NextSiblingElement();
    }

    return true;
}

void Camera::setViewport(tinyxml2::XMLElement* xmlNode) {
    if (xmlNode->Attribute("x1") && xmlNode->Attribute("y1")
            && xmlNode->Attribute("x2") && xmlNode->Attribute("y2")) {
        setCameraViewport(xmlNode->FloatAttribute("x1"),
                          xmlNode->FloatAttribute("y1"),
                          xmlNode->FloatAttribute("x2"),
                          xmlNode->FloatAttribute("y2"));
    }
}

irr::scene::ICameraSceneNode* Camera::getCamera() const {
    return camera;
}

irr::core::rect<irr::f32> Camera::getViewport() {
    return viewport;
}

irr::core::rect<irr::s32> Camera::getAbsoluteViewport() {
    return absoluteViewport;
}

QScriptValue Camera::getPosition() {
    irr::core::vector3df pos = camera->getPosition();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Camera::setPosition(const QScriptValue &position) {
    irr::core::vector3df vect;
    vect.X = position.property("X").toNumber();
    vect.Y = position.property("Y").toNumber();
    vect.Z = position.property("Z").toNumber();

    camera->setPosition(vect);
    camera->updateAbsolutePosition();
}

QScriptValue Camera::getUpVector() {
    irr::core::vector3df pos = camera->getUpVector();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Camera::setUpVector(const QScriptValue &upVector) {
    irr::core::vector3df vect;
    vect.X = upVector.property("X").toNumber();
    vect.Y = upVector.property("Y").toNumber();
    vect.Z = upVector.property("Z").toNumber();

    camera->setUpVector(vect);
}

QScriptValue Camera::getTarget() {
    irr::core::vector3df pos = camera->getTarget();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Camera::setTarget(const QScriptValue &target) {
    irr::core::vector3df vect;
    vect.X = target.property("X").toNumber();
    vect.Y = target.property("Y").toNumber();
    vect.Z = target.property("Z").toNumber();

    camera->setTarget(vect);
}

QScriptValue Camera::getRotation() {
    irr::core::vector3df pos = camera->getRotation();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Camera::setRotation(const QScriptValue &rotation) {
    irr::core::vector3df vect;
    vect.X = rotation.property("X").toNumber();
    vect.Y = rotation.property("Y").toNumber();
    vect.Z = rotation.property("Z").toNumber();

    camera->setRotation(vect);
}

void Camera::setFarValue(float farValue) {
    camera->setFarValue(farValue);
}

void Camera::setNearValue(float nearValue) {
    camera->setNearValue(nearValue);
}

void Camera::setFOV(float fov) {
    camera->setFOV(fov);
}

void Camera::setCameraViewport(float upperLeftX, float upperLeftY,
                               float lowerRightX, float lowerRightY) {
    irr::core::dimension2d<irr::u32> size =
            Engine::getGraphicsEngine()->getDriver()->getScreenSize();
    irr::u32 width = size.Width;
    irr::u32 height = size.Height;

    viewport = irr::core::rect<irr::f32>(upperLeftX, upperLeftY,
                                         lowerRightX, lowerRightY);
    absoluteViewport = irr::core::rect<irr::s32>(upperLeftX * width,
                                                 upperLeftY * height,
                                                 lowerRightX * width,
                                                 lowerRightY * height);

    irr::f32 absWidth = absoluteViewport.getWidth();
    irr::f32 absHeight = absoluteViewport.getHeight();
    camera->setAspectRatio(absWidth / absHeight);
}

QScriptValue Camera::getCameraViewport() {
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("UpperLeftX", viewport.UpperLeftCorner.X);
    ret.setProperty("UpperLeftY", viewport.UpperLeftCorner.Y);
    ret.setProperty("LowerRightX", viewport.LowerRightCorner.X);
    ret.setProperty("LowerRightY", viewport.LowerRightCorner.Y);
    return ret;
}

}  // namespace MarbleEngine
