/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Constraint.h"

#include "Marble/Elements/Mesh.h"
#include "Marble/Engines/Engine.h"
#include "Marble/Engines/PhysicsEngine.h"
#include "Marble/Elements/Script.h"

namespace MarbleEngine {

Constraint::Constraint(QString name) : Element(name) {
    bodyA = NULL;
    bodyB = NULL;
    constaint = NULL;
}

Constraint::~Constraint() {
    delete constaint;
    constaint = NULL;
}

void Constraint::init() {
    if (constaintType == "Ball") {
        createPoint2PointConstraint(nameA, pivotInA, nameB, pivotInB);
    } else if (constaintType == "Hinge") {
        createHingeConstraint(nameA, pivotInA, axisInA,
                              nameB, pivotInB, axisInB);
    }
}

bool Constraint::createPoint2PointConstraint(QString nameA,
                                             const btVector3 &pivotInA,
                                             QString nameB,
                                             const btVector3 &pivotInB) {
    Element::init();

    Element* elt = Engine::getElements()[nameA];
    if (elt == NULL)
        return false;
    bodyA = elt->getRigidBody();
    if (bodyA == NULL)
        return false;

    elt = Engine::getElements()[nameB];
    if (elt != NULL)
        bodyB = elt->getRigidBody();
    else
        bodyB = NULL;

    if (bodyB) {
        constaint = new btPoint2PointConstraint(*bodyA, *bodyB,
                                                pivotInA, pivotInB);
    } else {
        constaint = new btPoint2PointConstraint(*bodyA, pivotInA);
    }

    Engine::getPhysicsEngine()->getWorld()->addConstraint(constaint);

    return true;
}

bool Constraint::createHingeConstraint(QString nameA, const btVector3 &pivotInA,
                                       const btVector3 &axisInA, QString nameB,
                                       const btVector3 &pivotInB,
                                       const btVector3 &axisInB) {
    Element::init();

    Element* elt = Engine::getElements()[nameA];
    if (elt == NULL)
        return false;
    bodyA = elt->getRigidBody();
    if (bodyA == NULL)
        return false;

    elt = Engine::getElements()[nameB];
    if (elt != NULL)
        bodyB = elt->getRigidBody();
    else
        bodyB = NULL;

    if (bodyB) {
        constaint = new btHingeConstraint(*bodyA, *bodyB, pivotInA,
                                          pivotInB, axisInA, axisInB);
    } else {
        constaint = new btHingeConstraint(*bodyA, pivotInA, axisInA);
    }

    Engine::getPhysicsEngine()->getWorld()->addConstraint(constaint);

    return true;
}

bool Constraint::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("type")) {
        constaintType = elem->Attribute("type");
    }

    if (elem->Attribute("nameA")) {
        nameA = elem->Attribute("nameA");
    }

    if (elem->Attribute("nameB")) {
        nameB = elem->Attribute("nameB");
    }

    if (elem->Attribute("pivotInA_X") && elem->Attribute("pivotInA_Y")
            && elem->Attribute("pivotInA_Z")) {
        pivotInA.setX(elem->FloatAttribute("pivotInA_X"));
        pivotInA.setY(elem->FloatAttribute("pivotInA_Y"));
        pivotInA.setZ(elem->FloatAttribute("pivotInA_Z"));
    }

    if (elem->Attribute("pivotInB_X") && elem->Attribute("pivotInB_Y")
            && elem->Attribute("pivotInB_Z")) {
        pivotInB.setX(elem->FloatAttribute("pivotInB_X"));
        pivotInB.setY(elem->FloatAttribute("pivotInB_Y"));
        pivotInB.setZ(elem->FloatAttribute("pivotInB_Z"));
    }

    if (elem->Attribute("axisInA_X") && elem->Attribute("axisInA_Y")
            && elem->Attribute("axisInA_Z")) {
        axisInA.setX(elem->FloatAttribute("axisInA_X"));
        axisInA.setY(elem->FloatAttribute("axisInA_Y"));
        axisInA.setZ(elem->FloatAttribute("axisInA_Z"));
    }

    if (elem->Attribute("axisInB_X") && elem->Attribute("axisInB_Y")
            && elem->Attribute("axisInB_Z")) {
        axisInB.setX(elem->FloatAttribute("axisInB_X"));
        axisInB.setY(elem->FloatAttribute("axisInB_Y"));
        axisInB.setZ(elem->FloatAttribute("axisInB_Z"));
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            Element::readXML(elem);
        }

        elem = elem->NextSiblingElement();
    }

    return true;
}

}  // namespace MarbleEngine
