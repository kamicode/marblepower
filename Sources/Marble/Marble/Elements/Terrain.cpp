/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Terrain.h"

#include "Marble/Materials/Material3D.h"
#include "Marble/Engines/Engine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Engines/PhysicsEngine.h"
#include "Marble/Elements/Script.h"

namespace MarbleEngine {

Terrain::Terrain(QString name) : Mesh(name) {
    maxLod = 7;
}

Terrain::~Terrain() {
}

bool Terrain::create(QString heightMapFile, QString patchSize,
                     irr::core::vector3df pos, irr::core::vector3df rot,
                     irr::core::vector3df scale, int maxLOD, int smoothFactor,
                     bool addAlsoIfHeightmapEmpty, int alpha, int red,
                     int green, int blue, irr::core::vector3df physiqueSize,
                     QString materialPhysic) {
    Mesh::init();

    heightMapFile = Engine::getDirMedia() + heightMapFile;
    irr::video::SColor color(alpha, red, green, blue);

    this->maxLod = maxLOD;

    irr::scene::ISceneManager*  m =
            Engine::getGraphicsEngine()->getSceneManager();
    node = m->addTerrainSceneNode(heightMapFile.toUtf8().data(),
                                  0, -1, pos, rot, scale, color, maxLOD,
                                  Convert::stringTerrainPatch(patchSize),
                                  smoothFactor, addAlsoIfHeightmapEmpty);

    loadBody(physiqueSize, materialPhysic);

    return true;
}

bool Terrain::readXML(tinyxml2::XMLElement* elem) {
    bool ret = false;

    irr::core::vector3df scale(1, 1, 1);
    irr::core::vector3df pos(0, 0, 0);
    irr::core::vector3df rot(0, 0, 0);

    QString heightMapFile(""), patchSize("");
    irr::video::SColor color(0, 0, 0, 0);
    int smoothFactor;
    bool addAlsoIfHeightmapEmpty = false;

    if (elem->Attribute("heightMapFile")) {
        heightMapFile = Engine::getDirMedia()
                + elem->Attribute("heightMapFile");
    }

    if (elem->Attribute("patchSize")) {
        patchSize = elem->Attribute("patchSize");
    }

    if (elem->Attribute("maxLOD")) {
        this->maxLod = elem->IntAttribute("maxLOD");
    }
    if (elem->Attribute("smoothFactor")) {
        smoothFactor = elem->IntAttribute("smoothFactor");
    }
    if (elem->Attribute("addAlsoIfHeightmapEmpty")) {
        addAlsoIfHeightmapEmpty =
                elem->BoolAttribute("addAlsoIfHeightmapEmpty");
    }

    if (elem->Attribute("alpha") && elem->Attribute("red")
            && elem->Attribute("green") && elem->Attribute("blue")) {
        color.setAlpha(elem->IntAttribute("alpha"));
        color.setRed(elem->IntAttribute("red"));
        color.setGreen(elem->IntAttribute("green"));
        color.setBlue(elem->IntAttribute("blue"));
    }

    if (elem->Attribute("scaleX") && elem->Attribute("scaleY")
            && elem->Attribute("scaleZ")) {
        scale.X = elem->FloatAttribute("scaleX");
        scale.Y = elem->FloatAttribute("scaleY");
        scale.Z = elem->FloatAttribute("scaleZ");
    }

    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
    }

    if (elem->Attribute("rotX") && elem->Attribute("rotY")
            && elem->Attribute("rotZ")) {
        rot.X = elem->FloatAttribute("rotX");
        rot.Y = elem->FloatAttribute("rotY");
        rot.Z = elem->FloatAttribute("rotZ");
    }

    irr::scene::ISceneManager*  m =
            Engine::getGraphicsEngine()->getSceneManager();
    node = m->addTerrainSceneNode(heightMapFile.toUtf8().data(),
                                  0, -1, pos, rot, scale, color,
                                  this->maxLod,
                                  Convert::stringTerrainPatch(patchSize),
                                  smoothFactor, addAlsoIfHeightmapEmpty);

    elem = elem->FirstChildElement();
    ret = true;
    while (elem) {
        if (ret) {
            if (QString(elem->Value()) == "TerrainMaterial3D") {
                tinyxml2::XMLElement* Material3DNode =
                        elem->FirstChildElement();
                while (Material3DNode) {
                    if (QString(Material3DNode->Value()) == "Material3D") {
                        unsigned int num =
                                Material3DNode->UnsignedAttribute("num");
                        if (num < node->getMaterialCount()) {
                            Material3D* m =
                                    new Material3D(&(node->getMaterial(num)));
                            m->readXML(Material3DNode);
                            delete m;
                            m = NULL;
                        }
                    }
                    Material3DNode = Material3DNode->NextSiblingElement();
                }
                if (elem->Attribute("scaleTexture1")
                        && elem->Attribute("scaleTexture2")) {
                    float scaleTexture1 = elem->FloatAttribute("scaleTexture1");
                    float scaleTexture2 = elem->FloatAttribute("scaleTexture2");
                    ((irr::scene::ITerrainSceneNode*)node)->scaleTexture(
                                scaleTexture1, scaleTexture2);
                }
            } else if (QString(elem->Value()) == "Script") {
                Element::readXML(elem);
            } else if (QString(elem->Value()) == "Physic") {
                irr::core::vector3df size;
                if (elem->Attribute("sizeX") && elem->Attribute("sizeY")
                        && elem->Attribute("sizeZ")) {
                    size.X = elem->FloatAttribute("sizeX");
                    size.Y = elem->FloatAttribute("sizeY");
                    size.Z = elem->FloatAttribute("sizeZ");
                } else {
                    size = node->getScale();
                }

                loadBody(size, elem->Attribute("material"));
            }
        }

        elem = elem->NextSiblingElement();
    }
    return ret;
}

void Terrain::setScaleTexture(float scaleTexture1, float scaleTexture2) {
    ((irr::scene::ITerrainSceneNode*)node)->scaleTexture(scaleTexture1,
                                                         scaleTexture2);
}

void Terrain::setLODOfPatch(int patchX, int patchZ, int lod) {
    ((irr::scene::ITerrainSceneNode*)node)->setLODOfPatch(patchX, patchZ, lod);
}

bool Terrain::loadBody(irr::core::vector3df size, QString material) {
    btDiscreteDynamicsWorld* nWorld = Engine::getPhysicsEngine()->getWorld();
    if (nWorld) {
        irr::scene::ITerrainSceneNode* n = (irr::scene::ITerrainSceneNode*)node;
        irr::scene::IMeshBuffer* meshBuffer = n->getMesh()->getMeshBuffer(0);

        irr::scene::CDynamicMeshBuffer* buffer =
                new irr::scene::CDynamicMeshBuffer(
                    meshBuffer->getVertexType(),
                    meshBuffer->getIndexType());

        ((irr::scene::ITerrainSceneNode*)node)->getMeshBufferForLOD(
                    *buffer, this->maxLod);

        btVector3 vertices[3];
        irr::u16* mb_indices = buffer->getIndices();
        btTriangleMesh* mTriMesh = new btTriangleMesh();
        const irr::u32 indexCount = buffer->getIndexCount();
        irr::video::S3DVertex2TCoords* mb_vertices =
                static_cast<irr::video::S3DVertex2TCoords*>
                (buffer->getVertexBuffer().getData());

        for (irr::u32 j = 0; j < indexCount; j += 3) {
            for (irr::u32 k = 0; k < 3; ++k) {
                irr::s32 index = mb_indices[j + k];
                vertices[k] = btVector3(mb_vertices[index].Pos.X * size.X,
                                        mb_vertices[index].Pos.Y * size.Y,
                                        mb_vertices[index].Pos.Z * size.Z);
            }
            mTriMesh->addTriangle(vertices[0], vertices[1], vertices[2]);
        }

        buffer->drop();

        m_collisionShape = new btBvhTriangleMeshShape(mTriMesh, true);

        btTransform trans;
        trans.setIdentity();
        trans.setOrigin(btVector3(node->getPosition().X,
                                  node->getPosition().Y,
                                  node->getPosition().Z));
        btQuaternion btq = Convert::EulerToQuaternion(
                    btVector3(node->getRotation().X * 0.0174532925f,
                              node->getRotation().Y * 0.0174532925f,
                              node->getRotation().Z * 0.0174532925f));
        trans.setRotation(btq);

        m_motionState = new MotionState(trans, node);
        btVector3 inertia(0, 0, 0);
        btRigidBody::btRigidBodyConstructionInfo rigidBodyInfo(
                    0, m_motionState, m_collisionShape, inertia);
        m_rigidBody = new btRigidBody(rigidBodyInfo);
        m_collisionShape->setUserPointer(this);
        m_rigidBody->setUserPointer(this);
        setMaterialPhys(material);
        showPhysic(false);

        nWorld->addRigidBody(m_rigidBody);
    }
    return true;
}

}  // namespace MarbleEngine
