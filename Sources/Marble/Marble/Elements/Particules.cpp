/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Particules.h"

#include "Marble/Materials/Material3D.h"
#include "Marble/Engines/Engine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Elements/Script.h"

namespace MarbleEngine {

Particules::Particules(QString name) : Element(name) {
    particleSystem = NULL;
}

Particules::~Particules() {
    particleSystem->remove();
}

bool Particules::create(irr::core::vector3df pos, irr::core::vector3df rot,
                        irr::core::vector3df scale, irr::core::vector3df boxMin,
                        irr::core::vector3df boxMax, irr::core::vector3df dir,
                        int minParticlesPerSecond, int maxParticlesPerSecond,
                        int lifeTimeMin, int lifeTimeMax, int maxAngleDegrees,
                        int minStartColorA, int minStartColorR,
                        int minStartColorG, int minStartColorB,
                        int maxStartColorA, int maxStartColorR,
                        int maxStartColorG, int maxStartColorB,
                        float minStartWidth, float minStartHeight,
                        float maxStartWidth, float maxStartHeight) {
    Element::init();

    irr::scene::ISceneManager* m =
            Engine::getGraphicsEngine()->getSceneManager();
    particleSystem = m->addParticleSystemSceneNode(false);

    irr::core::aabbox3d<irr::f32> box;
    box.MinEdge = boxMin;
    box.MaxEdge = boxMax;

    irr::video::SColor minStartColor(minStartColorA, minStartColorR,
                                     minStartColorG, minStartColorB);
    irr::video::SColor maxStartColor(maxStartColorA, maxStartColorR,
                                     maxStartColorG, maxStartColorB);
    irr::core::dimension2df minStartSize(minStartWidth, minStartHeight);
    irr::core::dimension2df maxStartSize(maxStartWidth, maxStartHeight);

    irr::scene::IParticleEmitter* emitter = particleSystem->createBoxEmitter(
                box, dir, minParticlesPerSecond, maxParticlesPerSecond,
                minStartColor, maxStartColor, lifeTimeMin, lifeTimeMax,
                maxAngleDegrees, minStartSize, maxStartSize);

    particleSystem->setEmitter(emitter);
    emitter->drop();

    particleSystem->setScale(scale);
    particleSystem->setPosition(pos);
    particleSystem->setRotation(rot);

    return true;
}

bool Particules::readXML(tinyxml2::XMLElement* elem) {
    bool ret = true;

    irr::scene::ISceneManager* m =
            Engine::getGraphicsEngine()->getSceneManager();
    particleSystem = m->addParticleSystemSceneNode(false);

    if (elem->Attribute("scaleX") && elem->Attribute("scaleY")
            && elem->Attribute("scaleZ")) {
        irr::core::vector3df scale;
        scale.X = elem->FloatAttribute("scaleX");
        scale.Y = elem->FloatAttribute("scaleY");
        scale.Z = elem->FloatAttribute("scaleZ");
        particleSystem->setScale(scale);
    }

    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        irr::core::vector3df pos;
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
        particleSystem->setPosition(pos);
    }

    if (elem->Attribute("rotX") && elem->Attribute("rotY")
            && elem->Attribute("rotZ")) {
        irr::core::vector3df rot;
        rot.X = elem->FloatAttribute("rotX");
        rot.Y = elem->FloatAttribute("rotY");
        rot.Z = elem->FloatAttribute("rotZ");
        particleSystem->setRotation(rot);
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Material3D") {
            int num = elem->IntAttribute("num");
            Material3D* m = new Material3D(
                        &(particleSystem->getMaterial(num)));
            m->readXML(elem);
            delete m;
            m = NULL;
        } else if (QString(elem->Value()) == "BoxEmetteur") {
            boxEmetteur(elem);
        } else if (QString(elem->Value()) == "Script") {
            Element::readXML(elem);
        }

        elem = elem->NextSiblingElement();
    }

    return ret;
}

void Particules::boxEmetteur(tinyxml2::XMLElement* elem) {
    irr::core::aabbox3d<irr::f32> box;
    irr::core::vector3df dir;
    irr::u32 minParticlesPerSecond = 0;
    irr::u32 maxParticlesPerSecond = 0;
    irr::video::SColor minStartColor;
    irr::video::SColor maxStartColor;
    irr::u32 lifeTimeMin = 0;
    irr::u32 lifeTimeMax = 0;
    irr::s32 maxAngleDegrees = 0;
    irr::core::dimension2df minStartSize;
    irr::core::dimension2df maxStartSize;

    if (elem->Attribute("boxMinX") && elem->Attribute("boxMinY")
            && elem->Attribute("boxMinZ")) {
        box.MinEdge.X = elem->FloatAttribute("boxMinX");
        box.MinEdge.Y = elem->FloatAttribute("boxMinY");
        box.MinEdge.Z = elem->FloatAttribute("boxMinZ");
    }

    if (elem->Attribute("boxMaxX") && elem->Attribute("boxMaxY")
            && elem->Attribute("boxMaxZ")) {
        box.MaxEdge.X = elem->FloatAttribute("boxMaxX");
        box.MaxEdge.Y = elem->FloatAttribute("boxMaxY");
        box.MaxEdge.Z = elem->FloatAttribute("boxMaxZ");
    }

    if (elem->Attribute("dirX") && elem->Attribute("dirY")
            && elem->Attribute("dirZ")) {
        dir.X = elem->FloatAttribute("dirX");
        dir.Y = elem->FloatAttribute("dirY");
        dir.Z = elem->FloatAttribute("dirZ");
    }

    if (elem->Attribute("minParticlesPerSecond")) {
        minParticlesPerSecond = elem->IntAttribute("minParticlesPerSecond");
    }

    if (elem->Attribute("maxParticlesPerSecond")) {
        maxParticlesPerSecond = elem->IntAttribute("maxParticlesPerSecond");
    }

    if (elem->Attribute("minStartColorA")
            && elem->Attribute("minStartColorR")
            && elem->Attribute("minStartColorG")
            && elem->Attribute("minStartColorB")) {
        minStartColor.setRed(elem->IntAttribute("minStartColorR"));
        minStartColor.setGreen(elem->IntAttribute("minStartColorG"));
        minStartColor.setBlue(elem->IntAttribute("minStartColorB"));
        minStartColor.setAlpha(elem->IntAttribute("minStartColorA"));
    }

    if (elem->Attribute("maxStartColorA")
            && elem->Attribute("maxStartColorR")
            && elem->Attribute("maxStartColorG")
            && elem->Attribute("maxStartColorB")) {
        maxStartColor.setRed(elem->IntAttribute("maxStartColorR"));
        maxStartColor.setGreen(elem->IntAttribute("maxStartColorG"));
        maxStartColor.setBlue(elem->IntAttribute("maxStartColorB"));
        maxStartColor.setAlpha(elem->IntAttribute("maxStartColorA"));
    }

    if (elem->Attribute("lifeTimeMin")) {
        lifeTimeMin = elem->IntAttribute("lifeTimeMin");
    }

    if (elem->Attribute("lifeTimeMax")) {
        lifeTimeMax = elem->IntAttribute("lifeTimeMax");
    }

    if (elem->Attribute("maxAngleDegrees")) {
        maxAngleDegrees = elem->IntAttribute("maxAngleDegrees");
    }

    if (elem->Attribute("minStartWidth")
            && elem->Attribute("minStartHeight")) {
        minStartSize.Width = elem->FloatAttribute("minStartWidth");
        minStartSize.Height = elem->FloatAttribute("minStartHeight");
    }

    if (elem->Attribute("maxStartWidth")
            && elem->Attribute("maxStartHeight")) {
        maxStartSize.Width = elem->FloatAttribute("maxStartWidth");
        maxStartSize.Height = elem->FloatAttribute("maxStartHeight");
    }

    irr::scene::IParticleEmitter* emitter = particleSystem->createBoxEmitter(
                box, dir, minParticlesPerSecond, maxParticlesPerSecond,
                minStartColor, maxStartColor, lifeTimeMin, lifeTimeMax,
                maxAngleDegrees, minStartSize, maxStartSize);

    particleSystem->setEmitter(emitter);
    emitter->drop();
}

void Particules::setMaterial3D(int num, bool wireframe,
                               bool pointCloud, bool gouraudShading,
                               bool lighting, bool zwriteEnable,
                               bool backfaceCulling, bool frontfaceCulling,
                               bool fogEnable, bool normalizeNormals,
                               float shininess, QString materialTypeParam,
                               QString materialTypeParam2,
                               QString materialType,
                               QString zBuffer, QString antiAliasing,
                               QString colorMask, QString colorMaterial,
                               QString texture, int ambiantAlpha,
                               int ambiantRed, int ambiantGreen,
                               int ambiantBlue, int diffuseAlpha,
                               int diffuseRed, int diffuseGreen,
                               int diffuseBlue, int emissiveAlpha,
                               int emissiveRed, int emissiveGreen,
                               int emissiveBlue, int specularAlpha,
                               int specularRed, int specularGreen,
                               int specularBlue) {
    irr::video::SMaterial* material = &(particleSystem->getMaterial(num));
    material->Wireframe = wireframe;
    material->PointCloud = pointCloud;
    material->GouraudShading = gouraudShading;
    material->Lighting = lighting;
    material->ZWriteEnable = zwriteEnable;
    material->BackfaceCulling = backfaceCulling;
    material->FrontfaceCulling = frontfaceCulling;
    material->FogEnable = fogEnable;
    material->NormalizeNormals = normalizeNormals;
    material->Shininess = shininess;
    material->MaterialTypeParam =
            Convert::stringMaterialType(materialTypeParam);
    material->MaterialTypeParam2 =
            Convert::stringMaterialType(materialTypeParam2);
    material->MaterialType = Convert::stringMaterialType(materialType);
    material->ZBuffer = Convert::stringZBuffer(zBuffer);
    material->AntiAliasing = Convert::stringAntiAliasing(antiAliasing);
    material->ColorMask = Convert::stringColorMask(colorMask);
    material->ColorMaterial = Convert::stringColorMaterial(colorMaterial);

    material->setTexture(
                0,
                Engine::getGraphicsEngine()->getDriver()->getTexture(
                    (Engine::getDirMedia() + texture).toUtf8().data()));

    irr::video::SColor ambiantColor(ambiantAlpha, ambiantRed,
                                    ambiantGreen, ambiantBlue);
    material->AmbientColor = ambiantColor;
    irr::video::SColor diffuseColor(diffuseAlpha, diffuseRed,
                                    diffuseGreen, diffuseBlue);
    material->DiffuseColor = diffuseColor;
    irr::video::SColor emissiveColor(emissiveAlpha, emissiveRed,
                                     emissiveGreen, emissiveBlue);
    material->EmissiveColor = emissiveColor;
    irr::video::SColor specularColor(specularAlpha, specularRed,
                                     specularGreen, specularBlue);
    material->SpecularColor = specularColor;
}

void Particules::addFadeOutAffector(int alpha, int red, int green,
                                    int blue, int timeNeededToFadeOut) {
    irr::video::SColor color(alpha, red, green, blue);
    irr::scene::IParticleAffector* affector =
            particleSystem->createFadeOutParticleAffector(color,
                                                          timeNeededToFadeOut);
    particleSystem->addAffector(affector);
    affector->drop();
}

void Particules::addAttractionAffector(float posX, float posY, float posZ,
                                       float speed, bool attract, bool affectX,
                                       bool affectY, bool affectZ) {
    irr::core::vector3df pos(posX, posY, posZ);

    irr::scene::IParticleAffector* affector =
            particleSystem->createAttractionAffector(pos, speed, attract,
                                                     affectX, affectY, affectZ);
    particleSystem->addAffector(affector);
    affector->drop();
}

void Particules::addGravityAffector(float posX, float posY, float posZ,
                                    int timeForceLost) {
    irr::core::vector3df pos(posX, posY, posZ);

    irr::scene::IParticleAffector* affector =
            particleSystem->createGravityAffector(pos, timeForceLost);
    particleSystem->addAffector(affector);
    affector->drop();
}

void Particules::addRotationAffector(float pivotX, float pivotY, float pivotZ,
                                     float speedX, float speedY, float speedZ) {
    irr::core::vector3df speed(speedX, speedY, speedZ);
    irr::core::vector3df pivot(pivotX, pivotY, pivotZ);
    irr::scene::IParticleAffector* affector =
            particleSystem->createRotationAffector(speed, pivot);
    particleSystem->addAffector(affector);
    affector->drop();
}

void Particules::removeAllAffectors() {
    particleSystem->removeAllAffectors();
}

QScriptValue Particules::getPosition() {
    irr::core::vector3df pos = particleSystem->getPosition();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Particules::setPosition(const QScriptValue &pos) {
    irr::core::vector3df vect;
    vect.X = pos.property("X").toNumber();
    vect.Y = pos.property("Y").toNumber();
    vect.Z = pos.property("Z").toNumber();

    particleSystem->setPosition(vect);
    particleSystem->updateAbsolutePosition();
}

QScriptValue Particules::getRotation() {
    irr::core::vector3df pos = particleSystem->getRotation();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Particules::setRotation(const QScriptValue &pos) {
    irr::core::vector3df vect;
    vect.X = pos.property("X").toNumber();
    vect.Y = pos.property("Y").toNumber();
    vect.Z = pos.property("Z").toNumber();

    particleSystem->setRotation(vect);
}

QScriptValue Particules::getScale() {
    irr::core::vector3df pos = particleSystem->getScale();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", pos.X);
    ret.setProperty("Y", pos.Y);
    ret.setProperty("Z", pos.Z);
    return ret;
}

void Particules::setScale(const QScriptValue &pos) {
    irr::core::vector3df vect;
    vect.X = pos.property("X").toNumber();
    vect.Y = pos.property("Y").toNumber();
    vect.Z = pos.property("Z").toNumber();

    particleSystem->setScale(vect);
}

void Particules::setTarget(const QScriptValue &target) {
    irr::core::vector3df pos;
    pos.X = target.property("X").toNumber();
    pos.Y = target.property("Y").toNumber();
    pos.Z = target.property("Z").toNumber();

    irr::core::vector3df nodePos = particleSystem->getPosition();
    irr::core::vector3df posDiff = pos - nodePos;
    posDiff.normalize();
    particleSystem->setRotation(posDiff.getHorizontalAngle());
}

}  // namespace MarbleEngine
