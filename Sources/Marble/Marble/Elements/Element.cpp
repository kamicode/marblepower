/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Element.h"

#include <map>

#include "Marble/Elements/Script.h"
#include "Marble/Engines/Engine.h"

namespace MarbleEngine {


Element::Element(QString name) {
    this->name = name;

    std::map<QString, Script*>::const_iterator it, end = scripts.end();
    for (it = scripts.begin(); it != end; ++it) {
        delete it->second;
    }
    scripts.clear();
}

Element::~Element() {
    if (Engine::getElt(name))
        Engine::getInstance()->removeElement(name);

    std::map<QString, Script*>::const_iterator it, end = scripts.end();
    for (it = scripts.begin(); it != end; ++it) {
        delete it->second;
    }
    scripts.clear();
}

void Element::destroy() {
    delete this;
}

void Element::update(float time) {
    QScriptEngine* buf = Engine::getScriptEngine();

    std::map<QString, Script*>::const_iterator it, end = scripts.end();
    for (it = scripts.begin(); it != end; ++it) {
        it->second->update(time);
    }

    Engine::setScriptEngine(buf);
}

bool Element::readXML(tinyxml2::XMLElement* elem) {
    if (QString(elem->Value()) == "Element") {
        elem = elem->FirstChildElement();
        while (elem) {
            if (QString(elem->Value()) == "Script") {
                readScript(elem);
            }
            elem = elem->NextSiblingElement();
        }
    } else {
        readScript(elem);
    }

    return true;
}

bool Element::readScript(tinyxml2::XMLElement* elem) {
    Script* s = new Script(this);
    QString scriptFile = Engine::getDirMedia() + elem->Attribute("script");
    scripts.insert(std::make_pair(scriptFile, s));
    return s->readXML(elem);
}

void Element::init() {
    QScriptEngine* buf = Engine::getScriptEngine();

    std::map<QString, Script*>::const_iterator it, end = scripts.end();
    for (it = scripts.begin(); it != end;) {
        bool ret = it->second->init();
        if (!ret) {
            delete it->second;
            it = scripts.erase(it);
        } else {
            ++it;
        }
    }

    Engine::setScriptEngine(buf);
}

void Element::execute(QString code, QString script) {
    QScriptEngine* buf = Engine::getScriptEngine();

    if (script.isEmpty()) {
        std::map<QString, Script*>::const_iterator it, end = scripts.end();
        for (it = scripts.begin(); it != end; ++it) {
            it->second->execute(code);
        }
    } else {
        std::map<QString, Script*>::iterator it;
        it = scripts.find(script);
        if (it != scripts.end()) {
            it->second->execute(code);
        }
    }

    Engine::setScriptEngine(buf);
}

bool Element::eventGUI(const CEGUI::EventArgs& e) {
    const CEGUI::WindowEventArgs* args =
            static_cast<const CEGUI::WindowEventArgs*>(&e);
    GuiScriptData* u = static_cast<GuiScriptData*>(args->window->getUserData());
    QString code = u->code;
    QString script = u->script;
    execute(code, script);
    return true;
}

}  // namespace MarbleEngine
