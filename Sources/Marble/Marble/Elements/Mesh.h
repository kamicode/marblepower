/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Mesh.h
 * \brief Element of type "Mesh". Use MotionState to manage physics
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_MESH_H_
#define MARBLE_ELEMENTS_MESH_H_

#include "Marble/Elements/Element.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Mesh
 * \brief
 *
 */
class Mesh: public Element {
    Q_OBJECT

    public:
        explicit Mesh(QString name);
        virtual ~Mesh();

        virtual bool create(irr::core::vector3df pos, irr::core::vector3df rot,
                            irr::core::vector3df scale, QString mesh,
                            QString type = "None",
                            irr::core::vector3df size = irr::core::vector3df(),
                            QString material = "", float masse = 0);

        virtual bool loadBody(irr::core::vector3df size, QString material);

        virtual bool readXML(tinyxml2::XMLElement* elem);
        void animation(tinyxml2::XMLElement* elem);
        void physic(tinyxml2::XMLElement* elem);
        void shadow(tinyxml2::XMLElement* elem);
        bool loadMesh(irr::core::vector3df size, irr::core::vector3df pos,
                      irr::core::vector3df rot, irr::io::path ficMesh);
        void setMaterialPhys(QString material);
        irr::scene::IMesh* getMesh() const;
        irr::scene::ISceneNode* getNode() const;
        btRigidBody* getRigidBody() const;
        btCollisionShape* getCollisionShape() const;
        void addShadow(const irr::scene::IMesh* shadowMesh = 0,
                       bool zfailmethod = true, irr::f32 infinity = 10000.0f);
        QString getMaterialPhys() const;
        void setPosition(const irr::core::vector3df &vect, bool updatePhysic);
        void setRotation(const irr::core::vector3df &vect, bool updatePhysic);
        void addImpulse(const irr::core::vector3df &pointDeltaVeloc,
                        const irr::core::vector3df &pointPosit);
        int nbCollisions(Element *obj);

    public:
        Q_INVOKABLE virtual QString getTypeName() { return "Mesh"; }
        Q_INVOKABLE QScriptValue getPosition();
        Q_INVOKABLE void setPosition(const QScriptValue &pos);
        Q_INVOKABLE QScriptValue getRotation();
        Q_INVOKABLE void setRotation(const QScriptValue &pos);
        Q_INVOKABLE QScriptValue getScale();
        Q_INVOKABLE void setScale(const QScriptValue &scale);
        Q_INVOKABLE void setPhysicScale(const QScriptValue &scale);
        Q_INVOKABLE void setTarget(const QScriptValue &target);
        Q_INVOKABLE void addForce(float x, float y, float z);
        Q_INVOKABLE void addForce(const QScriptValue &pos);
        Q_INVOKABLE void addTorque(float x, float y, float z);
        Q_INVOKABLE void addTorque(const QScriptValue &pos);
        Q_INVOKABLE void addImpulse(float pointDeltaVelocX,
                                    float pointDeltaVelocY,
                                    float pointDeltaVelocZ,
                                    float pointPositX,
                                    float pointPositY,
                                    float pointPositZ);
        Q_INVOKABLE void addImpulse(const QScriptValue &pointDeltaVeloc,
                                    const QScriptValue &pointPosit);
        Q_INVOKABLE float getMasse() const;
        Q_INVOKABLE void setMasse(float masse);
        Q_INVOKABLE int nbCollisions(QString name);
        Q_INVOKABLE bool isInCollisionWithMap();
        Q_INVOKABLE void showPhysic(bool value);
        Q_INVOKABLE void setAnimationSpeed(int speed);
        Q_INVOKABLE void setAnimationFrameLoop(int begin, int end);
        Q_INVOKABLE void setAnimationCurrentFrame(int current);
        Q_INVOKABLE void setAnimationLoopMode(bool loop);
        Q_INVOKABLE void setShadow(QString mesh, bool zfailmethod,
                                   float infinity);
        Q_INVOKABLE void setMaterial3D(int num, bool wireframe, bool pointCloud,
                                       bool gouraudShading, bool lighting,
                                       bool zwriteEnable, bool backfaceCulling,
                                       bool frontfaceCulling, bool fogEnable,
                                       bool normalizeNormals, float shininess,
                                       QString materialTypeParam,
                                       QString materialTypeParam2,
                                       QString materialType, QString zBuffer,
                                       QString antiAliasing,
                                       QString colorMask, QString colorMaterial,
                                       QString texture,
                                       int ambiantAlpha, int ambiantRed,
                                       int ambiantGreen,
                                       int ambiantBlue, int diffuseAlpha,
                                       int diffuseRed,
                                       int diffuseGreen, int diffuseBlue,
                                       int emissiveAlpha,
                                       int emissiveRed, int emissiveGreen,
                                       int emissiveBlue,
                                       int specularAlpha, int specularRed,
                                       int specularGreen,
                                       int specularBlue);

    protected:
        irr::scene::ISceneNode* node;
        btCollisionShape* m_collisionShape;
        btMotionState* m_motionState;
        btRigidBody* m_rigidBody;

    private:
        irr::scene::IMesh* mesh;
        irr::core::matrix4 mat;
        TypeBody typeBody;
        irr::core::vector3df forces;
        irr::core::vector3df torques;
        float masse;
        QString materialPhys;

    private:
        DISALLOW_COPY_AND_ASSIGN(Mesh);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_MESH_H_
