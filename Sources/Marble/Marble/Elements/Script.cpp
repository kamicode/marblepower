/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Script.h"
#include "Marble/Elements/Element.h"
#include "Marble/Engines/Engine.h"
#include "Marble/Engines/EventEngine.h"
#include "Marble/Engines/GuiEngine.h"
#include "Marble/Engines/SoundEngine.h"
#include "Marble/Engines/Logger.h"
#include "Marble/Engines/GraphicsEngine.h"

namespace MarbleEngine {

Script::Script(Element* s) {
    element = s;
    scriptEngine = new QScriptEngine();

    scriptFile = "";
    content = "";
}

Script::~Script() {
    delete scriptEngine;
    scriptEngine = NULL;
}

bool Script::init() {
    if (content != "") {
        Engine::setScriptEngine(scriptEngine);

        QScriptValue global = scriptEngine->globalObject();
        global.setProperty("self",
                           scriptEngine->newQObject(element));
        global.setProperty("engine",
                           scriptEngine->newQObject(Engine::getInstance()));
        global.setProperty("eventEngine",
                           scriptEngine->newQObject(Engine::getEventEngine()));
        global.setProperty("guiEngine",
                           scriptEngine->newQObject(Engine::getCeguiEngine()));
        global.setProperty("soundEngine",
                           scriptEngine->newQObject(Engine::getSoundEngine()));

        scriptEngine->evaluate(content, scriptFile);
        if (scriptEngine->hasUncaughtException()) {
#ifdef DEBUG
            QString erreurMessage = QString("Element Id : ")
                    + element->getName()
                    + QString("") + QString("\nFile : ") + scriptFile
                    + QString("\n") + QString("Line : ")
                    + QString::number(
                        scriptEngine->uncaughtExceptionLineNumber())
                    + QString("\n")
                    + scriptEngine->uncaughtException().toString();
            LOGGER_WRITE(LOG_Error, erreurMessage.toUtf8().data());
#endif
            return false;
        }
        return true;
    }

    return false;
}

void Script::update(float time) {
    if (scriptFile != "") {
        Engine::setScriptEngine(scriptEngine);
        scriptEngine->evaluate(QString("update("
                                       + QString::number(time) + ");"));
    }
}

void Script::execute(QString code) {
    if (scriptFile != "") {
        Engine::setScriptEngine(scriptEngine);
        scriptEngine->evaluate(code);
    }
}

bool Script::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("script")) {
        scriptFile = Engine::getDirMedia();
        scriptFile += elem->Attribute("script");

        irr::IrrlichtDevice* device = Engine::getGraphicsEngine()->getDevice();
        irr::io::IFileSystem* fileSys = device->getFileSystem();
        irr::io::IReadFile* file = fileSys->createAndOpenFile(
                    scriptFile.toUtf8().data());
        char* buffer = new char[10000000];
        int size = file->read(buffer, 10000000);
        buffer[size] = '\0';
        content = buffer;
        delete [] buffer;
        file->drop();
    }
    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Variable") {
            replaceVariable(elem);
        }
        elem = elem->NextSiblingElement();
    }

    return true;
}

void Script::replaceVariable(tinyxml2::XMLElement* elem) {
    QRegExp exp("var\\s*" + QString(elem->Attribute("name"))
                 + "\\s*=\\s*(\\$\\$)\\s*;");
    if (content.contains(exp)) {
        QString value = elem->Attribute("value");
        if (QString(elem->Attribute("type")) == "String") {
            value = QString("\"") + value + QString("\"");
        }
        content.replace(exp, "var " +  QString(elem->Attribute("name"))
                        + " = " + value + ";");
    }
}

QScriptEngine* Script::getScriptEngine() {
    return scriptEngine;
}

}  // namespace MarbleEngine
