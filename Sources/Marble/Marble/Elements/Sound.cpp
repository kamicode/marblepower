/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Sound.h"

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/SoundEngine.h"
#include "Marble/Elements/Script.h"

namespace MarbleEngine {

Sound::Sound(QString name) : Element(name) {
    mSource = 0;
    file = "";
    pitch = 0;
    gain = 0;
    pos[0] = 0;
    pos[1] = 0;
    pos[2] = 0;
    velocity[0] = 0;
    velocity[1] = 0;
    velocity[2] = 0;
    loop = false;
}

Sound::~Sound() {
}

bool Sound::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("file")) {
        file = elem->Attribute("file");
    }
    if (elem->Attribute("pitch")) {
        pitch = elem->FloatAttribute("pitch");
    }
    if (elem->Attribute("gain")) {
        gain = elem->FloatAttribute("gain");
    }
    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        pos[0] = elem->FloatAttribute("posX");
        pos[1] = elem->FloatAttribute("posY");
        pos[2] = elem->FloatAttribute("posZ");
    }
    if (elem->Attribute("velocityX") && elem->Attribute("velocityY")
            && elem->Attribute("velocityZ")) {
        velocity[0] = elem->FloatAttribute("velocityX");
        velocity[1] = elem->FloatAttribute("velocityY");
        velocity[2] = elem->FloatAttribute("velocityZ");
    }
    if (elem->Attribute("loop")) {
        loop = elem->BoolAttribute("loop");
    }

    SoundEngine* soundEngine = Engine::getSoundEngine();
    mSource = soundEngine->playSound(file);
    soundEngine->setSoundProperties(mSource, pitch, gain, pos, velocity, loop);

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            Element::readXML(elem);
        }

        elem = elem->NextSiblingElement();
    }
    return true;
}

QScriptValue Sound::getPosition() {
    QScriptValue ret = Engine::getScriptEngine()->newObject();

    ret.setProperty("X", pos[0]);
    ret.setProperty("Y", pos[1]);
    ret.setProperty("Z", pos[2]);
    return ret;
}

QScriptValue Sound::getVelocity() {
    QScriptValue ret = Engine::getScriptEngine()->newObject();

    ret.setProperty("X", velocity[0]);
    ret.setProperty("Y", velocity[1]);
    ret.setProperty("Z", velocity[2]);
    return ret;
}

float Sound::getGain() {
    return gain;
}

float Sound::getPitch() {
    return pitch;
}

bool Sound::getLoop() {
    return loop;
}

void Sound::setPosition(const QScriptValue &position) {
    pos[0] = position.property("X").toNumber();
    pos[1] = position.property("Y").toNumber();
    pos[2] = position.property("Z").toNumber();
    Engine::getSoundEngine()->setSoundProperties(mSource, pitch, gain, pos,
                                                 velocity, loop);
}

void Sound::setVelocity(const QScriptValue &vel) {
    velocity[0] = vel.property("X").toNumber();
    velocity[1] = vel.property("Y").toNumber();
    velocity[2] = vel.property("Z").toNumber();
    Engine::getSoundEngine()->setSoundProperties(mSource, pitch, gain, pos,
                                                 velocity, loop);
}

void Sound::setGain(float value) {
    gain = value;
    Engine::getSoundEngine()->setSoundProperties(mSource, pitch, gain, pos,
                                                 velocity, loop);
}

void Sound::setPitch(float value) {
    pitch = value;
    Engine::getSoundEngine()->setSoundProperties(mSource, pitch, gain, pos,
                                                 velocity, loop);
}

void Sound::setLoop(bool value) {
    loop = value;
    Engine::getSoundEngine()->setSoundProperties(mSource, pitch, gain, pos,
                                                 velocity, loop);
}

}  // namespace MarbleEngine
