/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Video2D.h
 * \brief Element of type "Video2D"
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ELEMENTS_VIDEO2D_H_
#define MARBLE_ELEMENTS_VIDEO2D_H_

#include "Marble/Elements/Element.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Video2D
 * \brief
 *
 */
class Video2D: public Element {
    Q_OBJECT

    public:
        explicit Video2D(QString name);
        virtual ~Video2D();
        void render();
        bool create(irr::core::vector2df position,
                    irr::core::dimension2du scale,
                    QString file, bool repeatPlayback,
                    bool startPlayback, bool preloadIntoRAM);

        bool readXML(tinyxml2::XMLElement* elem);

    public:
        Q_INVOKABLE QString getTypeName() { return "Video2D"; }

    private:
        irr::video::ITexture* texture;
        irr::core::position2d<irr::s32> pos;
        irr::core::dimension2du scale;
        QString file;
        bool repeatPlayback;
        bool startPlayback;
        bool preloadIntoRAM;

    private:
        DISALLOW_COPY_AND_ASSIGN(Video2D);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ELEMENTS_VIDEO2D_H_
