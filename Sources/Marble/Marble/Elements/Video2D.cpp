/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Elements/Video2D.h"

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Engines/VideoEngine.h"

namespace MarbleEngine {

Video2D::Video2D(QString name) : Element(name) {
    texture = NULL;
    pos = irr::core::position2d<irr::s32>();
    scale = irr::core::dimension2du();
    file = "";
    repeatPlayback = false;
    startPlayback = false;
    preloadIntoRAM = false;
}

Video2D::~Video2D() {
}

bool Video2D::create(irr::core::vector2df position,
                     irr::core::dimension2du scale,
                     QString file, bool repeatPlayback, bool startPlayback,
                     bool preloadIntoRAM) {
    Element::init();


    irr::video::IVideoDriver* driver = Engine::getGraphicsEngine()->getDriver();
    irr::core::dimension2du screenSize = driver->getScreenSize();

    this->pos.X = position.X * screenSize.Width;
    this->pos.Y = position.Y * screenSize.Height;

    this->scale.Width = scale.Width * screenSize.Width;
    this->scale.Height = scale.Height * screenSize.Height;

    this->file = Engine::getDirMedia() + file.toUtf8().data();

    this->repeatPlayback = repeatPlayback;

    this->startPlayback = startPlayback;

    this->preloadIntoRAM = preloadIntoRAM;

    this->texture =
            Engine::getVideoEngine()->addVideoClip(this->file.toUtf8().data(),
                                                   name.toUtf8().data(),
                                                   this->scale,
                                                   this->repeatPlayback,
                                                   this->startPlayback,
                                                   this->preloadIntoRAM);

    return true;
}

bool Video2D::readXML(tinyxml2::XMLElement* elem) {
    irr::video::IVideoDriver* driver = Engine::getGraphicsEngine()->getDriver();
    irr::core::dimension2du screenSize = driver->getScreenSize();

    if (elem->Attribute("posX") && elem->Attribute("posY")) {
        pos.X = elem->FloatAttribute("posX") * screenSize.Width;
        pos.Y = elem->FloatAttribute("posY") * screenSize.Height;
    }
    if (elem->Attribute("scaleX") && elem->Attribute("scaleY")) {
        scale.Width = elem->FloatAttribute("scaleX") * screenSize.Width;
        scale.Height = elem->FloatAttribute("scaleY") * screenSize.Height;
    }
    if (elem->Attribute("file")) {
        file = Engine::getDirMedia() + elem->Attribute("file");
    }
    if (elem->Attribute("repeatPlayback")) {
        repeatPlayback = elem->BoolAttribute("repeatPlayback");
    }
    if (elem->Attribute("startPlayback")) {
        startPlayback = elem->BoolAttribute("startPlayback");
    }
    if (elem->Attribute("preloadIntoRAM")) {
        preloadIntoRAM = elem->BoolAttribute("preloadIntoRAM");
    }

    texture = Engine::getVideoEngine()->addVideoClip(file.toUtf8().data(),
                                                     name.toUtf8().data(),
                                                     scale, repeatPlayback,
                                                     startPlayback,
                                                     preloadIntoRAM);

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            Element::readXML(elem);
        }

        elem = elem->NextSiblingElement();
    }

    return true;
}

void Video2D::render() {
    if (texture) {
        Engine::getGraphicsEngine()->getDriver()->draw2DImage(texture, pos);
    }
}

}  // namespace MarbleEngine
