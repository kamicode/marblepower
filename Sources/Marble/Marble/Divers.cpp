/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Divers.h"

namespace MarbleEngine {

IrrlichtDebugDrawer::IrrlichtDebugDrawer(irr::video::IVideoDriver* driver)
    : m_driver(driver), m_debugMode(0) {
}

void IrrlichtDebugDrawer::drawLine(const btVector3& from,
                                   const btVector3& to,
                                   const btVector3& color) {
    irr::video::SColor newColor(255,
                                (irr::u32)color[0] * 255.0,
                                (irr::u32)color[1] * 255.0,
                                (irr::u32)color[2] * 255.0);
    m_driver->draw3DLine(irr::core::vector3df(from[0], from[1], from[2]),
                         irr::core::vector3df(to[0], to[1], to[2]),
                         newColor);
}

void IrrlichtDebugDrawer::drawContactPoint(const btVector3& PointOnB,
                                           const btVector3& normalOnB,
                                           btScalar distance,
                                           int /*lifeTime*/,
                                           const btVector3& color) {
    irr::video::SColor newColor(255, (irr::u32)color[0] * 255.0,
                                (irr::u32)color[1] * 255.0,
                                (irr::u32)color[2] * 255.0);

    const btVector3 to(PointOnB + normalOnB * distance);
    m_driver->draw3DLine(irr::core::vector3df(PointOnB[0],
                         PointOnB[1], PointOnB[2]),
                         irr::core::vector3df(to[0], to[1], to[2]),
                         newColor);
}

void IrrlichtDebugDrawer::drawTriangle(const btVector3& v0,
                                       const btVector3& v1,
                                       const btVector3& v2,
                                       const btVector3& color,
                                       btScalar alpha) {
    irr::core::vector3df line0(v0[0], v0[1], v0[2]);
    irr::core::vector3df line1(v1[0], v1[1], v1[2]);
    irr::core::vector3df line2(v2[0], v2[1], v2[2]);
    irr::video::SColor irrcolor(alpha * 255, color[0] * 255,
                                color[1] * 255, color[2] * 255);
    m_driver->draw3DTriangle(irr::core::triangle3df(line0,
                                                    line1,
                                                    line2),
                                                    irrcolor);
}

void IrrlichtDebugDrawer::drawSphere(btScalar radius,
                                     const btTransform& transform,
                                     const btVector3& color) {
    irr::video::SColor irrcolor(255, color[0] * 255,
                                color[1] * 255,
                                color[2] * 255);

    btVector3 buf = transform.getOrigin();
    irr::core::vector3df centre(buf[0], buf[1], buf[2]);

    irr::core::vector3df oldPos, pos;

    float angleStep = 0.1f * irr::core::PI;
    float precision = 0.2f;
    float PI2 = irr::core::PI * 2;

    for (float theta = 0; theta < PI2; theta += angleStep) {
        float cosTheta = cos(theta);
        float sinTheta = sin(theta);
        oldPos = irr::core::vector3df(0, 0, radius) + centre;
        for (float phy = 0; phy < PI2; phy += precision) {
            float x = radius * cosTheta * sin(phy);
            float y = radius * sinTheta * sin(phy);
            float z = radius * cos(phy);

            pos = irr::core::vector3df(x, y, z) + centre;
            m_driver->draw3DLine(oldPos, pos, irrcolor);
            oldPos = pos;
        }
        pos = irr::core::vector3df(0, 0, radius) + centre;
        m_driver->draw3DLine(oldPos, pos, irrcolor);
        oldPos = pos;
    }

    for (float phy = angleStep; phy < PI2; phy += angleStep) {
        float cosPhy = cos(phy);
        float sinPhy = sin(phy);
        oldPos = irr::core::vector3df(radius * sinPhy, 0, radius * cosPhy);
        oldPos += centre;
        for (float theta = 0; theta < PI2; theta += precision) {
            float x = radius * cos(theta) * sinPhy;
            float y = radius * sin(theta) * sinPhy;
            float z = radius * cosPhy;

            pos = irr::core::vector3df(x, y, z) + centre;
            m_driver->draw3DLine(oldPos, pos, irrcolor);
            oldPos = pos;
        }
        pos = irr::core::vector3df(radius * sinPhy, 0, radius * cosPhy);
        pos += centre;
        m_driver->draw3DLine(oldPos, pos, irrcolor);
        oldPos = pos;
    }
}

void IrrlichtDebugDrawer::setDebugMode(int debugMode) {
    m_debugMode = debugMode;
}

int IrrlichtDebugDrawer::getDebugMode() const {
    return m_debugMode;
}

MotionState::MotionState(const btTransform& initialpos,
                         irr::scene::ISceneNode* node) {
    m_pos = initialpos;
    m_node = node;
}

void MotionState::setWorldTransform(const btTransform &worldTrans) {
    btVector3 rot = Convert::QuaternionToEuler(worldTrans.getRotation());
    m_node->setRotation(irr::core::vector3df(rot[0], rot[1], rot[2]));

    btVector3 pos = worldTrans.getOrigin();
    m_node->setPosition(irr::core::vector3df(pos.x(), pos.y(), pos.z()));

    m_pos = worldTrans;
}

void MotionState::getWorldTransform(btTransform &worldTrans) const {
    worldTrans = m_pos;
}

namespace Convert {

irr::video::E_DRIVER_TYPE stringDriver(QString s) {
    if (s == "EDT_NULL")
        return irr::video::EDT_NULL;
    if (s == "EDT_SOFTWARE")
        return irr::video::EDT_SOFTWARE;
    if (s == "EDT_BURNINGSVIDEO")
        return irr::video::EDT_BURNINGSVIDEO;
    if (s == "EDT_DIRECT3D8")
        return irr::video::EDT_DIRECT3D8;
    if (s == "EDT_DIRECT3D9")
        return irr::video::EDT_DIRECT3D9;
    if (s == "EDT_OPENGL")
        return irr::video::EDT_OPENGL;
    if (s == "EDT_COUNT")
        return irr::video::EDT_COUNT;

    return irr::video::EDT_NULL;
}

QString driverString(irr::video::E_DRIVER_TYPE s) {
    if (s == irr::video::EDT_NULL)
        return "EDT_NULL";
    if (s == irr::video::EDT_SOFTWARE)
        return "EDT_SOFTWARE";
    if (s == irr::video::EDT_BURNINGSVIDEO)
        return "EDT_BURNINGSVIDEO";
    if (s == irr::video::EDT_DIRECT3D8)
        return "EDT_DIRECT3D8";
    if (s == irr::video::EDT_DIRECT3D9)
        return "EDT_DIRECT3D9";
    if (s == irr::video::EDT_OPENGL)
        return "EDT_OPENGL";
    if (s == irr::video::EDT_COUNT)
        return "EDT_COUNT";

    return "";
}

TypeBody stringBody(QString s) {
    if (s == "Box")
        return Box;
    if (s == "Sphere")
        return Sphere;
    if (s == "Cone")
        return Cone;
    if (s == "Capsule")
        return Capsule;
    if (s == "Cylinder")
        return Cylinder;
    if (s == "ChamferCylinder")
        return ChamferCylinder;
    if (s == "ConvexHull")
        return ConvexHull;
    if (s == "Compound")
        return Compound;
    if (s == "Tree")
        return Tree;
    if (s == "TerrainTree")
        return TerrainTree;

    return None;
}

QString bodyString(TypeBody s) {
    if (s == Box)
        return "Box";
    if (s == Sphere)
        return "Sphere";
    if (s == Cone)
        return "Cone";
    if (s == Capsule)
        return "Capsule";
    if (s == Cylinder)
        return "Cylinder";
    if (s == ChamferCylinder)
        return "ChamferCylinder";
    if (s == ConvexHull)
        return "ConvexHull";
    if (s == Compound)
        return "Compound";
    if (s == Tree)
        return "Tree";
    if (s == TerrainTree)
        return "TerrainTree";

    return "None";
}

bool stringBool(QString s) {
    if (s == "true" || s == "True" || s == "TRUE")
        return true;

    return false;
}

QString boolString(bool s) {
    if (s)
        return "true";

    return "false";
}

irr::video::E_MATERIAL_TYPE stringMaterialType(QString s) {
    if (s == "EMT_SOLID")
        return irr::video::EMT_SOLID;
    if (s == "EMT_SOLID_2_LAYER")
        return irr::video::EMT_SOLID_2_LAYER;
    if (s == "EMT_LIGHTMAP")
        return irr::video::EMT_LIGHTMAP;
    if (s == "EMT_LIGHTMAP_ADD")
        return irr::video::EMT_LIGHTMAP_ADD;
    if (s == "EMT_LIGHTMAP_M2")
        return irr::video::EMT_LIGHTMAP_M2;
    if (s == "EMT_LIGHTMAP_M4")
        return irr::video::EMT_LIGHTMAP_M4;
    if (s == "EMT_LIGHTMAP_LIGHTING")
        return irr::video::EMT_LIGHTMAP_LIGHTING;
    if (s == "EMT_LIGHTMAP_LIGHTING_M2")
        return irr::video::EMT_LIGHTMAP_LIGHTING_M2;
    if (s == "EMT_LIGHTMAP_LIGHTING_M4")
        return irr::video::EMT_LIGHTMAP_LIGHTING_M4;
    if (s == "EMT_DETAIL_MAP")
        return irr::video::EMT_DETAIL_MAP;
    if (s == "EMT_SPHERE_MAP")
        return irr::video::EMT_SPHERE_MAP;
    if (s == "EMT_REFLECTION_2_LAYER")
        return irr::video::EMT_REFLECTION_2_LAYER;
    if (s == "EMT_TRANSPARENT_ADD_COLOR")
        return irr::video::EMT_TRANSPARENT_ADD_COLOR;
    if (s == "EMT_TRANSPARENT_ALPHA_CHANNEL")
        return irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL;
    if (s == "EMT_TRANSPARENT_ALPHA_CHANNEL_REF")
        return irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF;
    if (s == "EMT_TRANSPARENT_VERTEX_ALPHA")
        return irr::video::EMT_TRANSPARENT_VERTEX_ALPHA;
    if (s == "EMT_TRANSPARENT_REFLECTION_2_LAYER")
        return irr::video::EMT_TRANSPARENT_REFLECTION_2_LAYER;
    if (s == "EMT_NORMAL_MAP_SOLID")
        return irr::video::EMT_NORMAL_MAP_SOLID;
    if (s == "EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR")
        return irr::video::EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR;
    if (s == "EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA")
        return irr::video::EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA;
    if (s == "EMT_PARALLAX_MAP_SOLID")
        return irr::video::EMT_PARALLAX_MAP_SOLID;
    if (s == "EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR")
        return irr::video::EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR;
    if (s == "EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA")
        return irr::video::EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA;
    if (s == "EMT_ONETEXTURE_BLEND")
        return irr::video::EMT_ONETEXTURE_BLEND;
    if (s == "EMT_FORCE_32BIT")
        return irr::video::EMT_FORCE_32BIT;

    return irr::video::EMT_SOLID;
}

irr::video::E_COMPARISON_FUNC stringZBuffer(QString s) {
    if (s == "ECFN_NEVER")
        return irr::video::ECFN_NEVER;
    if (s == "ECFN_LESSEQUAL")
        return irr::video::ECFN_LESSEQUAL;
    if (s == "ECFN_EQUAL")
        return irr::video::ECFN_EQUAL;
    if (s == "ECFN_LESS")
        return irr::video::ECFN_LESS;
    if (s == "ECFN_NOTEQUAL")
        return irr::video::ECFN_NOTEQUAL;
    if (s == "ECFN_GREATEREQUAL")
        return irr::video::ECFN_GREATEREQUAL;
    if (s == "ECFN_GREATER")
        return irr::video::ECFN_GREATER;
    if (s == "ECFN_ALWAYS")
        return irr::video::ECFN_ALWAYS;

    return irr::video::ECFN_NEVER;
}

irr::video::E_ANTI_ALIASING_MODE stringAntiAliasing(QString s) {
    if (s == "EAAM_OFF")
        return irr::video::EAAM_OFF;
    if (s == "EAAM_SIMPLE")
        return irr::video::EAAM_SIMPLE;
    if (s == "EAAM_QUALITY")
        return irr::video::EAAM_QUALITY;
    if (s == "EAAM_LINE_SMOOTH")
        return irr::video::EAAM_LINE_SMOOTH;
    if (s == "EAAM_POINT_SMOOTH")
        return irr::video::EAAM_POINT_SMOOTH;
    if (s == "EAAM_FULL_BASIC")
        return irr::video::EAAM_FULL_BASIC;
    if (s == "EAAM_ALPHA_TO_COVERAGE")
        return irr::video::EAAM_ALPHA_TO_COVERAGE;

    return irr::video::EAAM_OFF;
}

irr::video::E_COLOR_PLANE stringColorMask(QString s) {
    if (s == "ECP_NONE")
        return irr::video::ECP_NONE;
    if (s == "ECP_ALPHA")
        return irr::video::ECP_ALPHA;
    if (s == "ECP_RED")
        return irr::video::ECP_RED;
    if (s == "ECP_GREEN")
        return irr::video::ECP_GREEN;
    if (s == "ECP_BLUE")
        return irr::video::ECP_BLUE;
    if (s == "ECP_RGB")
        return irr::video::ECP_RGB;
    if (s == "ECP_ALL")
        return irr::video::ECP_ALL;

    return irr::video::ECP_NONE;
}

irr::video::E_COLOR_MATERIAL stringColorMaterial(QString s) {
    if (s == "ECM_NONE")
        return irr::video::ECM_NONE;
    if (s == "ECM_DIFFUSE")
        return irr::video::ECM_DIFFUSE;
    if (s == "ECM_AMBIENT")
        return irr::video::ECM_AMBIENT;
    if (s == "ECM_EMISSIVE")
        return irr::video::ECM_EMISSIVE;
    if (s == "ECM_SPECULAR")
        return irr::video::ECM_SPECULAR;
    if (s == "ECM_DIFFUSE_AND_AMBIENT")
        return irr::video::ECM_DIFFUSE_AND_AMBIENT;

    return irr::video::ECM_NONE;
}

irr::video::E_LIGHT_TYPE stringLightType(QString s) {
    if (s == "ELT_POINT")
        return irr::video::ELT_POINT;
    if (s == "ELT_SPOT")
        return irr::video::ELT_SPOT;
    if (s == "ELT_DIRECTIONAL")
        return irr::video::ELT_DIRECTIONAL;

    return irr::video::ELT_POINT;
}

irr::video::E_FOG_TYPE stringFogType(QString s) {
    if (s == "EFT_FOG_EXP")
        return irr::video::EFT_FOG_EXP;
    if (s == "EFT_FOG_LINEAR")
        return irr::video::EFT_FOG_LINEAR;
    if (s == "EFT_FOG_EXP2")
        return irr::video::EFT_FOG_EXP2;

    return irr::video::EFT_FOG_LINEAR;
}

irr::EKEY_CODE stringKeyCode(QString s) {
    if (s == "KEY_LBUTTON") {
        return irr::KEY_LBUTTON;
    }
    if (s == "KEY_RBUTTON") {
        return irr::KEY_RBUTTON;
    }
    if (s == "KEY_CANCEL") {
        return irr::KEY_CANCEL;
    }
    if (s == "KEY_MBUTTON") {
        return irr::KEY_MBUTTON;
    }
    if (s == "KEY_XBUTTON1") {
        return irr::KEY_XBUTTON1;
    }
    if (s == "KEY_XBUTTON2") {
        return irr::KEY_XBUTTON2;
    }
    if (s == "KEY_BACK") {
        return irr::KEY_BACK;
    }
    if (s == "KEY_TAB") {
        return irr::KEY_TAB;
    }
    if (s == "KEY_CLEAR") {
        return irr::KEY_CLEAR;
    }
    if (s == "KEY_RETURN") {
        return irr::KEY_RETURN;
    }
    if (s == "KEY_SHIFT") {
        return irr::KEY_SHIFT;
    }
    if (s == "KEY_CONTROL") {
        return irr::KEY_CONTROL;
    }
    if (s == "KEY_MENU") {
        return irr::KEY_MENU;
    }
    if (s == "KEY_PAUSE") {
        return irr::KEY_PAUSE;
    }
    if (s == "KEY_CAPITAL") {
        return irr::KEY_CAPITAL;
    }
    if (s == "KEY_KANA") {
        return irr::KEY_KANA;
    }
    if (s == "KEY_HANGUEL") {
        return irr::KEY_HANGUEL;
    }
    if (s == "KEY_HANGUL") {
        return irr::KEY_HANGUL;
    }
    if (s == "KEY_JUNJA") {
        return irr::KEY_JUNJA;
    }
    if (s == "KEY_FINAL") {
        return irr::KEY_FINAL;
    }
    if (s == "KEY_HANJA") {
        return irr::KEY_HANJA;
    }
    if (s == "KEY_KANJI") {
        return irr::KEY_KANJI;
    }
    if (s == "KEY_ESCAPE") {
        return irr::KEY_ESCAPE;
    }
    if (s == "KEY_CONVERT") {
        return irr::KEY_CONVERT;
    }
    if (s == "KEY_NONCONVERT") {
        return irr::KEY_NONCONVERT;
    }
    if (s == "KEY_ACCEPT") {
        return irr::KEY_ACCEPT;
    }
    if (s == "KEY_MODECHANGE") {
        return irr::KEY_MODECHANGE;
    }
    if (s == "KEY_SPACE") {
        return irr::KEY_SPACE;
    }
    if (s == "KEY_PRIOR") {
        return irr::KEY_PRIOR;
    }
    if (s == "KEY_NEXT") {
        return irr::KEY_NEXT;
    }
    if (s == "KEY_END") {
        return irr::KEY_END;
    }
    if (s == "KEY_HOME") {
        return irr::KEY_HOME;
    }
    if (s == "KEY_LEFT") {
        return irr::KEY_LEFT;
    }
    if (s == "KEY_UP") {
        return irr::KEY_UP;
    }
    if (s == "KEY_RIGHT") {
        return irr::KEY_RIGHT;
    }
    if (s == "KEY_DOWN") {
        return irr::KEY_DOWN;
    }
    if (s == "KEY_SELECT") {
        return irr::KEY_SELECT;
    }
    if (s == "KEY_PRINT") {
        return irr::KEY_PRINT;
    }
    if (s == "KEY_EXECUT") {
        return irr::KEY_EXECUT;
    }
    if (s == "KEY_SNAPSHOT") {
        return irr::KEY_SNAPSHOT;
    }
    if (s == "KEY_INSERT") {
        return irr::KEY_INSERT;
    }
    if (s == "KEY_DELETE") {
        return irr::KEY_DELETE;
    }
    if (s == "KEY_HELP") {
        return irr::KEY_HELP;
    }
    if (s == "KEY_KEY_0") {
        return irr::KEY_KEY_0;
    }
    if (s == "KEY_KEY_1") {
        return irr::KEY_KEY_1;
    }
    if (s == "KEY_KEY_2") {
        return irr::KEY_KEY_2;
    }
    if (s == "KEY_KEY_3") {
        return irr::KEY_KEY_3;
    }
    if (s == "KEY_KEY_4") {
        return irr::KEY_KEY_4;
    }
    if (s == "KEY_KEY_5") {
        return irr::KEY_KEY_5;
    }
    if (s == "KEY_KEY_6") {
        return irr::KEY_KEY_6;
    }
    if (s == "KEY_KEY_7") {
        return irr::KEY_KEY_7;
    }
    if (s == "KEY_KEY_8") {
        return irr::KEY_KEY_8;
    }
    if (s == "KEY_KEY_9") {
        return irr::KEY_KEY_9;
    }
    if (s == "KEY_KEY_A") {
        return irr::KEY_KEY_A;
    }
    if (s == "KEY_KEY_B") {
        return irr::KEY_KEY_B;
    }
    if (s == "KEY_KEY_C") {
        return irr::KEY_KEY_C;
    }
    if (s == "KEY_KEY_D") {
        return irr::KEY_KEY_D;
    }
    if (s == "KEY_KEY_E") {
        return irr::KEY_KEY_E;
    }
    if (s == "KEY_KEY_F") {
        return irr::KEY_KEY_F;
    }
    if (s == "KEY_KEY_G") {
        return irr::KEY_KEY_G;
    }
    if (s == "KEY_KEY_H") {
        return irr::KEY_KEY_H;
    }
    if (s == "KEY_KEY_I") {
        return irr::KEY_KEY_I;
    }
    if (s == "KEY_KEY_J") {
        return irr::KEY_KEY_J;
    }
    if (s == "KEY_KEY_K") {
        return irr::KEY_KEY_K;
    }
    if (s == "KEY_KEY_L") {
        return irr::KEY_KEY_L;
    }
    if (s == "KEY_KEY_M") {
        return irr::KEY_KEY_M;
    }
    if (s == "KEY_KEY_N") {
        return irr::KEY_KEY_N;
    }
    if (s == "KEY_KEY_O") {
        return irr::KEY_KEY_O;
    }
    if (s == "KEY_KEY_P") {
        return irr::KEY_KEY_P;
    }
    if (s == "KEY_KEY_Q") {
        return irr::KEY_KEY_Q;
    }
    if (s == "KEY_KEY_R") {
        return irr::KEY_KEY_R;
    }
    if (s == "KEY_KEY_S") {
        return irr::KEY_KEY_S;
    }
    if (s == "KEY_KEY_T") {
        return irr::KEY_KEY_T;
    }
    if (s == "KEY_KEY_U") {
        return irr::KEY_KEY_U;
    }
    if (s == "KEY_KEY_V") {
        return irr::KEY_KEY_V;
    }
    if (s == "KEY_KEY_W") {
        return irr::KEY_KEY_W;
    }
    if (s == "KEY_KEY_X") {
        return irr::KEY_KEY_X;
    }
    if (s == "KEY_KEY_Y") {
        return irr::KEY_KEY_Y;
    }
    if (s == "KEY_KEY_Z") {
        return irr::KEY_KEY_Z;
    }
    if (s == "KEY_LWIN") {
        return irr::KEY_LWIN;
    }
    if (s == "KEY_RWIN") {
        return irr::KEY_RWIN;
    }
    if (s == "KEY_APPS") {
        return irr::KEY_APPS;
    }
    if (s == "KEY_SLEEP") {
        return irr::KEY_SLEEP;
    }
    if (s == "KEY_NUMPAD0") {
        return irr::KEY_NUMPAD0;
    }
    if (s == "KEY_NUMPAD1") {
        return irr::KEY_NUMPAD1;
    }
    if (s == "KEY_NUMPAD2") {
        return irr::KEY_NUMPAD2;
    }
    if (s == "KEY_NUMPAD3") {
        return irr::KEY_NUMPAD3;
    }
    if (s == "KEY_NUMPAD4") {
        return irr::KEY_NUMPAD4;
    }
    if (s == "KEY_NUMPAD5") {
        return irr::KEY_NUMPAD5;
    }
    if (s == "KEY_NUMPAD6") {
        return irr::KEY_NUMPAD6;
    }
    if (s == "KEY_NUMPAD7") {
        return irr::KEY_NUMPAD7;
    }
    if (s == "KEY_NUMPAD8") {
        return irr::KEY_NUMPAD8;
    }
    if (s == "KEY_NUMPAD9") {
        return irr::KEY_NUMPAD9;
    }
    if (s == "KEY_MULTIPLY") {
        return irr::KEY_MULTIPLY;
    }
    if (s == "KEY_ADD") {
        return irr::KEY_ADD;
    }
    if (s == "KEY_SEPARATOR") {
        return irr::KEY_SEPARATOR;
    }
    if (s == "KEY_SUBTRACT") {
        return irr::KEY_SUBTRACT;
    }
    if (s == "KEY_DECIMAL") {
        return irr::KEY_DECIMAL;
    }
    if (s == "KEY_DIVIDE") {
        return irr::KEY_DIVIDE;
    }
    if (s == "KEY_F1") {
        return irr::KEY_F1;
    }
    if (s == "KEY_F2") {
        return irr::KEY_F2;
    }
    if (s == "KEY_F3") {
        return irr::KEY_F3;
    }
    if (s == "KEY_F4") {
        return irr::KEY_F4;
    }
    if (s == "KEY_F5") {
        return irr::KEY_F5;
    }
    if (s == "KEY_F6") {
        return irr::KEY_F6;
    }
    if (s == "KEY_F7") {
        return irr::KEY_F7;
    }
    if (s == "KEY_F8") {
        return irr::KEY_F8;
    }
    if (s == "KEY_F9") {
        return irr::KEY_F9;
    }
    if (s == "KEY_F10") {
        return irr::KEY_F10;
    }
    if (s == "KEY_F11") {
        return irr::KEY_F11;
    }
    if (s == "KEY_F12") {
        return irr::KEY_F12;
    }
    if (s == "KEY_F13") {
        return irr::KEY_F13;
    }
    if (s == "KEY_F14") {
        return irr::KEY_F14;
    }
    if (s == "KEY_F15") {
        return irr::KEY_F15;
    }
    if (s == "KEY_F16") {
        return irr::KEY_F16;
    }
    if (s == "KEY_F17") {
        return irr::KEY_F17;
    }
    if (s == "KEY_F18") {
        return irr::KEY_F18;
    }
    if (s == "KEY_F19") {
        return irr::KEY_F19;
    }
    if (s == "KEY_F20") {
        return irr::KEY_F20;
    }
    if (s == "KEY_F21") {
        return irr::KEY_F21;
    }
    if (s == "KEY_F22") {
        return irr::KEY_F22;
    }
    if (s == "KEY_F23") {
        return irr::KEY_F23;
    }
    if (s == "KEY_F24") {
        return irr::KEY_F24;
    }
    if (s == "KEY_NUMLOCK") {
        return irr::KEY_NUMLOCK;
    }
    if (s == "KEY_SCROLL") {
        return irr::KEY_SCROLL;
    }
    if (s == "KEY_LSHIFT") {
        return irr::KEY_LSHIFT;
    }
    if (s == "KEY_RSHIFT") {
        return irr::KEY_RSHIFT;
    }
    if (s == "KEY_LCONTROL") {
        return irr::KEY_LCONTROL;
    }
    if (s == "KEY_RCONTROL") {
        return irr::KEY_RCONTROL;
    }
    if (s == "KEY_LMENU") {
        return irr::KEY_LMENU;
    }
    if (s == "KEY_RMENU") {
        return irr::KEY_RMENU;
    }
    if (s == "KEY_PLUS") {
        return irr::KEY_PLUS;
    }
    if (s == "KEY_COMMA") {
        return irr::KEY_COMMA;
    }
    if (s == "KEY_MINUS") {
        return irr::KEY_MINUS;
    }
    if (s == "KEY_PERIOD") {
        return irr::KEY_PERIOD;
    }
    if (s == "KEY_ATTN") {
        return irr::KEY_ATTN;
    }
    if (s == "KEY_CRSEL") {
        return irr::KEY_CRSEL;
    }
    if (s == "KEY_EXSEL") {
        return irr::KEY_EXSEL;
    }
    if (s == "KEY_EREOF") {
        return irr::KEY_EREOF;
    }
    if (s == "KEY_PLAY") {
        return irr::KEY_PLAY;
    }
    if (s == "KEY_ZOOM") {
        return irr::KEY_ZOOM;
    }
    if (s == "KEY_PA1") {
        return irr::KEY_PA1;
    }
    if (s == "KEY_OEM_CLEAR") {
        return irr::KEY_OEM_CLEAR;
    }

    return irr::KEY_KEY_CODES_COUNT;
}


Etat stringEtat(QString s) {
    if (s == "launch") {
        return launchState;
    }
    if (s == "changeMap") {
        return changeMapState;
    }
    if (s == "quit") {
        return quitState;
    }
    return noneState;
}

irr::scene::E_TERRAIN_PATCH_SIZE stringTerrainPatch(QString s) {
    if (s == "ETPS_9")
        return irr::scene::ETPS_9;
    if (s == "ETPS_17")
        return irr::scene::ETPS_17;
    if (s == "ETPS_33")
        return irr::scene::ETPS_33;
    if (s == "ETPS_65")
        return irr::scene::ETPS_65;
    if (s == "ETPS_129")
        return irr::scene::ETPS_129;

    return irr::scene::ETPS_17;
}

QString terrainPatchString(irr::scene::E_TERRAIN_PATCH_SIZE s) {
    if (s == irr::scene::ETPS_9)
        return "ETPS_9";
    if (s == irr::scene::ETPS_17)
        return "ETPS_17";
    if (s == irr::scene::ETPS_33)
        return "ETPS_33";
    if (s == irr::scene::ETPS_65)
        return "ETPS_65";
    if (s == irr::scene::ETPS_129)
        return "ETPS_129";
    return "ETPS_17";
}

btQuaternion EulerToQuaternion(const btVector3 &euler) {
    btMatrix3x3 mat;
    mat.setIdentity();
    mat.setEulerZYX(euler.getX(), euler.getY(), euler.getZ());
    btQuaternion quat;
    mat.getRotation(quat);
    return quat;
}

btVector3 QuaternionToEuler(const btQuaternion &TQuat) {
    btScalar W = TQuat.getW();
    btScalar X = TQuat.getX();
    btScalar Y = TQuat.getY();
    btScalar Z = TQuat.getZ();
    float WSquared = W * W;
    float XSquared = X * X;
    float YSquared = Y * Y;
    float ZSquared = Z * Z;

    btVector3 TEuler;
    TEuler.setX(atan2f(2.0f * (Y * Z + X * W),
                       -XSquared - YSquared + ZSquared + WSquared));
    TEuler.setY(asinf(-2.0f * (X * Z - Y * W)));
    TEuler.setZ(atan2f(2.0f * (X * Y + Z * W),
                       XSquared - YSquared - ZSquared + WSquared));
    TEuler *= irr::core::RADTODEG;
    return TEuler;
}

btTriangleMesh* meshToBulletTriangleMesh(
        irr::scene::IMesh* _mesh,
        const irr::core::vector3df& _scaling) {
    btVector3 vertices[3];
    irr::u32 i, j, k, index, numVertices, numIndices;
    irr::u16* mb_indices;
    btTriangleMesh *pTriMesh = new btTriangleMesh();
    for (i = 0; i < _mesh->getMeshBufferCount(); ++i) {
        irr::scene::IMeshBuffer* mb = _mesh->getMeshBuffer(i);
        if (mb->getVertexType() == irr::video::EVT_STANDARD) {
            irr::video::S3DVertex* mb_vert =
                    static_cast<irr::video::S3DVertex*>
                    (mb->getVertices());
            mb_indices = mb->getIndices();
            numVertices = mb->getVertexCount();
            numIndices = mb->getIndexCount();
            for (j = 0; j < numIndices; j += 3) {
                for (k = 0; k < 3; ++k) {
                    index = mb_indices[j + k];
                    vertices[k] = btVector3(
                                mb_vert[index].Pos.X * _scaling.X,
                                mb_vert[index].Pos.Y * _scaling.Y,
                                mb_vert[index].Pos.Z * _scaling.Z);
                }
                pTriMesh->addTriangle(vertices[0],
                                      vertices[1],
                                      vertices[2]);
            }
        } else if (mb->getVertexType() == irr::video::EVT_2TCOORDS) {
            irr::video::S3DVertex2TCoords* mb_vertices =
                    static_cast<irr::video::S3DVertex2TCoords*>
                    (mb->getVertices());
            mb_indices = mb->getIndices();
            numVertices = mb->getVertexCount();
            numIndices = mb->getIndexCount();
            for (j = 0; j < numIndices; j += 3) {
                for (k = 0; k < 3; ++k) {
                    index = mb_indices[j + k];
                    vertices[k] = btVector3(
                                mb_vertices[index].Pos.X * _scaling.X,
                                mb_vertices[index].Pos.Y * _scaling.Y,
                                mb_vertices[index].Pos.Z * _scaling.Z);
                }
                pTriMesh->addTriangle(vertices[0],
                                      vertices[1],
                                      vertices[2]);
            }
        }
    }
    return pTriMesh;
}

btConvexHullShape* meshToBulletConvexHullShape(
        irr::scene::IMesh* mesh,
        const irr::core::vector3df& _scaling) {
    btConvexHullShape* convexHullShape = new btConvexHullShape();
    for (irr::u32 i = 0; i < mesh->getMeshBufferCount(); ++i) {
        irr::scene::IMeshBuffer* currentMB = mesh->getMeshBuffer(i);
        if (currentMB->getVertexType() == irr::video::EVT_STANDARD) {
            irr::video::S3DVertex* tmpVerts =
                    static_cast<irr::video::S3DVertex*>
                    (currentMB->getVertices());
            for (irr::u32 j = 0; j < currentMB->getVertexCount(); ++j) {
                convexHullShape->addPoint(
                            btVector3(tmpVerts[j].Pos.X * _scaling.X,
                                      tmpVerts[j].Pos.Y * _scaling.Y,
                                      tmpVerts[j].Pos.Z * _scaling.Z));
            }
        } else if (currentMB->getVertexType() ==
                 irr::video::EVT_2TCOORDS) {
            irr::video::S3DVertex2TCoords *tmpVerts =
                    static_cast<irr::video::S3DVertex2TCoords*>
                    (currentMB->getVertices());
            for (irr::u32 i = 0; i < currentMB->getVertexCount(); ++i) {
                convexHullShape->addPoint(
                            btVector3(tmpVerts[i].Pos.X * _scaling.X,
                                      tmpVerts[i].Pos.Y * _scaling.Y,
                                      tmpVerts[i].Pos.Z * _scaling.Z));
            }
        }
    }
    return convexHullShape;
}


Priority stringPriority(QString s) {
    if (s == "Error")
        return Error;
    if (s == "Warning")
        return Warning;
    if (s == "Debug")
        return Debug;
    if (s == "Info")
        return Info;
    if (s == "Config")
        return Config;

    return Debug;
}

QString PriorityString(Priority s) {
    if (s == Error)
        return "Error";
    if (s == Warning)
        return "Warning";
    if (s == Debug)
        return "Debug";
    if (s == Info)
        return "Info";
    if (s == Config)
        return "Config";
    return "Debug";
}

float radToDeg(float radian) {
    return 180 * radian / irr::core::PI;
}

float degToRad(float degree) {
    return irr::core::PI * degree / 180;
}

irr::core::vector3df cartesianToSpheric(
        const irr::core::vector3df &cartesian) {
    float rho = sqrt(pow(cartesian.X, 2)
                     + pow(cartesian.Y, 2)
                     + pow(cartesian.Z, 2));
    float phi = acos(cartesian.Z / rho);
    float theta = 0;
    if (cartesian.Y >= 0) {
        theta = acos(cartesian.X /
                     sqrt(pow(cartesian.X, 2) + pow(cartesian.Y, 2)));
    } else {
        theta = irr::core::PI * 2 - acos(cartesian.X /
                                         sqrt(pow(cartesian.X, 2)
                                              + pow(cartesian.Y, 2)));
    }

    return irr::core::vector3df(rho, phi, theta);
}

irr::core::vector3df sphericToCartesian(
        const irr::core::vector3df &spheric) {
    float rho = spheric.X;
    float phi = spheric.Y;
    float theta = spheric.Z;
    irr::core::vector3df res;

    res.X = rho * sin(phi) * cos(theta);
    res.Y = rho * sin(phi) * sin(theta);
    res.Z = rho * cos(phi);

    return res;
}

ALshort float2short(ALfloat f) {
    if (f >  1) {
        f = 1;
    } else if (f < -1) {
        f = -1;
    }
    return (ALshort) (f*32767);
}

}  // namespace Convert

}  // namespace MarbleEngine
