/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Materials/MaterialPhys.h"

#include <map>

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/PhysicsEngine.h"
#include "Marble/Engines/SoundEngine.h"
#include "Marble/Elements/Mesh.h"

namespace MarbleEngine {

MaterialPhys::MaterialPhys() {
}

MaterialPhys::~MaterialPhys() {
}

void MaterialPhys::unLoadMap() {
    for (std::map<QString, Material*>::iterator it = materials.begin();
         it != materials.end(); ++it) {
        delete it->second;
        it->second = NULL;
    }
    materials.clear();
    materialPairs.clear();
}

Material* MaterialPhys::addMaterial(const QString &nomMaterial) {
    if (materials.find(nomMaterial) == materials.end()) {
        Material* mat = new Material();
        materials.insert(std::make_pair(nomMaterial, mat));
    }
    return materials[nomMaterial];
}

Material* MaterialPhys::getMaterial(const QString &nomMaterial) {
    if (materials.find(nomMaterial) != materials.end()) {
        return materials[nomMaterial];
    }
    return NULL;
}

void MaterialPhys::setRestitution(const QString &nomMaterial, float value) {
    if (materials.find(nomMaterial) != materials.end()) {
        materials.at(nomMaterial)->m_restitution = value;
    }
}

void MaterialPhys::setFriction(const QString &nomMaterial, float value) {
    if (materials.find(nomMaterial) != materials.end()) {
        materials.at(nomMaterial)->m_friction = value;
    }
}

void MaterialPhys::setDamping(const QString &nomMaterial, float lin_damping,
                              float ang_damping) {
    if (materials.find(nomMaterial) != materials.end()) {
        materials.at(nomMaterial)->lin_damping = lin_damping;
        materials.at(nomMaterial)->ang_damping = ang_damping;
    }
}

void MaterialPhys::update() {
    btDiscreteDynamicsWorld* world = Engine::getPhysicsEngine()->getWorld();
    if (world) {
        int numManifolds = world->getDispatcher()->getNumManifolds();
        for (int i = 0; i< numManifolds; ++i) {
            btPersistentManifold* contactManifold =
                    world->getDispatcher()->getManifoldByIndexInternal(i);
            int numContacts = contactManifold->getNumContacts();
            for (int j = 0; j < numContacts; ++j) {
                btManifoldPoint point = contactManifold->getContactPoint(j);

                const btCollisionObject* obA =
                        static_cast<const btCollisionObject*>(
                            contactManifold->getBody0());
                Mesh* mA = static_cast<Mesh*> (obA->getUserPointer());

                const btCollisionObject* obB =
                        static_cast<const btCollisionObject*>(
                            contactManifold->getBody1());
                Mesh* mB = static_cast<Mesh*> (obB->getUserPointer());

                if (point.getDistance() < 0.5f && point.getLifeTime() == 1
                        && point.getAppliedImpulse() > 500.f) {
                    QString matPair = mA->getMaterialPhys() + "/"
                            + mB->getMaterialPhys();
                    if (materialPairs.find(matPair) != materialPairs.end()) {
                        int source =
                                Engine::getSoundEngine()->playSound(
                                    materialPairs.at(matPair));
                        btVector3 pos = obB->getWorldTransform().getOrigin();
                        float p[3] = { pos.getX(), pos.getY(), pos.getZ() };
                        Engine::getSoundEngine()->setSoundPos(source, p);
                        float gain = point.getAppliedImpulse();
                        if (gain > 5000) {
                            gain = 5000;
                        }
                        Engine::getSoundEngine()->setSoundGain(
                                    source, gain/5000.f);
                    }
                }
            }
        }
    }
}

void MaterialPhys::readMaterialXML(tinyxml2::XMLElement* elem) {
    Material* mat = addMaterial(elem->Attribute("name"));
    mat->m_friction = elem->FloatAttribute("friction");
    mat->m_restitution = elem->FloatAttribute("restitution");
    mat->lin_damping = elem->FloatAttribute("lin_damping");
    mat->ang_damping = elem->FloatAttribute("ang_damping");
}

void MaterialPhys::readConfigMaterialXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("mat1") && elem->Attribute("mat2")) {
        QString mat1 = elem->Attribute("mat1");
        QString mat2 = elem->Attribute("mat2");
        materialPairs.insert(std::make_pair(mat1 + "/" + mat2,
                                            elem->Attribute("sound")));
    }
}

}  // namespace MarbleEngine
