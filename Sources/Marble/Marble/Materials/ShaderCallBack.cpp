/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Materials/ShaderCallBack.h"

#include <vector>

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Engines/EventEngine.h"
#include "Marble/Engines/GuiEngine.h"
#include "Marble/Engines/SoundEngine.h"
#include "Marble/Elements/Element.h"
#include "Marble/Engines/Logger.h"

namespace MarbleEngine {

ShaderCallBack::ShaderCallBack(QString name, QString materialType) {
    scriptEngine = new QScriptEngine();
    mName = name;

    irr::video::IVideoDriver* driver = Engine::getGraphicsEngine()->getDriver();

    QString shaderFile = Engine::getDirMedia() + mName + "/shader.cg";
    QString vsFileName = shaderFile;
    QString psFileName = shaderFile;

    if (!driver->queryFeature(irr::video::EVDF_PIXEL_SHADER_1_1) &&
            !driver->queryFeature(irr::video::EVDF_ARB_FRAGMENT_PROGRAM_1)) {
        psFileName = "";
    }

    if (!driver->queryFeature(irr::video::EVDF_VERTEX_SHADER_1_1) &&
            !driver->queryFeature(irr::video::EVDF_ARB_VERTEX_PROGRAM_1)) {
        vsFileName = "";
    }

    irr::video::IGPUProgrammingServices* gpu =
            driver->getGPUProgrammingServices();
    shaderID = gpu->addHighLevelShaderMaterialFromFiles(
                vsFileName.toUtf8().data(), "VertexShaderFunction",
                irr::video::EVST_VS_1_1,
                psFileName.toUtf8().data(), "PixelShaderFunction",
                irr::video::EPST_PS_1_1,
                this, Convert::stringMaterialType(materialType), 0,
                irr::video::EGSL_CG);
}

ShaderCallBack::~ShaderCallBack() {
    delete scriptEngine;
    scriptEngine = NULL;
}

bool ShaderCallBack::init() {
    QScriptEngine* buf = Engine::getScriptEngine();
    Engine::setScriptEngine(scriptEngine);

    QScriptValue global = scriptEngine->globalObject();
    global.setProperty("engine",
                       scriptEngine->newQObject(Engine::getInstance()));
    global.setProperty("eventEngine",
                       scriptEngine->newQObject(Engine::getEventEngine()));
    global.setProperty("guiEngine",
                       scriptEngine->newQObject(Engine::getCeguiEngine()));
    global.setProperty("soundEngine",
                       scriptEngine->newQObject(Engine::getSoundEngine()));

    QString shaderDir = Engine::getDirMedia() + mName + "/";

    script = shaderDir + QString("Constants.shad");

    if (!QFileInfo(script).isFile()) {
        return false;
    }

    irr::IrrlichtDevice* device = Engine::getGraphicsEngine()->getDevice();
    irr::io::IFileSystem* fileSys = device->getFileSystem();
    irr::io::IReadFile* file =
            fileSys->createAndOpenFile(script.toUtf8().data());
    char* buffer = new char[10000000];
    int size = file->read(buffer, 10000000);
    buffer[size] = '\0';
    QByteArray content = buffer;
    delete [] buffer;
    file->drop();

    QScriptValue result = scriptEngine->evaluate(content, script);
    if (scriptEngine->hasUncaughtException()) {
#ifdef DEBUG
        QString erreurMessage =
                QString("File : ") + script + QString("\n") +
                QString("Line : ") +
                QString::number(scriptEngine->uncaughtExceptionLineNumber()) +
                QString("\n") + result.toString();
        LOGGER_WRITE(LOG_Error, erreurMessage.toUtf8().data());
#endif

        Engine::setScriptEngine(buf);
        return false;
    }
    Engine::setScriptEngine(buf);
    return true;
}

int ShaderCallBack::getShaderID() {
    return shaderID;
}

void ShaderCallBack::OnSetConstants(
        irr::video::IMaterialRendererServices* services ,
        irr::s32 /*userData*/) {
    irr::video::IVideoDriver* driver = Engine::getGraphicsEngine()->getDriver();

    irr::core::matrix4 world = driver->getTransform(irr::video::ETS_WORLD);
    irr::core::matrix4 projection =
            driver->getTransform(irr::video::ETS_PROJECTION);
    irr::core::matrix4 view = driver->getTransform(irr::video::ETS_VIEW);

    services->setVertexShaderConstant(
                services->getVertexShaderConstantID("World"),
                world.pointer(), 16);
    services->setVertexShaderConstant(
                services->getVertexShaderConstantID("Projection"),
                projection.pointer(), 16);
    services->setVertexShaderConstant(
                services->getVertexShaderConstantID("View"),
                view.pointer(), 16);

    irr::core::matrix4 transWorld = world.getTransposed();
    services->setVertexShaderConstant(
                services->getVertexShaderConstantID("TransWorld"),
                transWorld.pointer(), 16);

    irr::core::matrix4 invWorld = world;
    invWorld.makeInverse();
    services->setVertexShaderConstant(
                services->getVertexShaderConstantID("InvWorld"),
                invWorld.pointer(), 16);

    irr::core::matrix4 worldViewProj = projection * view * world;
    services->setVertexShaderConstant(
                services->getVertexShaderConstantID("WorldViewProj"),
                worldViewProj.pointer(), 16);

    if (script != "") {
        QScriptEngine* buf = Engine::getScriptEngine();
        Engine::setScriptEngine(scriptEngine);
        setVertexShaderConstant(services);
        setPixelShaderConstant(services);
        Engine::setScriptEngine(buf);
    }
}

void ShaderCallBack::setVertexShaderConstant(
        irr::video::IMaterialRendererServices* services) {
    QScriptValue listConstants =
            scriptEngine->evaluate(QString("setVertexShaderConstant();"));
    if (scriptEngine->hasUncaughtException()) {
#ifdef DEBUG
        QString erreurMessage =
                QString("File : ") + script + QString("\n") +
                QString("Error in setVertexShaderConstant") +
                QString("\n") + listConstants.toString();
        LOGGER_WRITE(LOG_Error, erreurMessage.toUtf8().data());
#endif
        return;
    }

    QScriptValueIterator it(listConstants);
    while (it.hasNext()) {
        try {
            it.next();
            QString name = it.name();
            int len = it.value().property("length").toInteger();
            std::vector<float> v;
            v.reserve(len);
            for (int i = 0; i < len; ++i) {
                QScriptValue item = it.value().property(i);
                v.push_back(item.toNumber());
            }
            services->setVertexShaderConstant(
                        services->getVertexShaderConstantID(
                            name.toUtf8().data()), &v[0], v.size());
        }
        catch(...) {
#ifdef DEBUG
        QString erreurMessage =
                QString("File : ") + script + QString("\n") +
                QString("Error in setVertexShaderConstant");
        LOGGER_WRITE(LOG_Error, erreurMessage.toUtf8().data());
#endif
        }
    }
}

void ShaderCallBack::setPixelShaderConstant(
        irr::video::IMaterialRendererServices* services) {
    QScriptValue listConstants =
            scriptEngine->evaluate(QString("setPixelShaderConstant();"));
    if (scriptEngine->hasUncaughtException()) {
#ifdef DEBUG
        QString erreurMessage =
                QString("File : ") + script + QString("\n") +
                QString("Error in setPixelShaderConstant") +
                QString("\n") + listConstants.toString();
        LOGGER_WRITE(LOG_Error, erreurMessage.toUtf8().data());
#endif
        return;
    }

    QScriptValueIterator it(listConstants);
    while (it.hasNext()) {
        try {
            it.next();
            QString name = it.name();
            int len = it.value().property("length").toInteger();
            std::vector<float> v;
            v.reserve(len);
            for (int i = 0; i < len; ++i) {
                QScriptValue item = it.value().property(i);
                v.push_back(item.toNumber());
            }
            services->setPixelShaderConstant(
                        services->getPixelShaderConstantID(
                            name.toUtf8().data()), &v[0], v.size());
        }
        catch(...) {
#ifdef DEBUG
        QString erreurMessage =
                QString("File : ") + script + QString("\n") +
                QString("Error in setPixelShaderConstant");
        LOGGER_WRITE(LOG_Error, erreurMessage.toUtf8().data());
#endif
        }
    }
}

}  // namespace MarbleEngine
