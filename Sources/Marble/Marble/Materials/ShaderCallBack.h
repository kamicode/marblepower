/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_ShaderCallBack.h
 * \brief Shaders Manager
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_MATERIALS_SHADERCALLBACK_H_
#define MARBLE_MATERIALS_SHADERCALLBACK_H_

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class ShaderCallBack
 * \brief
 *
 */
class ShaderCallBack : public irr::video::IShaderConstantSetCallBack {
    public:
        explicit ShaderCallBack(QString name, QString materialType);
        virtual ~ShaderCallBack();
        bool init();
        void OnSetConstants(irr::video::IMaterialRendererServices* services,
                            irr::s32 /*userData*/);
        int getShaderID();

    private:
        void setVertexShaderConstant(
                irr::video::IMaterialRendererServices* services);
        void setPixelShaderConstant(
                irr::video::IMaterialRendererServices* services);

    private:
        QScriptEngine* scriptEngine;
        QString script;
        irr::s32 shaderID;
        QString mName;

    private:
        DISALLOW_COPY_AND_ASSIGN(ShaderCallBack);
};

}  // namespace MarbleEngine

#endif  // MARBLE_MATERIALS_SHADERCALLBACK_H_
