/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_MaterialPhys.h
 * \brief Manager of Physical Materials (friction, restitution, damping, ...)
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_MATERIALS_MATERIALPHYS_H_
#define MARBLE_MATERIALS_MATERIALPHYS_H_

#include <map>

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \struct Material
 * \brief
 *
 */
struct Material {
    float m_restitution;
    float m_friction;
    float lin_damping;
    float ang_damping;
};

/*! \class MaterialPhys
 * \brief
 *
 */
class MaterialPhys {
    public:
        explicit MaterialPhys();
        virtual ~MaterialPhys();
        void readMaterialXML(tinyxml2::XMLElement* elem);
        void readConfigMaterialXML(tinyxml2::XMLElement* elem);
        void unLoadMap();
        Material* addMaterial(const QString &nomMaterial);
        Material* getMaterial(const QString &nomMaterial);
        void setRestitution(const QString &nomMaterial, float value);
        void setFriction(const QString &nomMaterial, float value);
        void setDamping(const QString &nomMaterial, float lin_damping,
                        float ang_damping);
        void update();

    private:
        std::map<QString, Material*> materials;
        std::map<QString, QString> materialPairs;

    private:
        DISALLOW_COPY_AND_ASSIGN(MaterialPhys);
};

}  // namespace MarbleEngine

#endif  // MARBLE_MATERIALS_MATERIALPHYS_H_
