/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Material3D.h
 * \brief Manager of 3D Materials (texture, light reflexion, ...)
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_MATERIALS_MATERIAL3D_H_
#define MARBLE_MATERIALS_MATERIAL3D_H_

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Material3D
 * \brief
 *
 */
class Material3D {
    public:
        explicit Material3D(irr::video::SMaterial* s);
        virtual ~Material3D();
        void readXML(tinyxml2::XMLElement* elem);
        irr::video::SMaterial* getMaterial();
        void setMaterial(irr::video::SMaterial* material);
        void setMaterialType(irr::video::E_MATERIAL_TYPE materialType =
                irr::video::EMT_SOLID);
        void setAmbientColor(const irr::video::SColor &ambiantColor =
                irr::video::SColor(255, 255, 255, 255));
        void setDiffuseColor(const irr::video::SColor &diffuseColor =
                irr::video::SColor(255, 255, 255, 255));
        void setEmissiveColor(const irr::video::SColor &emissiveColor =
                irr::video::SColor(255, 255, 255, 255));
        void setSpecularColor(const irr::video::SColor &specularColor =
                irr::video::SColor(255, 255, 255, 255));
        void setShininess(irr::f32 shininess = 0.0f);
        void setMaterialTypeParam(
                irr::video::E_MATERIAL_TYPE materialTypeParam =
                irr::video::EMT_SOLID);
        void setMaterialTypeParam2(
                irr::video::E_MATERIAL_TYPE materialTypeParam2 =
                irr::video::EMT_SOLID);
        void setZBuffer(irr::u32 zbuffer = irr::video::ECFN_LESSEQUAL);
        void setAntiAliasing(irr::u32 antiAliasing = irr::video::EAAM_SIMPLE);
        void setColorMask(irr::u8 colorMask = irr::video::ECP_ALL);
        void setColorMaterial(irr::u8 colorMaterial = irr::video::ECM_DIFFUSE);
        void setWireframe(bool wireframe = false);
        void setPointCloud(bool pointCloud = false);
        void setGouraudShading(bool gouraudShading = true);
        void setLighting(bool lighting = true);
        void setZWriteEnable(bool zwriteEnable = true);
        void setBackfaceCulling(bool backfaceCulling = true);
        void setFrontfaceCulling(bool frontfaceCulling = false);
        void setFogEnable(bool fogEnable = false);
        void setNormalizeNormals(bool normalizeNormals = false);

    private:
        irr::video::SMaterial* material;

    private:
        DISALLOW_COPY_AND_ASSIGN(Material3D);
};

}  // namespace MarbleEngine

#endif  // MARBLE_MATERIALS_MATERIAL3D_H_
