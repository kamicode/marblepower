/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Materials/Material3D.h"

#include "Marble/Engines/Engine.h"
#include "Marble/Materials/ShaderCallBack.h"
#include "Marble/Engines/GraphicsEngine.h"

namespace MarbleEngine {

Material3D::Material3D(irr::video::SMaterial* s) {
    material = s;
}

Material3D::~Material3D() {
}

void Material3D::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("wireframe")) {
        setWireframe(elem->BoolAttribute("wireframe"));
    }

    if (elem->Attribute("pointCloud")) {
        setPointCloud(elem->BoolAttribute("pointCloud"));
    }

    if (elem->Attribute("gouraudShading")) {
        setGouraudShading(elem->BoolAttribute("gouraudShading"));
    }

    if (elem->Attribute("lighting")) {
        setLighting(elem->BoolAttribute("lighting"));
    }

    if (elem->Attribute("zwriteEnable")) {
        setZWriteEnable(elem->BoolAttribute("zwriteEnable"));
    }

    if (elem->Attribute("backfaceCulling")) {
        setBackfaceCulling(elem->BoolAttribute("backfaceCulling"));
    }

    if (elem->Attribute("frontfaceCulling")) {
        setFrontfaceCulling(elem->BoolAttribute("frontfaceCulling"));
    }

    if (elem->Attribute("fogEnable")) {
        setFogEnable(elem->BoolAttribute("fogEnable"));
    }

    if (elem->Attribute("normalizeNormals")) {
        setNormalizeNormals(elem->BoolAttribute("normalizeNormals"));
    }

    if (elem->Attribute("shininess")) {
        setShininess(elem->FloatAttribute("shininess"));
    }

    if (elem->Attribute("materialTypeParam")) {
        setMaterialTypeParam(Convert::stringMaterialType(
                                 elem->Attribute("materialTypeParam")));
    }

    if (elem->Attribute("materialTypeParam2")) {
        setMaterialTypeParam(Convert::stringMaterialType(
                                 elem->Attribute("materialTypeParam2")));
    }

    if (elem->Attribute("shaderMaterialType")
            && elem->Attribute("materialType")) {
        QString name = elem->Attribute("shaderMaterialType");
        ShaderCallBack* call =
                new ShaderCallBack(name, elem->Attribute("materialType"));
        int id = call->getShaderID();
        if (id != -1) {
            Engine::getShaders().push_back(call);
            setMaterialType((irr::video::E_MATERIAL_TYPE)id);
        }
    } else if (elem->Attribute("materialType")) {
        setMaterialType(Convert::stringMaterialType(
                            elem->Attribute("materialType")));
    }

    if (elem->Attribute("zBuffer")) {
        setZBuffer(Convert::stringZBuffer(elem->Attribute("zBuffer")));
    }

    if (elem->Attribute("antiAliasing")) {
        setAntiAliasing(Convert::stringAntiAliasing(
                            elem->Attribute("antiAliasing")));
    }

    if (elem->Attribute("colorMask")) {
        setColorMask(Convert::stringColorMask(elem->Attribute("colorMask")));
    }

    if (elem->Attribute("colorMaterial")) {
        setColorMaterial(Convert::stringColorMaterial(
                             elem->Attribute("colorMaterial")));
    }

    elem = elem->FirstChildElement();

    irr::video::IVideoDriver* d = Engine::getGraphicsEngine()->getDriver();
    while (elem) {
        if (QString(elem->Value()) == "texture") {
            if (elem->Attribute("level") && elem->Attribute("file")) {
                int level = elem->IntAttribute("level");
                irr::io::path texture =
                        (Engine::getDirMedia()
                         + elem->Attribute("file")).toUtf8().data();
                material->setTexture(
                            level,
                            d->getTexture(texture));
            }
        } else if (QString(elem->Value()) == "ambiantColor") {
            if (elem->Attribute("alpha") && elem->Attribute("red")
                    && elem->Attribute("green") && elem->Attribute("blue")) {
                irr::video::SColor color(0, 0, 0, 0);
                color.setRed(elem->IntAttribute("red"));
                color.setGreen(elem->IntAttribute("green"));
                color.setBlue(elem->IntAttribute("blue"));
                color.setAlpha(elem->IntAttribute("alpha"));
                setAmbientColor(color);
            }
        } else if (QString(elem->Value()) == "diffuseColor") {
            if (elem->Attribute("alpha") && elem->Attribute("red")
                    && elem->Attribute("green") && elem->Attribute("blue")) {
                irr::video::SColor color(0, 0, 0, 0);
                color.setRed(elem->IntAttribute("red"));
                color.setGreen(elem->IntAttribute("green"));
                color.setBlue(elem->IntAttribute("blue"));
                color.setAlpha(elem->IntAttribute("alpha"));
                setDiffuseColor(color);
            }
        } else if (QString(elem->Value()) == "emissiveColor") {
            if (elem->Attribute("alpha") && elem->Attribute("red")
                    && elem->Attribute("green") && elem->Attribute("blue")) {
                irr::video::SColor color(0, 0, 0, 0);
                color.setRed(elem->IntAttribute("red"));
                color.setGreen(elem->IntAttribute("green"));
                color.setBlue(elem->IntAttribute("blue"));
                color.setAlpha(elem->IntAttribute("alpha"));
                setEmissiveColor(color);
            }
        } else if (QString(elem->Value()) == "specularColor") {
            if (elem->Attribute("alpha") && elem->Attribute("red")
                    && elem->Attribute("green") && elem->Attribute("blue")) {
                irr::video::SColor color(0, 0, 0, 0);
                color.setRed(elem->IntAttribute("red"));
                color.setGreen(elem->IntAttribute("green"));
                color.setBlue(elem->IntAttribute("blue"));
                color.setAlpha(elem->IntAttribute("alpha"));
                setSpecularColor(color);
            }
        }

        elem = elem->NextSiblingElement();
    }
}

irr::video::SMaterial* Material3D::getMaterial() {
    return material;
}

void Material3D::setMaterial(irr::video::SMaterial* material) {
    this->material = material;
}

void Material3D::setMaterialType(irr::video::E_MATERIAL_TYPE materialType) {
    material->MaterialType = materialType;
}

void Material3D::setAmbientColor(const irr::video::SColor &ambiantColor) {
    material->AmbientColor = ambiantColor;
}

void Material3D::setDiffuseColor(const irr::video::SColor &diffuseColor) {
    material->DiffuseColor = diffuseColor;
}

void Material3D::setEmissiveColor(const irr::video::SColor &emissiveColor) {
    material->EmissiveColor = emissiveColor;
}

void Material3D::setSpecularColor(const irr::video::SColor &specularColor) {
    material->SpecularColor = specularColor;
}

void Material3D::setShininess(irr::f32 shininess) {
    material->Shininess = shininess;
}

void Material3D::setMaterialTypeParam(
        irr::video::E_MATERIAL_TYPE materialTypeParam) {
    material->MaterialTypeParam = materialTypeParam;
}

void Material3D::setMaterialTypeParam2(
        irr::video::E_MATERIAL_TYPE materialTypeParam2) {
    material->MaterialTypeParam2 = materialTypeParam2;
}

void Material3D::setZBuffer(irr::u32 zbuffer) {
    material->ZBuffer = zbuffer;
}

void Material3D::setAntiAliasing(irr::u32 antiAliasing) {
    material->AntiAliasing = antiAliasing;
}

void Material3D::setColorMask(irr::u8 colorMask) {
    material->ColorMask = colorMask;
}

void Material3D::setColorMaterial(irr::u8 colorMaterial) {
    material->ColorMaterial = colorMaterial;
}

void Material3D::setWireframe(bool wireframe) {
    material->Wireframe = wireframe;
}

void Material3D::setPointCloud(bool pointCloud) {
    material->PointCloud = pointCloud;
}

void Material3D::setGouraudShading(bool gouraudShading) {
    material->GouraudShading = gouraudShading;
}

void Material3D::setLighting(bool lighting) {
    material->Lighting = lighting;
}

void Material3D::setZWriteEnable(bool zwriteEnable) {
    material->ZWriteEnable = zwriteEnable;
}

void Material3D::setBackfaceCulling(bool backfaceCulling) {
    material->BackfaceCulling = backfaceCulling;
}

void Material3D::setFrontfaceCulling(bool frontfaceCulling) {
    material->FrontfaceCulling = frontfaceCulling;
}

void Material3D::setFogEnable(bool fogEnable) {
    material->FogEnable = fogEnable;
}

void Material3D::setNormalizeNormals(bool normalizeNormals) {
    material->NormalizeNormals = normalizeNormals;
}

}  // namespace MarbleEngine
