/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include <string>

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/Logger.h"

int main(int argc, char * argv[]) {
    try {
        QCoreApplication a(argc, argv);

        srand(time(NULL));

        MarbleEngine::Engine* instance = MarbleEngine::Engine::getInstance();


        if (argc == 3) {
            instance->loadProject(argv[1]);
            instance->setNextMap(argv[2]);
        } else {
            instance->loadProject(a.applicationDirPath());
        }

        instance->loadMap();
        instance->launch();
        instance->destroyComplete();
    }
    catch(const std::exception & e) {
        std::string message = std::string("std::exception : ");
        message += std::string(e.what());
        LOGGER_WRITE(LOG_Error, message.c_str());
        return EXIT_FAILURE;
    }
    catch(...) {
        LOGGER_WRITE(LOG_Error, "Erreur non geree");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
