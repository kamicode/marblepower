/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Engines/GuiEngine.h"

#include <vector>

#include "Marble/Engines/Engine.h"
#include "Marble/Elements/Element.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Engines/Logger.h"

namespace MarbleEngine {

GuiEngine::GuiEngine() {
    try {
        irr::IrrlichtDevice* device = Engine::getGraphicsEngine()->getDevice();
        ceguiRenderer = &(CEGUI::IrrlichtRenderer::bootstrapSystem(*device));
    }
    catch(const CEGUI::Exception& e) {
        QString message = QString("CEGUI (GuiEngine) : ")
                + QString(e.getMessage().c_str());
        LOGGER_WRITE(LOG_Error, message.toUtf8().data());
    }
}

GuiEngine::~GuiEngine() {
    unLoadMap();
    try {
        CEGUI::IrrlichtRenderer::destroySystem();
    }
    catch(const CEGUI::Exception& e) {
        QString message = QString("CEGUI (~GuiEngine) : ")
                + QString(e.getMessage().c_str());
        LOGGER_WRITE(LOG_Error, message.toUtf8().data());
    }
}

void GuiEngine::unLoadMap() {
    try {
        std::vector<GuiScriptData*>::iterator it, end = userDatas.end();
        for (it = userDatas.begin(); it != end; ++it) {
            delete *it;
            *it = NULL;
        }
        userDatas.clear();

        CEGUI::WindowManager::getSingleton().destroyAllWindows();
    }
    catch(const CEGUI::Exception& e) {
        QString message = QString("CEGUI (unLoadMap) : ")
                + QString(e.getMessage().c_str());
        LOGGER_WRITE(LOG_Error, message.toUtf8().data());
    }
}


void GuiEngine::render() {
    try {
        CEGUI::System::getSingleton().renderGUI();
    }
    catch(const CEGUI::Exception& e) {
        QString message = QString("CEGUI (render) : ")
                + QString(e.getMessage().c_str());
        LOGGER_WRITE(LOG_Error, message.toUtf8().data());
    }
}

void GuiEngine::injectEvent(const irr::SEvent& event) {
    try {
        ceguiRenderer->injectEvent(event);
    }
    catch(const CEGUI::Exception& e) {
        QString message = QString("CEGUI (injectEvent) : ")
                + QString(e.getMessage().c_str());
        LOGGER_WRITE(LOG_Error, message.toUtf8().data());
    }
}

void GuiEngine::init(tinyxml2::XMLElement* elem) {
    try {
        CEGUI::System& sys = CEGUI::System::getSingleton();

        {
            CEGUI::DefaultResourceProvider* rp =
                    static_cast<CEGUI::DefaultResourceProvider*>
                    (sys.getResourceProvider());

            if (elem->Attribute("schemes")) {
                rp->setResourceGroupDirectory(
                            "schemes",
                            (Engine::getDirMedia()
                             + elem->Attribute("schemes")).toUtf8().data());
                CEGUI::Scheme::setDefaultResourceGroup("schemes");
            }
            if (elem->Attribute("imagesets")) {
                rp->setResourceGroupDirectory(
                            "imagesets",
                            (Engine::getDirMedia()
                             + elem->Attribute("imagesets")).toUtf8().data());
                CEGUI::Imageset::setDefaultResourceGroup("imagesets");
            }
            if (elem->Attribute("fonts")) {
                rp->setResourceGroupDirectory(
                            "fonts",
                            (Engine::getDirMedia()
                             + elem->Attribute("fonts")).toUtf8().data());
                CEGUI::Font::setDefaultResourceGroup("fonts");
            }
            if (elem->Attribute("layouts")) {
                rp->setResourceGroupDirectory(
                            "layouts",
                            (Engine::getDirMedia()
                             + elem->Attribute("layouts")).toUtf8().data());
                CEGUI::WindowManager::setDefaultResourceGroup("layouts");
            }
            if (elem->Attribute("looknfeels")) {
                rp->setResourceGroupDirectory(
                            "looknfeels",
                            (Engine::getDirMedia()
                             + elem->Attribute("looknfeels")).toUtf8().data());
                CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
            }
            if (elem->Attribute("schemas")) {
                rp->setResourceGroupDirectory(
                            "schemas",
                            (Engine::getDirMedia()
                             + elem->Attribute("schemas")).toUtf8().data());
            }
            if (elem->Attribute("animations")) {
                rp->setResourceGroupDirectory(
                            "animations",
                            (Engine::getDirMedia()
                             + elem->Attribute("animations")).toUtf8().data());
            }
        }

        // Managers
        elem = elem->FirstChildElement();
        {
            tinyxml2::XMLElement* node = elem->FirstChildElement();
            while (node) {
                if (QString(node->Value()) == "SchemeManager") {
                    CEGUI::SchemeManager::getSingleton().create(
                                node->Attribute("create"));
                } else if (QString(node->Value()) == "FontManager") {
                    CEGUI::FontManager::getSingleton().create(
                                node->Attribute("create"));
                } else if (QString(node->Value()) == "FontManager") {
                    CEGUI::FontManager::getSingleton().create(
                                node->Attribute("create"));
                } else if (QString(node->Value()) == "ImagesetsManager") {
                    CEGUI::ImagesetManager::getSingleton().create(
                                node->Attribute("create"));
                }
                node = node->NextSiblingElement();
            }
        }

        // Defaults
        elem = elem->NextSiblingElement();
        {
            if (elem->Attribute("defaultFont")) {
                sys.setDefaultFont(elem->Attribute("defaultFont"));
            }
            if (elem->Attribute("defaultMouseImageSet")
                    && elem->Attribute("defaultMouseCursor")) {
                sys.setDefaultMouseCursor(
                            elem->Attribute("defaultMouseImageSet"),
                            elem->Attribute("defaultMouseCursor"));
            }
            if (elem->Attribute("defaultTooltip")) {
                sys.setDefaultTooltip(elem->Attribute("defaultTooltip"));
            }
        }

        // Layout
        elem = elem->NextSiblingElement();
        {
            if (elem->Attribute("file")) {
                CEGUI::WindowManager& wmgr =
                        CEGUI::WindowManager::getSingleton();
                CEGUI::Window *newWindow =
                        wmgr.loadWindowLayout(elem->Attribute("file"));
                CEGUI::System::getSingleton().setGUISheet(newWindow);

                elem = elem->FirstChildElement();
                while (elem) {
                    QString name = elem->Attribute("elementName");
                    QString widget = elem->Attribute("widget");
                    QString event = elem->Attribute("event");
                    QString code = elem->Attribute("code");
                    QString script = Engine::getDirMedia()
                            + elem->Attribute("script");
                    connect(widget, event, name, code, script);

                    elem = elem->NextSiblingElement();
                }
            }
        }
    }
    catch(const CEGUI::Exception& e) {
        QString message = QString("CEGUI (init) : ")
                + QString(e.getMessage().c_str());
        LOGGER_WRITE(LOG_Error, message.toUtf8().data());
    }
}

void GuiEngine::update(float timeInMs) {
    CEGUI::System::getSingleton().injectTimePulse(timeInMs * 0.001f);
}

void GuiEngine::connect(QString widget, QString event, QString name,
                        QString code, QString script) {
    CEGUI::Window* win =
            CEGUI::WindowManager::getSingleton().getWindow(
                widget.toUtf8().data());

    Element* elt = Engine::getElt(name);

    if (win != NULL && elt != NULL) {
        GuiScriptData* u  = new GuiScriptData();
        u->code = code;
        u->script = script;
        userDatas.push_back(u);

        win->setUserData(u);
        win->subscribeEvent(event.toUtf8().data(),
                            CEGUI::Event::Subscriber(&Element::eventGUI, elt));
    }
}

QString GuiEngine::getGuiValue(QString name) {
    CEGUI::Window *win = CEGUI::WindowManager::getSingleton().getWindow(
                name.toUtf8().data());
    if (win)
        return win->getText().c_str();
    return "";
}

void GuiEngine::GuiSetEnable(QString name, bool enable) {
    CEGUI::Window *win = CEGUI::WindowManager::getSingleton().getWindow(
                name.toUtf8().data());
    if (win)
        win->setEnabled(enable);
}

bool GuiEngine::GuiIsVisible(QString name) {
    CEGUI::Window *win = CEGUI::WindowManager::getSingleton().getWindow(
                name.toUtf8().data());
    if (win)
        return win->isVisible();
    return false;
}

void GuiEngine::GuiSetVisible(QString name, bool visible) {
    CEGUI::Window *win = CEGUI::WindowManager::getSingleton().getWindow(
                name.toUtf8().data());
    if (win)
        win->setVisible(visible);
}

QString GuiEngine::GuiComboGetSelected(QString name) {
    CEGUI::Combobox *win =
            static_cast<CEGUI::Combobox*>
            (CEGUI::WindowManager::getSingleton().getWindow(
                 name.toUtf8().data()));
    if (win)
        return win->getSelectedItem()->getText().c_str();
    return "";
}

void GuiEngine::GuiComboAddItem(QString name, QString item) {
    CEGUI::Combobox *win = static_cast<CEGUI::Combobox*>
            (CEGUI::WindowManager::getSingleton().getWindow(
                 name.toUtf8().data()));
    if (win) {
        CEGUI::ListboxTextItem* itembox =
                new CEGUI::ListboxTextItem(item.toUtf8().data());
        win->addItem(itembox);
    }
}

void GuiEngine::GuiComboAddItem(QString name, QString item,
                                QString imageSet, QString image) {
    CEGUI::Combobox *win = static_cast<CEGUI::Combobox*>
            (CEGUI::WindowManager::getSingleton().getWindow(
                 name.toUtf8().data()));
    if (win) {
        CEGUI::ListboxTextItem* itembox =
                new CEGUI::ListboxTextItem(item.toUtf8().data());
        itembox->setSelectionBrushImage(imageSet.toUtf8().data(),
                                        image.toUtf8().data());
        win->addItem(itembox);
    }
}

void GuiEngine::GuiComboSetSelected(QString name, QString item) {
    CEGUI::Combobox *win = static_cast<CEGUI::Combobox*>
            (CEGUI::WindowManager::getSingleton().getWindow(
                 name.toUtf8().data()));
    if (win) {
        CEGUI::ListboxItem* itembox = win->findItemWithText(
                    item.toUtf8().data(), NULL);
        itembox->setSelected(true);
        win->setText(itembox->getText());
    }
}

void GuiEngine::GuiComboSetReadOnly(QString name, bool enable) {
    CEGUI::Combobox *win = static_cast<CEGUI::Combobox*>
            (CEGUI::WindowManager::getSingleton().getWindow(
                 name.toUtf8().data()));
    if (win)
        win->setReadOnly(enable);
}

float GuiEngine::GuiSpinnerGetValue(QString name) {
    CEGUI::Spinner *win = static_cast<CEGUI::Spinner*>
            (CEGUI::WindowManager::getSingleton().getWindow(
                 name.toUtf8().data()));
    if (win)
        return win->getCurrentValue();
    return 0.0f;
}

void GuiEngine::GuiSpinnerSetValue(QString name, float value) {
    CEGUI::Spinner *win = static_cast<CEGUI::Spinner*>
            (CEGUI::WindowManager::getSingleton().getWindow(
                 name.toUtf8().data()));
    if (win)
        win->setCurrentValue(value);
}

bool GuiEngine::isCheckBoxSelected(QString name) {
    CEGUI::Checkbox *win = static_cast<CEGUI::Checkbox*>
            (CEGUI::WindowManager::getSingleton().getWindow(
                 name.toUtf8().data()));
    if (win)
        return win->isSelected();
    return false;
}

void GuiEngine::setCheckBoxSelect(QString name, bool select) {
    CEGUI::Checkbox *win = static_cast<CEGUI::Checkbox*>
            (CEGUI::WindowManager::getSingleton().getWindow(
                 name.toUtf8().data()));
    if (win)
        win->setSelected(select);
}

}  // namespace MarbleEngine
