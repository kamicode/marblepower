/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_VideoEngine.h
 * \brief Video Manager
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ENGINES_VIDEOENGINE_H_
#define MARBLE_ENGINES_VIDEOENGINE_H_

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

class VideoAudioInterface;
class VideoAudioInterfaceFactory;

/*! \struct SIrrVideoClip
 * \brief
 *
 */
struct SIrrVideoClip {
    TheoraVideoClip* pTheoClip;
    irr::video::IImage* pImg;
    irr::video::ITexture* pTex;
    irr::io::path clipName;
};

/*! \enum E_SCALE_MODE
 * \brief
 *
 */
enum E_SCALE_MODE {
    // No scaling.
    ESM_NONE,
    // Scale to window size.
    ESM_WINDOW_FIT,
    // Scale width or height to window size and keep aspect ratio.
    // This doesn't always work correcty since Irrlicht might adjust
    // texture dimensions internally
    // even if NPOT textures are enabled and supported by the hardware.
    ESM_WINDOW_RATIO,
    // Scale to nearest power-of-two, bigger than the original size.
    ESM_POT_UP,
    // Scale to nearest power-of-two, smaller than the original size.
    ESM_POT_DOWN
};

/*! \class VideoEngine
 * \brief
 *
 */
class VideoEngine {
    public:
        explicit VideoEngine(irr::IrrlichtDevice* pDevice,
                             bool readAudio = true,
                             irr::u32 workerThreads = 1);
        virtual ~VideoEngine();
        void update();
        SIrrVideoClip* getIrrClipByName(const irr::io::path &clipname);
        TheoraVideoClip* getTheoClipByName(const irr::io::path &clipname);
        irr::video::IImage* getImageByName(const irr::io::path &clipname);
        irr::video::ITexture* getTextureByName(const irr::io::path &clipname);
        irr::video::ITexture* addVideoClip(const irr::io::path &filename,
                                           const irr::io::path &clipname,
                                           E_SCALE_MODE scaleMode = ESM_NONE,
                                           bool repeatPlayback = false,
                                           bool startPlayback = true,
                                           bool preloadIntoRAM = false);
        irr::video::ITexture* addVideoClip(
                const irr::io::path &filename,
                const irr::io::path &clipname,
                const irr::core::dimension2du &scaleToSize,
                bool repeatPlayback = false, bool startPlayback = true,
                bool preloadIntoRAM = false);
        bool removeVideoClip(const irr::io::path &clipname,
                             bool irrRemoveTexture = false,
                             bool irrDropImage = true);
        void removeVideoClipAll(bool irrRemoveTexture = false,
                                bool irrDropImage = true);

    public:
        static const bool isPOT(const irr::core::dimension2du &size);
        static const irr::core::dimension2du getNextPOT(
                const irr::core::dimension2du &size, bool up = false);

    private:
        SIrrVideoClip* getIrrClipByName(const irr::io::path &clipname,
                                        irr::u32 &out_clipindex);
        void processFrames();

    private :
        irr::IrrlichtDevice* mDevice;
        irr::video::IVideoDriver* mDriver;
        irr::ITimer* mTimer;
        TheoraVideoManager* mVmgr;
        VideoAudioInterfaceFactory* mAIF;
        irr::core::array<SIrrVideoClip*> mIrrClip;
        irr::f32 mTimeThen;

    private:
        DISALLOW_COPY_AND_ASSIGN(VideoEngine);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ENGINES_VIDEOENGINE_H_
