/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_GuiEngine.h
 * \brief GUI Manager
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ENGINES_GUIENGINE_H_
#define MARBLE_ENGINES_GUIENGINE_H_

#include <vector>

#include "Marble/Divers.h"

namespace MarbleEngine {

struct GuiScriptData;

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
class GuiEngine: public QObject {
    Q_OBJECT

    public:
        explicit GuiEngine();
        virtual ~GuiEngine();
        void init(tinyxml2::XMLElement* elem);
        void update(float timeInMs);
        void unLoadMap();
        void connect(QString widget, QString event, QString name,
                     QString code, QString script);
        void render();
        void injectEvent(const irr::SEvent& event);

    public:
        Q_INVOKABLE QString getGuiValue(QString name);
        Q_INVOKABLE void GuiSetEnable(QString name, bool enable);
        Q_INVOKABLE bool GuiIsVisible(QString name);
        Q_INVOKABLE void GuiSetVisible(QString name, bool visible);

    public:
        Q_INVOKABLE float GuiSpinnerGetValue(QString name);
        Q_INVOKABLE void GuiSpinnerSetValue(QString name, float value);

    public:
        Q_INVOKABLE QString GuiComboGetSelected(QString name);
        Q_INVOKABLE void GuiComboAddItem(QString name, QString item);
        Q_INVOKABLE void GuiComboAddItem(QString name, QString item,
                                         QString imageSet, QString image);
        Q_INVOKABLE void GuiComboSetReadOnly(QString name, bool enable);
        Q_INVOKABLE void GuiComboSetSelected(QString name, QString item);

    public:
        Q_INVOKABLE bool isCheckBoxSelected(QString name);
        Q_INVOKABLE void setCheckBoxSelect(QString name, bool select);

    private:
        CEGUI::IrrlichtRenderer* ceguiRenderer;
        std::vector<GuiScriptData*> userDatas;

    private:
        DISALLOW_COPY_AND_ASSIGN(GuiEngine);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ENGINES_GUIENGINE_H_
