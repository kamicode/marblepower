/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Engines/SoundEngine.h"

#include <map>
#include <vector>

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/Logger.h"

namespace MarbleEngine {

SoundEngine::SoundEngine() {
    ALCdevice* Device = alcOpenDevice("");
    if (alcGetError(Device) != ALC_NO_ERROR) {
        Device = NULL;
        QString message = QString("SoundEngine : ")
                + QString("Impossible d'ouvrir le device par defaut");
        LOGGER_WRITE(LOG_Error, message.toUtf8().data());
        return;
    }

    ALCcontext* Context = alcCreateContext(Device, NULL);
    if (alcGetError(Device) != ALC_NO_ERROR) {
        Device = NULL;
        Context = NULL;
        QString message = QString("SoundEngine : ")
                + QString("Impossible de creer un contexte audio");
        LOGGER_WRITE(LOG_Error, message.toUtf8().data());
        return;
    }

    alcMakeContextCurrent(Context);
    if (alcGetError(Device) != ALC_NO_ERROR) {
        Device = NULL;
        Context = NULL;
        QString message = QString("SoundEngine : ")
                + QString("Impossible d'activer le contexte audio");
        LOGGER_WRITE(LOG_Error, message.toUtf8().data());
        return;
    }

    musics.clear();
}

SoundEngine::~SoundEngine() {
    ALCcontext* Context = alcGetCurrentContext();
    ALCdevice*  Device  = alcGetContextsDevice(Context);
    alcMakeContextCurrent(NULL);
    alcDestroyContext(Context);
    alcCloseDevice(Device);
}


void SoundEngine::unLoadMap() {
    {
        std::map<QString, ALuint>::const_iterator it, end = sounds.end();
        for (it = sounds.begin(); it != end; ++it)  {
            alDeleteBuffers(1, &(it->second));
        }
        sounds.clear();
    }
    {
        std::vector<ALuint>::const_iterator it, end = Sources.end();
        for (it = Sources.begin(); it != end; ++it)  {
            alSourcei(*it, AL_BUFFER, 0);
            alDeleteSources(1, &(*it));
        }
        Sources.clear();
    }
}

int SoundEngine::playSound(QString sound) {
    if (sounds.find(sound) == sounds.end()) {
        SF_INFO FileInfos;
        SNDFILE* File = sf_open((Engine::getDirMedia()
                                 + sound).toUtf8().data(),
                                SFM_READ, &FileInfos);

        ALsizei NbSamples  = static_cast<ALsizei>(
                    FileInfos.channels * FileInfos.frames);
        ALsizei SampleRate = static_cast<ALsizei>(FileInfos.samplerate);

        std::vector<ALshort> Samples(NbSamples);
        sf_read_short(File, &Samples[0], NbSamples);

        sf_close(File);

        ALenum Format = (FileInfos.channels) == 1 ? AL_FORMAT_MONO16
                                                  : AL_FORMAT_STEREO16;

        ALuint Buffer;
        alGenBuffers(1, &Buffer);

        alBufferData(Buffer, Format, &Samples[0],
                NbSamples * sizeof(ALushort), SampleRate);

        sounds.insert(std::make_pair(sound, Buffer));
    }

    ALuint Source;
    alGenSources(1, &Source);
    alSourcei(Source, AL_BUFFER, sounds[sound]);

    alSourcePlay(Source);
    Sources.push_back(Source);

    return Source;
}

void SoundEngine::setSoundProperties(int source, float pitch, float gain,
                                     float pos[3], float velocity[3],
                                     bool loop) {
    alSourcef(source, AL_PITCH, pitch);
    alSourcef(source, AL_GAIN, gain);
    alSource3f(source, AL_POSITION, pos[0], pos[1], -pos[2]);
    alSource3f(source, AL_VELOCITY, velocity[0], velocity[1], -velocity[2]);
    alSourcei(source, AL_LOOPING, loop);
    alSourcePlay(source);
}

void SoundEngine::setSoundPitch(int source, float pitch) {
    alSourcef(source, AL_PITCH, pitch);
    alSourcePlay(source);
}

void SoundEngine::setSoundGain(int source, float gain) {
    alSourcef(source, AL_GAIN, gain);
    alSourcePlay(source);
}

void SoundEngine::setSoundPos(int source, float pos[3]) {
    alSource3f(source, AL_POSITION, pos[0], pos[1], -pos[2]);
    alSourcePlay(source);
}

void SoundEngine::setSoundVelocity(int source, float velocity[3]) {
    alSource3f(source, AL_VELOCITY, velocity[0], velocity[1], -velocity[2]);
    alSourcePlay(source);
}

void SoundEngine::setSoundLoop(int source, bool loop) {
    alSourcei(source, AL_LOOPING, loop);
    alSourcePlay(source);
}

void SoundEngine::setMusicProperties(QString name, float pitch, float gain,
                                     float pos[3], float velocity[3],
                                     bool loop) {
    if (musics.find(name) != musics.end()) {
        ALuint source = musics.at(name)->Source;
        alSourcef(source, AL_PITCH, pitch);
        alSourcef(source, AL_GAIN, gain);
        alSource3f(source, AL_POSITION, pos[0], pos[1], -pos[2]);
        alSource3f(source, AL_VELOCITY, velocity[0], velocity[1], -velocity[2]);
        alSourcei(source, AL_LOOPING, loop);
        alSourcePlay(source);
    }
}

void SoundEngine::setMusicPitch(QString name, float pitch) {
    if (musics.find(name) != musics.end()) {
        ALuint source = musics.at(name)->Source;
        alSourcef(source, AL_PITCH, pitch);
        alSourcePlay(source);
    }
}

void SoundEngine::setMusicGain(QString name, float gain) {
    if (musics.find(name) != musics.end()) {
        ALuint source = musics.at(name)->Source;
        alSourcef(source, AL_GAIN, gain);
        alSourcePlay(source);
    }
}

void SoundEngine::setMusicPos(QString name, float pos[3]) {
    if (musics.find(name) != musics.end()) {
        ALuint source = musics.at(name)->Source;
        alSource3f(source, AL_POSITION, pos[0], pos[1], -pos[2]);
        alSourcePlay(source);
    }
}

void SoundEngine::setMusicVelocity(QString name, float velocity[3]) {
    if (musics.find(name) != musics.end()) {
        ALuint source = musics.at(name)->Source;
        alSource3f(source, AL_VELOCITY, velocity[0], velocity[1], -velocity[2]);
        alSourcePlay(source);
    }
}

void SoundEngine::setMusicLoop(QString name, bool loop) {
    if (musics.find(name) != musics.end()) {
        ALuint source = musics.at(name)->Source;
        alSourcei(source, AL_LOOPING, loop);
        alSourcePlay(source);
    }
}

int SoundEngine::playMusic(QString name, QString file) {
    if (musics.find(name) == musics.end()) {
        Music* music = new Music();
        music->File = sf_open((Engine::getDirMedia() + file).toUtf8().data(),
                              SFM_READ, &(music->fileInfos));
        music->NbSamples = static_cast<ALsizei>(
                    music->fileInfos.frames * music->fileInfos.channels);
        music->SampleRate = static_cast<ALsizei>(
                    music->fileInfos.samplerate);

        music->Format = (music->fileInfos.channels) == 1 ? AL_FORMAT_MONO16
                                                         : AL_FORMAT_STEREO16;

        alGenBuffers(2, music->Buffers);

        for (int i = 0; i < 2; ++i) {
            std::vector<ALshort> Samples(music->NbSamples);
            ALsizei read(static_cast<ALsizei>(sf_read_short(music->File,
                                                             &Samples[0],
                                               music->NbSamples)));
            alBufferData(music->Buffers[i], music->Format, &Samples[0],
                    read * sizeof(ALshort), music->SampleRate);
        }

        alGenSources(1, &(music->Source));
        alSourceQueueBuffers(music->Source, 3, music->Buffers);
        alSourcePlay(music->Source);
        musics.insert(std::make_pair(name, music));
        return music->Source;
    }
    return -1;
}

void SoundEngine::update() {
    for (std::map<QString, Music*>::iterator it = musics.begin();
         it != musics.end();)  {
        Music* music = it->second;

        alGetSourcei(music->Source, AL_SOURCE_STATE, &(music->Status));

        if (music->Status == AL_PLAYING) {
            ALint NbProcessed;
            alGetSourcei(music->Source, AL_BUFFERS_PROCESSED, &NbProcessed);
            for (ALint i = 0; i < NbProcessed; ++i) {
                ALuint Buffer;
                alSourceUnqueueBuffers(music->Source, 1, &Buffer);
                std::vector<ALshort> Samples(music->NbSamples);
                ALsizei read = static_cast<ALsizei>(
                            sf_read_short(music->File, &Samples[0],
                            music->NbSamples));
                alBufferData(Buffer, music->Format, &Samples[0],
                        read * sizeof(ALshort), music->SampleRate);
                alSourceQueueBuffers(music->Source, 1, &Buffer);
            }
            ++it;
        } else {
            ALint  NbQueued;
            ALuint Buffer;
            alGetSourcei(music->Source, AL_BUFFERS_QUEUED, &NbQueued);
            for (ALint i = 0; i < NbQueued; ++i) {
                alSourceUnqueueBuffers(music->Source, 1, &Buffer);
            }
            alDeleteBuffers(3, music->Buffers);
            alSourcei(music->Source, AL_BUFFER, 0);
            alDeleteSources(1, &music->Source);
            sf_close(music->File);

            delete music;
            music = NULL;
            it = musics.erase(it);
        }
    }

    for (std::vector<ALuint>::iterator it = Sources.begin();
         it != Sources.end();)  {
        ALint stat;
        alGetSourcei(*it, AL_SOURCE_STATE, &stat);
        if (stat == AL_STOPPED) {
            alSourcei(*it, AL_BUFFER, 0);
            alDeleteSources(1, &(*it));
            it = Sources.erase(it);
        } else {
            ++it;
        }
    }
}

void SoundEngine::stopMusic(QString name) {
    if (musics.find(name) != musics.end()) {
        Music* music = musics.at(name);
        alSourceStop(music->Source);
        ALint  NbQueued;
        ALuint Buffer;
        alGetSourcei(music->Source, AL_BUFFERS_QUEUED, &NbQueued);
        for (ALint i = 0; i < NbQueued; ++i) {
            alSourceUnqueueBuffers(music->Source, 1, &Buffer);
        }
        alDeleteBuffers(3, music->Buffers);
        alSourcei(music->Source, AL_BUFFER, 0);
        alDeleteSources(1, &music->Source);
        sf_close(music->File);

        delete music;
        music = NULL;
        musics.erase(name);
    }
}

bool SoundEngine::musicFinished(QString name) {
    if (musics.find(name) != musics.end()) {
        Music* music = musics.at(name);
        return (music->Status == AL_STOPPED);
    }
    return true;
}

void SoundEngine::setListenerProperties(float gain, float position[3],
                                        float velocity[3],
                                        float orientation[6]) {
    alListenerf(AL_GAIN, gain);
    alListener3f(AL_POSITION, position[0], position[1], -position[2]);
    alListener3f(AL_VELOCITY, velocity[0], velocity[1], -velocity[2]);
    orientation[2] *= -1;
    orientation[5] *= -1;
    alListenerfv(AL_ORIENTATION, orientation);
}

void SoundEngine::setSoundProperties(int source, float pitch, float gain,
                                     const QScriptValue &position,
                                     const QScriptValue &velocity,
                                     bool loop) {
    float pos[3];
    pos[0] = position.property(0).toNumber();
    pos[1] = position.property(1).toNumber();
    pos[2] = position.property(2).toNumber();

    float vel[3];
    vel[0] = velocity.property(0).toNumber();
    vel[1] = velocity.property(1).toNumber();
    vel[2] = velocity.property(2).toNumber();

    setSoundProperties(source, pitch, gain, pos, vel, loop);
}

void SoundEngine::setSoundPos(int source, const QScriptValue &position) {
    float pos[3];
    pos[0] = position.property(0).toNumber();
    pos[1] = position.property(1).toNumber();
    pos[2] = position.property(2).toNumber();

    setSoundPos(source, pos);
}

void SoundEngine::setSoundVelocity(int source, const QScriptValue &velocity) {
    float vel[3];
    vel[0] = velocity.property(0).toNumber();
    vel[1] = velocity.property(1).toNumber();
    vel[2] = velocity.property(2).toNumber();

    setSoundVelocity(source, vel);
}

void SoundEngine::setMusicProperties(QString name, float pitch, float gain,
                                     const QScriptValue &position,
                                     const QScriptValue &velocity, bool loop) {
    float pos[3];
    pos[0] = position.property(0).toNumber();
    pos[1] = position.property(1).toNumber();
    pos[2] = position.property(2).toNumber();
    float vel[3];
    vel[0] = velocity.property(0).toNumber();
    vel[1] = velocity.property(1).toNumber();
    vel[2] = velocity.property(2).toNumber();

    setMusicProperties(name, pitch, gain, pos, vel, loop);
}

void SoundEngine::setMusicPos(QString name, const QScriptValue &position) {
    float pos[3];
    pos[0] = position.property(0).toNumber();
    pos[1] = position.property(1).toNumber();
    pos[2] = position.property(2).toNumber();

    setMusicPos(name, pos);
}

void SoundEngine::setMusicVelocity(QString name, const QScriptValue &velocity) {
    float vel[3];
    vel[0] = velocity.property(0).toNumber();
    vel[1] = velocity.property(1).toNumber();
    vel[2] = velocity.property(2).toNumber();

    setMusicVelocity(name, vel);
}

void SoundEngine::setSoundListenerProperties(float gain,
                                             const QScriptValue &position,
                                             const QScriptValue &velocity,
                                             const QScriptValue &target,
                                             const QScriptValue &up) {
    float pos[3];
    pos[0] = position.property(0).toNumber();
    pos[1] = position.property(1).toNumber();
    pos[2] = position.property(2).toNumber();

    float vel[3];
    vel[0] = velocity.property(0).toNumber();
    vel[1] = velocity.property(1).toNumber();
    vel[2] = velocity.property(2).toNumber();

    float orientation[6];
    orientation[0] = target.property(0).toNumber();
    orientation[1] = target.property(1).toNumber();
    orientation[2] = target.property(2).toNumber();
    orientation[3] = up.property(0).toNumber();
    orientation[4] = up.property(1).toNumber();
    orientation[5] = up.property(2).toNumber();

    setListenerProperties(gain, pos, vel, orientation);
}

}  // namespace MarbleEngine
