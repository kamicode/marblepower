/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_EventEngine.h
 * \brief User Event manager
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ENGINES_EVENTENGINE_H_
#define MARBLE_ENGINES_EVENTENGINE_H_

#include <map>

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class EventEngine
 * \brief
 *
 */
class EventEngine: public QObject, private irr::IEventReceiver {
        Q_OBJECT

    public:
        explicit EventEngine();
        virtual ~EventEngine();
        bool OnEvent(const irr::SEvent& event);
        void update();
        bool isKeyDown(irr::EKEY_CODE keyCode) const;
        bool isJustKeyDown(irr::EKEY_CODE keyCode) const;
        void setMousePos(irr::core::position2df pos);

    public:
        Q_INVOKABLE bool isKeyDown(QString keyCode);
        Q_INVOKABLE bool isJustKeyDown(QString keyCode);
        Q_INVOKABLE void setMousePos(const QScriptValue &pos);
        Q_INVOKABLE void setMouseVisible(bool visible);
        Q_INVOKABLE QScriptValue getMousePos();
        Q_INVOKABLE int getWheel();
        Q_INVOKABLE void setWheel(int value);

        Q_INVOKABLE int numJoysticks();
        Q_INVOKABLE QScriptValue listJoysticks();
        Q_INVOKABLE QString getJoystickName(int id);
        Q_INVOKABLE int getJoystickNumAxes(int id);
        Q_INVOKABLE int getJoystickButtons(int id);
        Q_INVOKABLE bool hasJoystickPovHat(int id);
        Q_INVOKABLE int getJoystickPovDegree(int id);
        Q_INVOKABLE bool isJoystickButtonPressed(int id, int button);
        Q_INVOKABLE int getJoystickAxisValue(int id, QString axis);

    private:
        bool bufKeys[irr::KEY_KEY_CODES_COUNT];
        bool keys[irr::KEY_KEY_CODES_COUNT];
        bool oldKeys[irr::KEY_KEY_CODES_COUNT];
        int wheel;
        irr::gui::ICursorControl* cursorControl;

        struct Joystick {
            irr::SEvent::SJoystickEvent event;
            irr::SJoystickInfo info;
        };

        std::map<irr::u32, Joystick> JoystickState;

    private:
        DISALLOW_COPY_AND_ASSIGN(EventEngine);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ENGINES_EVENTENGINE_H_
