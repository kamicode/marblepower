/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_VideoAudioInterface.h
 * \brief Manager of interactions between video and audio
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ENGINES_VIDEOAUDIOINTERFACE_H_
#define MARBLE_ENGINES_VIDEOAUDIOINTERFACE_H_

#include <queue>

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class VideoAudioInterface
 * \brief
 *
 */
class VideoAudioInterface: public TheoraAudioInterface, private TheoraTimer {
    public:
        explicit VideoAudioInterface(TheoraVideoClip* owner,
                                     int nChannels, int freq);
        virtual ~VideoAudioInterface();
        void insertData(float* data, int nSamples);
        void destroy();
        float getQueuedAudioSize();
        void update(float time_increase);
        void pause();
        void play();
        void seek(float time);
        ALuint getSource();

    private:
        struct OpenAL_Buffer {
            ALuint id;
            int nSamples;
        };

        int mMaxBuffSize;
        int mBuffSize;
        ALshort* mTempBuffer;
        float mCurrentTimer;
        ALuint mSource;
        int mNumProcessedSamples;
        int mNumPlayedSamples;
        std::queue<OpenAL_Buffer> mBufferQueue;

    private:
        DISALLOW_COPY_AND_ASSIGN(VideoAudioInterface);
    };

    /*! \class VideoAudioInterfaceFactory
     * \brief
     *
     */
    class VideoAudioInterfaceFactory: public TheoraAudioInterfaceFactory {
    public:
        explicit VideoAudioInterfaceFactory();
        virtual ~VideoAudioInterfaceFactory();
        VideoAudioInterface* createInstance(TheoraVideoClip* owner,
                                            int nChannels, int freq);

    private:
        DISALLOW_COPY_AND_ASSIGN(VideoAudioInterfaceFactory);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ENGINES_VIDEOAUDIOINTERFACE_H_
