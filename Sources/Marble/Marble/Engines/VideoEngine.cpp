/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Engines/VideoEngine.h"

#include "Marble/Engines/VideoAudioInterface.h"

namespace MarbleEngine {

VideoEngine::VideoEngine(irr::IrrlichtDevice* pDevice, bool readAudio,
                         irr::u32 workerThreads) :
    mDevice(pDevice), mDriver(0), mTimer(0), mVmgr(0), mAIF(0), mTimeThen(0) {
    mDriver = mDevice->getVideoDriver();
    mTimer = mDevice->getTimer();
    mVmgr = new TheoraVideoManager(workerThreads);
    if (readAudio) {
        mAIF = new VideoAudioInterfaceFactory();
        mVmgr->setAudioInterfaceFactory(mAIF);
    }
    mIrrClip = irr::core::array<SIrrVideoClip*>();
    mTimeThen = (irr::f32)mTimer->getRealTime();
}

VideoEngine::~VideoEngine() {
    removeVideoClipAll();

    if (mVmgr) {
        delete mVmgr;
        mVmgr = NULL;
        mAIF = NULL;
    }
}

void VideoEngine::processFrames() {
    if (mIrrClip.empty()) return;

    for (irr::u32 i = 0; i < mIrrClip.size(); ++i) {
        TheoraVideoFrame* frame = mIrrClip[i]->pTheoClip->getNextFrame();

        if (frame) {
            irr::video::IImage* pImg  = mIrrClip[i]->pImg;
            void* imgData = pImg->lock();
            irr::u8* frameData = frame->getBuffer();

            if (imgData && frameData) {
                memcpy(imgData, frameData, pImg->getImageDataSizeInBytes());
            }
            if (imgData) pImg->unlock();


            irr::video::ITexture* pTex = mIrrClip[i]->pTex;
            void* texData  = pTex->lock();

            if (texData) {
                if (pTex->getSize() == pImg->getDimension()) {
                    memcpy(texData, frameData, pImg->getImageDataSizeInBytes());
                } else {
                    pImg->copyToScaling(texData, pTex->getSize().Width,
                                        pTex->getSize().Height,
                                        irr::video::ECF_A8R8G8B8,
                                        pTex->getPitch());
                }

                pTex->unlock();
            }

            mIrrClip[i]->pTheoClip->popFrame();
        }
    }
}

void VideoEngine::update() {
    processFrames();

    irr::f32 timeNow   = (irr::f32)mTimer->getRealTime();
    irr::f32 timeDelta = (timeNow - mTimeThen) / 1000.0f;

    if (timeDelta > 0.25f)
        timeDelta = 0.05f;

    mVmgr->update(timeDelta);

    mTimeThen = timeNow;
}

SIrrVideoClip* VideoEngine::getIrrClipByName(const irr::io::path &clipname,
                                             irr::u32 &out_clipindex) {
    if (mIrrClip.empty()) return NULL;

    for (irr::u32 i = 0; i < mIrrClip.size(); ++i) {
        if (mIrrClip[i]->clipName == clipname) {
            out_clipindex = i;
            return mIrrClip[i];
        }
    }

    return NULL;
}

SIrrVideoClip* VideoEngine::getIrrClipByName(const irr::io::path &clipname) {
    if (mIrrClip.empty())
        return NULL;

    for (irr::u32 i = 0; i < mIrrClip.size(); ++i) {
        if (mIrrClip[i]->clipName == clipname) {
            return mIrrClip[i];
        }
    }

    return NULL;
}

TheoraVideoClip* VideoEngine::getTheoClipByName(const irr::io::path &clipname) {
    if (mIrrClip.empty()) return NULL;

    for (irr::u32 i = 0; i < mIrrClip.size(); ++i) {
        if (mIrrClip[i]->clipName == clipname) {
            return mIrrClip[i]->pTheoClip;
        }
    }

    return NULL;
}

irr::video::IImage* VideoEngine::getImageByName(const irr::io::path &clipname) {
    if (mIrrClip.empty()) return NULL;

    for (irr::u32 i = 0; i < mIrrClip.size(); ++i) {
        if (mIrrClip[i]->clipName == clipname) {
            return mIrrClip[i]->pImg;
        }
    }

    return NULL;
}

irr::video::ITexture* VideoEngine::getTextureByName(
        const irr::io::path &clipname) {
    if (mIrrClip.empty()) return NULL;

    for (irr::u32 i = 0; i < mIrrClip.size(); ++i) {
        if (mIrrClip[i]->clipName == clipname) {
            return mIrrClip[i]->pTex;
        }
    }

    return NULL;
}

const bool VideoEngine::isPOT(const irr::core::dimension2du &size) {
    irr::u32 x = size.Width;
    irr::u32 y = size.Height;

    bool xTrue = ((x != 0) && !(x & (x - 1)));
    bool yTrue = ((y != 0) && !(y & (y - 1)));

    return (xTrue && yTrue);
}

const irr::core::dimension2du VideoEngine::getNextPOT(
        const irr::core::dimension2du &size, bool up) {
    irr::core::dimension2du temp(1, 1);

    if (isPOT(size)) {
        temp = size;
        return temp;
    }

    while (temp.Width  < size.Width)  { temp.Width  *= 2; }
    while (temp.Height < size.Height) { temp.Height *= 2; }

    if (up) {
        return temp;
    } else {
        temp.Width  /= 2;
        temp.Height /= 2;
        return temp;
    }
}

irr::video::ITexture* VideoEngine::addVideoClip(
        const irr::io::path &filename, const irr::io::path &clipname,
        E_SCALE_MODE scaleMode, bool repeatPlayback, bool startPlayback,
        bool preloadIntoRAM) {
    const bool oldFlag = mDriver->getTextureCreationFlag(
                irr::video::ETCF_CREATE_MIP_MAPS);
    mDriver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);

    if (mDriver->queryFeature(irr::video::EVDF_TEXTURE_NPOT)) {
        mDriver->setTextureCreationFlag(
                    irr::video::ETCF_ALLOW_NON_POWER_2, true);
    }

    TheoraVideoClip* pClip = NULL;
    irr::video::IImage* pImg = NULL;
    irr::video::ITexture* pTex = NULL;

    if (filename == "" || clipname == "") {
        mDriver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS,
                                        oldFlag);
        return NULL;
    }

    if (preloadIntoRAM) {
        pClip = mVmgr->createVideoClip(
                    new TheoraMemoryFileDataSource(filename.c_str()), TH_BGRA);
    } else {
        pClip = mVmgr->createVideoClip(filename.c_str(), TH_BGRA);
    }

    if (!pClip) {
        mDriver->setTextureCreationFlag(
                    irr::video::ETCF_CREATE_MIP_MAPS, oldFlag);
        return NULL;
    }

    if (startPlayback) {
        pClip->play();
    } else {
        pClip->stop();
    }

    pClip->setAutoRestart(repeatPlayback);

    const irr::core::dimension2du sizeClip(
                pClip->getWidth(), pClip->getHeight());
    const irr::core::dimension2du sizeScreen(mDriver->getScreenSize());

    pImg = mDriver->createImage(irr::video::ECF_A8R8G8B8, sizeClip);

    if (!pImg) {
        mVmgr->destroyVideoClip(pClip);
        mDriver->setTextureCreationFlag(
                    irr::video::ETCF_CREATE_MIP_MAPS, oldFlag);
        return NULL;
    }

    void* imgData = pImg->lock();
    if (imgData) {
        memset(imgData, 0xFF, pImg->getImageDataSizeInBytes());
        pImg->unlock();
    }

    switch (scaleMode) {
    case ESM_NONE: {
        pTex = mDriver->addTexture(sizeClip, clipname,
                                   irr::video::ECF_A8R8G8B8);
    } break;
    case ESM_WINDOW_FIT: {
        pTex = mDriver->addTexture(sizeScreen, clipname,
                                   irr::video::ECF_A8R8G8B8);
    } break;
    case ESM_WINDOW_RATIO: {
        irr::f32 ratioX =
                (irr::f32)sizeScreen.Width  / (irr::f32)sizeClip.Width;
        irr::f32 ratioY =
                (irr::f32)sizeScreen.Height / (irr::f32)sizeClip.Height;

        if (ratioX < ratioY) {
            irr::core::dimension2du dim(sizeScreen.Width,
                                        sizeClip.Height * (irr::u32)ratioX);
            pTex = mDriver->addTexture(dim, clipname,
                                       irr::video::ECF_A8R8G8B8);
        } else if (ratioX > ratioY) {
            irr::core::dimension2du dim(sizeClip.Width * (irr::u32)ratioY,
                                        sizeScreen.Height);
            pTex = mDriver->addTexture(dim, clipname, irr::video::ECF_A8R8G8B8);
        } else {
            pTex = mDriver->addTexture(sizeScreen, clipname,
                                       irr::video::ECF_A8R8G8B8);
        }
    } break;
    case ESM_POT_UP: {
        pTex = mDriver->addTexture(getNextPOT(sizeClip, true), clipname,
                                   irr::video::ECF_A8R8G8B8);
    } break;
    case ESM_POT_DOWN: {
        pTex = mDriver->addTexture(getNextPOT(sizeClip, false), clipname,
                                   irr::video::ECF_A8R8G8B8);
    } break;
    }

    if (!pTex) {
        mVmgr->destroyVideoClip(pClip);
        pImg->drop();
        mDriver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS,
                                        oldFlag);
        return NULL;
    }

    void* texData = pTex->lock();
    if (texData) {
        pImg->copyToScaling(texData, pTex->getSize().Width,
                            pTex->getSize().Height, irr::video::ECF_A8R8G8B8,
                            pTex->getPitch());
        pTex->unlock();
    }

    SIrrVideoClip *temp = new SIrrVideoClip();
    temp->pTheoClip = pClip;
    temp->pImg      = pImg;
    temp->pTex      = pTex;
    temp->clipName  = clipname;

    mIrrClip.push_back(temp);
    mDriver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, oldFlag);
    return pTex;
}

irr::video::ITexture* VideoEngine::addVideoClip(
        const irr::io::path &filename, const irr::io::path &clipname,
        const irr::core::dimension2du &scaleToSize, bool repeatPlayback,
        bool startPlayback, bool preloadIntoRAM) {
    const bool oldFlag = mDriver->getTextureCreationFlag(
                irr::video::ETCF_CREATE_MIP_MAPS);
    mDriver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);

    if (mDriver->queryFeature(irr::video::EVDF_TEXTURE_NPOT))
        mDriver->setTextureCreationFlag(
                    irr::video::ETCF_ALLOW_NON_POWER_2, true);

    TheoraVideoClip* pClip = NULL;
    irr::video::IImage* pImg = NULL;
    irr::video::ITexture* pTex = NULL;

    if (filename == "" || clipname == "") {
        mDriver->setTextureCreationFlag(
                    irr::video::ETCF_CREATE_MIP_MAPS, oldFlag);
        return NULL;
    }

    if (preloadIntoRAM) {
        pClip = mVmgr->createVideoClip(
                    new TheoraMemoryFileDataSource(filename.c_str()), TH_BGRX);
    } else {
        pClip = mVmgr->createVideoClip(filename.c_str(), TH_BGRX);
    }

    if (!pClip) {
        mDriver->setTextureCreationFlag(
                    irr::video::ETCF_CREATE_MIP_MAPS, oldFlag);
        return NULL;
    }

    if (startPlayback) {
        pClip->play();
    } else {
        pClip->stop();
    }

    pClip->setAutoRestart(repeatPlayback);

    const irr::core::dimension2du sizeClip(
                pClip->getWidth(), pClip->getHeight());

    pImg = mDriver->createImage(irr::video::ECF_A8R8G8B8, sizeClip);

    if (!pImg) {
        if (pClip) mVmgr->destroyVideoClip(pClip);
        mDriver->setTextureCreationFlag(
                    irr::video::ETCF_CREATE_MIP_MAPS, oldFlag);
        return NULL;
    }

    void* imgData = pImg->lock();
    if (imgData) {
        memset(imgData, 0xFF, pImg->getImageDataSizeInBytes());
        pImg->unlock();
    }

    pTex = mDriver->addTexture(
                scaleToSize, clipname, irr::video::ECF_A8R8G8B8);

    if (!pTex) {
        if (pClip) mVmgr->destroyVideoClip(pClip);
        if (pImg)  pImg->drop();
        mDriver->setTextureCreationFlag(
                    irr::video::ETCF_CREATE_MIP_MAPS, oldFlag);
        return NULL;
    }

    void* texData = pTex->lock();
    if (texData) {
        pImg->copyToScaling(
                    texData, pTex->getSize().Width,
                    pTex->getSize().Height, irr::video::ECF_A8R8G8B8,
                    pTex->getPitch());
        pTex->unlock();
    }

    SIrrVideoClip* temp = new SIrrVideoClip();
    temp->pTheoClip = pClip;
    temp->pImg = pImg;
    temp->pTex = pTex;
    temp->clipName = clipname;

    mIrrClip.push_back(temp);
    mDriver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, oldFlag);
    return pTex;
}

bool VideoEngine::removeVideoClip(const irr::io::path &clipname,
                                  bool irrRemoveTexture, bool irrDropImage) {
    irr::u32 clipIndex;
    SIrrVideoClip* pIrrClip = getIrrClipByName(clipname, clipIndex);

    if (!pIrrClip) return false;

    if (pIrrClip->pTheoClip)
        mVmgr->destroyVideoClip(pIrrClip->pTheoClip);

    if (pIrrClip->pImg && irrDropImage)
        pIrrClip->pImg->drop();

    if (pIrrClip->pTex && irrRemoveTexture)
        mDriver->removeTexture(pIrrClip->pTex);

    delete pIrrClip;
    mIrrClip.erase(clipIndex);

    return true;
}

void VideoEngine::removeVideoClipAll(bool irrRemoveTexture, bool irrDropImage) {
    while (!mIrrClip.empty()) {
        SIrrVideoClip* pIrrClip = mIrrClip.getLast();
        if (pIrrClip) {
            if (pIrrClip->pTheoClip)
                mVmgr->destroyVideoClip(pIrrClip->pTheoClip);

            if (pIrrClip->pImg && irrDropImage)
                pIrrClip->pImg->drop();

            if (pIrrClip->pTex && irrRemoveTexture)
                mDriver->removeTexture(pIrrClip->pTex);

            delete pIrrClip;
        }

        mIrrClip.erase(mIrrClip.size() - 1);
    }
}

}  // namespace MarbleEngine
