/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_SoundEngine.h
 * \brief Sound Manager
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ENGINES_SOUNDENGINE_H_
#define MARBLE_ENGINES_SOUNDENGINE_H_

#include <map>
#include <vector>

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {


/*! \class SoundEngine
 * \brief
 *
 */
class SoundEngine: public QObject {
    Q_OBJECT

    public:
        explicit SoundEngine();
        virtual ~SoundEngine();
        void unLoadMap();
        void setSoundProperties(int source, float pitch, float gain,
                                float pos[3], float velocity[3], bool loop);
        void setSoundPos(int source, float pos[3]);
        void setSoundVelocity(int source, float velocity[3]);
        void setMusicProperties(QString name, float pitch, float gain,
                                float pos[3], float velocity[3], bool loop);
        void setMusicPos(QString name, float pos[3]);
        void setMusicVelocity(QString name, float velocity[3]);
        void setListenerProperties(float gain, float position[3],
                                   float velocity[3], float orientation[6]);
        void update();

    public:
        Q_INVOKABLE int playSound(QString sound);
        Q_INVOKABLE void setSoundProperties(
                int source, float pitch, float gain,
                const QScriptValue &position, const QScriptValue &velocity,
                                            bool loop);
        Q_INVOKABLE void setSoundPitch(int source, float pitch);
        Q_INVOKABLE void setSoundGain(int source, float gain);
        Q_INVOKABLE void setSoundPos(int source, const QScriptValue &position);
        Q_INVOKABLE void setSoundVelocity(int source,
                                          const QScriptValue &velocity);
        Q_INVOKABLE void setSoundLoop(int source, bool loop);
        Q_INVOKABLE int playMusic(QString name, QString music);
        Q_INVOKABLE bool musicFinished(QString name);
        Q_INVOKABLE void setMusicProperties(QString name, float pitch,
                                            float gain,
                                            const QScriptValue &position,
                                            const QScriptValue &velocity,
                                            bool loop);
        Q_INVOKABLE void setMusicPitch(QString name, float pitch);
        Q_INVOKABLE void setMusicGain(QString name, float gain);
        Q_INVOKABLE void setMusicPos(QString name,
                                     const QScriptValue &position);
        Q_INVOKABLE void setMusicVelocity(QString name,
                                          const QScriptValue &velocity);
        Q_INVOKABLE void setMusicLoop(QString name, bool loop);
        Q_INVOKABLE void stopMusic(QString name);
        Q_INVOKABLE void setSoundListenerProperties(
                float gain, const QScriptValue &position,
                const QScriptValue &velocity, const QScriptValue &target,
                const QScriptValue &up);

    private:
        std::map<QString, ALuint> sounds;
        std::vector<ALuint> Sources;

        /*! \struct Music
         * \brief
         *
         */
        struct Music {
            SNDFILE* File;
            SF_INFO fileInfos;
            ALsizei NbSamples;
            ALsizei SampleRate;
            ALenum Format;
            ALuint Source;
            ALint Status;
            ALuint Buffers[2];
        };

        std::map<QString, Music*> musics;

    private:
        DISALLOW_COPY_AND_ASSIGN(SoundEngine);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ENGINES_SOUNDENGINE_H_
