/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Engines/EventEngine.h"

#include <map>

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/GuiEngine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Engines/Logger.h"
#include "Marble/Elements/Camera.h"

namespace MarbleEngine {

EventEngine::EventEngine() : irr::IEventReceiver() {
    irr::IrrlichtDevice* device = Engine::getGraphicsEngine()->getDevice();
    device->setEventReceiver(this);
    cursorControl = device->getCursorControl();

    wheel = 0;

    for (irr::u32 i = 0; i < irr::KEY_KEY_CODES_COUNT; ++i) {
        keys[i] = false;
        oldKeys[i] = false;
        bufKeys[i] = false;
    }

    JoystickState.clear();
    irr::core::array<irr::SJoystickInfo> joystickInfo;
    if (device->activateJoysticks(joystickInfo)) {
        irr::u32 size = joystickInfo.size();
        for (irr::u32 i = 0; i < size; ++i) {
            Joystick joy;
            joy.event = irr::SEvent::SJoystickEvent();
            joy.info = joystickInfo[i];
            JoystickState.insert(std::make_pair(joy.info.Joystick, joy));
        }
    }
}

EventEngine::~EventEngine() {
}

void EventEngine::update() {
    memcpy(oldKeys, keys, sizeof keys);
    memcpy(keys, bufKeys, sizeof keys);
}

bool EventEngine::OnEvent(const irr::SEvent& event) {
    Engine::getCeguiEngine()->injectEvent(event);

    if (event.EventType == irr::EET_KEY_INPUT_EVENT) {
        bufKeys[event.KeyInput.Key] = event.KeyInput.PressedDown;
    } else if (event.MouseInput.Event == irr::EMIE_MOUSE_WHEEL) {
        wheel = event.MouseInput.Wheel;
    } else if (event.EventType == irr::EET_JOYSTICK_INPUT_EVENT) {
        JoystickState[event.JoystickEvent.Joystick].event = event.JoystickEvent;
    } else if (event.EventType == irr::EET_GUI_EVENT) {
        if (event.GUIEvent.EventType == irr::gui::EGET_MESSAGEBOX_OK) {
            Engine::setEtat(launchState);
        }
    } else if (event.EventType == irr::EET_LOG_TEXT_EVENT) {
        QString s = "Irrlicht : ";
        s += event.LogEvent.Text;
        LOGGER_WRITE(Logger::irrlichtLevelToLoggerLevel(event.LogEvent.Level),
                     s.toUtf8().data());
    };

    return false;
}

bool EventEngine::isKeyDown(irr::EKEY_CODE keyCode) const {
    return keys[keyCode];
}

bool EventEngine::isKeyDown(QString keyCode) {
    irr::EKEY_CODE code = Convert::stringKeyCode(keyCode);
    if (code != irr::KEY_KEY_CODES_COUNT)
        return keys[code];

    return false;
}

bool EventEngine::isJustKeyDown(irr::EKEY_CODE keyCode) const {
    return keys[keyCode] && !oldKeys[keyCode];
}

bool EventEngine::isJustKeyDown(QString keyCode) {
    irr::EKEY_CODE code = Convert::stringKeyCode(keyCode);
    if (code != irr::KEY_KEY_CODES_COUNT)
        return keys[code] && !oldKeys[code];

    return false;
}

QScriptValue EventEngine::getMousePos() {
    irr::core::position2df cursorPos = cursorControl->getRelativePosition();
    QScriptValue ret = Engine::getScriptEngine()->newObject();
    ret.setProperty("X", cursorPos.X);
    ret.setProperty("Y", cursorPos.Y);
    return ret;
}

void EventEngine::setMousePos(irr::core::position2df pos) {
    cursorControl->setPosition(pos.X, pos.Y);
}

void EventEngine::setMousePos(const QScriptValue &pos) {
    irr::core::vector2df vect;
    vect.X = pos.property("X").toNumber();
    vect.Y = pos.property("Y").toNumber();
    setMousePos(vect);
}

void EventEngine::setMouseVisible(bool visible) {
    CEGUI::MouseCursor::getSingleton().setVisible(visible);
}

int EventEngine::getWheel() {
    return wheel;
}

void EventEngine::setWheel(int value) {
    wheel = value;
}

int EventEngine::numJoysticks() {
    return JoystickState.size();
}

QScriptValue EventEngine::listJoysticks() {
    QScriptValue ret = Engine::getScriptEngine()->newObject();

    std::map<irr::u32, Joystick>::const_iterator it;
    for (it = JoystickState.begin(); it != JoystickState.end(); ++it) {
        ret.setProperty(it->first, QString(it->second.info.Name.c_str()));
    }

    return ret;
}

QString EventEngine::getJoystickName(int id) {
    std::map<irr::u32, Joystick>::const_iterator it = JoystickState.find(id);

    if (it != JoystickState.end()) {
        return QString(it->second.info.Name.c_str());
    }
    return "";
}

int EventEngine::getJoystickNumAxes(int id) {
    std::map<irr::u32, Joystick>::const_iterator it = JoystickState.find(id);

    if (it != JoystickState.end()) {
        return it->second.info.Axes;
    }
    return 0;
}

int EventEngine::getJoystickButtons(int id) {
    std::map<irr::u32, Joystick>::const_iterator it = JoystickState.find(id);

    if (it != JoystickState.end()) {
        return it->second.info.Buttons;
    }
    return 0;
}

bool EventEngine::hasJoystickPovHat(int id) {
    std::map<irr::u32, Joystick>::const_iterator it = JoystickState.find(id);

    if (it != JoystickState.end()) {
        return it->second.info.PovHat == irr::SJoystickInfo::POV_HAT_PRESENT;
    }
    return false;
}

int EventEngine::getJoystickPovDegree(int id) {
    std::map<irr::u32, Joystick>::const_iterator it = JoystickState.find(id);

    if (it != JoystickState.end()) {
        int povDegrees = it->second.event.POV;
        if (povDegrees < 36000) {
            return povDegrees * 0.01;
        }
    }
    return -1;
}

bool EventEngine::isJoystickButtonPressed(int id, int button) {
    std::map<irr::u32, Joystick>::const_iterator it = JoystickState.find(id);

    if (it != JoystickState.end()) {
        return it->second.event.IsButtonPressed(button);
    }
    return false;
}

int EventEngine::getJoystickAxisValue(int id, QString axis) {
    std::map<irr::u32, Joystick>::const_iterator it = JoystickState.find(id);

    if (it != JoystickState.end()) {
        if (axis == "AXIS_X") {
            return it->second.event.Axis[irr::SEvent::SJoystickEvent::AXIS_X];
        } else if (axis == "AXIS_Y") {
            return it->second.event.Axis[irr::SEvent::SJoystickEvent::AXIS_Y];
        } else if (axis == "AXIS_Z") {
            return it->second.event.Axis[irr::SEvent::SJoystickEvent::AXIS_Z];
        } else if (axis == "AXIS_R") {
            return it->second.event.Axis[irr::SEvent::SJoystickEvent::AXIS_R];
        } else if (axis == "AXIS_U") {
            return it->second.event.Axis[irr::SEvent::SJoystickEvent::AXIS_U];
        } else if (axis == "AXIS_V") {
            return it->second.event.Axis[irr::SEvent::SJoystickEvent::AXIS_V];
        }
    }
    return 0;
}

}  // namespace MarbleEngine
