/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Engines/Engine.h"

#include <map>
#include <vector>

#include "Marble/Engines/SoundEngine.h"
#include "Marble/Engines/EventEngine.h"
#include "Marble/Engines/GuiEngine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Materials/MaterialPhys.h"
#include "Marble/Engines/PhysicsEngine.h"
#include "Marble/Engines/VideoEngine.h"
#include "Marble/Engines/Logger.h"
#include "Marble/Materials/ShaderCallBack.h"
#include "Marble/Elements/Light.h"
#include "Marble/Elements/Particules.h"
#include "Marble/Elements/Constraint.h"
#include "Marble/Elements/Element.h"
#include "Marble/Elements/Camera.h"
#include "Marble/Elements/Mesh.h"
#include "Marble/Elements/Terrain.h"
#include "Marble/Elements/Video2D.h"
#include "Marble/Elements/Sound.h"

namespace MarbleEngine {

Engine* Engine::singleton = NULL;
QScriptEngine* Engine::scriptEngine = NULL;

Engine::Engine() {
    ceguiEngine = NULL;
    videoEngine = NULL;
    graphicsEngine = NULL;
    physicsEngine = NULL;
    materialPhysEngine = NULL;
    soundEngine = NULL;
    eventEngine = NULL;
    elements.clear();
    cameras.clear();
    video2Ds.clear();
    etat = launchState;
    dirMedias = "";
    currentMap = "";
    nextMap = "";
    dirProject = "";
}

Engine* Engine::getInstance() {
    if (singleton == NULL) {
        singleton = new Engine();
    }
    return singleton;
}

Engine::~Engine() {
    if (ceguiEngine != NULL) {
        delete ceguiEngine;
        ceguiEngine = NULL;
    }
    if (videoEngine != NULL) {
        delete videoEngine;
        videoEngine = NULL;
    }
    if (soundEngine != NULL) {
        delete soundEngine;
        soundEngine = NULL;
    }
    if (materialPhysEngine != NULL) {
        delete materialPhysEngine;
        materialPhysEngine = NULL;
    }
    if (physicsEngine != NULL) {
        delete physicsEngine;
        physicsEngine = NULL;
    }
    if (graphicsEngine != NULL) {
        delete graphicsEngine;
        graphicsEngine = NULL;
    }
    LOGGER_STOP();
}

void Engine::destroyComplete() {
    unloadMap();
    irr::io::IFileSystem* f = graphicsEngine->getDevice()->getFileSystem();
    for (unsigned int i = 0; i < f->getFileArchiveCount(); ++i) {
        f->removeFileArchive(i);
    }

    if (singleton != NULL) {
        delete singleton;
        singleton = NULL;
    }
}

void Engine::removeElement(QString name) {
    {
        std::map<QString, Element*>::iterator it;
        it = elements.find(name);
        if (it != elements.end())
            elements.erase(it);
    }
    {
        std::map<QString, Camera*>::iterator it;
        it = cameras.find(name);
        if (it != cameras.end())
            cameras.erase(it);
    }
}

void Engine::destroyElement(Element* obj) {
    if (obj != NULL) {
        QString name = obj->getName();
        removeElement(name);
        obj->destroy();
    }
}

void Engine::loadProject(QString projectDir) {
    QDir dir(projectDir);
    dirProject = dir.canonicalPath() + "/";
    QString res = readProjectFile();
    tinyxml2::XMLDocument doc;
    if (doc.Parse(res.toUtf8().data()) != tinyxml2::XML_NO_ERROR) {
        exit(EXIT_FAILURE);
    }

    tinyxml2::XMLElement* elem = doc.FirstChildElement()->FirstChildElement();
    if (!elem) {
        exit(EXIT_FAILURE);
    }

    graphicsEngine = new GraphicsEngine();
    graphicsEngine->init(elem);
    elem = elem->NextSiblingElement();

    ceguiEngine = new GuiEngine();
    materialPhysEngine = new MaterialPhys();
    physicsEngine = new PhysicsEngine();
    soundEngine = new SoundEngine();
    videoEngine = new VideoEngine(graphicsEngine->getDevice(), true, 1);

#ifdef ENABLE_LOGGER
    LOGGER_START(LOG_Debug, "Debug.log");
#endif

    while (elem) {
        if (QString(elem->Value()) == "Map") {
            if (elem->BoolAttribute("main")) {
                nextMap = elem->Attribute("name");
            }
        } else if (QString(elem->Value()) == "Ressources") {
            dirMedias = dirProject + elem->Attribute("medias");
        }
        elem = elem->NextSiblingElement();
    }

    irr::io::IFileSystem* f = graphicsEngine->getDevice()->getFileSystem();
    if (QFile(dirProject + "Maps.dat").exists()) {
        f->addFileArchive(QString(dirProject + "Maps.dat").toUtf8().data(),
                          true, true, irr::io::EFAT_ZIP,
                          "HaiecapiqueMdpMarble");
    } else {
        f->addFileArchive(QString(dirProject).toUtf8().data());
    }
    if (QFile(dirMedias + "Data.dat").exists()) {
        f->addFileArchive(QString(dirMedias + "Data.dat").toUtf8().data(),
                          true, true, irr::io::EFAT_ZIP,
                          "HaiecapiqueMdpMarble");
    } else {
        f->addFileArchive(QString(dirMedias).toUtf8().data());
    }
}

void Engine::readXML(QString filname) {
    filname += ".dat";
    QFileInfo fileProjInfo(filname);
    tinyxml2::XMLDocument doc;

    if (fileProjInfo.isFile()) {
        if (doc.LoadFile(filname.toUtf8().data()) != tinyxml2::XML_NO_ERROR) {
            return;
        }
    } else {
        irr::IrrlichtDevice* device = Engine::getGraphicsEngine()->getDevice();
        irr::io::IFileSystem* fileSys = device->getFileSystem();
        irr::io::IReadFile* file =
                fileSys->createAndOpenFile(filname.toUtf8().data());
        char* buffer = new char[10000000];
        int size = file->read(buffer, 10000000);
        buffer[size] = '\0';
        file->drop();

        if (doc.Parse(buffer) != tinyxml2::XML_NO_ERROR) {
            return;
        }
        delete [] buffer;
    }

    tinyxml2::XMLElement* elem = doc.FirstChildElement()->FirstChildElement();
    if (!elem) {
        return;
    }

    while (elem) {
        if (QString(elem->Value()) == "Physic") {
            physicsEngine->init(elem);
        } else if (QString(elem->Value()) == "Material") {
            if (physicsEngine->getWorld() != NULL) {
                materialPhysEngine->readMaterialXML(elem);
            }
        } else if (QString(elem->Value()) == "ConfigMaterial") {
            if (physicsEngine->getWorld() != NULL) {
                materialPhysEngine->readConfigMaterialXML(elem);
            }
        } else if (QString(elem->Value()) == "Gui") {
            ceguiEngine->init(elem);
        } else if (QString(elem->Value()) == "SkyDome") {
            setSkyDome(elem);
        } else if (QString(elem->Value()) == "SkyBox") {
            setSkyBox(elem);
        } else if (QString(elem->Value()) == "Fog") {
            configFog(elem);
        } else if (QString(elem->Value()) == "Element") {
            QString name = elem->Attribute("name");
            Element* elt = new Element(name);
            elt->readXML(elem);
            elements.insert(std::make_pair(name, elt));
        } else if (QString(elem->Value()) == "Light") {
            QString name = elem->Attribute("name");
            Light* elt = new Light(name);
            elt->readXML(elem);
            elements.insert(std::make_pair(name, elt));
        } else if (QString(elem->Value()) == "Particules") {
            QString name = elem->Attribute("name");
            Particules* elt = new Particules(name);
            elt->readXML(elem);
            elements.insert(std::make_pair(name, elt));
        } else if (QString(elem->Value()) == "Mesh") {
            QString name = elem->Attribute("name");
            Mesh* elt = new Mesh(name);
            elt->readXML(elem);
            elements.insert(std::make_pair(name, elt));
        } else if (QString(elem->Value()) == "Terrain") {
            QString name = elem->Attribute("name");
            Terrain* elt = new Terrain(name);
            elt->readXML(elem);
            elements.insert(std::make_pair(name, elt));
        } else if (QString(elem->Value()) == "Constraint") {
            QString name = elem->Attribute("name");
            Constraint* elt = new Constraint(name);
            elt->readXML(elem);
            elements.insert(std::make_pair(name, elt));
        } else if (QString(elem->Value()) == "Sound") {
            QString name = elem->Attribute("name");
            Sound* elt = new Sound(name);
            elt->readXML(elem);
            elements.insert(std::make_pair(name, elt));
        } else if (QString(elem->Value()) == "Video2D") {
            QString name = elem->Attribute("name");
            Video2D* elt = new Video2D(name);
            elt->readXML(elem);
            elements.insert(std::make_pair(name, elt));
            video2Ds.push_back(elt);
        } else if (QString(elem->Value()) == "Camera") {
            QString name = elem->Attribute("name");
            Camera* elt = new Camera(name);
            elt->readXML(elem);
            elements.insert(std::make_pair(name, elt));
            cameras.insert(std::make_pair(name, elt));
        }

        elem = elem->NextSiblingElement();
    }

    {
        std::map<QString, Element*>::const_iterator it, end = elements.end();
        for (it = elements.begin(); it != end; ++it) {
            it->second->init();
        }
    }
    {
        std::vector<ShaderCallBack*>::const_iterator it,
                end = shaderCallBacks.end();
        for (it = shaderCallBacks.begin(); it != end; ++it) {
            (*it)->init();
        }
    }
}

void Engine::loadMap(QString mapName, bool unloadBefore) {
    if (unloadBefore) {
        unloadMap();
    }

    if (mapName.isEmpty()) {
        mapName = nextMap;
    }
    currentMap = mapName;

    eventEngine = new EventEngine();
    readXML(currentMap);
}

void Engine::unloadMap() {
    if (eventEngine != NULL) {
        graphicsEngine->getDevice()->setEventReceiver(NULL);
        delete eventEngine;
        eventEngine = NULL;
    }

    {
        std::vector<ShaderCallBack*>::const_iterator it,
                end = shaderCallBacks.end();
        for (it = shaderCallBacks.begin(); it != end; ++it) {
            (*it)->drop();
        }
        shaderCallBacks.clear();
    }

    {
        std::map<QString, Element*>::iterator iter = elements.begin();
        while (iter != elements.end()) {
            destroyElement(iter->second);
            iter = elements.begin();
        }
        elements.clear();
        cameras.clear();
        video2Ds.clear();
    }

    sharedPool.clear();

    materialPhysEngine->unLoadMap();
    physicsEngine->unLoadMap();
    graphicsEngine->unLoadMap();
    ceguiEngine->unLoadMap();
    soundEngine->unLoadMap();
    videoEngine->removeVideoClipAll(true, true);
}

void Engine::launch() {
    etat = launchState;
    irr::IrrlichtDevice* device = graphicsEngine->getDevice();
    irr::ITimer* timer = device->getTimer();
    irr::core::stringw str = QDir(dirProject).dirName().toUtf8().data();
    device->setWindowCaption(str.c_str());
    do {
        int lastTime = timer->getTime();
        while (etat == launchState && device->run()) {
            if (device->isWindowActive()) {
                int now = timer->getTime();
                timeSinceLastTick = now - lastTime;
                lastTime = now;
                updateAll(timeSinceLastTick);
                graphicsEngine->render();
            } else {
                device->yield();
            }
        }
        if (etat == changeMapState) {
            loadMap();
            etat = launchState;
        }
    } while (etat != quitState);
}

void Engine::updateAll(float timeInMs) {
#ifdef DEBUG
    irr::core::stringw str = QDir(dirProject).dirName().toUtf8().data();
    str += " - FPS : ";
    str += graphicsEngine->getDriver()->getFPS();
    graphicsEngine->getDevice()->setWindowCaption(str.c_str());
#endif

    eventEngine->update();
    ceguiEngine->update(timeInMs);
    physicsEngine->update(timeInMs);
    materialPhysEngine->update();
    videoEngine->update();
    soundEngine->update();

    std::map<QString, Element*>::const_iterator it, end = elements.end();
    for (it = elements.begin(); it != end; ++it) {
        it->second->update(timeInMs);
    }
}

void Engine::setSkyDome(tinyxml2::XMLElement* elem) {
    irr::io::path dome = (Engine::getDirMedia()
                          + elem->Attribute("dome")).toUtf8().data();
    irr::video::IVideoDriver* driver = graphicsEngine->getDriver();
    irr::scene::ISceneManager* sceneManager = graphicsEngine->getSceneManager();
    driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);
    sceneManager->addSkyDomeSceneNode(driver->getTexture(dome));
    driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, true);
}

void Engine::setSkyBox(tinyxml2::XMLElement* elem) {
    irr::io::path top = (Engine::getDirMedia()
                         + elem->Attribute("top")).toUtf8().data();
    irr::io::path bottom = (Engine::getDirMedia()
                            + elem->Attribute("bottom")).toUtf8().data();
    irr::io::path left = (Engine::getDirMedia()
                          + elem->Attribute("left")).toUtf8().data();
    irr::io::path right = (Engine::getDirMedia()
                           + elem->Attribute("right")).toUtf8().data();
    irr::io::path front = (Engine::getDirMedia()
                           + elem->Attribute("front")).toUtf8().data();
    irr::io::path back = (Engine::getDirMedia()
                          + elem->Attribute("back")).toUtf8().data();

    irr::video::IVideoDriver* driver = graphicsEngine->getDriver();
    irr::scene::ISceneManager* sceneManager = graphicsEngine->getSceneManager();
    driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, false);
    sceneManager->addSkyBoxSceneNode(driver->getTexture(top),
                                     driver->getTexture(bottom),
                                     driver->getTexture(left),
                                     driver->getTexture(right),
                                     driver->getTexture(front),
                                     driver->getTexture(back));
    driver->setTextureCreationFlag(irr::video::ETCF_CREATE_MIP_MAPS, true);
}

GraphicsEngine* Engine::getGraphicsEngine() {
    return Engine::getInstance()->graphicsEngine;
}

GuiEngine* Engine::getCeguiEngine() {
    return Engine::getInstance()->ceguiEngine;
}

PhysicsEngine* Engine::getPhysicsEngine() {
    return Engine::getInstance()->physicsEngine;
}

MaterialPhys* Engine::getMaterialPhysEngine() {
    return Engine::getInstance()->materialPhysEngine;
}

SoundEngine* Engine::getSoundEngine() {
    return Engine::getInstance()->soundEngine;
}

EventEngine* Engine::getEventEngine() {
    return Engine::getInstance()->eventEngine;
}

QScriptEngine* Engine::getScriptEngine() {
    return Engine::getInstance()->scriptEngine;
}

VideoEngine* Engine::getVideoEngine() {
    return Engine::getInstance()->videoEngine;
}

void Engine::setScriptEngine(QScriptEngine* engine) {
    Engine::getInstance()->scriptEngine = engine;
}

std::map<QString, Element*>& Engine::getElements() {
    return Engine::getInstance()->elements;
}

std::map<QString, Camera*>& Engine::getCameras() {
    return Engine::getInstance()->cameras;
}

std::vector<Video2D*>& Engine::getVideo2Ds() {
    return Engine::getInstance()->video2Ds;
}

std::vector<ShaderCallBack*>& Engine::getShaders() {
    return Engine::getInstance()->shaderCallBacks;
}

Element* Engine::getElt(QString name) {
    if (Engine::getInstance()->elements.find(name)
            != Engine::getInstance()->elements.end())
        return Engine::getInstance()->elements[name];
    else
        return NULL;
}

int Engine::getTimeSinceLastTick() {
    return Engine::getInstance()->timeSinceLastTick;
}

Etat Engine::getEtat() {
    return Engine::getInstance()->etat;
}

void Engine::setEtat(Etat etat) {
    Engine::getInstance()->etat = etat;
}

QString Engine::getDirMedia() {
    return Engine::getInstance()->dirMedias;
}

void Engine::setDirMedia(QString media) {
    Engine::getInstance()->dirMedias = media;
}

QString Engine::getNameMap() {
    return Engine::getInstance()->currentMap;
}

QString Engine::getCurrentMap() {
    return Engine::getInstance()->currentMap;
}

void Engine::setRealNextMap(QString nextMap) {
    Engine::getInstance()->nextMap = nextMap;
}

void Engine::configFog(tinyxml2::XMLElement* elem) {
    irr::video::SColor color(255, 255, 255, 255);
    QString type = "EFT_FOG_LINEAR";
    float start = 50.0f, end = 100.0f, density = 0.01f;
    bool pixelFog = false, rangeFog = false;

    if (elem->Attribute("alpha") && elem->Attribute("red")
            && elem->Attribute("green") && elem->Attribute("blue")) {
        color.setRed(elem->IntAttribute("red"));
        color.setGreen(elem->IntAttribute("green"));
        color.setBlue(elem->IntAttribute("blue"));
        color.setAlpha(elem->IntAttribute("alpha"));
    }

    if (elem->Attribute("type")) {
        type = elem->Attribute("type");
    }

    if (elem->Attribute("start")) {
        start = elem->FloatAttribute("start");
    }

    if (elem->Attribute("end")) {
        end = elem->FloatAttribute("end");
    }

    if (elem->Attribute("density")) {
        density = elem->FloatAttribute("density");
    }

    if (elem->Attribute("pixelFog")) {
        pixelFog = elem->BoolAttribute("pixelFog");
    }

    if (elem->Attribute("rangeFog")) {
        rangeFog = elem->BoolAttribute("rangeFog");
    }

    irr::video::IVideoDriver* driver = graphicsEngine->getDriver();
    driver->setFog(color, Convert::stringFogType(type), start, end,
                   density, pixelFog, rangeFog);
}

Mesh* Engine::createMesh(QString name, irr::core::vector3df pos,
                         irr::core::vector3df rot, irr::core::vector3df scale,
                         QString mesh, QString type, irr::core::vector3df size,
                         QString material, float masse) {
    Mesh* obj = new Mesh(name);
    if (obj->create(pos, rot, scale, mesh, type, size, material, masse)) {
        Engine::getInstance()->elements.insert(std::make_pair(name, obj));
    } else {
        delete obj;
        obj = NULL;
    }
    return obj;
}

Light* Engine::createLumiere(QString name, QString type, bool shadow,
                             irr::core::vector3df pos,
                             irr::core::vector3df rot, float outerCone,
                             float innerCone, float falloff, int ambiantA,
                             int ambiantR, int ambiantG, int ambiantB,
                             int diffuseA, int diffuseR, int diffuseG,
                             int diffuseB, int specularA, int specularR,
                             int specularG, int specularB,
                             float attenuationConst, float attenuationLin,
                             float attenuationQuad) {
    Light* obj = new Light(name);
    if (obj->create(type, shadow, pos, rot, outerCone, innerCone, falloff,
                    ambiantA, ambiantR, ambiantG, ambiantB, diffuseA, diffuseR,
                    diffuseG, diffuseB, specularA, specularR, specularG,
                    specularB, attenuationConst,
                    attenuationLin, attenuationQuad)) {
        Engine::getInstance()->elements.insert(std::make_pair(name, obj));
    } else {
        delete obj;
        obj = NULL;
    }
    return obj;
}
/*
Constraint* Engine::createConstraint(QString name, QString type,
                                     QString idFils, QString idParent,
                                     irr::core::vector3df pos,
                                     irr::core::vector3df pin,
                                     irr::core::vector3df pin2,
                                     bool collision, float stiffness) {
    Joint* obj = new Joint(name);
    if (obj->create(type, idFils, idParent, pos, pin,
                    pin2, collision, stiffness)) {
        Engine::getInstance()->elements.insert(std::make_pair(name, obj));
    } else {
        delete obj;
        obj = NULL;
    }
    return obj;
    return NULL;
}
*/
Particules* Engine::createParticules(QString name, irr::core::vector3df pos,
                                     irr::core::vector3df rot,
                                     irr::core::vector3df scale,
                                     irr::core::vector3df boxMin,
                                     irr::core::vector3df boxMax,
                                     irr::core::vector3df dir,
                                     int minParticlesPerSecond,
                                     int maxParticlesPerSecond,
                                     int lifeTimeMin, int lifeTimeMax,
                                     int maxAngleDegrees, int minStartColorA,
                                     int minStartColorR, int minStartColorG,
                                     int minStartColorB, int maxStartColorA,
                                     int maxStartColorR, int maxStartColorG,
                                     int maxStartColorB, float minStartWidth,
                                     float minStartHeight, float maxStartWidth,
                                     float maxStartHeight) {
    Particules* obj = new Particules(name);
    if (obj->create(pos, rot, scale, boxMin, boxMax, dir, minParticlesPerSecond,
                    maxParticlesPerSecond, lifeTimeMin, lifeTimeMax,
                    maxAngleDegrees, minStartColorA, minStartColorR,
                    minStartColorG, minStartColorB, maxStartColorA,
                    maxStartColorR, maxStartColorG, maxStartColorB,
                    minStartWidth, minStartHeight,
                    maxStartWidth, maxStartHeight)) {
        Engine::getInstance()->elements.insert(std::make_pair(name, obj));
    } else {
        delete obj;
        obj = NULL;
    }
    return obj;
}

Video2D* Engine::createVideo2D(QString name, irr::core::vector2df position,
                               irr::core::dimension2du scale, QString file,
                               bool repeatPlayback, bool startPlayback,
                               bool preloadIntoRAM) {
    Video2D* obj = new Video2D(name);
    if (obj->create(position, scale, file, repeatPlayback,
                    startPlayback, preloadIntoRAM)) {
        Engine::getInstance()->elements.insert(std::make_pair(name, obj));
    } else {
        delete obj;
        obj = NULL;
    }
    return obj;
}

QScriptValue Engine::createMesh(QString name, const QScriptValue &pos,
                                const QScriptValue &rot,
                                const QScriptValue &scale,
                                QString mesh, QString type,
                                const QScriptValue &size, QString material,
                                float masse) {
    irr::core::vector3df bufPos;
    bufPos.X = pos.property("X").toNumber();
    bufPos.Y = pos.property("Y").toNumber();
    bufPos.Z = pos.property("Z").toNumber();

    irr::core::vector3df bufRot;
    bufRot.X = rot.property("X").toNumber();
    bufRot.Y = rot.property("Y").toNumber();
    bufRot.Z = rot.property("Z").toNumber();

    irr::core::vector3df bufScale;
    bufScale.X = scale.property("X").toNumber();
    bufScale.Y = scale.property("Y").toNumber();
    bufScale.Z = scale.property("Z").toNumber();

    irr::core::vector3df bufSize;
    bufSize.X = size.property("X").toNumber();
    bufSize.Y = size.property("Y").toNumber();
    bufSize.Z = size.property("Z").toNumber();

    Mesh* obj = Engine::createMesh(name, bufPos, bufRot, bufScale, mesh,
                                   type, bufSize, material, masse);
    if (obj != NULL) {
        return scriptEngine->newQObject(obj);
    }
    return scriptEngine->nullValue();
}

QScriptValue Engine::createLumiere(QString name, QString type, bool shadow,
                                   const QScriptValue &pos,
                                   const QScriptValue &rot, float outerCone,
                                   float innerCone, float falloff, int ambiantA,
                                   int ambiantR, int ambiantG, int ambiantB,
                                   int diffuseA, int diffuseR, int diffuseG,
                                   int diffuseB, int specularA, int specularR,
                                   int specularG, int specularB,
                                   float attenuationConst, float attenuationLin,
                                   float attenuationQuad) {
    irr::core::vector3df bufPos;
    bufPos.X = pos.property("X").toNumber();
    bufPos.Y = pos.property("Y").toNumber();
    bufPos.Z = pos.property("Z").toNumber();

    irr::core::vector3df bufRot;
    bufRot.X = rot.property("X").toNumber();
    bufRot.Y = rot.property("Y").toNumber();
    bufRot.Z = rot.property("Z").toNumber();

    Light* obj = Engine::createLumiere(name, type, shadow, bufPos, bufRot,
                                       outerCone, innerCone, falloff, ambiantA,
                                       ambiantR, ambiantG, ambiantB, diffuseA,
                                       diffuseR, diffuseG, diffuseB, specularA,
                                       specularR, specularG, specularB,
                                       attenuationConst, attenuationLin,
                                       attenuationQuad);
    if (obj != NULL) {
        return scriptEngine->newQObject(obj);
    }
    return scriptEngine->nullValue();
}

QScriptValue Engine::createParticules(QString name, const QScriptValue &pos,
                                      const QScriptValue &rot,
                                      const QScriptValue &scale,
                                      const QScriptValue &boxMin,
                                      const QScriptValue &boxMax,
                                      const QScriptValue &dir,
                                      int minParticlesPerSecond,
                                      int maxParticlesPerSecond,
                                      int lifeTimeMin, int lifeTimeMax,
                                      int maxAngleDegrees, int minStartColorA,
                                      int minStartColorR, int minStartColorG,
                                      int minStartColorB, int maxStartColorA,
                                      int maxStartColorR, int maxStartColorG,
                                      int maxStartColorB, float minStartWidth,
                                      float minStartHeight, float maxStartWidth,
                                      float maxStartHeight) {
    irr::core::vector3df bufPos;
    bufPos.X = pos.property("X").toNumber();
    bufPos.Y = pos.property("Y").toNumber();
    bufPos.Z = pos.property("Z").toNumber();

    irr::core::vector3df bufRot;
    bufRot.X = rot.property("X").toNumber();
    bufRot.Y = rot.property("Y").toNumber();
    bufRot.Z = rot.property("Z").toNumber();

    irr::core::vector3df bufScale;
    bufScale.X = scale.property("X").toNumber();
    bufScale.Y = scale.property("Y").toNumber();
    bufScale.Z = scale.property("Z").toNumber();

    irr::core::vector3df bufBoxMin;
    bufBoxMin.X = boxMin.property("X").toNumber();
    bufBoxMin.Y = boxMin.property("Y").toNumber();
    bufBoxMin.Z = boxMin.property("Z").toNumber();

    irr::core::vector3df bufBoxMax;
    bufBoxMax.X = boxMax.property("X").toNumber();
    bufBoxMax.Y = boxMax.property("Y").toNumber();
    bufBoxMax.Z = boxMax.property("Z").toNumber();

    irr::core::vector3df bufDir;
    bufDir.X = dir.property("X").toNumber();
    bufDir.Y = dir.property("Y").toNumber();
    bufDir.Z = dir.property("Z").toNumber();

    Particules* obj = Engine::createParticules(name, bufPos, bufRot,
                                               bufScale, bufBoxMin, bufBoxMax,
                                               bufDir, minParticlesPerSecond,
                                               maxParticlesPerSecond,
                                               lifeTimeMin, lifeTimeMax,
                                               maxAngleDegrees, minStartColorA,
                                               minStartColorR, minStartColorG,
                                               minStartColorB, maxStartColorA,
                                               maxStartColorR, maxStartColorG,
                                               maxStartColorB, minStartWidth,
                                               minStartHeight, maxStartWidth,
                                               maxStartHeight);
    if (obj != NULL) {
        return scriptEngine->newQObject(obj);
    }
    return scriptEngine->nullValue();
}

QScriptValue Engine::createVideo2D(QString name, const QScriptValue &pos,
                                   const QScriptValue &scale, QString file,
                                   bool repeatPlayback, bool startPlayback,
                                   bool preloadIntoRAM) {
    irr::core::vector2df bufPos;
    bufPos.X = pos.property("X").toNumber();
    bufPos.Y = pos.property("Y").toNumber();

    irr::core::dimension2du bufScale;
    bufScale.Width = scale.property("Width").toNumber();
    bufScale.Height = scale.property("Height").toNumber();

    Video2D* obj = Engine::createVideo2D(name, bufPos, bufScale, file,
                                         repeatPlayback, startPlayback,
                                         preloadIntoRAM);
    if (obj != NULL) {
        return scriptEngine->newQObject(obj);
    }
    return scriptEngine->nullValue();
}

QScriptValue Engine::getElement(QString name) {
    Element* obj = Engine::getElt(name);
    if (obj != NULL) {
        return scriptEngine->newQObject(obj);
    }
    return scriptEngine->nullValue();
}

QScriptValue Engine::normalizeVector(const QScriptValue &pos) {
    irr::core::vector3df vect;
    vect.X = pos.property("X").toNumber();
    vect.Y = pos.property("Y").toNumber();
    vect.Z = pos.property("Z").toNumber();
    vect.normalize();

    QScriptValue ret = scriptEngine->newObject();
    ret.setProperty("X", vect.X);
    ret.setProperty("Y", vect.Y);
    ret.setProperty("Z", vect.Z);
    return ret;
}

float Engine::radToDeg(float rad) {
    return Convert::radToDeg(rad);
}

float Engine::degToRad(float deg) {
    return Convert::degToRad(deg);
}

QScriptValue Engine::cartesianToSpheric(const QScriptValue &cartesian) {
    irr::core::vector3df vect;
    vect.X = cartesian.property("X").toNumber();
    vect.Y = cartesian.property("Y").toNumber();
    vect.Z = cartesian.property("Z").toNumber();

    vect = Convert::cartesianToSpheric(vect);

    QScriptValue ret = scriptEngine->newObject();
    ret.setProperty("rho", vect.X);
    ret.setProperty("phi", vect.Y);
    ret.setProperty("theta", vect.Z);
    return ret;
}

QScriptValue Engine::sphericToCartesian(const QScriptValue &spheric) {
    irr::core::vector3df vect;
    vect.X = spheric.property("rho").toNumber();
    vect.Y = spheric.property("phi").toNumber();
    vect.Z = spheric.property("theta").toNumber();

    vect = Convert::sphericToCartesian(vect);

    QScriptValue ret = scriptEngine->newObject();
    ret.setProperty("X", vect.X);
    ret.setProperty("Y", vect.Y);
    ret.setProperty("Z", vect.Z);
    return ret;
}

QScriptValue Engine::crossProductVector(const QScriptValue &vect1,
                                        const QScriptValue &vect2) {
    irr::core::vector3df vec1;
    vec1.X = vect1.property("X").toNumber();
    vec1.Y = vect1.property("Y").toNumber();
    vec1.Z = vect1.property("Z").toNumber();

    irr::core::vector3df vec2;
    vec2.X = vect2.property("X").toNumber();
    vec2.Y = vect2.property("Y").toNumber();
    vec2.Z = vect2.property("Z").toNumber();

    irr::core::vector3df vec = vec1.crossProduct(vec2);

    QScriptValue ret = scriptEngine->newObject();
    ret.setProperty("X", vec.X);
    ret.setProperty("Y", vec.Y);
    ret.setProperty("Z", vec.Z);
    return ret;
}

QScriptValue Engine::rotateXZBy(float degrees, const QScriptValue &vect1,
                                const QScriptValue &vect2) {
    irr::core::vector3df vec1;
    vec1.X = vect1.property("X").toNumber();
    vec1.Y = vect1.property("Y").toNumber();
    vec1.Z = vect1.property("Z").toNumber();

    irr::core::vector3df vec2;
    vec2.X = vect2.property("X").toNumber();
    vec2.Y = vect2.property("Y").toNumber();
    vec2.Z = vect2.property("Z").toNumber();

    vec1.rotateXZBy(degrees, vec2);

    QScriptValue ret = scriptEngine->newObject();
    ret.setProperty("X", vec1.X);
    ret.setProperty("Y", vec1.Y);
    ret.setProperty("Z", vec1.Z);
    return ret;
}

QScriptValue Engine::rotateYZBy(float degrees, const QScriptValue &vect1,
                                const QScriptValue &vect2) {
    irr::core::vector3df vec1;
    vec1.X = vect1.property("X").toNumber();
    vec1.Y = vect1.property("Y").toNumber();
    vec1.Z = vect1.property("Z").toNumber();

    irr::core::vector3df vec2;
    vec2.X = vect2.property("X").toNumber();
    vec2.Y = vect2.property("Y").toNumber();
    vec2.Z = vect2.property("Z").toNumber();

    vec1.rotateYZBy(degrees, vec2);

    QScriptValue ret = scriptEngine->newObject();
    ret.setProperty("X", vec1.X);
    ret.setProperty("Y", vec1.Y);
    ret.setProperty("Z", vec1.Z);
    return ret;
}

QScriptValue Engine::rotateXYBy(float degrees, const QScriptValue &vect1,
                                const QScriptValue &vect2) {
    irr::core::vector3df vec1;
    vec1.X = vect1.property("X").toNumber();
    vec1.Y = vect1.property("Y").toNumber();
    vec1.Z = vect1.property("Z").toNumber();

    irr::core::vector3df vec2;
    vec2.X = vect2.property("X").toNumber();
    vec2.Y = vect2.property("Y").toNumber();
    vec2.Z = vect2.property("Z").toNumber();

    vec1.rotateXYBy(degrees, vec2);

    QScriptValue ret = scriptEngine->newObject();
    ret.setProperty("X", vec1.X);
    ret.setProperty("Y", vec1.Y);
    ret.setProperty("Z", vec1.Z);
    return ret;
}

void Engine::setEtat(QString etat) {
    Etat e = Convert::stringEtat(etat);
    if (e != noneState) {
        Engine::setEtat(e);
    }
}

void Engine::setNextMap(QString nextMap) {
    Engine::setRealNextMap(nextMap);
}

int Engine::random(int min, int max) {
    return rand() % (max - min) + min;
}

float Engine::frandom(float min, float max) {
    return (static_cast<float>(rand()) / static_cast<float>(RAND_MAX))
            * (max - min) + min;
}

void Engine::destroyElement(QString name) {
    destroyElement(getElt(name));
}

void Engine::startLogger(QString priority, QString logFile) {
    LOGGER_START(Convert::stringPriority(priority), logFile.toUtf8().data());
}

void Engine::stoptLogger() {
    LOGGER_STOP();
}

void Engine::log(QString priority, QString s) {
    LOGGER_WRITE(Convert::stringPriority(priority), s.toUtf8().data());
}

void Engine::setLogLevel(QString level) {
    LOGGER_SET_LEVEL(Convert::stringPriority(level));
}

void Engine::setSharedPool(QString name, QString value) {
    sharedPool[name] = value;
}

QString Engine::getSharedPoolVariable(QString name) {
    if (sharedPool.find(name) != sharedPool.end()) {
        return sharedPool.at(name);
    }
    return "";
}

QString Engine::readFile(QString name) {
    QFile file(name);
    if (!file.open(QIODevice::ReadOnly))
        return "";
    QString fileContent = file.readAll();
    file.close();
    return fileContent;
}

void Engine::writeFile(QString name, QString text) {
    QFile file(name);
    if (!file.open(QIODevice::WriteOnly))
        return;
    file.write(text.toUtf8());
    file.close();
}

QString Engine::readProjectFile() {
    QString fileContent = "";
    QString projFile = "Project.dat";
    QFileInfo fileProjInfo(dirProject + projFile);

    if (fileProjInfo.isFile()) {
        QFile file(dirProject + projFile);
        if (!file.open(QIODevice::ReadOnly))
            return "";
        fileContent = file.readAll();
        file.close();
    } else {
        projFile = "Project.marb";
        QFile file(dirProject + projFile);
        if (!file.open(QIODevice::ReadOnly))
            return "";
        fileContent = qUncompress(file.readAll());
        file.close();
    }
    return fileContent;
}


void Engine::writeProjectFile(QString text) {
    QByteArray fileContent = "";
    QString projFile = "Project.dat";
    QFileInfo fileProjInfo(dirProject + projFile);
    if (fileProjInfo.isFile()) {
        fileContent = text.toUtf8();
    } else {
        projFile = "Project.marb";
        fileContent = qCompress(text.toUtf8(), 1);
    }

    QFile file(dirProject + projFile);
    if (!file.open(QIODevice::WriteOnly)) {
        return;
    }
    file.write(fileContent);
    file.close();
}

}  // namespace MarbleEngine
