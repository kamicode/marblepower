/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Engine.h
 * \brief Brain of the Engine
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ENGINES_ENGINE_H_
#define MARBLE_ENGINES_ENGINE_H_

#include <map>
#include <vector>

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

class GraphicsEngine;
class GuiEngine;
class PhysicsEngine;
class MaterialPhys;
class SoundEngine;
class EventEngine;
class VideoEngine;

class Camera;
class Terrain;
class Mesh;
class Element;
class Light;
class Constraint;
class Particules;
class ShaderCallBack;
class Video2D;
class Sound;

/*! \class Engine
 * \brief
 *
 */
class Engine: public QObject {
    Q_OBJECT

    public:
        static Engine* getInstance();
        void loadProject(QString projectDir);
        void loadMap(QString mapName = "", bool unloadBefore = true);
        void unloadMap();
        void removeElement(QString name);
        void destroyElement(Element* obj);
        void launch();
        void destroyComplete();

    public:
        static std::map<QString, Camera*>& getCameras();
        static std::vector<Video2D*>& getVideo2Ds();
        static GraphicsEngine* getGraphicsEngine();
        static GuiEngine* getCeguiEngine();
        static PhysicsEngine* getPhysicsEngine();
        static MaterialPhys* getMaterialPhysEngine();
        static SoundEngine* getSoundEngine();
        static EventEngine* getEventEngine();
        static QScriptEngine* getScriptEngine();
        static VideoEngine* getVideoEngine();
        static void setScriptEngine(QScriptEngine* engine);
        static std::map<QString, Element*>& getElements();
        static Element* getElt(QString name);
        static std::vector<ShaderCallBack*>& getShaders();
        static int getTimeSinceLastTick();
        static Etat getEtat();
        static void setEtat(Etat etat);
        static QString getDirMedia();
        static void setDirMedia(QString media);
        static QString getNameMap();
        static QString getCurrentMap();
        static void setRealNextMap(QString nextMap);

        static Mesh* createMesh(QString name, irr::core::vector3df pos,
                                irr::core::vector3df rot,
                                irr::core::vector3df scale, QString mesh,
                                QString type, irr::core::vector3df size,
                                QString material, float masse);

        static Light* createLumiere(QString name, QString type, bool shadow,
                                    irr::core::vector3df pos,
                                    irr::core::vector3df rot, float outerCone,
                                    float innerCone, float falloff,
                                    int ambiantA, int ambiantR, int ambiantG,
                                    int ambiantB, int diffuseA, int diffuseR,
                                    int diffuseG, int diffuseB, int specularA,
                                    int specularR, int specularG,
                                    int specularB, float attenuationConst,
                                    float attenuationLin,
                                    float attenuationQuad);

        // static Constraint* createConstraint(QString name, QString type,
        //                                     QString idFils, QString idParent,
        //                                     irr::core::vector3df pos,
        //                                     irr::core::vector3df pin,
        //                                     irr::core::vector3df pin2,
        //                                     bool collision, float stiffness);

        static Particules* createParticules(QString name,
                                            irr::core::vector3df pos,
                                            irr::core::vector3df rot,
                                            irr::core::vector3df scale,
                                            irr::core::vector3df boxMin,
                                            irr::core::vector3df boxMax,
                                            irr::core::vector3df dir,
                                            int minParticlesPerSecond,
                                            int maxParticlesPerSecond,
                                            int lifeTimeMin, int lifeTimeMax,
                                            int maxAngleDegrees,
                                            int minStartColorA,
                                            int minStartColorR,
                                            int minStartColorG,
                                            int minStartColorB,
                                            int maxStartColorA,
                                            int maxStartColorR,
                                            int maxStartColorG,
                                            int maxStartColorB,
                                            float minStartWidth,
                                            float minStartHeight,
                                            float maxStartWidth,
                                            float maxStartHeight);

        static Video2D* createVideo2D(QString name,
                                      irr::core::vector2df position,
                                      irr::core::dimension2du scale,
                                      QString file, bool repeatPlayback,
                                      bool startPlayback, bool preloadIntoRAM);

    public:
        Q_INVOKABLE QScriptValue getElement(QString name);
        Q_INVOKABLE QScriptValue normalizeVector(const QScriptValue &pos);
        Q_INVOKABLE float radToDeg(float rad);
        Q_INVOKABLE float degToRad(float deg);
        Q_INVOKABLE QScriptValue cartesianToSpheric(
                const QScriptValue &cartesian);
        Q_INVOKABLE QScriptValue sphericToCartesian(
                const QScriptValue &spheric);
        Q_INVOKABLE QScriptValue crossProductVector(
                const QScriptValue &vect1, const QScriptValue &vect2);
        Q_INVOKABLE QScriptValue rotateXZBy(float degrees,
                                            const QScriptValue &vect1,
                                            const QScriptValue &vect2);
        Q_INVOKABLE QScriptValue rotateYZBy(float degrees,
                                            const QScriptValue &vect1,
                                            const QScriptValue &vect2);
        Q_INVOKABLE QScriptValue rotateXYBy(float degrees,
                                            const QScriptValue &vect1,
                                            const QScriptValue &vect2);
        Q_INVOKABLE void setEtat(QString etat);
        Q_INVOKABLE void setNextMap(QString nextMap);
        Q_INVOKABLE int random(int min = 0, int max = RAND_MAX);
        Q_INVOKABLE float frandom(float min = 0, float max = 1);

        Q_INVOKABLE QScriptValue createMesh(QString name,
                                            const QScriptValue &pos,
                                            const QScriptValue &rot,
                                            const QScriptValue &scale,
                                            QString mesh, QString type,
                                            const QScriptValue &size,
                                            QString material, float masse);

        Q_INVOKABLE QScriptValue createLumiere(QString name, QString type,
                                               bool shadow,
                                               const QScriptValue &pos,
                                               const QScriptValue &rot,
                                               float outerCone,
                                               float innerCone, float falloff,
                                               int ambiantA, int ambiantR,
                                               int ambiantG, int ambiantB,
                                               int diffuseA, int diffuseR,
                                               int diffuseG, int diffuseB,
                                               int specularA, int specularR,
                                               int specularG, int specularB,
                                               float attenuationConst,
                                               float attenuationLin,
                                               float attenuationQuad);

        // Q_INVOKABLE QScriptValue createConstraint(QString name,
        //                                           QString type,
        //                                           QString idFils,
        //                                           QString idParent,
        //                                           const QScriptValue &pos,
        //                                           const QScriptValue &pin,
        //                                           const QScriptValue &pin2,
        //                                           bool collision,
        //                                           float stiffness);

        Q_INVOKABLE QScriptValue createParticules(QString name,
                                                  const QScriptValue &pos,
                                                  const QScriptValue &rot,
                                                  const QScriptValue &scale,
                                                  const QScriptValue &boxMin,
                                                  const QScriptValue &boxMax,
                                                  const QScriptValue &dir,
                                                  int minParticlesPerSecond,
                                                  int maxParticlesPerSecond,
                                                  int lifeTimeMin,
                                                  int lifeTimeMax,
                                                  int maxAngleDegrees,
                                                  int minStartColorA,
                                                  int minStartColorR,
                                                  int minStartColorG,
                                                  int minStartColorB,
                                                  int maxStartColorA,
                                                  int maxStartColorR,
                                                  int maxStartColorG,
                                                  int maxStartColorB,
                                                  float minStartWidth,
                                                  float minStartHeight,
                                                  float maxStartWidth,
                                                  float maxStartHeight);

        Q_INVOKABLE QScriptValue createVideo2D(QString name,
                                               const QScriptValue &pos,
                                               const QScriptValue &scale,
                                               QString file,
                                               bool repeatPlayback,
                                               bool startPlayback,
                                               bool preloadIntoRAM);

        Q_INVOKABLE void destroyElement(QString name);
        Q_INVOKABLE void startLogger(QString priority, QString logFile);
        Q_INVOKABLE void stoptLogger();
        Q_INVOKABLE void log(QString priority, QString s);
        Q_INVOKABLE void setLogLevel(QString level);
        Q_INVOKABLE void setSharedPool(QString name, QString value);
        Q_INVOKABLE QString getSharedPoolVariable(QString name);
        Q_INVOKABLE QString readFile(QString name);
        Q_INVOKABLE void writeFile(QString name, QString text);
        Q_INVOKABLE QString readProjectFile();
        Q_INVOKABLE void writeProjectFile(QString text);

    private:
        explicit Engine();
        virtual ~Engine();
        void updateAll(float timeInMs);
        void configFog(tinyxml2::XMLElement* elem);
        void setSkyDome(tinyxml2::XMLElement* elem);
        void setSkyBox(tinyxml2::XMLElement* elem);
        void readXML(QString filname);

    private:
        static Engine* singleton;
        EventEngine* eventEngine;
        GuiEngine* ceguiEngine;
        GraphicsEngine* graphicsEngine;
        PhysicsEngine* physicsEngine;
        SoundEngine* soundEngine;
        MaterialPhys* materialPhysEngine;
        std::map<QString, Element*> elements;
        std::vector<ShaderCallBack*> shaderCallBacks;
        Etat etat;
        QString dirProject;
        QString dirMedias;
        QString currentMap;
        QString nextMap;
        std::map<QString, QString> sharedPool;
        VideoEngine* videoEngine;
        std::map<QString, Camera*> cameras;
        int timeSinceLastTick;
        std::vector<Video2D*> video2Ds;
        static QScriptEngine *scriptEngine;

    private:
        DISALLOW_COPY_AND_ASSIGN(Engine);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ENGINES_ENGINE_H_
