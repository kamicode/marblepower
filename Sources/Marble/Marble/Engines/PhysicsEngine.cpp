/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Engines/PhysicsEngine.h"

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/GraphicsEngine.h"
#include "Marble/Engines/GuiEngine.h"
#include "Marble/Elements/Mesh.h"

namespace MarbleEngine {

PhysicsEngine::PhysicsEngine() {
    nWorld = NULL;
    collisionConfiguration = NULL;
    dispatcher = NULL;
    broadPhase = NULL;
    constraintSolver = NULL;
}

PhysicsEngine::~PhysicsEngine() {
}

void PhysicsEngine::unLoadMap() {
    if (nWorld != NULL) {
        delete nWorld;
        delete constraintSolver;
        delete dispatcher;
        delete broadPhase;
        delete collisionConfiguration;
        nWorld = NULL;
    }
}

btDiscreteDynamicsWorld* PhysicsEngine::getWorld() {
    return nWorld;
}

void PhysicsEngine::init(tinyxml2::XMLElement* elem) {
    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);
    broadPhase = new btAxisSweep3(btVector3(elem->FloatAttribute("minX"),
                                            elem->FloatAttribute("minY"),
                                            elem->FloatAttribute("minZ")),
                                  btVector3(elem->FloatAttribute("maxX"),
                                            elem->FloatAttribute("maxY"),
                                            elem->FloatAttribute("maxZ")));
    constraintSolver = new btSequentialImpulseConstraintSolver();
    nWorld = new btDiscreteDynamicsWorld(dispatcher, broadPhase,
                                         constraintSolver,
                                         collisionConfiguration);

    nWorld->setGravity(btVector3(elem->FloatAttribute("gravityX"),
                                 elem->FloatAttribute("gravityY"),
                                 elem->FloatAttribute("gravityZ")));

    nWorld->setDebugDrawer(new IrrlichtDebugDrawer(
                               Engine::getGraphicsEngine()->getDriver()));
    nWorld->getDebugDrawer()->setDebugMode(btIDebugDraw::DBG_DrawWireframe
                                           | btIDebugDraw::DBG_FastWireframe);
}

void PhysicsEngine::update(float timeInMs) {
    if (nWorld != NULL) {
        nWorld->stepSimulation(timeInMs * 0.001f, 5);
    }
}

}  // namespace MarbleEngine
