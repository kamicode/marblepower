/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Engines/VideoAudioInterface.h"

#include "Marble/Divers.h"

namespace MarbleEngine {

VideoAudioInterface::VideoAudioInterface(TheoraVideoClip* owner,
                                         int nChannels, int freq)
    : TheoraAudioInterface(owner, nChannels, freq), TheoraTimer() {
    mMaxBuffSize = freq * mNumChannels * 2;
    mBuffSize = 0;
    mNumProcessedSamples = 0;
    mCurrentTimer = 0;

    mTempBuffer = new ALshort[mMaxBuffSize];
    alGenSources(1, &mSource);
    owner->setTimer(this);
    mNumPlayedSamples = 0;
}

void VideoAudioInterface::destroy() {
    if (mTempBuffer) {
        delete [] mTempBuffer;
    }

    if (mSource) {
        alSourcei(mSource, AL_BUFFER, NULL);
        alDeleteSources(1, &mSource);
    }
    while (!mBufferQueue.empty()) {
        alDeleteBuffers(1, &mBufferQueue.front().id);
        mBufferQueue.pop();
    }
}

VideoAudioInterface::~VideoAudioInterface() {
    destroy();
}

float VideoAudioInterface::getQueuedAudioSize() {
    return (static_cast<float>(
                mNumProcessedSamples - mNumPlayedSamples)) / mFreq;
}

void VideoAudioInterface::insertData(float* data, int nSamples) {
    for (int i = 0; i < nSamples; ++i) {
        if (mBuffSize < mMaxBuffSize) {
            mTempBuffer[mBuffSize++] = Convert::float2short(data[i]);
        }
        if (mBuffSize == mFreq * mNumChannels / 10) {
            OpenAL_Buffer buff;
            alGenBuffers(1, &buff.id);

            ALuint format = (mNumChannels == 1) ? AL_FORMAT_MONO16
                                                : AL_FORMAT_STEREO16;
            alBufferData(buff.id, format, mTempBuffer, mBuffSize * 2, mFreq);
            alSourceQueueBuffers(mSource, 1, &buff.id);
            buff.nSamples = mBuffSize / mNumChannels;
            mNumProcessedSamples += mBuffSize / mNumChannels;
            mBufferQueue.push(buff);

            mBuffSize = 0;

            int state;
            alGetSourcei(mSource, AL_SOURCE_STATE, &state);
            if (state != AL_PLAYING) {
                alSourcePlay(mSource);
            }
        }
    }
}

void VideoAudioInterface::update(float time_increase) {
    int i, nProcessed;
    OpenAL_Buffer buff;

    // process played buffers
    alGetSourcei(mSource, AL_BUFFERS_PROCESSED, &nProcessed);

    for (i = 0; i < nProcessed; ++i) {
        buff = mBufferQueue.front();
        mBufferQueue.pop();
        mNumPlayedSamples += buff.nSamples;
        alSourceUnqueueBuffers(mSource, 1, &buff.id);
        alDeleteBuffers(1, &buff.id);
    }
    if (nProcessed != 0) {
        // update offset
        alGetSourcef(mSource, AL_SEC_OFFSET, &mCurrentTimer);
    }

    // control playback and return time position
    mCurrentTimer += time_increase;

    mTime = mCurrentTimer + static_cast<float>(mNumPlayedSamples) / mFreq;

    float duration = mClip->getDuration();
    if (mTime > duration)
        mTime = duration;
}

void VideoAudioInterface::pause() {
    alSourcePause(mSource);
    TheoraTimer::pause();
}

void VideoAudioInterface::play() {
    alSourcePlay(mSource);
    TheoraTimer::play();
}

void VideoAudioInterface::seek(float time) {
    OpenAL_Buffer buff;

    alSourceStop(mSource);
    while (!mBufferQueue.empty()) {
        buff = mBufferQueue.front();
        mBufferQueue.pop();
        alSourceUnqueueBuffers(mSource, 1, &buff.id);
        alDeleteBuffers(1, &buff.id);
    }

    mBuffSize = 0;
    mCurrentTimer = 0;
    mNumProcessedSamples = static_cast<int>(time * mFreq);
    mNumPlayedSamples = mNumProcessedSamples;
    mTime = time;
}

ALuint VideoAudioInterface::getSource() {
    return mSource;
}

VideoAudioInterfaceFactory::VideoAudioInterfaceFactory() {
}

VideoAudioInterfaceFactory::~VideoAudioInterfaceFactory() {
}

VideoAudioInterface* VideoAudioInterfaceFactory::createInstance(
        TheoraVideoClip* owner, int nChannels, int freq) {
    return new VideoAudioInterface(owner , nChannels, freq);
}

}  // namespace MarbleEngine
