/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_GraphicsEngine.h
 * \brief 3D Graphic manager
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ENGINES_GRAPHICSENGINE_H_
#define MARBLE_ENGINES_GRAPHICSENGINE_H_

#include "Marble/Divers.h"

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class GraphicsEngine
 * \brief
 *
 */
class GraphicsEngine: private QObject {
    Q_OBJECT

    public:
        explicit GraphicsEngine();
        virtual ~GraphicsEngine();
        void init(tinyxml2::XMLElement* elem);
        irr::IrrlichtDevice* getDevice();
        irr::video::IVideoDriver* getDriver();
        irr::scene::ISceneManager* getSceneManager();
        void unLoadMap();
        void render();
        void lineRender();

    private:
        irr::IrrlichtDevice* device;
        irr::video::IVideoDriver* driver;
        irr::scene::ISceneManager* sceneManager;

    private:
        DISALLOW_COPY_AND_ASSIGN(GraphicsEngine);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ENGINES_GRAPHICSENGINE_H_
