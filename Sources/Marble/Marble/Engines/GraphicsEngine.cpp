/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "Marble/Engines/GraphicsEngine.h"

#include <map>
#include <vector>

#include "Marble/Engines/Engine.h"
#include "Marble/Engines/GuiEngine.h"
#include "Marble/Engines/PhysicsEngine.h"
#include "Marble/Elements/Element.h"
#include "Marble/Elements/Camera.h"
#include "Marble/Elements/Video2D.h"

namespace MarbleEngine {

GraphicsEngine::GraphicsEngine() {
    device = NULL;
    driver = NULL;
    sceneManager = NULL;
}

GraphicsEngine::~GraphicsEngine() {
    unLoadMap();
    device->drop();
    device = NULL;
}

void GraphicsEngine::unLoadMap() {
    sceneManager->clear();
}

void GraphicsEngine::init(tinyxml2::XMLElement* elem) {
    irr::SIrrlichtCreationParameters s = irr::SIrrlichtCreationParameters();

    s.DriverType = Convert::stringDriver(elem->Attribute("driverType"));
    s.Fullscreen = elem->BoolAttribute("fullscreen");
    s.Bits = elem->IntAttribute("bits");
    s.ZBufferBits = elem->IntAttribute("zBufferBits");
    s.Stencilbuffer = elem->BoolAttribute("stencilbuffer");
    s.Vsync = elem->BoolAttribute("vsync");
    s.AntiAlias = elem->IntAttribute("antiAlias");
    s.WithAlphaChannel = elem->BoolAttribute("withAlphaChannel");
    s.Doublebuffer = elem->BoolAttribute("doublebuffer");
    s.Stereobuffer = elem->BoolAttribute("stereobuffer");
    s.HighPrecisionFPU = elem->BoolAttribute("highPrecisionFPU");
    s.DriverMultithreaded = elem->BoolAttribute("driverMultithreaded");
    s.UsePerformanceTimer = elem->BoolAttribute("usePerformanceTimer");

    if (s.Fullscreen && elem->BoolAttribute("autoSize")) {
        irr::IrrlichtDevice* nulldevice =
                irr::createDevice(irr::video::EDT_NULL);
        irr::core::dimension2d<irr::u32> deskres =
                nulldevice->getVideoModeList()->getDesktopResolution();
        nulldevice->drop();
        s.WindowSize.Width = deskres.Width;
        s.WindowSize.Height = deskres.Height;
    } else {
        s.WindowSize.Width = elem->IntAttribute("width");
        s.WindowSize.Height = elem->IntAttribute("height");
    }

    device = irr::createDeviceEx(s);

    if (device == NULL) {
        exit(0);
    }

    driver = device->getVideoDriver();
    if (driver == NULL) {
        exit(0);
    }

    sceneManager = device->getSceneManager();
    if (sceneManager == NULL) {
        exit(0);
    }

    device->getCursorControl()->setVisible(false);
}

void GraphicsEngine::render() {
    irr::core::dimension2d<irr::u32> size = driver->getScreenSize();
    irr::u32 width = size.Width;
    irr::u32 height = size.Height;

    driver->beginScene(true, true, irr::video::SColor(0, 0, 0, 0));

    {
        driver->setViewPort(irr::core::rect<irr::s32>(0, 0, width, height));

        std::map<QString, Camera*>cameras = Engine::getCameras();
        std::map<QString, Camera*>::const_iterator it, end = cameras.end();
        for (it = cameras.begin(); it != end; ++it) {
            Camera* cam = it->second;
            sceneManager->setActiveCamera(cam->getCamera());
            driver->setViewPort(cam->getAbsoluteViewport());
            sceneManager->drawAll();

            lineRender();
        }
    }

    {
        driver->setViewPort(irr::core::rect<irr::s32>(0, 0, width, height));

        std::vector<Video2D*> videos = Engine::getVideo2Ds();
        std::vector<Video2D*>::const_iterator it, end = videos.end();
        for (it = videos.begin(); it != end; ++it) {
            (*it)->render();
        }
    }

    Engine::getCeguiEngine()->render();

    driver->endScene();
}

void GraphicsEngine::lineRender() {
    irr::core::matrix4 oldTransform =
            irr::core::matrix4(driver->getTransform(irr::video::ETS_WORLD));
    driver->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);

    irr::video::SMaterial oldMaterial =
            irr::video::SMaterial(driver->getMaterial2D());
    irr::video::SMaterial mat;
    mat.Lighting = false;
    mat.Thickness = 1;
    driver->setMaterial(mat);

    std::map<QString, Element*>elements = Engine::getElements();
    std::map<QString, Element*>::const_iterator it = elements.begin(),
            end = elements.end();
    for (it = elements.begin(); it != end; ++it) {
        it->second->lineRender();
    }

    Engine::getPhysicsEngine()->getWorld()->debugDrawWorld();

    driver->setMaterial(oldMaterial);
    driver->setTransform(irr::video::ETS_WORLD, oldTransform);
}

irr::IrrlichtDevice* GraphicsEngine::getDevice() {
    return device;
}

irr::video::IVideoDriver* GraphicsEngine::getDriver() {
    return driver;
}

irr::scene::ISceneManager* GraphicsEngine::getSceneManager() {
    return sceneManager;
}

}  // namespace MarbleEngine
