/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file EN_Logger.h
 * \brief User Log Manager
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLE_ENGINES_LOGGER_H_
#define MARBLE_ENGINES_LOGGER_H_

#include <string>

#include "Marble/Divers.h"

#define LOGGER_START(MIN_PRIORITY, FILE) MarbleEngine::Logger::Start( \
    MIN_PRIORITY, FILE)
#define LOGGER_STOP() MarbleEngine::Logger::Stop()
#define LOGGER_WRITE(PRIORITY, MESSAGE) MarbleEngine::Logger::Write( \
    PRIORITY, MESSAGE)
#define LOGGER_SET_LEVEL(PRIORITY) MarbleEngine::Logger::setLogLevel(PRIORITY)

#define LOG_Config MarbleEngine::Config
#define LOG_Info MarbleEngine::Info
#define LOG_Debug MarbleEngine::Debug
#define LOG_Warning MarbleEngine::Warning
#define LOG_Error MarbleEngine::Error

/*! \namespace MarbleEngine
 *
 * Namespace of the "Engine" part
 */
namespace MarbleEngine {

/*! \class Logger
 * \brief
 *
 */
class Logger : private CEGUI::Logger {
    public:
        virtual ~Logger();

        void logEvent(const CEGUI::String& message, CEGUI::LoggingLevel log);
        void setLogFilename(const CEGUI::String&, bool) { }

        static void Write(Priority priority, const std::string& message);
        static void Start(Priority minPriority, const std::string& logFile);
        static void Stop();
        static void setLogLevel(Priority level);
        static CEGUI::LoggingLevel loggerLevelToCeguiLevel(Priority level);
        static Priority ceguiLevelToLoggerLevel(CEGUI::LoggingLevel level);
        static irr::ELOG_LEVEL loggerLevelToIrrlichtLevel(Priority level);
        static Priority irrlichtLevelToLoggerLevel(irr::ELOG_LEVEL level);

    private:
        explicit Logger();

    private:
        bool active;
        std::ofstream fileStream;
        Priority minPriority;

        static const std::string PRIORITY_NAMES[];
        static Logger instance;

    private:
        DISALLOW_COPY_AND_ASSIGN(Logger);
};

}  // namespace MarbleEngine

#endif  // MARBLE_ENGINES_LOGGER_H_
