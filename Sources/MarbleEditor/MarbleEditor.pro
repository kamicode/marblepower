#Copyright(C) 2012 Tristan Kahn
#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU Lesser General Public
#License as published by the Free Software Foundation; either
#version 2.1 of the License, or(at your option) any later version.
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
#Lesser General Public License for more details.
#You should have received a copy of the GNU Lesser General Public
#License along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

TEMPLATE = app
LANGUAGE = C++
CONFIG -= console app_bundle
CONFIG *= ordered windows embed_manifest_exe

greaterThan(QT_MAJOR_VERSION, 4) {
    QT *= widgets
    CONFIG *= c++11
} else {
}

# BugFix QtCreator
debug_and_release {
    CONFIG -= debug_and_release
}

QMAKE_CXXFLAGS *= -Zc:wchar_t
QMAKE_CFLAGS_RELEASE *= -O2 -GL
QMAKE_LFLAGS_RELEASE *= /LTCG

# Target
DEPENDPATH *= "$$_PRO_FILE_PWD_"
INCLUDEPATH *= "$$_PRO_FILE_PWD_"
DESTDIR = "$$_PRO_FILE_PWD_/../../Build"

CONFIG(debug, debug|release) {
    message("MarbleEditor : Debug compilation")
    TARGET = MarbleEditor_d
    DEFINES *= _DEBUG _WIN32 ENABLE_LOGGER
    OBJECTS_DIR = "$$_PRO_FILE_PWD_/../../Obj/Debug/MarbleEditor/Obj"
    MOC_DIR = "$$_PRO_FILE_PWD_/../../Obj/Debug/MarbleEditor/Moc"
    RCC_DIR = "$$_PRO_FILE_PWD_/../../Obj/Debug/MarbleEditor/Rcc"
    UI_DIR = "$$_PRO_FILE_PWD_/../../Obj/Debug/MarbleEditor/Ui"
} else {
    message("MarbleEditor : Release compilation")
    TARGET = MarbleEditor
    DEFINES *= RELEASE NDEBUG _WIN32
    OBJECTS_DIR = "$$_PRO_FILE_PWD_/../../Obj/Release/MarbleEditor/Obj"
    MOC_DIR = "$$_PRO_FILE_PWD_/../../Obj/Release/MarbleEditor/Moc"
    RCC_DIR = "$$_PRO_FILE_PWD_/../../Obj/Release/MarbleEditor/Rcc"
    UI_DIR = "$$_PRO_FILE_PWD_/../../Obj/Release/MarbleEditor/Ui"
}

# Precompilation
PRECOMPILHEADER = "$$_PRO_FILE_PWD_/MarbleEditor/Precompiled.h"
DEFINES *= USING_PCH
CONFIG *= precompile_header

# Win32 Libs
LIBS *= User32.lib Advapi32.lib Gdi32.lib

# DirectX
LIBS *= -L"$$system(echo %DXSDK_DIR%)Lib/x86"

# Cg
LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/Cg-3.1/Lib"
LIBS *= cg.lib

# Irrlicht
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Include"
DEFINES *= _IRR_STATIC_LIB_
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Lib/Debug" -lIrrlicht
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Lib/Debug/Irrlicht.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Lib/Release" -lIrrlicht
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/irrlicht-Rev4560/Lib/Release/Irrlicht.lib"
}

# Bullet
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Include"
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Debug"
    LIBS *= -lBulletCollision_vs2010_debug -lBulletDynamics_vs2010_debug -lLinearMath_vs2010_debug
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Debug/BulletCollision_vs2010_debug.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Debug/BulletDynamics_vs2010_debug.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Debug/LinearMath_vs2010_debug.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Release"
    LIBS *= -lBulletCollision_vs2010 -lBulletDynamics_vs2010 -lLinearMath_vs2010
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Release/BulletCollision_vs2010.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Release/BulletDynamics_vs2010.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/bullet-2.81-rev2613/Lib/Release/LinearMath_vs2010.lib"
}

# TinyXml2
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Include"
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Lib/Debug"
    LIBS *= -ltinyxml2
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Lib/Debug/tinyxml2.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Lib/Release"
    LIBS *= -ltinyxml2
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/tinyxml2-Rev264/Lib/Release/tinyxml2.lib"
}

# CEGUI
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Include"
DEFINES *= CEGUI_STATIC
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug"
    LIBS *= -lCEGUIBase_Static_d -lCEGUIIrrlichtRenderer_Static_d -lCEGUIExpatParser_Static_d -lCEGUICoronaImageCodec_Static_d -lCEGUIDevILImageCodec_Static_d -lCEGUIFreeImageImageCodec_Static_d -lCEGUISILLYImageCodec_Static_d -lCEGUISTBImageCodec_Static_d -lCEGUITGAImageCodec_Static_d -lCEGUIFalagardWRBase_Static_d
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIBase_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIIrrlichtRenderer_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIExpatParser_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUICoronaImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIDevILImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIFreeImageImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUISILLYImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUISTBImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUITGAImageCodec_Static_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Debug/CEGUIFalagardWRBase_Static_d.lib"
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug"
    LIBS *= -lfreetype_d -lpcre_d -lexpat_d -lcorona_d
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug/freetype_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug/pcre_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug/expat_d.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Debug/corona_d.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release"
    LIBS *= -lCEGUIBase_Static -lCEGUIIrrlichtRenderer_Static -lCEGUIExpatParser_Static -lCEGUICoronaImageCodec_Static -lCEGUIDevILImageCodec_Static -lCEGUIFreeImageImageCodec_Static -lCEGUISILLYImageCodec_Static -lCEGUISTBImageCodec_Static -lCEGUITGAImageCodec_Static -lCEGUIFalagardWRBase_Static
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIBase_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIIrrlichtRenderer_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIExpatParser_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUICoronaImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIDevILImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIFreeImageImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUISILLYImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUISTBImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUITGAImageCodec_Static.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Lib/Release/CEGUIFalagardWRBase_Static.lib"
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release"
    LIBS *= -lfreetype -lpcre -lexpat -lcorona
    PRE_TARGETDEPS *= \
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release/freetype.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release/pcre.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release/expat.lib"
        "$$_PRO_FILE_PWD_/../../Bibliotheques/CEGUI-0.7.9/Dependencies/Lib/Release/corona.lib"
}

# QScintilla
INCLUDEPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/QScintilla-gpl-2.7.2/Include"
DEPENDPATH *= "$$_PRO_FILE_PWD_/../../Bibliotheques/QScintilla-gpl-2.7.2/Include"
CONFIG(debug, debug|release) {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/QScintilla-gpl-2.7.2/Lib/Debug"
    LIBS *= -lqscintilla2
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/QScintilla-gpl-2.7.2/Lib/Debug/qscintilla2.lib"
} else {
    LIBS *= -L"$$_PRO_FILE_PWD_/../../Bibliotheques/QScintilla-gpl-2.7.2/Lib/Release"
    LIBS *= -lqscintilla2
    PRE_TARGETDEPS *= "$$_PRO_FILE_PWD_/../../Bibliotheques/QScintilla-gpl-2.7.2/Lib/Release/qscintilla2.lib"
}

RESOURCES *= \
    "$$_PRO_FILE_PWD_/MarbleEditor/Resources.qrc" \

FORMS *= \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/Engine.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/PhysicEngine.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/ProjectChooser.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/Medias.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/Editor.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/HelpWindow.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/GuiEngine.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/FileChooser.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/ElementEditor.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/MapsManager.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Materials/Material3D.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Materials/MaterialPhys.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Fog.ui" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Sky.ui" \

SOURCES *= \
    "$$_PRO_FILE_PWD_/MarbleEditor/Main.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Divers.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/Engine.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/Logger.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/GraphicsEngine.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/PhysicsEngine.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/HelpWindow.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/GuiEngine.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/Medias.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/MapsManager.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/FileChooser.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/ProjectChooser.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Editors/ElementEditor.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Editors/ScriptEditor.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Editors/XMLEditor.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Materials/Material3D.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Materials/MaterialPhys.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Fog.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Sky.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Scripts.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Element.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Mesh.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Light.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Constraint.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Camera.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Terrain.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Particules.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Video2D.cpp" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Sound.cpp" \

HEADERS *= \
    "$$_PRO_FILE_PWD_/MarbleEditor/Precompiled.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Divers.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/Engine.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/Logger.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/GraphicsEngine.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/PhysicsEngine.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/HelpWindow.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/GuiEngine.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/Medias.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/MapsManager.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/FileChooser.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Engines/ProjectChooser.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Editors/ElementEditor.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Editors/ScriptEditor.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Editors/XMLEditor.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Materials/MaterialPhys.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Materials/Material3D.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Fog.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Sky.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Scripts.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Element.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Mesh.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Light.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Constraint.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Camera.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Terrain.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Particules.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Video2D.h" \
    "$$_PRO_FILE_PWD_/MarbleEditor/Elements/Sound.h" \
