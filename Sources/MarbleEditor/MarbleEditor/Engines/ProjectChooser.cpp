/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Engines/ProjectChooser.h"

#include "MarbleEditor/Engines/Engine.h"

namespace MarbleEditor {

ProjectChooser::ProjectChooser(Engine* main, QWidget* parent)
    : QDialog(parent) {
    setupUi(this);
    this->main = main;

    QFile fichier("RecentProjects");
    if (fichier.open(QIODevice::ReadOnly)) {
        QTextStream flux(&fichier);
        while (!flux.atEnd()) {
            QString name = flux.readLine();
            if (QFile::exists(name)) {
                QListWidgetItem* item = new QListWidgetItem(name);
                listWidget->addItem(item);
            }
        }
        fichier.close();
    } else if (fichier.open(QIODevice::WriteOnly)) {
        fichier.write("Sample");
        listWidget->addItem(new QListWidgetItem("Sample"));
    }

    connect(ouvrir, &QPushButton::clicked,
            this, &ProjectChooser::ouvrirProjetSel);

    connect(choisir, &QPushButton::clicked,
            this, &ProjectChooser::choisirProjet);

    connect(nouveau, &QPushButton::clicked,
            this, &ProjectChooser::nouveauProjet);

    connect(listWidget, &QListWidget::doubleClicked,
            this, &ProjectChooser::ouvrirProjetSel);
}

void ProjectChooser::ouvrirProjetSel() {
    QListWidgetItem* item = listWidget->currentItem();
    if (item != NULL) {
        ouvrirProjet(item->text());
    }
}

void ProjectChooser::ouvrirProjet(QString proj) {
    proj = QDir(proj).canonicalPath();
    if (QFile::exists(proj + "/Project.dat")) {
        QFile fichier("recentProjects");
        QString texte = proj + '\n';

        if (fichier.open(QIODevice::ReadOnly)) {
            QTextStream flux(&fichier);
            while (!flux.atEnd()) {
                QString folder = flux.readLine();
                folder = QDir(folder).canonicalPath();
                if (folder != proj && QFile::exists(
                            folder + "/Project.dat")) {
                    texte += folder + '\n';
                }
            }
            texte.resize(texte.size() - 1);  // remove last '\n'
            fichier.close();
        }

        fichier.open(QIODevice::WriteOnly);
        fichier.write(texte.toUtf8());
        fichier.close();

        hide();
        main->loadProject(proj);
    }
}

void ProjectChooser::nouveauProjet() {
    if (!nameNewProject->text().isEmpty()) {
        QString dir = QFileDialog::getExistingDirectory(this,
                                                        tr("Creer un projet"));
        if (!dir.isEmpty()) {
            QString projDir =  dir + "/" + nameNewProject->text();

            QDir d(projDir);
            if (d.exists()) {
                QString error = "Directory : \"" + projDir
                        + "\" already exist";
                QMessageBox::warning(this, tr("Warning"),
                                     tr(error.toUtf8().data()),
                                     QMessageBox::Ok);
                return;
            }

            Divers::copyFolder("Model", projDir);
            ouvrirProjet(projDir);
        }
    }
}

void ProjectChooser::choisirProjet() {
    QString folder = QFileDialog::getExistingDirectory(
                this, tr("Ouvrir un Projet existant"));
    if (!folder.isEmpty()) {
        ouvrirProjet(folder);
    }
}

}  // namespace MarbleEditor
