/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_GraphicsEngine.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ENGINES_GRAPHICSENGINE_H_
#define MARBLEEDITOR_ENGINES_GRAPHICSENGINE_H_

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class GraphicsEngine
 * \brief
 *
 *
 */
class GraphicsEngine : public QWidget {
    Q_OBJECT

    public:
        explicit GraphicsEngine(Engine *engine, QWidget* parent = 0);
        virtual ~GraphicsEngine();
        void unLoadMap();
        irr::IrrlichtDevice* getDevice();
        irr::scene::ISceneManager* getSceneManager();
        irr::video::IVideoDriver* getDriver();
        irr::scene::ICameraSceneNode* getCamera();
        void specificRender();

    public slots:
        void switchCamera();

    private:
        void timerEvent(QTimerEvent* /*event*/);
        void keyPressEvent(QKeyEvent* event);
        void keyReleaseEvent(QKeyEvent* event);
        void resizeEvent(QResizeEvent* event);
        void mousePressEvent(QMouseEvent* event);
        void mouseReleaseEvent(QMouseEvent* /*event*/);
        void mouseDoubleClickEvent(QMouseEvent* /*event*/);
        void mouseMoveEvent(QMouseEvent* event);
        void wheelEvent(QWheelEvent* event);
        QPaintEngine* paintEngine() const;

    private:
        irr::IrrlichtDevice* device;
        irr::video::IVideoDriver* driver;
        irr::scene::ISceneManager* sceneManager;
        CEGUI::IrrlichtRenderer* ceguiRenderer;

        irr::scene::ICameraSceneNode* camera;

        Engine* engine;

        QPoint pos;
        bool majPress, ctrlPress, upPress, downPress, leftPress, rightPress;
        int timerId;
        bool cameraEditor;

    private:
        DISALLOW_COPY_AND_ASSIGN(GraphicsEngine);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ENGINES_GRAPHICSENGINE_H_
