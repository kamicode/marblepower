/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_PhysicsEngine.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ENGINES_PHYSICSENGINE_H_
#define MARBLEEDITOR_ENGINES_PHYSICSENGINE_H_

#include <ui_PhysicEngine.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class PhysicsEngine
 * \brief
 *
 *
 */
class PhysicsEngine : public QDialog, private Ui::PhysicEngine {
    Q_OBJECT

    public:
        explicit PhysicsEngine(Engine* engine, QWidget* parent = 0);
        virtual ~PhysicsEngine();
        void unLoadMap();

        bool readXML(tinyxml2::XMLElement* elem);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

        void update(float timeInMs);

        bool isPhysicEnabled();

        btDiscreteDynamicsWorld* getWorld();

    private:
        Engine* engine;
        btDiscreteDynamicsWorld* nWorld;
        btDefaultCollisionConfiguration* collisionConfiguration;
        btDispatcher* dispatcher;
        btBroadphaseInterface* broadPhase;
        btConstraintSolver* constraintSolver;

    private:
        DISALLOW_COPY_AND_ASSIGN(PhysicsEngine);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ENGINES_PHYSICSENGINE_H_
