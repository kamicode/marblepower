/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Engine.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ENGINES_ENGINE_H_
#define MARBLEEDITOR_ENGINES_ENGINE_H_

#include <ui_Engine.h>

#include <map>

#include "MarbleEditor/Editors/ElementEditor.h"
#include "MarbleEditor/Engines/GraphicsEngine.h"
#include "MarbleEditor/Engines/PhysicsEngine.h"
#include "MarbleEditor/Engines/ProjectChooser.h"
#include "MarbleEditor/Engines/GuiEngine.h"
#include "MarbleEditor/Materials/MaterialPhys.h"
#include "MarbleEditor/Engines/Medias.h"
#include "MarbleEditor/Engines/MapsManager.h"

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Element;
class Camera;
class Fog;
class Sky;

/*! \class Engine
 * \brief
 *
 *
 */
class Engine : public QMainWindow, private Ui::Engine {
    Q_OBJECT

    public:
        explicit Engine(QWidget* parent = 0);
        virtual ~Engine();

        GraphicsEngine* getGraphicsEngine();
        PhysicsEngine* getPhysicsEngine();
        MaterialPhys* getMaterialPhysWidget();
        QAction* getActionMaterialPhysWidget();
        ElementEditor* getElementEditor();

        std::map<QString, Element*>& getElements();
        std::map<QString, Camera*>& getCameras();
        Element* getElement(QString name) const;
        Element* getSelectedElement() const;
        void setSelectedElement(Element* newSelect);

        QString getDirProj();
        QString getDirMedias();

        void closeEvent(QCloseEvent* event);

    public slots:
        void loadProject(QString proj);
        void loadCurrentMap();
        void loadMap(QString map);
        void show();
        bool unloadProject();
        void updateList();

    private slots:
        void searchProject();
        bool unloadMap();
        void enableGUI(bool enable);
        bool save(bool confirm = true);
        void destroyElement(QString elem);
        void supprSelect();
        void add_Element();
        void add_Mesh();
        void add_Light();
        void add_Particules();
        void add_Constraint();
        void add_Terrain();
        void add_Camera();
        void add_Video2D();
        void add_Sound();
        void copy();
        void past();
        void lance();
        void selectElement();
        void helpScript();
        void openMedias();
        void generateGame();

    private:
        bool readXML(tinyxml2::XMLDocument* doc);
        QString getNameGen(QString prefix);

    private:
        GraphicsEngine* graphicsEngine;
        PhysicsEngine* physicsEngine;
        GuiEngine* guiEngine;
        ProjectChooser* projectChooser;
        Fog* fogMap;
        Sky* skyMap;
        MaterialPhys* mat;
        Medias* medias;
        MapsManager* mapsManager;

        std::map<QString, Element*> elements;
        ElementEditor* elementEditor;
        std::map<QString, Camera*> cameras;

        Element* selectedElement;
        QString copyElement;
        QString dirProject;
        QString dirMedias;
        QString currentMap;
        bool mapLoaded;
        bool projectLoaded;

    private:
        DISALLOW_COPY_AND_ASSIGN(Engine);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ENGINES_ENGINE_H_
