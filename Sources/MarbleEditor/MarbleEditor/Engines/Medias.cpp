/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


#include "MarbleEditor/Engines/Medias.h"

#include <algorithm>

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Editors/XMLEditor.h"
#include "MarbleEditor/Editors/ScriptEditor.h"
#include "MarbleEditor/Elements/Element.h"

namespace MarbleEditor {

Medias::Medias(Engine *engine, QWidget *parent) : QDockWidget(parent) {
    setupUi(this);
    this->engine = engine;

    tabWidget->setEnabled(false);

    modeleModels = new QFileSystemModel(this);
    modeleModels->sort(0);
    modeleTextures = new QFileSystemModel(this);
    modeleTextures->sort(0);
    modeleMusics = new QFileSystemModel(this);
    modeleMusics->sort(0);
    modeleVideos = new QFileSystemModel(this);
    modeleVideos->sort(0);
    modeleGui = new QFileSystemModel(this);
    modeleGui->sort(0);
    modeleScripts = new QFileSystemModel(this);
    modeleScripts->sort(0);
    modeleShaders = new QFileSystemModel(this);
    modeleShaders->sort(0);

    connect(treeViewModels, &QTreeView::doubleClicked,
            this, &Medias::openModel);

    connect(pushAddModelFolder, &QPushButton::clicked,
            this, &Medias::addModelFolder);

    connect(pushAddExistingModelFile, &QPushButton::clicked,
            this, &Medias::addExistingModelFile);

    connect(treeViewTextures, &QTreeView::doubleClicked,
            this, &Medias::openTexture);

    connect(pushAddTextureFolder, &QPushButton::clicked,
            this, &Medias::addTextureFolder);

    connect(pushAddExistingTextureFile, &QPushButton::clicked,
            this, &Medias::addExistingTextureFile);

    connect(treeViewMusics, &QTreeView::doubleClicked,
            this, &Medias::openMusic);

    connect(pushAddMusicFolder, &QPushButton::clicked,
            this, &Medias::addMusicFolder);

    connect(pushAddExistingMusicFile, &QPushButton::clicked,
            this, &Medias::addExistingMusicFile);

    connect(treeViewVideos, &QTreeView::doubleClicked,
            this, &Medias::openVideo);

    connect(pushAddVideoFolder, &QPushButton::clicked,
            this, &Medias::addVideoFolder);

    connect(pushAddExistingVideoFile, &QPushButton::clicked,
            this, &Medias::addExistingVideoFile);

    connect(treeViewGui, &QTreeView::doubleClicked,
            this, &Medias::editGuiFile);

    connect(pushAddNewGuiFile, &QPushButton::clicked,
            this, &Medias::addNewGuiFile);

    connect(pushAddExistingGuiFile, &QPushButton::clicked,
            this, &Medias::addExistingGuiFile);

    connect(treeViewScripts, &QTreeView::doubleClicked,
            this, &Medias::editScriptFile);
    connect(pushAddScriptFolder, &QPushButton::clicked,
            this, &Medias::addScriptFolder);

    connect(pushAddNewScriptFile, &QPushButton::clicked,
            this, &Medias::addNewScriptFile);

    connect(pushAddExistingScriptFile, &QPushButton::clicked,
            this, &Medias::addExistingScriptFile);

    connect(treeViewShaders, &QTreeView::doubleClicked,
            this, &Medias::editShaderFile);

    connect(pushAddNewShader, &QPushButton::clicked,
            this, &Medias::addNewShader);
}

void Medias::loadProject() {
    tabWidget->setEnabled(true);

    QString directory = engine->getDirMedias();

    treeViewGui->setModel(modeleGui);
    modeleGui->setRootPath(directory + "Gui");
    treeViewGui->setRootIndex(
                modeleGui->index(directory + "Gui"));
    treeViewGui->resizeColumnToContents(1);

    treeViewModels->setModel(modeleModels);
    modeleModels->setRootPath(directory + "Models");
    treeViewModels->setRootIndex(
                modeleModels->index(directory + "Models"));
    treeViewModels->resizeColumnToContents(1);

    treeViewMusics->setModel(modeleMusics);
    modeleMusics->setRootPath(directory + "Musics");
    treeViewMusics->setRootIndex(
                modeleMusics->index(directory + "Musics"));
    treeViewMusics->resizeColumnToContents(1);

    treeViewVideos->setModel(modeleVideos);
    modeleVideos->setRootPath(directory + "Videos");
    treeViewVideos->setRootIndex(
                modeleVideos->index(directory + "Videos"));
    treeViewVideos->resizeColumnToContents(1);

    treeViewScripts->setModel(modeleScripts);
    modeleScripts->setRootPath(directory + "Scripts");
    treeViewScripts->setRootIndex(
                modeleScripts->index(directory + "Scripts"));
    treeViewScripts->resizeColumnToContents(1);

    treeViewShaders->setModel(modeleShaders);
    modeleShaders->setRootPath(directory + "Shaders");
    treeViewShaders->setRootIndex(
                modeleShaders->index(directory + "Shaders"));
    treeViewShaders->resizeColumnToContents(1);

    treeViewTextures->setModel(modeleTextures);
    modeleTextures->setRootPath(directory + "Textures");
    treeViewTextures->setRootIndex(
                modeleTextures->index(directory + "Textures"));
    treeViewTextures->resizeColumnToContents(1);
}

void Medias::unloadProject() {
    treeViewGui->setModel(NULL);
    treeViewModels->setModel(NULL);
    treeViewMusics->setModel(NULL);
    treeViewVideos->setModel(NULL);
    treeViewScripts->setModel(NULL);
    treeViewShaders->setModel(NULL);
    treeViewTextures->setModel(NULL);

    tabWidget->setEnabled(false);
}

void Medias::addNewGuiFile() {
    addNewFile(treeViewGui, modeleGui);
}

void Medias::addExistingGuiFile() {
    addExistingFile(treeViewGui, modeleGui);
}

void Medias::editGuiFile() {
    editFileXML(treeViewGui, modeleGui);
}

void Medias::addModelFolder() {
    addFolder(treeViewModels, modeleModels);
}

void Medias::addExistingModelFile() {
    addExistingFile(treeViewModels, modeleModels);
}

void Medias::addMusicFolder() {
    addFolder(treeViewMusics, modeleMusics);
}

void Medias::addExistingMusicFile() {
    addExistingFile(treeViewVideos, modeleVideos);
}

void Medias::addVideoFolder() {
    addFolder(treeViewVideos, modeleVideos);
}

void Medias::addExistingVideoFile() {
    addExistingFile(treeViewMusics, modeleMusics);
}

void Medias::addScriptFolder() {
    addFolder(treeViewScripts, modeleScripts);
}

void Medias::addNewScriptFile() {
    addNewFile(treeViewScripts, modeleScripts);
}

void Medias::addExistingScriptFile() {
    addExistingFile(treeViewScripts, modeleScripts);
}

void Medias::editScriptFile() {
    editFileScript(treeViewScripts, modeleScripts);
}

void Medias::addTextureFolder() {
    addFolder(treeViewTextures, modeleTextures);
}

void Medias::addExistingTextureFile() {
    addExistingFile(treeViewTextures, modeleTextures);
}

void Medias::addNewShader() {
    bool ok;
    QString text = QInputDialog::getText(
                engine, tr("Add Folder"),
                tr("Folder Name : "), QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) {
        QString root = modeleShaders->rootPath();
        QDir d(root);
        if (d.mkdir(text)) {
            root.append("/" + text);

            QFile::copy("Gismos/Shader/Constants.shad",
                        root + "/Constants.shad");
            QFile::copy("Gismos/Shader/Shader.cg", root + "/Shader.cg");

            Element* elt = engine->getSelectedElement();
            if (elt) {
                elt->updateElementEditor();
            }

        } else {
            QMessageBox::warning(engine, tr("Warning"),
                                 tr("Shader Already exist"), QMessageBox::Ok);
        }
    }
}

void Medias::editShaderFile() {
    editFileScript(treeViewShaders, modeleShaders);
}

void Medias::editFileXML(QTreeView *view, QFileSystemModel *model) {
    QString select = model->filePath(view->currentIndex());
    if (QFileInfo(select).isFile()) {
        XMLEditor* editeur = new XMLEditor();
        editeur->load(select);
    }
}

void Medias::editFileScript(QTreeView* view, QFileSystemModel* model) {
    QString select = model->filePath(view->currentIndex());
    if (QFileInfo(select).isFile()) {
        ScriptEditor* editeur = new ScriptEditor(NULL, true, false);
        editeur->load(select);
    }
}

QString Medias::getCurrentViewFolder(QTreeView* view,
                                     QFileSystemModel* model) {
    QString select = model->filePath(view->currentIndex());
    if (select.isEmpty()) {
        select = model->rootPath();
    }
    QFileInfo f(select);
    if (f.isFile()) {
        select = f.canonicalPath();
    }
    return select;
}

void Medias::addFolder(QTreeView* view, QFileSystemModel* model) {
    bool ok;
    QString text = QInputDialog::getText(engine, tr("Add Folder"),
                                         tr("Folder Name : "),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) {
        QDir(getCurrentViewFolder(view, model)).mkdir(text);
    }
}

void Medias::addNewFile(QTreeView* view, QFileSystemModel* model) {
    bool ok;
    QString text = QInputDialog::getText(engine, tr("Add File"),
                                         tr("File Name : "),
                                         QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty()) {
        QFile fichier(getCurrentViewFolder(view, model) + "/" + text);
        if (fichier.exists()) {
            QMessageBox::warning(engine, tr("Warning"),
                                 tr("File Already exist"),
                                 QMessageBox::Ok);
        } else {
            fichier.open(QIODevice::WriteOnly);
            fichier.close();
            Element* elt = engine->getSelectedElement();
            if (elt) {
                elt->updateElementEditor();
            }
        }
    }
}

void Medias::addExistingFile(QTreeView* view, QFileSystemModel* model) {
    QString text = QFileDialog::getOpenFileName(engine, "Media", "Medias");
    if (!text.isEmpty()) {
        QString select = getCurrentViewFolder(view, model)
                + "/" + QFileInfo(text).fileName();
        QFile fichier(select);
        if (fichier.exists()) {
            QMessageBox::warning(engine, tr("Warning"),
                                 tr("File Already exist"), QMessageBox::Ok);
        } else {
            QFile::copy(text, select);
            Element* elt = engine->getSelectedElement();
            if (elt) {
                elt->updateElementEditor();
            }
        }
    }
}

void Medias::openModel() {
    QString select = modeleModels->filePath(treeViewModels->currentIndex());
    if (QFileInfo(select).isFile()) {
        QDesktopServices::openUrl(QUrl(QString("file:///") + select,
                                       QUrl::TolerantMode));
    }
}

void Medias::openMusic() {
    QString select = modeleMusics->filePath(treeViewMusics->currentIndex());
    if (QFileInfo(select).isFile()) {
        QDesktopServices::openUrl(QUrl(QString("file:///") + select,
                                       QUrl::TolerantMode));
    }
}

void Medias::openVideo() {
    QString select = modeleVideos->filePath(treeViewVideos->currentIndex());
    if (QFileInfo(select).isFile()) {
        QDesktopServices::openUrl(QUrl(QString("file:///") + select,
                                       QUrl::TolerantMode));
    }
}

void Medias::openTexture() {
    QString select = modeleTextures->filePath(treeViewTextures->currentIndex());
    if (QFileInfo(select).isFile()) {
        QDesktopServices::openUrl(QUrl(QString("file:///") + select,
                                       QUrl::TolerantMode));
    }
}

}  // namespace MarbleEditor
