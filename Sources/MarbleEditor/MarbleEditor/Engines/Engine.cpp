/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


#include "MarbleEditor/Engines/Engine.h"

#include <map>
#include <algorithm>

#include "MarbleEditor/Engines/FileChooser.h"
#include "MarbleEditor/Engines/HelpWindow.h"
#include "MarbleEditor/Engines/Logger.h"
#include "MarbleEditor/Materials/MaterialPhys.h"
#include "MarbleEditor/Elements/Fog.h"
#include "MarbleEditor/Elements/Sky.h"
#include "MarbleEditor/Elements/Element.h"
#include "MarbleEditor/Elements/Mesh.h"
#include "MarbleEditor/Elements/Light.h"
#include "MarbleEditor/Elements/Particules.h"
#include "MarbleEditor/Elements/Constraint.h"
#include "MarbleEditor/Elements/Terrain.h"
#include "MarbleEditor/Elements/Camera.h"
#include "MarbleEditor/Elements/Video2D.h"
#include "MarbleEditor/Elements/Sound.h"

namespace MarbleEditor {

Engine::Engine(QWidget* parent) : QMainWindow(parent) {
    setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);

    selectedElement = NULL;
    copyElement = "";
    dirProject = "";
    dirMedias = "";
    currentMap = "";
    mapLoaded = false;
    projectLoaded = false;


    graphicsEngine = new GraphicsEngine(this, this);
    setCentralWidget(graphicsEngine);

    physicsEngine = new PhysicsEngine(this);
    guiEngine = new GuiEngine(this);
    projectChooser = new ProjectChooser(this, this);
    mapsManager = new MapsManager(this, this);

    elementEditor = new ElementEditor(this);
    addDockWidget(Qt::RightDockWidgetArea, elementEditor);

    medias = new Medias(this);
    addDockWidget(Qt::BottomDockWidgetArea, medias);
    medias->hide();
    medias->setFloating(true);

    mat = new MaterialPhys(this);
    fogMap = new Fog(this);
    skyMap = new Sky(this);

#ifdef ENABLE_LOGGER
    LOGGER_START(LOG_Debug, "DebugEditor.log", this);
#endif

    connect(actionOuvrirProjet, &QAction::triggered,
            this, &Engine::searchProject);

    connect(actionSwitchCameras, &QAction::triggered,
            graphicsEngine, &GraphicsEngine::switchCamera);

    connect(actionOpen_Medias, &QAction::triggered,
            this, &Engine::openMedias);

    connect(actionSupprimer, &QAction::triggered,
            this, &Engine::supprSelect);

    connect(elementEditor->deleteElementButton, &QPushButton::clicked,
            this, &Engine::supprSelect);

    connect(actionSauver, &QAction::triggered,
            this, &Engine::save);

    connect(actionReglageGeneral, &QAction::triggered,
            physicsEngine, &PhysicsEngine::show);

    connect(actionFog, &QAction::triggered,
            fogMap, &Fog::show);

    connect(actionSky, &QAction::triggered,
            skyMap, &Sky::show);

    connect(actionMateriaux, &QAction::triggered,
            mat, &MaterialPhys::show);

    connect(actionGUI, &QAction::triggered,
            guiEngine, &GuiEngine::show);

    connect(actionElement, &QAction::triggered,
            this, &Engine::add_Element);

    connect(actionMesh, &QAction::triggered,
            this, &Engine::add_Mesh);

    connect(actionLumiere, &QAction::triggered,
            this, &Engine::add_Light);

    connect(actionParticules, &QAction::triggered,
            this, &Engine::add_Particules);

    connect(actionContrainte, &QAction::triggered,
            this, &Engine::add_Constraint);

    connect(actionTerrain, &QAction::triggered,
            this, &Engine::add_Terrain);

    connect(actionCamera, &QAction::triggered,
            this, &Engine::add_Camera);

    connect(actionVideos, &QAction::triggered,
            this, &Engine::add_Video2D);

    connect(actionSound, &QAction::triggered,
            this, &Engine::add_Sound);

    connect(actionCopier, &QAction::triggered,
            this, &Engine::copy);

    connect(actionColler, &QAction::triggered,
            this, &Engine::past);

    connect(actionLancer, &QAction::triggered,
            this, &Engine::lance);

    connect(actionA_propos, &QAction::triggered,
            &QApplication::aboutQt);

    connect(actionScriptHelp, &QAction::triggered,
            this, &Engine::helpScript);

    connect(mapsList, &QListWidget::doubleClicked,
            this, &Engine::loadCurrentMap);

    connect(actionGenerer, &QAction::triggered,
            this, &Engine::generateGame);

    connect(ElementsList,
            static_cast<void (QListWidget::*)
                              (QListWidgetItem*,
                               QListWidgetItem*)>
                              (&QListWidget::currentItemChanged),
            this, &Engine::selectElement);

    connect(manageMapsButton, &QPushButton::clicked,
            mapsManager, &MapsManager::loadProject);

    enableGUI(false);

    setWindowState(Qt::WindowMaximized);


    QSettings settings("setup.ini", QSettings::IniFormat);
    settings.beginGroup("MainWindow");
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());
    resize(settings.value("size", QSize(400, 400)).toSize());
    move(settings.value("pos", QPoint(200, 200)).toPoint());
    settings.endGroup();
}

Engine::~Engine() {
    delete mat;
    delete fogMap;
    delete skyMap;

    delete guiEngine;
    delete projectChooser;
    delete physicsEngine;
    delete graphicsEngine;
    delete mapsManager;

    LOGGER_STOP();
}

GraphicsEngine* Engine::getGraphicsEngine() {
    return graphicsEngine;
}

PhysicsEngine* Engine::getPhysicsEngine() {
    return physicsEngine;
}

MaterialPhys* Engine::getMaterialPhysWidget() {
    return mat;
}

QAction* Engine::getActionMaterialPhysWidget() {
    return actionMateriaux;
}

ElementEditor* Engine::getElementEditor() {
    return elementEditor;
}

void Engine::enableGUI(bool enable) {
    graphicsEngine->setEnabled(enable);
    actionMesh->setEnabled(enable);
    actionElement->setEnabled(enable);
    actionTerrain->setEnabled(enable);
    actionSauver->setEnabled(enable);
    actionFog->setEnabled(enable);
    actionSky->setEnabled(enable);
    actionGUI->setEnabled(enable);
    actionVideos->setEnabled(enable);
    actionOpen_Medias->setEnabled(enable);
    actionLancer->setEnabled(enable);
    actionGenerer->setEnabled(enable);
    actionSupprimer->setEnabled(enable);
    actionColler->setEnabled(enable);
    actionLumiere->setEnabled(enable);
    actionParticules->setEnabled(enable);
    actionCamera->setEnabled(enable);
    actionContrainte->setEnabled(enable);
    actionCopier->setEnabled(enable);
    actionSwitchCameras->setEnabled(enable);

    actionReglageGeneral->setEnabled(enable);
    actionMateriaux->setEnabled(physicsEngine->isPhysicEnabled());
}

void Engine::searchProject() {
    projectChooser->exec();
}

bool Engine::unloadProject() {
    if (!unloadMap()) {
        return false;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    manageMapsButton->setEnabled(false);

    dirProject = "";
    dirMedias = "";
    medias->unloadProject();

    enableGUI(false);
    mapsList->clear();

    projectLoaded = false;

    QApplication::restoreOverrideCursor();

    return true;
}

void Engine::loadProject(QString proj) {
    if (projectLoaded) {
        if (!unloadProject()) {
            return;
        }
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    dirProject = QDir(proj).canonicalPath() + "/";

    if (dirProject != "") {
        tinyxml2::XMLDocument xmlDoc;
        QString file = dirProject + "Project.dat";
        if (xmlDoc.LoadFile(file.toUtf8().data()) != tinyxml2::XML_NO_ERROR) {
            dirProject = "";
            dirMedias = "";
            return;
        }

        tinyxml2::XMLHandle hdl(xmlDoc);
        tinyxml2::XMLElement* elem =
                hdl.FirstChildElement().FirstChildElement().ToElement();

        if (elem == NULL) {
            dirProject = "";
            dirMedias = "";
            return;
        }

        while (elem != NULL) {
            if (QString(elem->Value()) == "Ressources") {
                dirMedias = dirProject + elem->Attribute("medias");
            }
            if (QString(elem->Value()) == "Map") {
                QString name = elem->Attribute("name");
                mapsList->addItem(name);
            }
            elem = elem->NextSiblingElement();
        }
        medias->loadProject();

        manageMapsButton->setEnabled(true);

        projectLoaded = true;
    }

    QApplication::restoreOverrideCursor();
}

bool Engine::unloadMap() {
    if (mapLoaded) {
        if (!save(true)) {
            return false;
        }
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    setSelectedElement(NULL);
    copyElement = "";

    {
        std::map<QString, Element*>::const_iterator it = elements.begin(),
                end = elements.end();
        while (it != end) {
            destroyElement(it->second->getName());
            it = elements.begin();
        }
    }

    elements.clear();
    cameras.clear();

    skyMap->unloadMap();
    physicsEngine->unLoadMap();
    mat->unLoadMap();
    graphicsEngine->unLoadMap();
    guiEngine->unloadMap();

    currentMap = "";

    mapLoaded = false;

    QApplication::restoreOverrideCursor();

    return true;
}

void Engine::loadCurrentMap() {
    loadMap("");
}

void Engine::loadMap(QString map) {
    if (!unloadMap()) {
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    if (!map.isEmpty()) {
        mapsList->setCurrentItem(
                    mapsList->findItems(map, Qt::MatchExactly).at((0)));
    }

    map = dirProject + mapsList->currentItem()->text() + ".dat";
    tinyxml2::XMLDocument xmlDoc;
    if (xmlDoc.LoadFile(map.toUtf8().data()) != tinyxml2::XML_NO_ERROR) {
        return;
    }

    readXML(&xmlDoc);
    guiEngine->setGUI();
    currentMap = map;
    mapLoaded = true;
    enableGUI(true);

    QApplication::restoreOverrideCursor();
}

bool Engine::readXML(tinyxml2::XMLDocument* doc) {
    tinyxml2::XMLElement* elem = doc->FirstChildElement()->FirstChildElement();

    if (elem == NULL) {
        return false;
    }

    while (elem != NULL) {
        if (QString(elem->Value()) == "Gui") {
            guiEngine->readXML(elem);
        } else if (QString(elem->Value()) == "SkyDome"
                   || QString(elem->Value()) == "SkyBox") {
            skyMap->readXML(elem);
        } else if (QString(elem->Value()) == "Fog") {
            fogMap->readXML(elem);
        } else if (QString(elem->Value()) == "Material"
                   || QString(elem->Value()) == "ConfigMaterial") {
            mat->readXML(elem);
        } else if (QString(elem->Value()) == "Physic") {
            physicsEngine->readXML(elem);
        } else if (QString(elem->Value()) == "Element") {
            QString name = elem->Attribute("name");
            if (elements.find(name) != elements.end()) {
                name = getNameGen("Element");
            }
            Element* elt = new Element(name, this);
            if (elt->readXML(elem)) {
                elements.insert(std::make_pair(name, elt));
            }
        } else if (QString(elem->Value()) == "Mesh") {
            QString name = elem->Attribute("name");
            if (elements.find(name) != elements.end()) {
                name = getNameGen("Mesh");
            }
            Mesh* obj = new Mesh(name, this);
            if (obj->readXML(elem)) {
                elements.insert(std::make_pair(name, obj));
            }
        } else if (QString(elem->Value()) == "Light") {
            QString name = elem->Attribute("name");
            if (elements.find(name) != elements.end()) {
                name = getNameGen("Light");
            }
            Light* obj = new Light(name, this);
            if (obj->readXML(elem)) {
                elements.insert(std::make_pair(name, obj));
            }
        } else if (QString(elem->Value()) == "Particules") {
            QString name = elem->Attribute("name");
            if (elements.find(name) != elements.end()) {
                name = getNameGen("Particules");
            }
            Particules* obj = new Particules(name, this);
            if (obj->readXML(elem)) {
                elements.insert(std::make_pair(name, obj));
            }
        } else if (QString(elem->Value()) == "Constraint") {
            QString name = elem->Attribute("name");
            if (elements.find(name) != elements.end()) {
                name = getNameGen("Constraint");
            }
            Constraint* obj = new Constraint(name, this);
            if (obj->readXML(elem)) {
                elements.insert(std::make_pair(name, obj));
            }
        } else if (QString(elem->Value()) == "Terrain") {
            QString name = elem->Attribute("name");
            if (elements.find(name) != elements.end()) {
                name = getNameGen("Terrain");
            }
            Terrain* obj = new Terrain(name, this);
            if (obj->readXML(elem)) {
                elements.insert(std::make_pair(name, obj));
            }
        } else if (QString(elem->Value()) == "Video2D") {
            QString name = elem->Attribute("name");
            if (elements.find(name) != elements.end()) {
                name = getNameGen("Video2D");
            }
            Video2D* obj = new Video2D(name, this);
            if (obj->readXML(elem)) {
                elements.insert(std::make_pair(name, obj));
            }
        } else if (QString(elem->Value()) == "Sound") {
            QString name = elem->Attribute("name");
            if (elements.find(name) != elements.end()) {
                name = getNameGen("Sound");
            }
            Sound* obj = new Sound(name, this);
            if (obj->readXML(elem)) {
                elements.insert(std::make_pair(name, obj));
            }
        } else if (QString(elem->Value()) == "Camera") {
            QString name = elem->Attribute("name");
            if (elements.find(name) != elements.end()) {
                name = getNameGen("Camera");
            }
            Camera* obj = new Camera(name, this);
            if (obj->readXML(elem)) {
                elements.insert(std::make_pair(name, obj));
                cameras.insert(std::make_pair(name, obj));
            }
        }
        elem = elem->NextSiblingElement();
    }

    updateList();
    return true;
}

void Engine::helpScript() {
    HelpWindow* help = new HelpWindow();
    help->show();
}

void Engine::show() {
    QMainWindow::show();
    if (graphicsEngine)
        graphicsEngine->resize(graphicsEngine->size());
    if (guiEngine)
        guiEngine->setGUI();
}

void Engine::generateGame() {
    QString dir = QFileDialog::getExistingDirectory(
                this, tr("Generer le Jeu"));
    QApplication::processEvents();
    if (!dir.isEmpty()) {
        QApplication::setOverrideCursor(Qt::WaitCursor);

        QDir d(dirProject);
        QString projName = d.dirName();
        QString projDir =  dir + "/" + projName + "/";

        QDir d2(projDir);

        if (d2.exists()) {
            QApplication::restoreOverrideCursor();
            QString error = "Directory : \"" + projDir + "\" already exist";
            QMessageBox::warning(this, tr("Warning"),
                                 tr(error.toUtf8().data()), QMessageBox::Ok);
            return;
        }

        d2.mkpath(projDir);

        {
            // Create Project.marb
            QFile fileDat(dirProject + "Project.dat");
            if (!fileDat.open(QIODevice::ReadOnly)) {
                QApplication::restoreOverrideCursor();
                d2.rmdir(dirProject);
                QString error = "The file : \"" + dirProject
                        + "Project.dat\" is corrupt";
                QMessageBox::warning(this, tr("Warning"),
                                     tr(error.toUtf8().data()),
                                     QMessageBox::Ok);
                return;
            }
            QFile fileMarb(projDir + "Project.marb");
            if (!fileMarb.open(QIODevice::WriteOnly)) {
                QApplication::restoreOverrideCursor();
                d2.rmdir(dirProject);
                QString error = "Impossible to write : \""
                        + projDir + "Project.marb\"";
                QMessageBox::warning(this, tr("Warning"),
                                     tr(error.toUtf8().data()),
                                     QMessageBox::Ok);
                return;
            }
            fileMarb.write(qCompress(fileDat.readAll(), 1));
            fileDat.close();
            fileMarb.close();
        }
        QApplication::processEvents();

        Divers::copyFolder(dirMedias + "Gui", projDir + "Medias/Gui");
        QApplication::processEvents();
        Divers::copyFolder(dirMedias + "Musics", projDir + "Medias/Musics");
        QApplication::processEvents();
        Divers::copyFolder(dirMedias + "Videos", projDir + "Medias/Videos");
        QApplication::processEvents();

        QProcess run;
        QString exec = QString("zip.exe a -r -y -tzip -mmt -mx9")
                + " -pHaiecapiqueMdpMarble -mem=aes "
                + "\"" + projDir + "Medias/Data.dat\" "
                + "\"" + dirMedias + "Textures\" "
                + "\"" + dirMedias + "Scripts\" "
                + "\"" + dirMedias + "Shaders\" "
                + "\"" + dirMedias + "Models\"";
        run.start(exec);
        run.waitForFinished(-1);
        QApplication::processEvents();

        exec = QString("zip.exe a -r -y -tzip -mmt -mx9")
                + " -pHaiecapiqueMdpMarble -mem=aes "
                + "\"" + projDir + "Maps.dat\" ";
        for (int i = 0; i < mapsList->count(); ++i) {
            QListWidgetItem* item = mapsList->item(i);
            exec += "\"" + dirProject + item->text() + ".dat\" ";
        }
        run.start(exec);
        run.waitForFinished(-1);
        QApplication::processEvents();


#ifdef DEBUG
        Divers::copyFolder("GenerateFilesNeeded/Debug/", projDir);
        QFile::copy("Marble_d.exe", projDir + projName + ".exe");
#else
        Divers::copyFolder("GenerateFilesNeeded/Release/", projDir);
        QFile::copy("Marble.exe", projDir + projName + ".exe");
#endif

        QDesktopServices::openUrl(QUrl("file:///" + projDir,
                                       QUrl::TolerantMode));
        QApplication::restoreOverrideCursor();
    }
}

void Engine::closeEvent(QCloseEvent* event) {
    if (unloadProject()) {
        QSettings settings("setup.ini", QSettings::IniFormat);
        settings.beginGroup("MainWindow");
        settings.setValue("geometry", saveGeometry());
        settings.setValue("windowState", saveState());
        settings.setValue("size", size());
        settings.setValue("pos", pos());
        settings.endGroup();
        event->accept();
    } else {
        event->ignore();
    }
}

std::map<QString, Element*>& Engine::getElements() {
    return elements;
}

std::map<QString, Camera*>& Engine::getCameras() {
    return cameras;
}

Element* Engine::getElement(QString name) const {
    std::map<QString, Element*>::const_iterator mit(elements.find(name));
    if (mit != elements.end()) {
        return mit->second;
    }
    return NULL;
}

Element* Engine::getSelectedElement() const {
    return selectedElement;
}

void Engine::setSelectedElement(Element* newSelect) {
    if (selectedElement) {
        selectedElement->setSelected(false);

        ElementsList->blockSignals(true);
        ElementsList->setCurrentItem(NULL);
        ElementsList->blockSignals(false);

        statusBarWidget->showMessage("");
    }

    selectedElement = newSelect;

    if (selectedElement != NULL) {
        selectedElement->setSelected(true);

        bool stop = false;
        int numElement = ElementsList->count();
        for (int i = 0; i < numElement && !stop; ++i) {
            QString val =(ElementsList->item(i))->text();
            if (val == selectedElement->getName()) {
                ElementsList->blockSignals(true);
                ElementsList->setCurrentItem(ElementsList->item(i));
                ElementsList->blockSignals(false);
                stop = true;
            }
        }

        QString message = QString(tr("Element \""))
                + selectedElement->getName() + QString(tr("\" sélectioné"));
        statusBarWidget->showMessage(message, 2000);
    }
}

void Engine::add_Element() {
    Element* elt = new Element(getNameGen("Element"), this);
    if (elt->init()) {
        elements.insert(std::make_pair(elt->getName(), elt));
        setSelectedElement(elt);
        updateList();
    }
}

void Engine::add_Mesh() {
    QString fichier = "";

    FileChooser* dialog = new FileChooser(dirMedias + "Models/", false, this);
    dialog->exec();
    if (dialog->result() == QDialog::Accepted) {
        fichier = "Models/" + dialog->getSelectedRelativeFilePath();
    }

    if (fichier != "") {
        Mesh* obj = new Mesh(getNameGen("Mesh"), this);
        if (obj->init(fichier)) {
            elements.insert(std::make_pair(obj->getName(), obj));
            setSelectedElement(obj);
            updateList();
        } else {
            delete obj;
            obj = NULL;
        }
    }
}

void Engine::add_Light() {
    Light* obj = new Light(getNameGen("Light"), this);
    if (obj->init()) {
        elements.insert(std::make_pair(obj->getName(), obj));
        setSelectedElement(obj);
        updateList();
    } else {
        delete obj;
        obj = NULL;
    }
}

void Engine::add_Particules() {
    Particules* obj = new Particules(getNameGen("Particules"), this);
    if (obj->init()) {
        elements.insert(std::make_pair(obj->getName(), obj));
        setSelectedElement(obj);
        updateList();
    } else {
        delete obj;
        obj = NULL;
    }
}

void Engine::add_Constraint() {
    Constraint* obj = new Constraint(getNameGen("Constraint"), this);
    if (obj->init()) {
        elements.insert(std::make_pair(obj->getName(), obj));
        setSelectedElement(obj);
        updateList();
    } else {
        delete obj;
        obj = NULL;
    }
}

void Engine::add_Terrain() {
    QString fichier = "";

    FileChooser* dialog = new FileChooser(dirMedias + "Textures/", true, this);
    dialog->exec();
    if (dialog->result() == QDialog::Accepted) {
        fichier = "Textures/" + dialog->getSelectedRelativeFilePath();
    }

    if (fichier != "") {
        Terrain* obj = new Terrain(getNameGen("Terrain"), this);
        if (obj->init(fichier)) {
            elements.insert(std::make_pair(obj->getName(), obj));
            setSelectedElement(obj);
            updateList();
        } else {
            delete obj;
            obj = NULL;
        }
    }
}

void Engine::add_Camera() {
    Camera* obj = new Camera(getNameGen("Camera"), this);
    if (obj->init()) {
        elements.insert(std::make_pair(obj->getName(), obj));
        cameras.insert(std::make_pair(obj->getName(), obj));
        setSelectedElement(obj);
        updateList();
    } else {
        delete obj;
        obj = NULL;
    }
}

void Engine::add_Video2D() {
    Video2D* obj = new Video2D(getNameGen("Video2D"), this);
    if (obj->init()) {
        elements.insert(std::make_pair(obj->getName(), obj));
        setSelectedElement(obj);
        updateList();
    } else {
        delete obj;
        obj = NULL;
    }
}

void Engine::add_Sound() {
    Sound* obj = new Sound(getNameGen("Sound"), this);
    if (obj->init()) {
        elements.insert(std::make_pair(obj->getName(), obj));
        setSelectedElement(obj);
        updateList();
    } else {
        delete obj;
        obj = NULL;
    }
}

bool Engine::save(bool confirm) {
    int reponse = QMessageBox::Yes;
    if (confirm) {
        reponse = QMessageBox::question(this, "Sauvegarde",
                                        tr("Voulez vous sauvegarder ?"),
                                        QMessageBox::Yes | QMessageBox::No
                                        | QMessageBox::Cancel);
    }

    if (reponse == QMessageBox::Cancel) {
        return false;
    }

    if (reponse == QMessageBox::Yes) {
        tinyxml2::XMLDocument xmlDoc;

        tinyxml2::XMLDeclaration* decl = xmlDoc.NewDeclaration(
                    "xml version=\"1.0\" encoding=\"UTF-8\"");
        xmlDoc.LinkEndChild(decl);

        tinyxml2::XMLElement* element = xmlDoc.NewElement("Root");
        physicsEngine->writeXML(&xmlDoc, element);
        mat->writeXML(&xmlDoc, element);
        skyMap->writeXML(&xmlDoc, element);
        fogMap->writeXML(&xmlDoc, element);

        {
            std::map<QString, Element*>::const_iterator it,
                    end = elements.end();
            for (it = elements.begin(); it != end; ++it) {
                (it->second)->writeXML(&xmlDoc, element);
            }
        }

        guiEngine->writeXML(&xmlDoc, element);

        xmlDoc.LinkEndChild(element);

        QFileInfo f(currentMap);
        FILE* fp;
        errno_t err = fopen_s(&fp, f.canonicalFilePath().toUtf8().data(), "w");
        if (err != 0) {
            QMessageBox::warning(this, tr("Warning"), tr("Save Failed"),
                                 QMessageBox::Ok);
            return false;
        }
        if (fp) {
            tinyxml2::XMLPrinter stream(fp, true);
            xmlDoc.Print(&stream);
            fclose(fp);

            statusBarWidget->showMessage(
                        tr("Le fichier a été sauvegardé"), 2000);
        } else {
            QMessageBox::warning(this, tr("Warning"), tr("Save Failed"),
                                 QMessageBox::Ok);
            return false;
        }
    }
    return true;
}

QString Engine::getDirProj() {
    return dirProject;
}

QString Engine::getDirMedias() {
    return dirMedias;
}

void Engine::destroyElement(QString elem) {
    Element* obj = getElement(elem);
    if (obj != NULL) {
        if (obj == selectedElement) {
            setSelectedElement(NULL);
        }
        elements.erase(elem);
        delete obj;
        obj = NULL;
        updateList();
    }
}

void Engine::supprSelect() {
    if (selectedElement != NULL) {
        Element* elt = selectedElement;
        setSelectedElement(NULL);
        elements.erase(elt->getName());
        delete elt;
        elt = NULL;
        updateList();
    }
}

void Engine::copy() {
    if (selectedElement) {
        tinyxml2::XMLDocument* xmlDoc = new tinyxml2::XMLDocument();
        xmlDoc->Parse("<Root></Root>");
        tinyxml2::XMLElement* element = xmlDoc->FirstChildElement();
        selectedElement->writeXML(xmlDoc, element);
        tinyxml2::XMLPrinter printer(NULL, true);
        xmlDoc->Print(&printer);
        copyElement = printer.CStr();
        delete xmlDoc;

        QString message = QString(tr("Element \""))
                + selectedElement->getName() + QString(tr("\" copié"));
        statusBarWidget->showMessage(message, 2000);
    }
}

void Engine::past() {
    if (!copyElement.isEmpty()) {
        tinyxml2::XMLDocument xmlDoc;
        xmlDoc.Parse(copyElement.toUtf8().data());
        readXML(&xmlDoc);

        std::map<QString, Element*>::const_iterator it =
                --elements.rbegin().base();
        setSelectedElement(it->second);
    }
}

void Engine::lance() {
    if (save()) {
#ifdef DEBUG
        QString prog = "Marble_d.exe";
#else
        QString prog = "Marble.exe";
#endif
        QStringList arguments;
        arguments.append(dirProject);
        arguments.append(mapsList->currentItem()->text());
        QProcess* run = new QProcess(this);
        run->connect(run, static_cast<void (QProcess::*)(int)>(&QProcess::finished), run, &QObject::deleteLater);
        run->start(prog, arguments);
        run->waitForStarted(-1);
    }
}

void Engine::selectElement() {
    QListWidgetItem* item = ElementsList->currentItem();
    if (item != NULL) {
        Element* elt = elements.at(item->text());
        setSelectedElement(elt);
    }
}

void Engine::updateList() {
    ElementsList->clear();
    {
        std::map<QString, Element*>::const_iterator it, end = elements.end();
        for (it = elements.begin(); it != end; ++it) {
            ElementsList->addItem(it->first);
        }
    }

    ElementsList->blockSignals(true);
    if (selectedElement != NULL) {
        for (int i = 0; i < ElementsList->count(); ++i) {
            QString val = ElementsList->item(i)->text();
            if (val == selectedElement->getName()) {
                ElementsList->setCurrentItem(ElementsList->item(i));
                continue;
            }
        }
    }
    ElementsList->blockSignals(false);
}

void Engine::openMedias() {
    medias->show();
}

QString Engine::getNameGen(QString prefix) {
    QString ret;
    int idGenerator = 0;
    do {
        ret = prefix + " " + QString::number(idGenerator);
        idGenerator++;
    } while (elements.find(ret) != elements.end());
    return ret;
}

}  // namespace MarbleEditor
