/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Engines/MapsManager.h"

#include "MarbleEditor/Engines/Engine.h"

namespace MarbleEditor {

MapsManager::MapsManager(Engine *engine, QWidget *parent) : QDialog(parent) {
    setupUi(this);
    this->engine = engine;

    connect(cancelButton, &QPushButton::clicked,
            this, &MapsManager::shutdown);

    connect(saveButton, &QPushButton::clicked,
            this, &MapsManager::writeProject);

    connect(addButton, &QPushButton::clicked,
            this, &MapsManager::add);

    connect(removeButton, &QPushButton::clicked,
            this, &MapsManager::remove);
}

void MapsManager::shutdown() {
    close();
    engine->loadProject(dirProj);
}

void MapsManager::add() {
    bool ok;
    QString text = QInputDialog::getText(
                engine, tr("Add Map"), tr("Map Name : "),
                QLineEdit::Normal, "", &ok);
    if (ok && !text.isEmpty() && listWidget->findItems(
                text, Qt::MatchExactly).isEmpty()) {
        QFile file(dirProj + text + ".dat");
        if (file.open(QFile::WriteOnly)) {
            QTextStream flux(&file);
            flux.setCodec("UTF-8");
            flux << "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Root></Root>";
            file.close();
            listWidget->addItem(text);
            mainMap->addItem(text);
        }
    }
}

void MapsManager::remove() {
    QString name = listWidget->currentItem()->text();
    listWidget->takeItem(listWidget->currentRow());
    listWidget->setCurrentRow(-1);
    mainMap->removeItem(mainMap->findText(name));
    QFile::remove(dirProj + name);
}

void MapsManager::loadProject() {
    dirProj = engine->getDirProj();

    if (engine->unloadProject()) {
        listWidget->clear();
        mainMap->clear();

        tinyxml2::XMLDocument xmlDoc;
        QFileInfo projectFileInfo(dirProj + "Project.dat");
        QString file = projectFileInfo.canonicalFilePath();
        if (xmlDoc.LoadFile(file.toUtf8().data()) != tinyxml2::XML_NO_ERROR) {
            QMessageBox::warning(this, tr("Warning"),
                                 tr("Project not loaded"),
                                 QMessageBox::Ok);
            return;
        }

        tinyxml2::XMLHandle hdl(xmlDoc);
        tinyxml2::XMLElement* elem =
                hdl.FirstChildElement().FirstChildElement().ToElement();

        QString main = "";
        while (elem != NULL) {
            if (QString(elem->Value()) == "Map") {
                QString name = elem->Attribute("name");
                listWidget->addItem(name);
                mainMap->addItem(name);
                if (elem->BoolAttribute("main")) {
                    main = name;
                }
            }
            elem = elem->NextSiblingElement();
        }

        mainMap->setCurrentIndex(mainMap->findText(main));

        exec();
    }
}

void MapsManager::writeProject() {
    tinyxml2::XMLDocument xmlDoc;
    QFileInfo projectFileInfo(dirProj + "Project.dat");
    QString file = projectFileInfo.canonicalFilePath();
    if (xmlDoc.LoadFile(file.toUtf8().data()) != tinyxml2::XML_NO_ERROR) {
        QMessageBox::warning(this, tr("Warning"),
                             tr("Project not loaded"),
                             QMessageBox::Ok);
        return;
    }

    tinyxml2::XMLHandle hdl(xmlDoc);
    tinyxml2::XMLElement* element = hdl.FirstChildElement().ToElement();

    // Remove Map Elements
    tinyxml2::XMLElement* elt = element->FirstChildElement("Map");
    while (elt != NULL) {
        element->DeleteChild(elt);
        elt = element->FirstChildElement("Map");
    }

    for (int i = 0; i < listWidget->count(); ++i) {
        QString map = listWidget->item(i)->text();
        tinyxml2::XMLElement* elem = xmlDoc.NewElement("Map");
        elem->SetAttribute("name", map.toUtf8().data());
        elem->SetAttribute("main", (mainMap->currentText() == map));
        element->LinkEndChild(elem);
    }

    FILE* fp;
    fopen_s(&fp, file.toUtf8().data(), "w");
    if (fp) {
        tinyxml2::XMLPrinter stream(fp, true);
        xmlDoc.Print(&stream);
        fclose(fp);
    }
    shutdown();
}

}  // namespace MarbleEditor
