/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Medias.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ENGINES_MEDIAS_H_
#define MARBLEEDITOR_ENGINES_MEDIAS_H_

#include <ui_Medias.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class Medias
 * \brief
 *
 *
 */
class Medias : public QDockWidget, private Ui::Medias {
    Q_OBJECT

    public:
        explicit Medias(Engine* engine, QWidget *parent = 0);
        virtual ~Medias() {}
        void loadProject();
        void unloadProject();

    private:
        QString getCurrentViewFolder(QTreeView* view, QFileSystemModel* model);
        void addFolder(QTreeView* view, QFileSystemModel* model);
        void addNewFile(QTreeView* view, QFileSystemModel* model);
        void addExistingFile(QTreeView* view, QFileSystemModel* model);
        void editFileXML(QTreeView* view, QFileSystemModel* model);
        void editFileScript(QTreeView *view, QFileSystemModel *model);

    private slots:

        void editGuiFile();
        void addNewGuiFile();
        void addExistingGuiFile();

        void openModel();
        void addModelFolder();
        void addExistingModelFile();

        void openMusic();
        void addMusicFolder();
        void addExistingMusicFile();

        void openVideo();
        void addVideoFolder();
        void addExistingVideoFile();

        void editScriptFile();
        void addScriptFolder();
        void addNewScriptFile();
        void addExistingScriptFile();

        void openTexture();
        void addTextureFolder();
        void addExistingTextureFile();

        void addNewShader();
        void editShaderFile();

    private:
        Engine* engine;
        QFileSystemModel* modeleGui;
        QFileSystemModel* modeleModels;
        QFileSystemModel* modeleMusics;
        QFileSystemModel* modeleVideos;
        QFileSystemModel* modeleScripts;
        QFileSystemModel* modeleShaders;
        QFileSystemModel* modeleTextures;

    private:
        DISALLOW_COPY_AND_ASSIGN(Medias);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ENGINES_MEDIAS_H_
