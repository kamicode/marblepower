/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Engines/FileChooser.h"

namespace MarbleEditor {

FileChooser::FileChooser(QString directory, bool showPreview, QWidget *parent)
    : QDialog(parent) {
    setupUi(this);
    modele = new QFileSystemModel(this);
    modele->setRootPath(directory);

    selectedFile = "";
    this->directory = directory;

    treeView->setModel(modele);
    modele->sort(0);
    treeView->setRootIndex(modele->index(directory));

    preview->setVisible(showPreview);

    connect(treeView, &QTreeView::clicked,
            this, &FileChooser::select);

    connect(treeView, &QTreeView::doubleClicked,
            this, &FileChooser::accept);

    connect(buttonBox, &QDialogButtonBox::accepted,
            this, &FileChooser::accept);

    connect(buttonBox, &QDialogButtonBox::rejected,
            this, &FileChooser::reject);
}

void FileChooser::select(QModelIndex index) {
    QString name = QFileInfo(modele->filePath(index)).canonicalFilePath();

    if (QFileInfo(name).isFile()) {
        selectedFile = name;
        QImage image(selectedFile);
        image.scaled(100, 100);
        label->setPixmap(QPixmap::fromImage(image));
    }
}

QString FileChooser::getSelectedAbsoluteFilePath() {
    return QFileInfo(selectedFile).canonicalFilePath();
}

QString FileChooser::getSelectedRelativeFilePath() {
    QDir dir(directory);
    return dir.relativeFilePath(selectedFile);
}

void FileChooser::accept() {
    if (QFileInfo(selectedFile).isFile()) {
        QDialog::accept();
    } else {
        QDialog::reject();
    }
}

}  // namespace MarbleEditor
