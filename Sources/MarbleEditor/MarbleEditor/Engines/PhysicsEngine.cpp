/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Engines/PhysicsEngine.h"
#include "MarbleEditor/Engines/GuiEngine.h"

namespace MarbleEditor {

PhysicsEngine::PhysicsEngine(Engine* engine, QWidget* parent)
    : QDialog(parent) {
    setupUi(this);
    this->engine = engine;

    nWorld = NULL;

    connect(enablePhysic, &QGroupBox::toggled,
            engine->getActionMaterialPhysWidget(), &QAction::setEnabled);
}

PhysicsEngine::~PhysicsEngine() {
}

void PhysicsEngine::unLoadMap() {
    enablePhysic->setChecked(false);
    if (nWorld != NULL) {
        delete nWorld;
        delete constraintSolver;
        delete dispatcher;
        delete broadPhase;
        delete collisionConfiguration;
        nWorld = NULL;
    }
}

bool PhysicsEngine::readXML(tinyxml2::XMLElement* elem) {
    enablePhysic->setChecked(true);

    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);

    minX->setValue(elem->FloatAttribute("minX"));
    minY->setValue(elem->FloatAttribute("minY"));
    minZ->setValue(elem->FloatAttribute("minZ"));

    maxX->setValue(elem->FloatAttribute("maxX"));
    maxY->setValue(elem->FloatAttribute("maxY"));
    maxZ->setValue(elem->FloatAttribute("maxZ"));

    broadPhase = new btAxisSweep3(btVector3(minX->value(),
                                            minY->value(),
                                            minZ->value()),
                                  btVector3(maxX->value(),
                                            maxY->value(),
                                            maxZ->value()));

    constraintSolver = new btSequentialImpulseConstraintSolver();

    nWorld = new btDiscreteDynamicsWorld(dispatcher, broadPhase,
                                         constraintSolver,
                                         collisionConfiguration);

    gravityX->setValue(elem->FloatAttribute("gravityX"));
    gravityY->setValue(elem->FloatAttribute("gravityY"));
    gravityZ->setValue(elem->FloatAttribute("gravityZ"));
    nWorld->setGravity(btVector3(gravityX->value(), gravityY->value(),
                                 gravityZ->value()));

    nWorld->setDebugDrawer(new IrrlichtDebugDrawer(
                               engine->getGraphicsEngine()->getDriver()));
    nWorld->getDebugDrawer()->setDebugMode(
                btIDebugDraw::DBG_DrawWireframe |
                btIDebugDraw::DBG_FastWireframe |
                btIDebugDraw::DBG_DrawContactPoints |
                btIDebugDraw::DBG_DrawConstraints);

    return true;
}

bool PhysicsEngine::writeXML(tinyxml2::XMLDocument* xmlDoc,
                             tinyxml2::XMLElement* elem) {
    if (enablePhysic->isChecked()) {
        tinyxml2::XMLElement* element = xmlDoc->NewElement("Physic");
        element->SetAttribute("minX", minX->value());
        element->SetAttribute("minY", minY->value());
        element->SetAttribute("minZ", minZ->value());
        element->SetAttribute("maxX", maxX->value());
        element->SetAttribute("maxY", maxY->value());
        element->SetAttribute("maxZ", maxZ->value());
        element->SetAttribute("gravityX", gravityX->value());
        element->SetAttribute("gravityY", gravityY->value());
        element->SetAttribute("gravityZ", gravityZ->value());

        elem->LinkEndChild(element);
    }

    return true;
}

void PhysicsEngine::update(float timeInMs) {
    if (nWorld != NULL) {
        nWorld->stepSimulation(timeInMs * 0.001f, 5);
    }
}

bool PhysicsEngine::isPhysicEnabled() {
    return enablePhysic->isChecked();
}

btDiscreteDynamicsWorld* PhysicsEngine::getWorld() {
    return nWorld;
}

}  // namespace MarbleEditor
