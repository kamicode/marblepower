/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_ProjectChooser.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ENGINES_PROJECTCHOOSER_H_
#define MARBLEEDITOR_ENGINES_PROJECTCHOOSER_H_

#include <ui_ProjectChooser.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class ProjectChooser
 * \brief
 *
 *
 */
class ProjectChooser : public QDialog, private Ui::ProjectChooser {
    Q_OBJECT

    public:
        explicit ProjectChooser(Engine* main, QWidget* parent = 0);
        virtual ~ProjectChooser() {}

        void ouvrirProjet(QString proj);

    private slots:
        void ouvrirProjetSel();
        void nouveauProjet();
        void choisirProjet();

    private:
        Engine* main;

    private:
        DISALLOW_COPY_AND_ASSIGN(ProjectChooser);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ENGINES_PROJECTCHOOSER_H_
