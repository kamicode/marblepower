/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_GuiEngine.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ENGINES_GUIENGINE_H_
#define MARBLEEDITOR_ENGINES_GUIENGINE_H_

#include <ui_GuiEngine.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class GuiEngine
 * \brief
 *
 *
 */
class GuiEngine : public QDialog, private Ui::GuiEngine {
    Q_OBJECT

    public:
        explicit GuiEngine(Engine* engine, QWidget* parent = 0);
        virtual ~GuiEngine();

        void unloadMap();

        bool readXML(tinyxml2::XMLElement* elem);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

    public slots:
        void setGUI();
        void show();
        void openSchemes();
        void openImagesets();
        void openFonts();
        void openLayouts();
        void openLooknfeels();
        void openSchemas();
        void openAnimations();
        void setFontManagerCombo();
        void setLayoutCombo();
        void setSchemeManagerCombo();
        void setImagesetManagerCombo();
        void editLayout();
        void addConnection();
        void addFontManager();
        void removeConnection();
        void removeFontManager();
        void addSchemeManager();
        void removeSchemeManager();
        void addImagesetManager();
        void removeImagesetManager();
        void updateCell(int currentRow = -1, int currentColumn = -1);
        void updateCellScripts(int currentRow);
        void updateIhm();

    private:
        Engine* engine;
        QString schemesPath, imagesetsPath, fontsPath, layoutsPath;
        QString looknfeelsPath, schemasPath, animationsPath;
        QString absschemesPath, absimagesetsPath, absfontsPath, abslayoutsPath;
        QString abslooknfeelsPath, absschemasPath, absanimationsPath;

    private:
        DISALLOW_COPY_AND_ASSIGN(GuiEngine);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ENGINES_GUIENGINE_H_
