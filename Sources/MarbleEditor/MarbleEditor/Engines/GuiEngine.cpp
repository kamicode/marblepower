/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Engines/GuiEngine.h"

#include <map>

#include "MarbleEditor/Editors/XMLEditor.h"
#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Element.h"

namespace MarbleEditor {

GuiEngine::GuiEngine(Engine* engine, QWidget* parent) : QDialog(parent) {
    setupUi(this);
    this->engine = engine;

    schemesPath = "";
    imagesetsPath = "";
    fontsPath = "";
    layoutsPath = "";
    looknfeelsPath = "";
    schemasPath = "";
    animationsPath = "";
    absschemesPath = "";
    absimagesetsPath = "";
    absfontsPath = "";
    abslayoutsPath = "";
    abslooknfeelsPath = "";
    absschemasPath = "";
    absanimationsPath = "";

    connect(schemes, &QPushButton::clicked,
            this, &GuiEngine::openSchemes);

    connect(imagesets, &QPushButton::clicked,
            this, &GuiEngine::openImagesets);

    connect(fonts, &QPushButton::clicked,
            this, &GuiEngine::openFonts);

    connect(layouts, &QPushButton::clicked,
            this, &GuiEngine::openLayouts);

    connect(looknfeels, &QPushButton::clicked,
            this, &GuiEngine::openLooknfeels);

    connect(schemas, &QPushButton::clicked,
            this, &GuiEngine::openSchemas);

    connect(animations, &QPushButton::clicked,
            this, &GuiEngine::openAnimations);

    connect(layoutEdit, &QPushButton::clicked,
            this, &GuiEngine::editLayout);

    connect(connectionAdd, &QPushButton::clicked,
            this, &GuiEngine::addConnection);

    connect(fontManagerAdd, &QPushButton::clicked,
            this, &GuiEngine::addFontManager);

    connect(SchemeManagerAdd, &QPushButton::clicked,
            this, &GuiEngine::addSchemeManager);

    connect(imagesetManagerAdd, &QPushButton::clicked,
            this, &GuiEngine::addImagesetManager);

    connect(connectionRemove, &QPushButton::clicked,
            this, &GuiEngine::removeConnection);

    connect(fontManagerRemove, &QPushButton::clicked,
            this, &GuiEngine::removeFontManager);

    connect(SchemeManagerRemove, &QPushButton::clicked,
            this, &GuiEngine::removeSchemeManager);

    connect(imagesetManagerRemove, &QPushButton::clicked,
            this, &GuiEngine::removeImagesetManager);

    connect(updateIHM, &QPushButton::clicked,
            this, &GuiEngine::updateIhm);

    connect(UpdateGUI, &QPushButton::clicked,
            this, &GuiEngine::setGUI);
}

GuiEngine::~GuiEngine() {
    CEGUI::WindowManager& wmgr = CEGUI::WindowManager::getSingleton();
    wmgr.destroyAllWindows();
}

void GuiEngine::unloadMap() {
    while (connections->rowCount() > 0) {
        connections->removeRow(0);
    }

    fontManagerList->clear();
    fontManagerCombo->clear();
    SchemeManagerList->clear();
    schemeManagerCombo->clear();
    imagesetManagerList->clear();
    imagesetManagerCombo->clear();
    layoutCombo->clear();

    defaultFont->setText("DejaVuSans-10");
    defaultMouseImageSet->setText("WindowsLook");
    defaultMouseCursor->setText("MouseArrow");
    defaultTooltip->setText("WindowsLook/Tooltip");

    schemesPath = "";
    imagesetsPath = "";
    fontsPath = "";
    layoutsPath = "";
    looknfeelsPath = "";
    schemasPath = "";
    animationsPath = "";
    absschemesPath = "";
    absimagesetsPath = "";
    absfontsPath = "";
    abslayoutsPath = "";
    abslooknfeelsPath = "";
    absschemasPath = "";
    absanimationsPath = "";
}

void GuiEngine::show() {
    QDialog::show();
    updateIhm();
}

void GuiEngine::updateIhm() {
    if (schemesPath == "") {
        schemesPath = "Gui/Schemes/";
        absschemesPath = engine->getDirMedias() + schemesPath;
    }
    if (imagesetsPath == "") {
        imagesetsPath = "Gui/Imagesets/";
        absimagesetsPath = engine->getDirMedias() + imagesetsPath;
    }
    if (fontsPath == "") {
        fontsPath = "Gui/Fonts/";
        absfontsPath = engine->getDirMedias() + fontsPath;
    }
    if (layoutsPath == "") {
        layoutsPath = "Gui/Layouts/";
        abslayoutsPath = engine->getDirMedias() + layoutsPath;
    }
    if (looknfeelsPath == "") {
        looknfeelsPath = "Gui/Looknfeels/";
        abslooknfeelsPath = engine->getDirMedias() + looknfeelsPath;
    }
    if (schemasPath == "") {
        schemasPath = "Gui/Schemas/";
        absschemasPath = engine->getDirMedias() + schemasPath;
    }
    if (animationsPath == "") {
        animationsPath = "Gui/Animations/";
        absanimationsPath = engine->getDirMedias() + animationsPath;
    }

    setSchemeManagerCombo();
    setFontManagerCombo();
    setImagesetManagerCombo();
    setLayoutCombo();


    for (int i = 0; i < connections->rowCount(); ++i) {
        updateCell(i, 2);
    }
}

void GuiEngine::addFontManager() {
    QString val = fontManagerCombo->currentText();
    if (!val.isEmpty() && fontManagerList->findItems(
                val, Qt::MatchExactly).isEmpty()) {
        fontManagerList->addItem(val);
    }
}

void GuiEngine::removeFontManager() {
    QListWidgetItem* it = fontManagerList->takeItem(
                fontManagerList->currentRow());
    delete it;
}

void GuiEngine::addSchemeManager() {
    QString val = schemeManagerCombo->currentText();
    if (!val.isEmpty() && SchemeManagerList->findItems(
                val, Qt::MatchExactly).isEmpty()) {
        SchemeManagerList->addItem(val);
    }
}

void GuiEngine::removeSchemeManager() {
    QListWidgetItem* it = SchemeManagerList->takeItem(
                SchemeManagerList->currentRow());
    delete it;
}

void GuiEngine::addImagesetManager() {
    QString val = imagesetManagerCombo->currentText();
    if (!val.isEmpty() && imagesetManagerList->findItems(
                val, Qt::MatchExactly).isEmpty()) {
        imagesetManagerList->addItem(val);
    }
}

void GuiEngine::removeImagesetManager() {
    QListWidgetItem* it = imagesetManagerList->takeItem(
                imagesetManagerList->currentRow());
    delete it;
}

void GuiEngine::addConnection() {
    connections->insertRow(connections->rowCount());

    for (int i = 0; i < connections->columnCount(); ++i) {
        if (i == 2) {
            QComboBox* listElements = new QComboBox(connections);
            connections->setCellWidget(
                        connections->rowCount() - 1, 2, listElements);

            QSignalMapper* signalMapper = new QSignalMapper(this);
            connect(listElements, static_cast<void (QComboBox::*)(int)>
                                             (&QComboBox::currentIndexChanged),
                    signalMapper, static_cast<void (QSignalMapper::*)()>
                                             (&QSignalMapper::map));

            signalMapper->setMapping(listElements, connections->rowCount() - 1);

            connect(signalMapper, static_cast<void (QSignalMapper::*)(int)>
                                              (&QSignalMapper::mapped),
                    this, &GuiEngine::updateCellScripts);

            updateCell(connections->rowCount() - 1, 2);
        } else if (i == 3) {
            QComboBox* listScripts = new QComboBox(connections);
            connections->setCellWidget(
                        connections->rowCount() - 1, 3, listScripts);
        } else {
            QTableWidgetItem* newItem = new QTableWidgetItem();
            connections->setItem(connections->rowCount() - 1, i, newItem);
        }
    }
}

void GuiEngine::removeConnection() {
    connections->removeRow(connections->currentRow());
}

void GuiEngine::updateCell(int currentRow, int currentColumn) {
    if (currentColumn == 2) {
        QComboBox* listElements = static_cast<QComboBox*>
                (connections->cellWidget(currentRow, currentColumn));

        listElements->blockSignals(true);
        QStringList elements;
        std::map<QString, Element*> liste = engine->getElements();
        std::map<QString, Element*>::const_iterator it, end = liste.end();
        for (it = liste.begin(); it != end; ++it) {
            elements.append(it->first);
        }
        QString current = listElements->currentText();
        listElements->clear();
        listElements->addItems(elements);
        listElements->setCurrentIndex(listElements->findText(current));
        listElements->blockSignals(false);

        updateCellScripts(currentRow);
    }
}

void GuiEngine::updateCellScripts(int currentRow) {
    QComboBox* listElement = static_cast<QComboBox*>
            (connections->cellWidget(currentRow, 2));
    Element* elt = engine->getElement(listElement->currentText());
    if (elt) {
        QComboBox* listScripts = static_cast<QComboBox*>
                (connections->cellWidget(currentRow, 3));
        listScripts->blockSignals(true);
        QStringList scripts = elt->getScripts();
        QString current = listScripts->currentText();
        listScripts->clear();
        listScripts->addItems(scripts);
        listScripts->setCurrentIndex(listScripts->findText(current));
        listScripts->blockSignals(false);
    }
}

void GuiEngine::editLayout() {
    XMLEditor* editeur = new XMLEditor();
    if (layoutCombo->currentText() != "") {
        editeur->load(abslayoutsPath + layoutCombo->currentText());
    }
}

void GuiEngine::setFontManagerCombo() {
    QString elt = fontManagerCombo->currentText();
    fontManagerCombo->clear();
    QStringList listFilter;
    listFilter << "*.font";
    QDirIterator dirIterator(absfontsPath, listFilter,
                             QDir::Files | QDir::NoSymLinks);
    while (dirIterator.hasNext()) {
        dirIterator.next();
        fontManagerCombo->addItem(dirIterator.fileInfo().fileName());
    }

    if (elt.isEmpty()) {
        fontManagerCombo->setCurrentIndex(0);
    } else {
        fontManagerCombo->setCurrentIndex(fontManagerCombo->findText(elt));
    }
}

void GuiEngine::setLayoutCombo() {
    QString elt = layoutCombo->currentText();
    layoutCombo->clear();
    layoutCombo->addItem("");

    QStringList listFilter;
    listFilter << "*.layout";
    QDirIterator dirIterator(abslayoutsPath, listFilter,
                             QDir::Files | QDir::NoSymLinks);
    while (dirIterator.hasNext()) {
        dirIterator.next();
        layoutCombo->addItem(dirIterator.fileInfo().fileName());
    }

    if (elt.isEmpty()) {
        layoutCombo->setCurrentIndex(0);
    } else {
        layoutCombo->setCurrentIndex(layoutCombo->findText(elt));
    }
}

void GuiEngine::setSchemeManagerCombo() {
    QString elt = schemeManagerCombo->currentText();
    schemeManagerCombo->clear();
    QStringList listFilter;
    listFilter << "*.scheme";
    QDirIterator dirIterator(absschemesPath, listFilter,
                             QDir::Files | QDir::NoSymLinks);
    while (dirIterator.hasNext()) {
        dirIterator.next();
        schemeManagerCombo->addItem(dirIterator.fileInfo().fileName());
    }

    if (elt.isEmpty()) {
        schemeManagerCombo->setCurrentIndex(0);
    } else {
        schemeManagerCombo->setCurrentIndex(schemeManagerCombo->findText(elt));
    }
}

void GuiEngine::setImagesetManagerCombo() {
    QString elt = imagesetManagerCombo->currentText();
    imagesetManagerCombo->clear();
    QStringList listFilter;
    listFilter << "*.imageset";
    QDirIterator dirIterator(absimagesetsPath, listFilter,
                             QDir::Files | QDir::NoSymLinks);
    while (dirIterator.hasNext()) {
        dirIterator.next();
        imagesetManagerCombo->addItem(dirIterator.fileInfo().fileName());
    }

    if (elt.isEmpty()) {
        imagesetManagerCombo->setCurrentIndex(0);
    } else {
        imagesetManagerCombo->setCurrentIndex(
                    imagesetManagerCombo->findText(elt));
    }
}

void GuiEngine::openSchemes() {
    QString path = QDir::toNativeSeparators(absschemesPath);
    QDesktopServices::openUrl(QUrl("file:///" + path, QUrl::TolerantMode));
}

void GuiEngine::openFonts() {
    QString path = QDir::toNativeSeparators(absfontsPath);
    QDesktopServices::openUrl(QUrl("file:///" + path, QUrl::TolerantMode));
}

void GuiEngine::openLayouts() {
    QString path = QDir::toNativeSeparators(abslayoutsPath);
    QDesktopServices::openUrl(QUrl("file:///" + path, QUrl::TolerantMode));
}

void GuiEngine::openImagesets() {
    QString path = QDir::toNativeSeparators(absimagesetsPath);
    QDesktopServices::openUrl(QUrl("file:///" + path, QUrl::TolerantMode));
}

void GuiEngine::openLooknfeels() {
    QString path = QDir::toNativeSeparators(abslooknfeelsPath);
    QDesktopServices::openUrl(QUrl("file:///" + path, QUrl::TolerantMode));
}

void GuiEngine::openSchemas() {
    QString path = QDir::toNativeSeparators(absschemasPath);
    QDesktopServices::openUrl(QUrl("file:///" + path, QUrl::TolerantMode));
}

void GuiEngine::openAnimations() {
    QString path = QDir::toNativeSeparators(absanimationsPath);
    QDesktopServices::openUrl(QUrl("file:///" + path, QUrl::TolerantMode));
}

bool GuiEngine::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("schemes")) {
        schemesPath = QString(elem->Attribute("schemes"));
        absschemesPath = engine->getDirMedias() + schemesPath;
    }
    if (elem->Attribute("imagesets")) {
        imagesetsPath = QString(elem->Attribute("imagesets"));
        absimagesetsPath = engine->getDirMedias() + imagesetsPath;
    }
    if (elem->Attribute("fonts")) {
        fontsPath = QString(elem->Attribute("fonts"));
        absfontsPath = engine->getDirMedias() + fontsPath;
    }
    if (elem->Attribute("layouts")) {
        layoutsPath = QString(elem->Attribute("layouts"));
        abslayoutsPath = engine->getDirMedias() + layoutsPath;
    }
    if (elem->Attribute("looknfeels")) {
        looknfeelsPath = QString(elem->Attribute("looknfeels"));
        abslooknfeelsPath = engine->getDirMedias() + looknfeelsPath;
    }
    if (elem->Attribute("schemas")) {
        schemasPath = QString(elem->Attribute("schemas"));
        absschemasPath = engine->getDirMedias() + schemasPath;
    }
    if (elem->Attribute("animations")) {
        animationsPath = QString(elem->Attribute("animations"));
        absanimationsPath = engine->getDirMedias() + animationsPath;
    }

    updateIhm();

    // Managers
    elem = elem->FirstChildElement();
    {
        tinyxml2::XMLElement* node = elem->FirstChildElement();
        while (node) {
            if (QString(node->Value()) == "SchemeManager") {
                SchemeManagerList->addItem(node->Attribute("create"));
            } else if (QString(node->Value()) == "FontManager") {
                fontManagerList->addItem(node->Attribute("create"));
            } else if (QString(node->Value()) == "ImagesetsManager") {
                imagesetManagerList->addItem(node->Attribute("create"));
            }
            node = node->NextSiblingElement();
        }
    }

    // Defaults
    elem = elem->NextSiblingElement();
    {
        if (elem->Attribute("defaultFont")) {
            defaultFont->setText(elem->Attribute("defaultFont"));
        }
        if (elem->Attribute("defaultMouseImageSet")
                && elem->Attribute("defaultMouseCursor")) {
            defaultMouseImageSet->setText(
                        elem->Attribute("defaultMouseImageSet"));
            defaultMouseCursor->setText(
                        elem->Attribute("defaultMouseCursor"));
        }
        if (elem->Attribute("defaultTooltip")) {
            defaultTooltip->setText(elem->Attribute("defaultTooltip"));
        }
    }

    // Layout
    elem = elem->NextSiblingElement();
    {
        if (elem->Attribute("file")) {
            layoutCombo->setCurrentIndex(
                        layoutCombo->findText(elem->Attribute("file")));

            elem = elem->FirstChildElement();
            while (elem) {
                QString elementId = elem->Attribute("elementName");
                QString widget = elem->Attribute("widget");
                QString event = elem->Attribute("event");
                QString code = elem->Attribute("code");
                QString script = elem->Attribute("script");

                addConnection();

                connections->item(
                            connections->rowCount() - 1, 0)->setText(widget);
                connections->item(
                            connections->rowCount() - 1, 1)->setText(event);

                QComboBox* listElements = static_cast<QComboBox*>
                        (connections->cellWidget(
                             connections->rowCount() - 1, 2));
                listElements->setCurrentIndex(
                            listElements->findText(elementId));

                QComboBox* listScripts = static_cast<QComboBox*>
                        (connections->cellWidget(
                             connections->rowCount() - 1, 3));
                listScripts->setCurrentIndex(listScripts->findText(script));

                //  connections->item(
                //            connections->rowCount() - 1, 3)->setText(script);
                connections->item(
                            connections->rowCount() - 1, 4)->setText(code);

                elem = elem->NextSiblingElement();
            }
        }
    }

    return true;
}

bool GuiEngine::writeXML(tinyxml2::XMLDocument* xmlDoc,
                         tinyxml2::XMLElement* elem) {
    if (layoutCombo->currentText() != "") {
        tinyxml2::XMLElement* GuiElement = xmlDoc->NewElement("Gui");
        GuiElement->SetAttribute("schemes", schemesPath.toUtf8().data());
        GuiElement->SetAttribute("imagesets", imagesetsPath.toUtf8().data());
        GuiElement->SetAttribute("fonts", fontsPath.toUtf8().data());
        GuiElement->SetAttribute("layouts", layoutsPath.toUtf8().data());
        GuiElement->SetAttribute("looknfeels", looknfeelsPath.toUtf8().data());
        GuiElement->SetAttribute("schemas", schemasPath.toUtf8().data());
        GuiElement->SetAttribute("animations", animationsPath.toUtf8().data());

        tinyxml2::XMLElement* ManagersElement = xmlDoc->NewElement("Managers");
        for (int i = 0; i < SchemeManagerList->count(); ++i) {
            tinyxml2::XMLElement* SchemeManagerElement =
                    xmlDoc->NewElement("SchemeManager");
            SchemeManagerElement->SetAttribute(
                        "create",
                        SchemeManagerList->item(i)->text().toUtf8().data());
            ManagersElement->LinkEndChild(SchemeManagerElement);
        }

        for (int i = 0; i < fontManagerList->count(); ++i) {
            tinyxml2::XMLElement* FontManagerElement =
                    xmlDoc->NewElement("FontManager");
            FontManagerElement->SetAttribute(
                        "create",
                        fontManagerList->item(i)->text().toUtf8().data());
            ManagersElement->LinkEndChild(FontManagerElement);
        }

        for (int i = 0; i < imagesetManagerList->count(); ++i) {
            tinyxml2::XMLElement* imageSetManagerElement =
                    xmlDoc->NewElement("ImagesetsManager");
            imageSetManagerElement->SetAttribute(
                        "create",
                        imagesetManagerList->item(i)->text().toUtf8().data());
            ManagersElement->LinkEndChild(imageSetManagerElement);
        }

        GuiElement->LinkEndChild(ManagersElement);


        tinyxml2::XMLElement* DefaultsElement = xmlDoc->NewElement("Defaults");
        DefaultsElement->SetAttribute(
                    "defaultFont", defaultFont->text().toUtf8().data());
        DefaultsElement->SetAttribute(
                    "defaultMouseImageSet",
                    defaultMouseImageSet->text().toUtf8().data());
        DefaultsElement->SetAttribute(
                    "defaultMouseCursor",
                    defaultMouseCursor->text().toUtf8().data());
        DefaultsElement->SetAttribute(
                    "defaultTooltip",
                    defaultTooltip->text().toUtf8().data());
        GuiElement->LinkEndChild(DefaultsElement);

        tinyxml2::XMLElement* LayoutElement = xmlDoc->NewElement("Layout");
        LayoutElement->SetAttribute(
                    "file", layoutCombo->currentText().toUtf8().data());

        for (int i = 0; i < connections->rowCount(); ++i) {
            tinyxml2::XMLElement* connectElement =
                    xmlDoc->NewElement("connect");
            connectElement->SetAttribute(
                        "widget",
                        connections->item(i, 0)->text().toUtf8().data());
            connectElement->SetAttribute(
                        "event",
                        connections->item(i, 1)->text().toUtf8().data());
            QComboBox* listElements = static_cast<QComboBox*>
                    (connections->cellWidget(i, 2));
            connectElement->SetAttribute(
                        "elementName",
                        listElements->currentText().toUtf8().data());
            QComboBox* listScripts = static_cast<QComboBox*>
                    (connections->cellWidget(i, 3));
            connectElement->SetAttribute(
                        "script",
                        listScripts->currentText().toUtf8().data());
            connectElement->SetAttribute(
                        "code",
                        connections->item(i, 4)->text().toUtf8().data());
            LayoutElement->LinkEndChild(connectElement);
        }

        GuiElement->LinkEndChild(LayoutElement);

        elem->LinkEndChild(GuiElement);
    }
    return true;
}

void GuiEngine::setGUI() {
    updateIhm();
    try {
        CEGUI::WindowManager& wmgr = CEGUI::WindowManager::getSingleton();
        CEGUI::System& sys = CEGUI::System::getSingleton();
        CEGUI::DefaultResourceProvider* rp =
                static_cast<CEGUI::DefaultResourceProvider*>
                (sys.getResourceProvider());
        CEGUI::MouseCursor& cursor = CEGUI::MouseCursor::getSingleton();

        wmgr.destroyAllWindows();
        cursor.hide();

        if (!layoutCombo->currentText().isEmpty()) {
            rp->setResourceGroupDirectory("schemes",
                                          absschemesPath.toUtf8().data());
            CEGUI::Scheme::setDefaultResourceGroup("schemes");
            rp->setResourceGroupDirectory("imagesets",
                                          absimagesetsPath.toUtf8().data());
            CEGUI::Imageset::setDefaultResourceGroup("imagesets");
            rp->setResourceGroupDirectory("fonts",
                                          absfontsPath.toUtf8().data());
            CEGUI::Font::setDefaultResourceGroup("fonts");
            rp->setResourceGroupDirectory("layouts",
                                          abslayoutsPath.toUtf8().data());
            CEGUI::WindowManager::setDefaultResourceGroup("layouts");
            rp->setResourceGroupDirectory("looknfeels",
                                          abslooknfeelsPath.toUtf8().data());
            CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
            rp->setResourceGroupDirectory("schemas",
                                          absschemasPath.toUtf8().data());
            rp->setResourceGroupDirectory("animations",
                                          absanimationsPath.toUtf8().data());

            for (int i = 0; i < SchemeManagerList->count(); ++i) {
                CEGUI::SchemeManager::getSingleton().create(
                            SchemeManagerList->item(i)->text().toUtf8().data());
            }
            for (int i = 0; i < fontManagerList->count(); ++i) {
                CEGUI::FontManager::getSingleton().create(
                            fontManagerList->item(i)->text().toUtf8().data());
            }

            sys.setDefaultFont(defaultFont->text().toUtf8().data());
            sys.setDefaultMouseCursor(
                        defaultMouseImageSet->text().toUtf8().data(),
                        defaultMouseCursor->text().toUtf8().data());
            sys.setDefaultTooltip(defaultTooltip->text().toUtf8().data());

            CEGUI::Window* myRoot = wmgr.createWindow("DefaultWindow", "Root");
            CEGUI::System::getSingleton().setGUISheet(myRoot);
            CEGUI::Window* newWindow = wmgr.loadWindowLayout(
                        layoutCombo->currentText().toUtf8().data());
            sys.getGUISheet()->addChildWindow(newWindow);
        }
    }
    catch(const CEGUI::Exception& e) {
        QMessageBox::warning(this, "Problem", e.getMessage().c_str(),
                             QMessageBox::Ok);
    }
}

}  // namespace MarbleEditor
