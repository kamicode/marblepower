/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_FileChooser.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ENGINES_FILECHOOSER_H_
#define MARBLEEDITOR_ENGINES_FILECHOOSER_H_

#include <ui_FileChooser.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

/*! \class FileChooser
 * \brief
 *
 *
 */
class FileChooser : public QDialog, private Ui::FileChooser {
    Q_OBJECT

    public:
        explicit FileChooser(QString directory, bool showPreview,
                             QWidget *parent = 0);
        virtual ~FileChooser() {}

        QString getSelectedAbsoluteFilePath();
        QString getSelectedRelativeFilePath();

    private slots:
        void select(QModelIndex index);
        void accept();

    private:
        QFileSystemModel* modele;
        QString directory;
        QString selectedFile;

    private:
        DISALLOW_COPY_AND_ASSIGN(FileChooser);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ENGINES_FILECHOOSER_H_
