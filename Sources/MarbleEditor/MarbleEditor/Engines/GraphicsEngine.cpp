/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Engines/GraphicsEngine.h"

#include <map>

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Element.h"
#include "MarbleEditor/Elements/Camera.h"

namespace MarbleEditor {

GraphicsEngine::GraphicsEngine(Engine* engine, QWidget* parent)
    : QWidget(parent) {
    this->engine = engine;
    setAttribute(Qt::WA_PaintOnScreen, true);
    setAttribute(Qt::WA_OpaquePaintEvent, true);
    setMouseTracking(true);
    setFocusPolicy(Qt::StrongFocus);
    setFocus(Qt::OtherFocusReason);
    setAutoFillBackground(false);
    majPress = false;
    ctrlPress = false;
    upPress = false;
    downPress = false;
    leftPress = false;
    rightPress = false;

    cameraEditor = true;

    irr::SIrrlichtCreationParameters createParams =
            irr::SIrrlichtCreationParameters();
    createParams.DriverType = irr::video::EDT_DIRECT3D9;
    createParams.WindowSize = irr::core::dimension2d<irr::u32>(
                size().width(), size().height());
    createParams.EventReceiver = 0;
    createParams.Stencilbuffer = true;
    createParams.IgnoreInput = true;
    createParams.Bits = 32;
    createParams.ZBufferBits = 32;
    createParams.Fullscreen = false;
    createParams.Stencilbuffer = true;
    createParams.Vsync = true;
    createParams.AntiAlias = 2;
    createParams.WithAlphaChannel = true;
    createParams.Doublebuffer = true;
    createParams.Stereobuffer = true;
    createParams.HighPrecisionFPU = true;
    createParams.WindowSize.Width = this->size().width();
    createParams.WindowSize.Height = this->size().height();

    createParams.WindowId = reinterpret_cast<void*>(winId());

    device = irr::createDeviceEx(createParams);
    sceneManager = device->getSceneManager();
    driver = device->getVideoDriver();

    camera = sceneManager->addCameraSceneNode(
                0, irr::core::vector3df(0, 5, -5));

    ceguiRenderer = &(CEGUI::IrrlichtRenderer::bootstrapSystem(* device));

    timerId = startTimer(30);
}

GraphicsEngine::~GraphicsEngine() {
    killTimer(timerId);
    CEGUI::IrrlichtRenderer::destroySystem();
    ceguiRenderer = NULL;

    device->closeDevice();
    device->drop();
}

void GraphicsEngine::unLoadMap() {
    sceneManager->clear();
    cameraEditor = true;
    camera = sceneManager->addCameraSceneNode(
                0, irr::core::vector3df(0, 5, -5));
}

irr::IrrlichtDevice* GraphicsEngine::getDevice() {
    return device;
}

irr::scene::ISceneManager* GraphicsEngine::getSceneManager() {
    return sceneManager;
}


irr::video::IVideoDriver* GraphicsEngine::getDriver() {
    return driver;
}

irr::scene::ICameraSceneNode* GraphicsEngine::getCamera() {
    return camera;
}

void GraphicsEngine::timerEvent(QTimerEvent* /*event*/) {
    static int lastTime = device->getTimer()->getTime();
    int now = device->getTimer()->getTime();
    int timeSinceLastTick = now - lastTime;
    lastTime = now;

    engine->getPhysicsEngine()->update(timeSinceLastTick);

    if (device->run()) {
        if (isVisible()) {
            engine->setWindowTitle("MarbleEditor --- FPS : "
                                   + QString::number(driver->getFPS()));

            int width = driver->getScreenSize().Width;
            int height = driver->getScreenSize().Height;

            driver->setViewPort(irr::core::rect<irr::s32>(0, 0, width, height));

            driver->beginScene(true, true, irr::video::SColor(255, 00, 0, 0));
            if (isEnabled()) {
                if (cameraEditor) {
                    sceneManager->setActiveCamera(camera);
                    sceneManager->drawAll();
                    specificRender();
                } else {
                    std::map<QString, Camera*>cameras = engine->getCameras();
                    std::map<QString, Camera*>::const_iterator it,
                            end = cameras.end();
                    for (it = cameras.begin(); it != end; ++it) {
                        Camera* cam = it->second;
                        sceneManager->setActiveCamera(cam->getCamera());
                        driver->setViewPort(
                                    cam->getAbsoluteViewport(width, height));
                        sceneManager->drawAll();
                    }
                    CEGUI::System::getSingleton().renderGUI();
                }
            }
            driver->endScene();
        } else {
            device->yield();
        }
    }
}

void GraphicsEngine::specificRender() {
    irr::core::matrix4 oldTransform = irr::core::matrix4(
                driver->getTransform(irr::video::ETS_WORLD));
    driver->setTransform(irr::video::ETS_WORLD, irr::core::IdentityMatrix);

    irr::video::SMaterial oldMaterial = irr::video::SMaterial(
                driver->getMaterial2D());
    irr::video::SMaterial mat;
    mat.Lighting = false;
    mat.Thickness = 1;
    driver->setMaterial(mat);

    std::map<QString, Element* > elements = engine->getElements();
    std::map<QString, Element*>::const_iterator it, end = elements.end();
    for (it = elements.begin(); it != end; ++it) {
        it->second->specificRender();
    }

    for (int i = -100; i <= 100; i += 2) {
        driver->draw3DLine(irr::core::vector3df(i, 0, -100),
                           irr::core::vector3df(i, 0, 100),
                           irr::video::SColor(255, 150, 150, 150));

        driver->draw3DLine(irr::core::vector3df(-100, 0, i),
                           irr::core::vector3df(100, 0, i),
                           irr::video::SColor(255, 150, 150, 150));
    }

    Element* elt = engine->getSelectedElement();
    if (elt != NULL) {
        mat.Thickness = 3;
        driver->setMaterial(mat);
        irr::scene::ISceneNode* node = elt->getNode();
        if (node) {
            irr::core::vector3df pos = node->getPosition();
            driver->draw3DLine(pos, pos + irr::core::vector3df(20, 0, 0),
                               irr::video::SColor(255, 255, 0, 0));
            driver->draw3DLine(pos, pos + irr::core::vector3df(0, 20, 0),
                               irr::video::SColor(255, 0, 255, 0));
            driver->draw3DLine(pos, pos + irr::core::vector3df(0, 0, 20),
                               irr::video::SColor(255, 0, 0, 255));
        }
    }


    btDiscreteDynamicsWorld* nWorld = engine->getPhysicsEngine()->getWorld();

    if (nWorld) {
        nWorld->debugDrawWorld();
    }

    driver->setMaterial(oldMaterial);
    driver->setTransform(irr::video::ETS_WORLD, oldTransform);
}

void GraphicsEngine::keyPressEvent(QKeyEvent* event) {
    if (event->key() == Qt::Key_Shift) {
        majPress = true;
    }
    if (event->key() == Qt::Key_Control) {
        ctrlPress = true;
    } else if (event->key() == Qt::Key_Left) {
        leftPress = true;
    } else if (event->key() == Qt::Key_Right) {
        rightPress = true;
    } else if (event->key() == Qt::Key_Up) {
        upPress = true;
    } else if (event->key() == Qt::Key_Down) {
        downPress = true;
    }

    if (cameraEditor) {
        if (upPress || downPress || rightPress || leftPress) {
            irr::core::vector3df posCam = camera->getPosition();
            irr::core::vector3df target = camera->getTarget();
            target.normalize();
            irr::core::vector3df upVector =
                    camera->getRotation().rotationToDirection(
                        camera->getUpVector());
            upVector.normalize();
            irr::core::vector3df leftVector = target.crossProduct(upVector);
            leftVector.normalize();

            if (upPress != downPress) {
                if (downPress) {
                    target *= -1;
                }
                if (majPress) {
                    target *= 2;
                }
            } else {
                target *= 0;
            }

            if (leftPress != rightPress) {
                if (rightPress) {
                    leftVector *= -1;
                }
                if (majPress) {
                    leftVector *= 2;
                }
            } else {
                leftVector *= 0;
            }

            posCam += leftVector + target;

            if (posCam.X > 1000) {
                posCam.X = 1000;
            }
            if (posCam.Y > 1000) {
                posCam.Y = 1000;
            }
            if (posCam.Z > 1000) {
                posCam.Z = 1000;
            }
            if (posCam.X < -1000) {
                posCam.X = -1000;
            }
            if (posCam.Y < -1000) {
                posCam.Y = -1000;
            }
            if (posCam.Z < -1000) {
                posCam.Z = -1000;
            }

            camera->setPosition(posCam);
            camera->updateAbsolutePosition();
            irr::core::vector3df rotCam = camera->getRotation();
            irr::core::vector3df vel(0.0f, 0.0f, 10000.0f);
            irr::core::matrix4 mat;
            mat.setRotationDegrees(rotCam);
            mat.transformVect(vel);
            camera->setTarget(camera->getAbsolutePosition() + vel);
        }
    }
}

void GraphicsEngine::keyReleaseEvent(QKeyEvent* event) {
    if (event->key() == Qt::Key_Shift) {
        majPress = false;
    }
    if (event->key() == Qt::Key_Control) {
        ctrlPress = false;
    } else if (event->key() == Qt::Key_Left) {
        leftPress = false;
    } else if (event->key() == Qt::Key_Right) {
        rightPress = false;
    } else if (event->key() == Qt::Key_Up) {
        upPress = false;
    } else if (event->key() == Qt::Key_Down) {
        downPress = false;
    }
}

void GraphicsEngine::resizeEvent(QResizeEvent* event) {
    int newWidth = event->size().width();
    int newHeight = event->size().height();

    driver->OnResize(irr::core::dimension2d<irr::u32>(newWidth, newHeight));
    CEGUI::System::getSingleton().notifyDisplaySizeChanged(
                CEGUI::Size(newWidth, newHeight));

    camera->setAspectRatio((irr::f32) newWidth /(irr::f32) newHeight);

    std::map<QString, Camera*>cameras = engine->getCameras();
    std::map<QString, Camera*>::const_iterator it, end = cameras.end();
    for (it = cameras.begin(); it != end; ++it) {
        Camera* cam = it->second;
        irr::core::rect<irr::s32> view =
                cam->getAbsoluteViewport(newWidth, newHeight);
        cam->getCamera()->setAspectRatio(
                    (irr::f32)view.getWidth() / (irr::f32)view.getHeight());
    }
}

void GraphicsEngine::mousePressEvent(QMouseEvent* event) {
    if (event->button() == Qt::LeftButton) {
        irr::core::position2d<irr::s32> pos(event->pos().x(),
                                            event->pos().y());

        irr::scene::ISceneCollisionManager* colManager =
                sceneManager->getSceneCollisionManager();
        irr::core::line3d<irr::f32> line =
                colManager->getRayFromScreenCoordinates(pos, camera);

        irr::scene::ISceneNode* selectedSceneNode =
                colManager->getSceneNodeFromRayBB(line);

        engine->setSelectedElement(NULL);

        if (selectedSceneNode != NULL) {
            std::map<QString, Element*> elements = engine->getElements();
            std::map<QString, Element*>::const_iterator it,
                    end = elements.end();
            for (it = elements.begin(); it != end; ++it) {
                if (it->second->getNode() == selectedSceneNode) {
                    engine->setSelectedElement(it->second);
                    return;
                }
            }
        }
    }
}

void GraphicsEngine::mouseReleaseEvent(QMouseEvent* /*event*/) {
}

void GraphicsEngine::mouseDoubleClickEvent(QMouseEvent* /*event*/) {
}

void GraphicsEngine::mouseMoveEvent(QMouseEvent* event) {
    if (event->buttons() == Qt::RightButton) {
        QPoint posMouse = event->pos();

        irr::core::vector3df rotCam = camera->getRotation() +
                irr::core::vector3df(posMouse.y() - pos.y(),
                                     posMouse.x() - pos.x(), 0);

        if (rotCam.X > 89.0f) {
            rotCam.X = 89.0f;
        }
        if (rotCam.X < -89.0f) {
            rotCam.X = -89.0f;
        }

        camera->setRotation(rotCam);
    } else if (event->buttons() == Qt::MiddleButton) {
        QPoint posMouse= event->pos();
        irr::core::vector3df posCam = camera->getPosition();
        irr::core::vector3df target = camera->getTarget();
        target.normalize();
        irr::core::vector3df upVector =
                camera->getRotation().rotationToDirection(
                    camera->getUpVector());
        upVector.normalize();
        irr::core::vector3df leftVector = target.crossProduct(upVector);
        leftVector.normalize();

        if (majPress) {
            upVector *=(pos.y() - posMouse.y()) / 10.0;
            leftVector *=(pos.x() - posMouse.x()) / 10.0;
        } else {
            upVector *=(pos.y() - posMouse.y()) / 100.0;
            leftVector *=(pos.x() - posMouse.x()) / 100.0;
        }

        posCam += upVector + leftVector;

        if (posCam.X > 1000) {
            posCam.X = 1000;
        }
        if (posCam.Y > 1000) {
            posCam.Y = 1000;
        }
        if (posCam.Z > 1000) {
            posCam.Z = 1000;
        }
        if (posCam.X < -1000) {
            posCam.X = -1000;
        }
        if (posCam.Y < -1000) {
            posCam.Y = -1000;
        }
        if (posCam.Z < -1000) {
            posCam.Z = -1000;
        }

        camera->setPosition(posCam);
        camera->updateAbsolutePosition();
    } else if (event->buttons() == Qt::LeftButton && ctrlPress) {
        Element* elt = engine->getSelectedElement();
        if (elt != NULL) {
            QPoint posMouse= event->pos();
            irr::core::vector3df posObj = elt->getNode()->getPosition();
            irr::core::vector3df target = camera->getTarget();
            target.normalize();
            irr::core::vector3df upVector =
                    camera->getRotation().rotationToDirection(
                        camera->getUpVector());
            upVector.normalize();
            irr::core::vector3df leftVector = target.crossProduct(upVector);
            leftVector.normalize();

            if (majPress) {
                upVector *=(pos.y() - posMouse.y()) / 10.0;
                leftVector *=(pos.x() - posMouse.x()) / 10.0;
            } else {
                upVector *=(pos.y() - posMouse.y()) / 100.0;
                leftVector *=(pos.x() - posMouse.x()) / 100.0;
            }

            posObj += upVector + leftVector;

            elt->setPosX(posObj.X);
            elt->setPosY(posObj.Y);
            elt->setPosZ(posObj.Z);
        }
    }

    irr::core::vector3df rotCam = camera->getRotation();
    irr::core::vector3df vel(0.0f, 0.0f, 10000.0f);
    irr::core::matrix4 mat;
    mat.setRotationDegrees(rotCam);
    mat.transformVect(vel);
    camera->setTarget(camera->getAbsolutePosition() + vel);

    this->pos = event->pos();
}

void GraphicsEngine::wheelEvent(QWheelEvent* event) {
    irr::core::vector3df pos = camera->getTarget()* event->delta();
    pos.normalize();

    if (majPress) {
        pos *= 10;
    }

    pos += camera->getPosition();

    if (pos.X > 1000) {
        pos.X = 1000;
    }
    if (pos.Y > 1000) {
        pos.Y = 1000;
    }
    if (pos.Z > 1000) {
        pos.Z = 1000;
    }
    if (pos.X < -1000) {
        pos.X = -1000;
    }
    if (pos.Y < -1000) {
        pos.Y = -1000;
    }
    if (pos.Z < -1000) {
        pos.Z = -1000;
    }

    camera->setPosition(pos);
    camera->updateAbsolutePosition();
}

QPaintEngine* GraphicsEngine::paintEngine() const {
    return 0;
}

void GraphicsEngine::switchCamera() {
    cameraEditor = !cameraEditor;
}

}  // namespace MarbleEditor
