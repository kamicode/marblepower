/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_MapsManager.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ENGINES_MAPSMANAGER_H_
#define MARBLEEDITOR_ENGINES_MAPSMANAGER_H_

#include <ui_MapsManager.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class MapsManager
 * \brief
 *
 *
 */
class MapsManager : public QDialog, private Ui::MapsManager {
    Q_OBJECT

    public:
        explicit MapsManager(Engine* engine, QWidget *parent = 0);
        virtual ~MapsManager() {}

    public slots:
        void loadProject();
        void writeProject();
        void shutdown();
        void add();
        void remove();

    private:
        Engine* engine;
        QString dirProj;

    private:
        DISALLOW_COPY_AND_ASSIGN(MapsManager);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ENGINES_MAPSMANAGER_H_
