/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Engines/Logger.h"

#include <string>

#include "MarbleEditor/Engines/Engine.h"

namespace MarbleEditor {

const std::string Logger::PRIORITY_NAMES[] = {
    "CONFIG",
    "INFO",
    "DEBUG",
    "WARNING",
    "ERROR"
};

Logger Logger::instance;

Logger::Logger() : active(false) {}

CEGUI::LoggingLevel Logger::loggerLevelToCeguiLevel(Priority level) {
    switch (level) {
    case Error: {
        return CEGUI::Errors;
    }
    case Warning: {
        return CEGUI::Warnings;
    }
    case Debug: {
        return CEGUI::Standard;
    }
    case Info: {
        return CEGUI::Informative;
    }
    case Config: {
        return CEGUI::Insane;
    }
    default: {
        return CEGUI::Standard;
    }
    }
    return CEGUI::Standard;
}

Priority Logger::ceguiLevelToLoggerLevel(CEGUI::LoggingLevel level) {
    switch (level) {
    case CEGUI::Errors: {
        return Error;
    }
    case CEGUI::Warnings: {
        return Warning;
    }
    case CEGUI::Standard: {
        return Debug;
    }
    case CEGUI::Informative: {
        return Info;
    }
    case CEGUI::Insane: {
        return Config;
    }
    default: {
        return Debug;
    }
    }
    return Debug;
}

irr::ELOG_LEVEL Logger::loggerLevelToIrrlichtLevel(Priority level) {
    switch (level) {
    case Error: {
        return irr::ELL_ERROR;
    }
    case Warning: {
        return irr::ELL_WARNING;
    }
    case Debug: {
        return irr::ELL_INFORMATION;
    }
    case Info: {
        return irr::ELL_INFORMATION;
    }
    case Config: {
        return irr::ELL_INFORMATION;
    }
    default: {
        return irr::ELL_INFORMATION;
    }
    }
    return irr::ELL_INFORMATION;
}

Priority Logger::irrlichtLevelToLoggerLevel(irr::ELOG_LEVEL level) {
    switch (level) {
    case irr::ELL_ERROR: {
        return Error;
    }
    case irr::ELL_WARNING: {
        return Warning;
    }
    case irr::ELL_INFORMATION: {
        return Debug;
    }
    default: {
        return Debug;
    }
    }
    return Debug;
}

void Logger::Start(Priority minPriority,
                   const std::string& logFile,
                   Engine* engine) {
    instance.engine = engine;

    instance.active = true;
    setLogLevel(minPriority);

    if (logFile != "") {
        if (instance.fileStream.is_open()) {
            instance.fileStream.close();
        }
        instance.fileStream.open(logFile.c_str());
    }
}

void Logger::Stop() {
    instance.active = false;
    if (instance.fileStream.is_open()) {
        instance.fileStream.close();
    }
}

void Logger::Write(Priority priority, const std::string& message) {
    if (instance.active && priority >= instance.minPriority
            && instance.fileStream.is_open()) {
        std::ostream& stream = instance.fileStream;
        stream  << PRIORITY_NAMES[priority] << " -> " << message << std::endl;
    }
}

void Logger::setLogLevel(Priority level) {
    if (instance.active) {
        instance.minPriority = level;
        instance.setLoggingLevel(Logger::loggerLevelToCeguiLevel(level));
        instance.engine->getGraphicsEngine()->getDevice()
                ->getLogger()->setLogLevel(
                    Logger::loggerLevelToIrrlichtLevel(level));
    }
}

void Logger::logEvent(const CEGUI::String& message, CEGUI::LoggingLevel log) {
    LOGGER_WRITE(Logger::ceguiLevelToLoggerLevel(log),
                 QString(QString("CEGUI : ")
                         + QString(message.c_str())).toUtf8().data());
}

}  // namespace MarbleEditor
