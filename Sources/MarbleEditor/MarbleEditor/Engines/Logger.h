/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Logger.h
 * \brief User Log Manager
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ENGINES_LOGGER_H_
#define MARBLEEDITOR_ENGINES_LOGGER_H_

#include <string>

#include "MarbleEditor/Divers.h"

#define LOGGER_START(MIN_PRIORITY, FILE, ENGINE) \
    MarbleEditor::Logger::Start(MIN_PRIORITY, FILE, ENGINE)
#define LOGGER_STOP() MarbleEditor::Logger::Stop()
#define LOGGER_WRITE(PRIORITY, MESSAGE) \
    MarbleEditor::Logger::Write(PRIORITY, MESSAGE)
#define LOGGER_SET_LEVEL(PRIORITY) MarbleEditor::Logger::setLogLevel(PRIORITY)

#define LOG_Config MarbleEditor::Config
#define LOG_Info MarbleEditor::Info
#define LOG_Debug MarbleEditor::Debug
#define LOG_Warning MarbleEditor::Warning
#define LOG_Error MarbleEditor::Error

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class Logger
 * \brief
 *
 *
 */
class Logger : private CEGUI::Logger {
    public:
        virtual ~Logger() {}

        void logEvent(const CEGUI::String& message, CEGUI::LoggingLevel log);
        void setLogFilename(const CEGUI::String&, bool) { }

        static void Write(Priority priority, const std::string& message);
        static void Start(Priority minPriority, const std::string& logFile,
                          Engine* engine);
        static void Stop();
        static void setLogLevel(Priority level);
        static CEGUI::LoggingLevel loggerLevelToCeguiLevel(Priority level);
        static Priority ceguiLevelToLoggerLevel(CEGUI::LoggingLevel level);
        static irr::ELOG_LEVEL loggerLevelToIrrlichtLevel(Priority level);
        static Priority irrlichtLevelToLoggerLevel(irr::ELOG_LEVEL level);

    private:
        explicit Logger();

    private:
        Engine* engine;

        bool active;
        std::ofstream fileStream;
        Priority minPriority;

        static const std::string PRIORITY_NAMES[];
        static Logger instance;

    private:
        DISALLOW_COPY_AND_ASSIGN(Logger);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ENGINES_LOGGER_H_
