/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file Precompiled.h
 * \brief Precompiled header
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_PRECOMPILED_H_
#define MARBLEEDITOR_PRECOMPILED_H_

#if defined __cplusplus

#include <QQueue>
#include <QDir>
#include <QFileSystemModel>
#include <QSplashScreen>
#include <QKeyEvent>
#include <QTranslator>
#include <QLibraryInfo>
#include <QInputDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <QDesktopServices>
#include <QSignalMapper>
#include <QTextStream>
#include <QTextEdit>
#include <QDesktopWidget>
#include <QColorDialog>
#include <QSettings>
#include <QProcess>

#include <Qsci/qsciscintilla.h>
#include <Qsci/qscilexerjavascript.h>
#include <Qsci/qscilexerxml.h>
#include <Qsci/qsciapis.h>

#include <irrlicht.h>

#include <btBulletDynamicsCommon.h>
#include <btBulletCollisionCommon.h>

#include <tinyxml2.h>

#include <CEGUI.h>
#include <RendererModules/Irrlicht/CEGUIIrrlichtRenderer.h>

#include <algorithm>
#include <ctime>
#include <queue>
#include <string>
#include <map>
#include <vector>
#include <set>

#endif

#endif  // MARBLEEDITOR_PRECOMPILED_H_
