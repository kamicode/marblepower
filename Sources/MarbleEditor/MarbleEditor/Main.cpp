/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Engines/ProjectChooser.h"
#include "MarbleEditor/Engines/Engine.h"

int main(int argc, char* *argv) {
    QApplication app(argc, argv);

    app.addLibraryPath(app.applicationDirPath() + "/Plugins");

    Q_INIT_RESOURCE(Resources);

    QApplication::setStyle("plastique");

    QSplashScreen splash(QPixmap(":/Resources/splash.jpg"));
    splash.show();
    QApplication::processEvents();
    splash.repaint();

    QString locale = QLocale::system().name();
    QString location = ":/Resources/Translations";

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + locale, location);
    app.installTranslator(&qtTranslator);

    QTranslator translator;
    translator.load(locale, location);
    app.installTranslator(&translator);


    MarbleEditor::Engine* m = new MarbleEditor::Engine();

    if (app.arguments().size() == 3) {
        QString f = app.arguments().at(1);
        QFileInfo  file(f);
        if (file.exists()) {
            f = file.canonicalFilePath();
        }
        m->loadProject(f);
        QString map = app.arguments().at(2);
        m->loadMap(map);
    }
    m->show();

    splash.finish(m);

    return app.exec();
}
