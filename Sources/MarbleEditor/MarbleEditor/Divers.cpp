/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Divers.h"

#include <algorithm>

namespace MarbleEditor {

IrrlichtDebugDrawer::IrrlichtDebugDrawer(irr::video::IVideoDriver* driver)
    : m_driver(driver), m_debugMode(0) {
}

void IrrlichtDebugDrawer::drawLine(const btVector3& from, const btVector3& to,
                                   const btVector3& color) {
    irr::video::SColor newColor(255, (irr::u32)color[0],
            (irr::u32)color[1], (irr::u32)color[2]);
    if (color[0] <= 1.0 && color[0] > 0.0)
        newColor.setRed((irr::u32)(color[0] * 255.0));
    if (color[1] <= 1.0 && color[1] > 0.0)
        newColor.setGreen((irr::u32)(color[1] * 255.0));
    if (color[2] <= 1.0 && color[2] > 0.0)
        newColor.setBlue((irr::u32)(color[2] * 255.0));

    m_driver->draw3DLine(irr::core::vector3df(from[0], from[1], from[2]),
            irr::core::vector3df(to[0], to[1], to[2]), newColor);
}

void IrrlichtDebugDrawer::drawContactPoint(const btVector3& PointOnB,
                                           const btVector3& normalOnB,
                                           btScalar distance, int /*lifeTime*/,
                                           const btVector3& color) {
    irr::video::SColor newColor(255, (irr::u32)color[0], (irr::u32)color[1],
            (irr::u32)color[2]);
    if (color[0] <= 1.0 && color[0] > 0.0)
        newColor.setRed((irr::u32)(color[0] * 255.0));
    if (color[1] <= 1.0 && color[1] > 0.0)
        newColor.setGreen((irr::u32)(color[1] * 255.0));
    if (color[2] <= 1.0 && color[2] > 0.0)
        newColor.setBlue((irr::u32)(color[2] * 255.0));

    const btVector3 to(PointOnB + normalOnB * distance);
    m_driver->draw3DLine(
                irr::core::vector3df(PointOnB[0], PointOnB[1], PointOnB[2]),
            irr::core::vector3df(to[0], to[1], to[2]), newColor);
}

void IrrlichtDebugDrawer::drawTriangle(const btVector3& v0,
                                       const btVector3& v1,
                                       const btVector3& v2,
                                       const btVector3& color,
                                       btScalar alpha) {
    irr::video::SColor irrcolor(alpha * 255, color[0] * 255,
            color[1] * 255, color[2] * 255);
    m_driver->draw3DTriangle(irr::core::triangle3df(
                                 irr::core::vector3df(v0[0], v0[1], v0[2]),
            irr::core::vector3df(v1[0], v1[1], v1[2]),
            irr::core::vector3df(v2[0], v2[1], v2[2])), irrcolor);
}

void IrrlichtDebugDrawer::drawSphere(btScalar radius,
                                     const btTransform& transform,
                                     const btVector3& color) {
    irr::video::SColor irrcolor(255, color[0] * 255,
            color[1] * 255, color[2] * 255);

    btVector3 buf = transform.getOrigin();
    irr::core::vector3df centre(buf[0], buf[1], buf[2]);

    irr::core::vector3df oldPos, pos;

    float angleStep = irr::core::PI / 8;
    float precision = 0.2f;

    for (float theta = 0; theta < irr::core::PI * 2; theta += angleStep) {
        float cosTheta = cos(theta);
        float sinTheta = sin(theta);
        oldPos = irr::core::vector3df(0, 0, radius) + centre;
        for (float phy = 0; phy < irr::core::PI * 2; phy += precision) {
            float x = radius * cosTheta * sin(phy);
            float y = radius * sinTheta * sin(phy);
            float z = radius * cos(phy);

            pos = irr::core::vector3df(x, y, z) + centre;
            m_driver->draw3DLine(oldPos, pos, irrcolor);
            oldPos = pos;
        }
        pos = irr::core::vector3df(0, 0, radius) + centre;
        m_driver->draw3DLine(oldPos, pos, irrcolor);
        oldPos = pos;
    }

    for (float phy = angleStep; phy < irr::core::PI * 2; phy += angleStep) {
        float cosPhy = cos(phy);
        float sinPhy = sin(phy);
        oldPos = irr::core::vector3df(
                    radius * sinPhy, 0, radius * cosPhy) + centre;
        for (float theta = 0; theta < irr::core::PI * 2; theta += precision) {
            float x = radius * cos(theta) * sinPhy;
            float y = radius * sin(theta) * sinPhy;
            float z = radius * cosPhy;

            pos = irr::core::vector3df(x, y, z) + centre;
            m_driver->draw3DLine(oldPos, pos, irrcolor);
            oldPos = pos;
        }
        pos = irr::core::vector3df(
                    radius * sinPhy, 0, radius * cosPhy) + centre;
        m_driver->draw3DLine(oldPos, pos, irrcolor);
        oldPos = pos;
    }
}

void IrrlichtDebugDrawer::setDebugMode(int debugMode) {
    m_debugMode = debugMode;
}

int IrrlichtDebugDrawer::getDebugMode() const {
    return m_debugMode;
}

irr::video::E_DRIVER_TYPE Convert::stringDriver(QString s) {
    if (s == "EDT_NULL")
        return irr::video::EDT_NULL;
    if (s == "EDT_SOFTWARE")
        return irr::video::EDT_SOFTWARE;
    if (s == "EDT_BURNINGSVIDEO")
        return irr::video::EDT_BURNINGSVIDEO;
    if (s == "EDT_DIRECT3D8")
        return irr::video::EDT_DIRECT3D8;
    if (s == "EDT_DIRECT3D9")
        return irr::video::EDT_DIRECT3D9;
    if (s == "EDT_OPENGL")
        return irr::video::EDT_OPENGL;
    if (s == "EDT_COUNT")
        return irr::video::EDT_COUNT;

    return irr::video::EDT_NULL;
}

QString Convert::driverString(irr::video::E_DRIVER_TYPE s) {
    if (s == irr::video::EDT_NULL)
        return "EDT_NULL";
    if (s == irr::video::EDT_SOFTWARE)
        return "EDT_SOFTWARE";
    if (s == irr::video::EDT_BURNINGSVIDEO)
        return "EDT_BURNINGSVIDEO";
    if (s == irr::video::EDT_DIRECT3D8)
        return "EDT_DIRECT3D8";
    if (s == irr::video::EDT_DIRECT3D9)
        return "EDT_DIRECT3D9";
    if (s == irr::video::EDT_OPENGL)
        return "EDT_OPENGL";
    if (s == irr::video::EDT_COUNT)
        return "EDT_COUNT";

    return "";
}

TypeBody Convert::stringBody(QString s) {
    if (s == "Box")
        return Box;
    if (s == "Sphere")
        return Sphere;
    if (s == "Cone")
        return Cone;
    if (s == "Capsule")
        return Capsule;
    if (s == "Cylinder")
        return Cylinder;
    if (s == "ChamferCylinder")
        return ChamferCylinder;
    if (s == "ConvexHull")
        return ConvexHull;
    if (s == "Compound")
        return Compound;
    if (s == "Tree")
        return Tree;
    if (s == "TerrainTree")
        return TerrainTree;

    return None;
}

QString Convert::bodyString(TypeBody s) {
    if (s == Box)
        return "Box";
    if (s == Sphere)
        return "Sphere";
    if (s == Cone)
        return "Cone";
    if (s == Capsule)
        return "Capsule";
    if (s == Cylinder)
        return "Cylinder";
    if (s == ChamferCylinder)
        return "ChamferCylinder";
    if (s == ConvexHull)
        return "ConvexHull";
    if (s == Compound)
        return "Compound";
    if (s == Tree)
        return "Tree";
    if (s == TerrainTree)
        return "TerrainTree";

    return "None";
}

bool Convert::stringBool(QString s) {
    if (s == "true" || s == "True" || s == "TRUE")
        return true;

    return false;
}

QString Convert::boolString(bool s) {
    if (s)
        return "true";

    return "false";
}

irr::video::E_MATERIAL_TYPE Convert::stringMaterialType(QString s) {
    if (s == "EMT_SOLID")
        return irr::video::EMT_SOLID;
    if (s == "EMT_SOLID_2_LAYER")
        return irr::video::EMT_SOLID_2_LAYER;
    if (s == "EMT_LIGHTMAP")
        return irr::video::EMT_LIGHTMAP;
    if (s == "EMT_LIGHTMAP_ADD")
        return irr::video::EMT_LIGHTMAP_ADD;
    if (s == "EMT_LIGHTMAP_M2")
        return irr::video::EMT_LIGHTMAP_M2;
    if (s == "EMT_LIGHTMAP_M4")
        return irr::video::EMT_LIGHTMAP_M4;
    if (s == "EMT_LIGHTMAP_LIGHTING")
        return irr::video::EMT_LIGHTMAP_LIGHTING;
    if (s == "EMT_LIGHTMAP_LIGHTING_M2")
        return irr::video::EMT_LIGHTMAP_LIGHTING_M2;
    if (s == "EMT_LIGHTMAP_LIGHTING_M4")
        return irr::video::EMT_LIGHTMAP_LIGHTING_M4;
    if (s == "EMT_DETAIL_MAP")
        return irr::video::EMT_DETAIL_MAP;
    if (s == "EMT_SPHERE_MAP")
        return irr::video::EMT_SPHERE_MAP;
    if (s == "EMT_REFLECTION_2_LAYER")
        return irr::video::EMT_REFLECTION_2_LAYER;
    if (s == "EMT_TRANSPARENT_ADD_COLOR")
        return irr::video::EMT_TRANSPARENT_ADD_COLOR;
    if (s == "EMT_TRANSPARENT_ALPHA_CHANNEL")
        return irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL;
    if (s == "EMT_TRANSPARENT_ALPHA_CHANNEL_REF")
        return irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF;
    if (s == "EMT_TRANSPARENT_VERTEX_ALPHA")
        return irr::video::EMT_TRANSPARENT_VERTEX_ALPHA;
    if (s == "EMT_TRANSPARENT_REFLECTION_2_LAYER")
        return irr::video::EMT_TRANSPARENT_REFLECTION_2_LAYER;
    if (s == "EMT_NORMAL_MAP_SOLID")
        return irr::video::EMT_NORMAL_MAP_SOLID;
    if (s == "EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR")
        return irr::video::EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR;
    if (s == "EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA")
        return irr::video::EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA;
    if (s == "EMT_PARALLAX_MAP_SOLID")
        return irr::video::EMT_PARALLAX_MAP_SOLID;
    if (s == "EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR")
        return irr::video::EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR;
    if (s == "EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA")
        return irr::video::EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA;
    if (s == "EMT_ONETEXTURE_BLEND")
        return irr::video::EMT_ONETEXTURE_BLEND;
    if (s == "EMT_FORCE_32BIT")
        return irr::video::EMT_FORCE_32BIT;

    return irr::video::EMT_SOLID;
}


QString Convert::materialTypeString(irr::video::E_MATERIAL_TYPE s) {
    if (s == irr::video::EMT_SOLID)
        return "EMT_SOLID";
    if (s == irr::video::EMT_SOLID_2_LAYER)
        return "EMT_SOLID_2_LAYER";
    if (s == irr::video::EMT_LIGHTMAP)
        return "EMT_LIGHTMAP";
    if (s == irr::video::EMT_LIGHTMAP_ADD)
        return "EMT_LIGHTMAP_ADD";
    if (s == irr::video::EMT_LIGHTMAP_M2)
        return "EMT_LIGHTMAP_M2";
    if (s == irr::video::EMT_LIGHTMAP_M4)
        return "EMT_LIGHTMAP_M4";
    if (s == irr::video::EMT_LIGHTMAP_LIGHTING)
        return "EMT_LIGHTMAP_LIGHTING";
    if (s == irr::video::EMT_LIGHTMAP_LIGHTING_M2)
        return "EMT_LIGHTMAP_LIGHTING_M2";
    if (s == irr::video::EMT_LIGHTMAP_LIGHTING_M4)
        return "EMT_LIGHTMAP_LIGHTING_M4";
    if (s == irr::video::EMT_DETAIL_MAP)
        return "EMT_DETAIL_MAP";
    if (s == irr::video::EMT_SPHERE_MAP)
        return "EMT_SPHERE_MAP";
    if (s == irr::video::EMT_REFLECTION_2_LAYER)
        return "EMT_REFLECTION_2_LAYER";
    if (s == irr::video::EMT_TRANSPARENT_ADD_COLOR)
        return "EMT_TRANSPARENT_ADD_COLOR";
    if (s == irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL)
        return "EMT_TRANSPARENT_ALPHA_CHANNEL";
    if (s == irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL_REF)
        return "EMT_TRANSPARENT_ALPHA_CHANNEL_REF";
    if (s == irr::video::EMT_TRANSPARENT_VERTEX_ALPHA)
        return "EMT_TRANSPARENT_VERTEX_ALPHA";
    if (s == irr::video::EMT_TRANSPARENT_REFLECTION_2_LAYER)
        return "EMT_TRANSPARENT_REFLECTION_2_LAYER";
    if (s == irr::video::EMT_NORMAL_MAP_SOLID)
        return "EMT_NORMAL_MAP_SOLID";
    if (s == irr::video::EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR)
        return "EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR";
    if (s == irr::video::EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA)
        return "EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA";
    if (s == irr::video::EMT_PARALLAX_MAP_SOLID)
        return "EMT_PARALLAX_MAP_SOLID";
    if (s == irr::video::EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR)
        return "EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR";
    if (s == irr::video::EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA)
        return "EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA";
    if (s == irr::video::EMT_ONETEXTURE_BLEND)
        return "EMT_ONETEXTURE_BLEND";
    if (s == irr::video::EMT_FORCE_32BIT)
        return "EMT_FORCE_32BIT";

    return "EMT_SOLID2222";
}

irr::video::E_COMPARISON_FUNC Convert::stringZBuffer(QString s) {
    if (s == "ECFN_NEVER")
        return irr::video::ECFN_NEVER;
    if (s == "ECFN_LESSEQUAL")
        return irr::video::ECFN_LESSEQUAL;
    if (s == "ECFN_EQUAL")
        return irr::video::ECFN_EQUAL;
    if (s == "ECFN_LESS")
        return irr::video::ECFN_LESS;
    if (s == "ECFN_NOTEQUAL")
        return irr::video::ECFN_NOTEQUAL;
    if (s == "ECFN_GREATEREQUAL")
        return irr::video::ECFN_GREATEREQUAL;
    if (s == "ECFN_GREATER")
        return irr::video::ECFN_GREATER;
    if (s == "ECFN_ALWAYS")
        return irr::video::ECFN_ALWAYS;

    return irr::video::ECFN_NEVER;
}


QString Convert::zBufferString(irr::video::E_COMPARISON_FUNC s) {
    if (s == irr::video::ECFN_NEVER)
        return "ECFN_NEVER";
    if (s == irr::video::ECFN_LESSEQUAL)
        return "ECFN_LESSEQUAL";
    if (s == irr::video::ECFN_EQUAL)
        return "ECFN_EQUAL";
    if (s == irr::video::ECFN_LESS)
        return "ECFN_LESS";
    if (s == irr::video::ECFN_NOTEQUAL)
        return "ECFN_NOTEQUAL";
    if (s == irr::video::ECFN_GREATEREQUAL)
        return "ECFN_GREATEREQUAL";
    if (s == irr::video::ECFN_GREATER)
        return "ECFN_GREATER";
    if (s == irr::video::ECFN_ALWAYS)
        return "ECFN_ALWAYS";

    return "ECFN_NEVER";
}

irr::video::E_ANTI_ALIASING_MODE Convert::stringAntiAliasing(QString s) {
    if (s == "EAAM_OFF")
        return irr::video::EAAM_OFF;
    if (s == "EAAM_SIMPLE")
        return irr::video::EAAM_SIMPLE;
    if (s == "EAAM_QUALITY")
        return irr::video::EAAM_QUALITY;
    if (s == "EAAM_LINE_SMOOTH")
        return irr::video::EAAM_LINE_SMOOTH;
    if (s == "EAAM_POINT_SMOOTH")
        return irr::video::EAAM_POINT_SMOOTH;
    if (s == "EAAM_FULL_BASIC")
        return irr::video::EAAM_FULL_BASIC;
    if (s == "EAAM_ALPHA_TO_COVERAGE")
        return irr::video::EAAM_ALPHA_TO_COVERAGE;

    return irr::video::EAAM_OFF;
}


QString Convert::antiAliasingString(irr::video::E_ANTI_ALIASING_MODE s) {
    if (s == irr::video::EAAM_OFF)
        return "EAAM_OFF";
    if (s == irr::video::EAAM_SIMPLE)
        return "EAAM_SIMPLE";
    if (s == irr::video::EAAM_QUALITY)
        return "EAAM_QUALITY";
    if (s == irr::video::EAAM_LINE_SMOOTH)
        return "EAAM_LINE_SMOOTH";
    if (s == irr::video::EAAM_POINT_SMOOTH)
        return "EAAM_POINT_SMOOTH";
    if (s == irr::video::EAAM_FULL_BASIC)
        return "EAAM_FULL_BASIC";
    if (s == irr::video::EAAM_ALPHA_TO_COVERAGE)
        return "EAAM_ALPHA_TO_COVERAGE";

    return "EAAM_OFF";
}

irr::video::E_COLOR_PLANE Convert::stringColorMask(QString s) {
    if (s == "ECP_NONE")
        return irr::video::ECP_NONE;
    if (s == "ECP_ALPHA")
        return irr::video::ECP_ALPHA;
    if (s == "ECP_RED")
        return irr::video::ECP_RED;
    if (s == "ECP_GREEN")
        return irr::video::ECP_GREEN;
    if (s == "ECP_BLUE")
        return irr::video::ECP_BLUE;
    if (s == "ECP_RGB")
        return irr::video::ECP_RGB;
    if (s == "ECP_ALL")
        return irr::video::ECP_ALL;

    return irr::video::ECP_NONE;
}

QString Convert::colorMaskString(irr::video::E_COLOR_PLANE s) {
    if (s == irr::video::ECP_NONE)
        return "ECP_NONE";
    if (s == irr::video::ECP_ALPHA)
        return "ECP_ALPHA";
    if (s == irr::video::ECP_RED)
        return "ECP_RED";
    if (s == irr::video::ECP_GREEN)
        return "ECP_GREEN";
    if (s == irr::video::ECP_BLUE)
        return "ECP_BLUE";
    if (s == irr::video::ECP_RGB)
        return "ECP_RGB";
    if (s == irr::video::ECP_ALL)
        return "ECP_ALL";

    return "ECP_NONE";
}


irr::video::E_COLOR_MATERIAL Convert::stringColorMaterial(QString s) {
    if (s == "ECM_NONE")
        return irr::video::ECM_NONE;
    if (s == "ECM_DIFFUSE")
        return irr::video::ECM_DIFFUSE;
    if (s == "ECM_AMBIENT")
        return irr::video::ECM_AMBIENT;
    if (s == "ECM_EMISSIVE")
        return irr::video::ECM_EMISSIVE;
    if (s == "ECM_SPECULAR")
        return irr::video::ECM_SPECULAR;
    if (s == "ECM_DIFFUSE_AND_AMBIENT")
        return irr::video::ECM_DIFFUSE_AND_AMBIENT;

    return irr::video::ECM_NONE;
}


QString Convert::colorMaterialString(irr::video::E_COLOR_MATERIAL s) {
    if (s == irr::video::ECM_NONE)
        return "ECM_NONE";
    if (s == irr::video::ECM_DIFFUSE)
        return "ECM_DIFFUSE";
    if (s == irr::video::ECM_AMBIENT)
        return "ECM_AMBIENT";
    if (s == irr::video::ECM_EMISSIVE)
        return "ECM_EMISSIVE";
    if (s == irr::video::ECM_SPECULAR)
        return "ECM_SPECULAR";
    if (s == irr::video::ECM_DIFFUSE_AND_AMBIENT)
        return "ECM_DIFFUSE_AND_AMBIENT";

    return "ECM_NONE";
}

irr::video::E_LIGHT_TYPE Convert::stringLightType(QString s) {
    if (s == "ELT_POINT")
        return irr::video::ELT_POINT;
    if (s == "ELT_SPOT")
        return irr::video::ELT_SPOT;
    if (s == "ELT_DIRECTIONAL")
        return irr::video::ELT_DIRECTIONAL;

    return irr::video::ELT_POINT;
}

QString Convert::lightTypeString(irr::video::E_LIGHT_TYPE s) {
    if (s == irr::video::ELT_POINT)
        return "ELT_POINT";
    if (s == irr::video::ELT_SPOT)
        return "ELT_SPOT";
    if (s == irr::video::ELT_DIRECTIONAL)
        return "ELT_DIRECTIONAL";

    return "ELT_POINT";
}

irr::video::E_FOG_TYPE Convert::stringFogType(QString s) {
    if (s == "EFT_FOG_EXP")
        return irr::video::EFT_FOG_EXP;
    if (s == "EFT_FOG_LINEAR")
        return irr::video::EFT_FOG_LINEAR;
    if (s == "EFT_FOG_EXP2")
        return irr::video::EFT_FOG_EXP2;

    return irr::video::EFT_FOG_LINEAR;
}

irr::scene::E_TERRAIN_PATCH_SIZE Convert::stringTerrainPatch(QString s) {
    if (s == "ETPS_9")
        return irr::scene::ETPS_9;
    if (s == "ETPS_17")
        return irr::scene::ETPS_17;
    if (s == "ETPS_33")
        return irr::scene::ETPS_33;
    if (s == "ETPS_65")
        return irr::scene::ETPS_65;
    if (s == "ETPS_129")
        return irr::scene::ETPS_129;

    return irr::scene::ETPS_17;
}

QString Convert::terrainPatchString(irr::scene::E_TERRAIN_PATCH_SIZE s) {
    if (s == irr::scene::ETPS_9)
        return "ETPS_9";
    if (s == irr::scene::ETPS_17)
        return "ETPS_17";
    if (s == irr::scene::ETPS_33)
        return "ETPS_33";
    if (s == irr::scene::ETPS_65)
        return "ETPS_65";
    if (s == irr::scene::ETPS_129)
        return "ETPS_129";
    return "ETPS_17";
}



void Divers::copyFolder(const QString& sourceFolder,
                        const QString& destFolder) {
    QQueue< QPair< QString, QString > > queue;
    queue.enqueue(qMakePair(sourceFolder, destFolder));
    while (!queue.isEmpty()) {
        QPair<QString, QString> pair = queue.dequeue();
        QDir sourceDir(pair.first);
        QDir destDir(pair.second);

        if (!sourceDir.exists())
            continue;

        if (!destDir.exists())
            destDir.mkpath(pair.second);

        QStringList files = sourceDir.entryList(QDir::Files);
        int numFiles = files.count();
        for (int i = 0; i < numFiles; ++i) {
            QString srcName = pair.first + QDir::separator() + files.at(i);
            QString destName = pair.second + QDir::separator() + files.at(i);
            QFile::copy(srcName, destName);
        }

        QStringList dirs = sourceDir.entryList(QDir::AllDirs
                                               | QDir::NoDotAndDotDot);
        int numDirs = dirs.count();
        for (int i = 0; i < numDirs; ++i) {
            QString srcName = pair.first + QDir::separator() + dirs.at(i);
            QString destName = pair.second + QDir::separator() + dirs.at(i);
            queue.enqueue(qMakePair(srcName, destName));
        }
    }
}

void Convert::EulerToQuaternion(const btVector3 &euler, btQuaternion &quat) {
    btMatrix3x3 mat;
    mat.setIdentity();
    mat.setEulerZYX(euler.getX(), euler.getY(), euler.getZ());
    mat.getRotation(quat);
}

void Convert::QuaternionToEuler(const btQuaternion &TQuat, btVector3 &TEuler) {
    btScalar W = TQuat.getW();
    btScalar X = TQuat.getX();
    btScalar Y = TQuat.getY();
    btScalar Z = TQuat.getZ();
    float WSquared = W * W;
    float XSquared = X * X;
    float YSquared = Y * Y;
    float ZSquared = Z * Z;

    TEuler.setX(atan2f(2.0f * (Y * Z + X * W),
                       -XSquared - YSquared + ZSquared + WSquared));
    TEuler.setY(asinf(-2.0f * (X * Z - Y * W)));
    TEuler.setZ(atan2f(2.0f * (X * Y + Z * W),
                       XSquared - YSquared - ZSquared + WSquared));
    TEuler *= irr::core::RADTODEG;
}

Priority Convert::stringPriority(QString s) {
    if (s == "Error")
        return Error;
    if (s == "Warning")
        return Warning;
    if (s == "Debug")
        return Debug;
    if (s == "Info")
        return Info;
    if (s == "Config")
        return Config;

    return Debug;
}

QString Convert::PriorityString(Priority s) {
    if (s == Error)
        return "Error";
    if (s == Warning)
        return "Warning";
    if (s == Debug)
        return "Debug";
    if (s == Info)
        return "Info";
    if (s == Config)
        return "Config";
    return "Debug";
}

}  // namespace MarbleEditor
