/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


#include "MarbleEditor/Materials/Material3D.h"

#include <map>

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Engines/FileChooser.h"

namespace MarbleEditor {

Material3D::Material3D(Engine* engine, irr::video::SMaterial *s, int num,
                       QWidget* parent)
    : QWidget(parent) {
    setupUi(this);

    material = s;
    save = *s;
    this->id = num;
    this->engine = engine;

    int i = 0;
    while (material->getTexture(i)) {
        QDir dir(engine->getDirMedias());
        textures[i] = dir.relativeFilePath(
                    material->getTexture(i)->getName().getPath().c_str());
        ++i;
    }

    updateElementEditor();

    chooseTextureLevel(0);

    wireframe->setChecked(material->Wireframe);
    pointCloud->setChecked(material->PointCloud);
    gouraudShading->setChecked(material->GouraudShading);
    lighting->setChecked(material->Lighting);
    zwriteEnable->setChecked(material->ZWriteEnable);
    backfaceCulling->setChecked(material->BackfaceCulling);
    frontfaceCulling->setChecked(material->FrontfaceCulling);
    fogEnable->setChecked(material->FogEnable);
    normalizeNormals->setChecked(material->NormalizeNormals);

    QString buf = Convert::materialTypeString(material->MaterialType);
    materialType->setCurrentIndex(materialType->findText(buf));

    buf = Convert::colorMaskString(
                (irr::video::E_COLOR_PLANE)material->ColorMask);
    colorMask->setCurrentIndex(colorMask->findText(buf));

    buf = Convert::colorMaterialString(
                (irr::video::E_COLOR_MATERIAL)material->ColorMaterial);
    colorMaterial->setCurrentIndex(colorMaterial->findText(buf));

    buf = Convert::zBufferString(
                (irr::video::E_COMPARISON_FUNC)material->ZBuffer);
    zBuffer->setCurrentIndex(zBuffer->findText(buf));

    buf = Convert::antiAliasingString(
                (irr::video::E_ANTI_ALIASING_MODE)material->AntiAliasing);
    antiAliasing->setCurrentIndex(antiAliasing->findText(buf));

    shininess->setValue(material->Shininess);

    QColor couleur;
    couleur.setAlpha(material->AmbientColor.getAlpha());
    couleur.setRed(material->AmbientColor.getRed());
    couleur.setGreen(material->AmbientColor.getGreen());
    couleur.setBlue(material->AmbientColor.getBlue());
    QPalette palette = ambiantColor->palette();
    palette.setColor(QPalette::Window, couleur);
    ambiantColor->setPalette(palette);

    couleur.setAlpha(material->SpecularColor.getAlpha());
    couleur.setRed(material->SpecularColor.getRed());
    couleur.setGreen(material->SpecularColor.getGreen());
    couleur.setBlue(material->SpecularColor.getBlue());
    palette = specularColor->palette();
    palette.setColor(QPalette::Window, couleur);
    specularColor->setPalette(palette);

    couleur.setAlpha(material->DiffuseColor.getAlpha());
    couleur.setRed(material->DiffuseColor.getRed());
    couleur.setGreen(material->DiffuseColor.getGreen());
    couleur.setBlue(material->DiffuseColor.getBlue());
    palette = diffuseColor->palette();
    palette.setColor(QPalette::Window, couleur);
    diffuseColor->setPalette(palette);

    couleur.setAlpha(material->EmissiveColor.getAlpha());
    couleur.setRed(material->EmissiveColor.getRed());
    couleur.setGreen(material->EmissiveColor.getGreen());
    couleur.setBlue(material->EmissiveColor.getBlue());
    palette = emissiveColor->palette();
    palette.setColor(QPalette::Window, couleur);
    emissiveColor->setPalette(palette);

    connect(textureLevel, static_cast<void (QSpinBox::*)(int)>
                                      (&QSpinBox::valueChanged),
            this, &Material3D::chooseTextureLevel);

    connect(selectTexture, &QPushButton::clicked,
            this, &Material3D::chooseNewTexture);

    connect(wireframe, &QCheckBox::toggled,
            this, &Material3D::setWireframe);

    connect(pointCloud, &QCheckBox::toggled,
            this, &Material3D::setPointCloud);

    connect(gouraudShading, &QCheckBox::toggled,
            this, &Material3D::setGouraudShading);

    connect(lighting, &QCheckBox::toggled,
            this, &Material3D::setLighting);

    connect(zwriteEnable, &QCheckBox::toggled,
            this, &Material3D::setZWriteEnable);

    connect(backfaceCulling, &QCheckBox::toggled,
            this, &Material3D::setBackfaceCulling);

    connect(frontfaceCulling, &QCheckBox::toggled,
            this, &Material3D::setFrontfaceCulling);

    connect(fogEnable, &QCheckBox::toggled,
            this, &Material3D::setFogEnable);

    connect(normalizeNormals, &QCheckBox::toggled,
            this, &Material3D::setNormalizeNormals);

    connect(shininess, static_cast<void (QDoubleSpinBox::*)(double)>
                                   (&QDoubleSpinBox::valueChanged),
            this, &Material3D::setShininess);

    connect(colorMask, static_cast<void (QComboBox::*)(const QString &)>
            (&QComboBox::activated),
            this, &Material3D::setColorMask);

    connect(colorMaterial, static_cast<void (QComboBox::*)(const QString &)>
            (&QComboBox::activated),
            this, &Material3D::setColorMaterial);

    connect(zBuffer, static_cast<void (QComboBox::*)(const QString &)>
            (&QComboBox::activated),
            this, &Material3D::setZBuffer);

    connect(antiAliasing, static_cast<void (QComboBox::*)(const QString &)>
            (&QComboBox::activated),
            this, &Material3D::setAntiAliasing);

    connect(ambiantColorButton, &QPushButton::clicked,
            this, &Material3D::chooseAmbiantColor);

    connect(specularColorButton, &QPushButton::clicked,
            this, &Material3D::chooseSpecularColor);

    connect(diffuseColorButton, &QPushButton::clicked,
            this, &Material3D::chooseDiffuseColor);

    connect(emissiveColorButton, &QPushButton::clicked,
            this, &Material3D::chooseEmissiveColor);

    connect(materialType, static_cast<void (QComboBox::*)(int)>
                                      (&QComboBox::currentIndexChanged),
            this, &Material3D::setCurrentMaterialType);

    connect(shaderBox, &QGroupBox::toggled,
            this, &Material3D::switchShader);
}

irr::video::SMaterial* Material3D::getMaterial() const {
    return material;
}

void Material3D::setMaterial(irr::video::SMaterial* mat) {
    material = mat;
}

irr::video::SMaterial Material3D::getSaveMaterial() const {
    return save;
}

void Material3D::updateElementEditor() {
    QString shad = shaderComboBox->currentText();
    loadAvailableShaders();
    shaderComboBox->setCurrentIndex(shaderComboBox->findText(shad));
}

void Material3D::loadAvailableShaders() {
    QDir d(engine->getDirMedias() + "Shaders");
    shaderComboBox->clear();
    QStringList l = d.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    for (int i = 0; i < l.size(); ++i) {
        shaderComboBox->addItem("Shaders/" + l.at(i));
    }
}

void Material3D::switchShader(bool value) {
    shaderBox->setChecked(value);
    loadAvailableShaders();
}

bool Material3D::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("wireframe"))
        setWireframe(elem->BoolAttribute("wireframe"));

    if (elem->Attribute("pointCloud"))
        setPointCloud(elem->BoolAttribute("pointCloud"));

    if (elem->Attribute("gouraudShading"))
        setGouraudShading(elem->BoolAttribute("gouraudShading"));

    if (elem->Attribute("lighting"))
        setLighting(elem->BoolAttribute("lighting"));

    if (elem->Attribute("zwriteEnable"))
        setZWriteEnable(elem->BoolAttribute("zwriteEnable"));

    if (elem->Attribute("backfaceCulling"))
        setBackfaceCulling(elem->BoolAttribute("backfaceCulling"));

    if (elem->Attribute("frontfaceCulling"))
        setFrontfaceCulling(elem->BoolAttribute("frontfaceCulling"));

    if (elem->Attribute("fogEnable"))
        setFogEnable(elem->BoolAttribute("fogEnable"));

    if (elem->Attribute("normalizeNormals"))
        setNormalizeNormals(elem->BoolAttribute("normalizeNormals"));

    if (elem->Attribute("shininess"))
        setShininess(elem->FloatAttribute("shininess"));

    if (elem->Attribute("materialType")) {
        setMaterialType(elem->Attribute("materialType"));

        if (elem->Attribute("shaderMaterialType")) {
            shaderBox->setChecked(true);
            loadAvailableShaders();
            setShader(elem->Attribute("shaderMaterialType"));
        }
    }

    if (elem->Attribute("zBuffer"))
        setZBuffer(elem->Attribute("zBuffer"));

    if (elem->Attribute("antiAliasing"))
        setAntiAliasing(elem->Attribute("antiAliasing"));

    if (elem->Attribute("colorMask"))
        setColorMask(elem->Attribute("colorMask"));

    if (elem->Attribute("colorMaterial"))
        setColorMaterial(elem->Attribute("colorMaterial"));

    elem = elem->FirstChildElement();

    while (elem) {
        if (QString(elem->Value()) == "texture") {
            textureLevel->setValue(elem->IntAttribute("level"));
            chooseTexture(elem->Attribute("file"));
        } else if (QString(elem->Value()) == "ambiantColor") {
            if (elem->Attribute("alpha") && elem->Attribute("red")
                    && elem->Attribute("green") && elem->Attribute("blue")) {
                irr::video::SColor color(0, 0, 0, 0);
                color.setRed(elem->IntAttribute("red"));
                color.setGreen(elem->IntAttribute("green"));
                color.setBlue(elem->IntAttribute("blue"));
                color.setAlpha(elem->IntAttribute("alpha"));
                setAmbientColor(color);
            }
        } else if (QString(elem->Value()) == "diffuseColor") {
            if (elem->Attribute("alpha") && elem->Attribute("red")
                    && elem->Attribute("green") && elem->Attribute("blue")) {
                irr::video::SColor color(0, 0, 0, 0);
                color.setRed(elem->IntAttribute("red"));
                color.setGreen(elem->IntAttribute("green"));
                color.setBlue(elem->IntAttribute("blue"));
                color.setAlpha(elem->IntAttribute("alpha"));
                setDiffuseColor(color);
            }
        } else if (QString(elem->Value()) == "emissiveColor") {
            if (elem->Attribute("alpha") && elem->Attribute("red")
                    && elem->Attribute("green") && elem->Attribute("blue")) {
                irr::video::SColor color(0, 0, 0, 0);
                color.setRed(elem->IntAttribute("red"));
                color.setGreen(elem->IntAttribute("green"));
                color.setBlue(elem->IntAttribute("blue"));
                color.setAlpha(elem->IntAttribute("alpha"));
                setEmissiveColor(color);
            }
        } else if (QString(elem->Value()) == "specularColor") {
            if (elem->Attribute("alpha") && elem->Attribute("red")
                    && elem->Attribute("green") && elem->Attribute("blue")) {
                irr::video::SColor color(0, 0, 0, 0);
                color.setRed(elem->IntAttribute("red"));
                color.setGreen(elem->IntAttribute("green"));
                color.setBlue(elem->IntAttribute("blue"));
                color.setAlpha(elem->IntAttribute("alpha"));
                setSpecularColor(color);
            }
        }

        elem = elem->NextSiblingElement();
    }

    return true;
}

bool Material3D::writeXML(tinyxml2::XMLDocument* xmlDoc,
                          tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Material3D");
    element->SetAttribute("num", id);

    element->SetAttribute("wireframe", wireframe->isChecked());
    element->SetAttribute("pointCloud", pointCloud->isChecked());
    element->SetAttribute("gouraudShading", gouraudShading->isChecked());
    element->SetAttribute("lighting", lighting->isChecked());
    element->SetAttribute("zwriteEnable", zwriteEnable->isChecked());
    element->SetAttribute("backfaceCulling", backfaceCulling->isChecked());
    element->SetAttribute("frontfaceCulling", frontfaceCulling->isChecked());
    element->SetAttribute("fogEnable", fogEnable->isChecked());
    element->SetAttribute("normalizeNormals", normalizeNormals->isChecked());
    element->SetAttribute("shininess", shininess->value());
    element->SetAttribute("materialType",
                          materialType->currentText().toUtf8().data());
    if (shaderBox->isChecked()) {
        element->SetAttribute("shaderMaterialType",
                              shaderComboBox->currentText().toUtf8().data());
    }
    element->SetAttribute("zBuffer",
                          zBuffer->currentText().toUtf8().data());
    element->SetAttribute("antiAliasing",
                          antiAliasing->currentText().toUtf8().data());
    element->SetAttribute("colorMask",
                          colorMask->currentText().toUtf8().data());
    element->SetAttribute("colorMaterial",
                          colorMaterial->currentText().toUtf8().data());

    std::map<int, QString>::const_iterator it, end = textures.end();
    for (it = textures.begin(); it != end; ++it) {
        QString name = it->second;
        if (name != "") {
            tinyxml2::XMLElement* text = xmlDoc->NewElement("texture");
            text->SetAttribute("level", it->first);
            text->SetAttribute("file", name.toUtf8().data());
            element->LinkEndChild(text);
        }
    }

    tinyxml2::XMLElement* ambiantColor = xmlDoc->NewElement("ambiantColor");
    ambiantColor->SetAttribute("alpha", material->AmbientColor.getAlpha());
    ambiantColor->SetAttribute("red", material->AmbientColor.getRed());
    ambiantColor->SetAttribute("green", material->AmbientColor.getGreen());
    ambiantColor->SetAttribute("blue", material->AmbientColor.getBlue());
    element->LinkEndChild(ambiantColor);

    tinyxml2::XMLElement* diffuseColor = xmlDoc->NewElement("diffuseColor");
    diffuseColor->SetAttribute("alpha", material->DiffuseColor.getAlpha());
    diffuseColor->SetAttribute("red", material->DiffuseColor.getRed());
    diffuseColor->SetAttribute("green", material->DiffuseColor.getGreen());
    diffuseColor->SetAttribute("blue", material->DiffuseColor.getBlue());
    element->LinkEndChild(diffuseColor);

    tinyxml2::XMLElement* specularColor = xmlDoc->NewElement("specularColor");
    specularColor->SetAttribute("alpha", material->SpecularColor.getAlpha());
    specularColor->SetAttribute("red", material->SpecularColor.getRed());
    specularColor->SetAttribute("green", material->SpecularColor.getGreen());
    specularColor->SetAttribute("blue", material->SpecularColor.getBlue());
    element->LinkEndChild(specularColor);

    tinyxml2::XMLElement* emissiveColor = xmlDoc->NewElement("emissiveColor");
    emissiveColor->SetAttribute("alpha", material->EmissiveColor.getAlpha());
    emissiveColor->SetAttribute("red", material->EmissiveColor.getRed());
    emissiveColor->SetAttribute("green", material->EmissiveColor.getGreen());
    emissiveColor->SetAttribute("blue", material->EmissiveColor.getBlue());
    element->LinkEndChild(emissiveColor);

    elem->LinkEndChild(element);
    return true;
}

void Material3D::setShader(QString shader) {
    shaderComboBox->setCurrentIndex(shaderComboBox->findText(shader));
}

void Material3D::chooseTextureLevel(int level) {
    if (textures[level] != "") {
        chooseTexture(textures[level]);
    } else {
        labelTexture->setPixmap(QPixmap());
        selectTexture->setText(tr("Selection texture"));
        nameTexture->setText("");
    }
}

void Material3D::chooseNewTexture() {
    chooseTexture();
}

void Material3D::chooseTexture(QString text) {
    if (text == "") {
        FileChooser* dialog = new FileChooser(
                    engine->getDirMedias() + "Textures/", true, this);
        dialog->exec();
        if (dialog->result() == QDialog::Accepted) {
            text = "Textures/" + dialog->getSelectedRelativeFilePath();
        }
    }

    if (text != "") {
        QString relText = text;
        QString absText = engine->getDirMedias() + relText;

        textures[textureLevel->value()] = relText;

        QImage image(absText);
        image.scaled(100, 100);
        labelTexture->setPixmap(QPixmap::fromImage(image));
        selectTexture->setText("texture :");
        nameTexture->setText("Name : " + relText);

        setTexture(engine->getGraphicsEngine()->getDriver()->getTexture(
                       absText.toUtf8().data()));

        emit modify(id);
    }
}

void Material3D::chooseAmbiantColor() {
    irr::video::SColor col = material->AmbientColor;
    QColor couleur(col.getRed(), col.getGreen(),
                   col.getBlue(), col.getAlpha());
    couleur = QColorDialog::getColor(couleur, this, tr("Couleur ambiante"),
                                     QColorDialog::ShowAlphaChannel);
    if (couleur.isValid()) {
        setAmbientColor(irr::video::SColor(couleur.alpha(), couleur.red(),
                                           couleur.green(), couleur.blue()));
    }
}

void Material3D::chooseSpecularColor() {
    irr::video::SColor col = material->SpecularColor;
    QColor couleur(col.getRed(), col.getGreen(), col.getBlue(), col.getAlpha());
    couleur = QColorDialog::getColor(couleur, this, tr("Couleur speculaire"),
                                     QColorDialog::ShowAlphaChannel);
    if (couleur.isValid()) {
        setSpecularColor(irr::video::SColor(couleur.alpha(), couleur.red(),
                                            couleur.green(), couleur.blue()));
    }
}

void Material3D::chooseDiffuseColor() {
    irr::video::SColor col = material->DiffuseColor;
    QColor couleur(col.getRed(), col.getGreen(), col.getBlue(), col.getAlpha());
    couleur = QColorDialog::getColor(couleur, this, tr("Couleur diffuse"),
                                     QColorDialog::ShowAlphaChannel);
    if (couleur.isValid()) {
        setDiffuseColor(irr::video::SColor(couleur.alpha(), couleur.red(),
                                           couleur.green(), couleur.blue()));
    }
}

void Material3D::chooseEmissiveColor() {
    irr::video::SColor col = material->EmissiveColor;
    QColor couleur(col.getRed(), col.getGreen(), col.getBlue(), col.getAlpha());
    couleur = QColorDialog::getColor(couleur, this, tr("Couleur emissive"),
                                     QColorDialog::ShowAlphaChannel);
    if (couleur.isValid()) {
        setEmissiveColor(irr::video::SColor(couleur.alpha(), couleur.red(),
                                            couleur.green(), couleur.blue()));
    }
}

void Material3D::setWireframe(bool wireframe) {
    material->Wireframe = wireframe;

    if (wireframe) {
        material->PointCloud = false;
        pointCloud->setChecked(false);
    }

    this->wireframe->setChecked(wireframe);

    emit modify(id);
}

void Material3D::setPointCloud(bool pointCloud) {
    material->PointCloud = pointCloud;

    if (pointCloud) {
        material->Wireframe = false;
        wireframe->setChecked(false);
    }

    this->pointCloud->setChecked(pointCloud);

    emit modify(id);
}

void Material3D::setGouraudShading(bool gouraudShading) {
    material->GouraudShading = gouraudShading;
    this->gouraudShading->setChecked(gouraudShading);
    emit modify(id);
}

void Material3D::setLighting(bool lighting) {
    material->Lighting = lighting;
    this->lighting->setChecked(lighting);
    emit modify(id);
}

void Material3D::setZWriteEnable(bool zwriteEnable) {
    material->ZWriteEnable = zwriteEnable;
    this->zwriteEnable->setChecked(zwriteEnable);
    emit modify(id);
}
void Material3D::setBackfaceCulling(bool backfaceCulling) {
    material->BackfaceCulling = backfaceCulling;
    this->backfaceCulling->setChecked(backfaceCulling);
    emit modify(id);
}

void Material3D::setFrontfaceCulling(bool frontfaceCulling) {
    material->FrontfaceCulling = frontfaceCulling;
    this->frontfaceCulling->setChecked(frontfaceCulling);
    emit modify(id);
}

void Material3D::setFogEnable(bool fogEnable) {
    material->FogEnable = fogEnable;
    this->fogEnable->setChecked(fogEnable);
    emit modify(id);
}

void Material3D::setNormalizeNormals(bool normalizeNormals) {
    material->NormalizeNormals = normalizeNormals;
    this->normalizeNormals->setChecked(normalizeNormals);
    emit modify(id);
}

void Material3D::setTexture(irr::video::ITexture* texture) {
    material->setTexture(textureLevel->value(), texture);
    emit modify(id);
}

void Material3D::setCurrentMaterialType() {
    material->MaterialType = Convert::stringMaterialType(
                materialType->currentText().toUtf8().data());
    emit modify(id);
}

void Material3D::setMaterialType(QString materialType) {
    material->MaterialType = Convert::stringMaterialType(materialType);
    this->materialType->setCurrentIndex(
                this->materialType->findText(materialType));
    emit modify(id);
}

void Material3D::setAmbientColor(irr::video::SColor ambiant) {
    material->AmbientColor = ambiant;

    QColor couleur(ambiant.getRed(), ambiant.getGreen(),
                   ambiant.getBlue(), ambiant.getAlpha());
    QPalette palette = ambiantColor->palette();
    palette.setColor(QPalette::Window, couleur);
    ambiantColor->setPalette(palette);

    emit modify(id);
}

void Material3D::setDiffuseColor(irr::video::SColor diffuse) {
    material->DiffuseColor = diffuse;

    QColor couleur(diffuse.getRed(), diffuse.getGreen(), diffuse.getBlue(),
                   diffuse.getAlpha());
    QPalette palette = diffuseColor->palette();
    palette.setColor(QPalette::Window, couleur);
    diffuseColor->setPalette(palette);

    emit modify(id);
}

void Material3D::setEmissiveColor(irr::video::SColor emissive) {
    material->EmissiveColor = emissive;

    QColor couleur(emissive.getRed(), emissive.getGreen(), emissive.getBlue(),
                   emissive.getAlpha());
    QPalette palette = emissiveColor->palette();
    palette.setColor(QPalette::Window, couleur);
    emissiveColor->setPalette(palette);

    emit modify(id);
}

void Material3D::setSpecularColor(irr::video::SColor specular) {
    material->SpecularColor = specular;

    QColor couleur(specular.getRed(), specular.getGreen(), specular.getBlue(),
                   specular.getAlpha());
    QPalette palette = specularColor->palette();
    palette.setColor(QPalette::Window, couleur);
    specularColor->setPalette(palette);

    emit modify(id);
}

void Material3D::setShininess(double shininess) {
    material->Shininess = shininess;
    this->shininess->setValue(shininess);
    emit modify(id);
}

void Material3D::setZBuffer(QString zbuffer) {
    material->ZBuffer = Convert::stringZBuffer(zbuffer);
    this->zBuffer->setCurrentIndex(this->zBuffer->findText(zbuffer));
    emit modify(id);
}

void Material3D::setAntiAliasing(QString antiAliasing) {
    material->AntiAliasing = Convert::stringAntiAliasing(antiAliasing);
    this->antiAliasing->setCurrentIndex(
                this->antiAliasing->findText(antiAliasing));
    emit modify(id);
}

void Material3D::setColorMask(QString colorMask) {
    material->ColorMask = Convert::stringColorMask(colorMask);
    this->colorMask->setCurrentIndex(this->colorMask->findText(colorMask));

    emit modify(id);
}

void Material3D::setColorMaterial(QString colorMaterial) {
    material->ColorMaterial = Convert::stringColorMaterial(colorMaterial);

    this->colorMaterial->setCurrentIndex(
                this->colorMaterial->findText(colorMaterial));

    emit modify(id);
}

}  // namespace MarbleEditor
