/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Material3D.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_MATERIALS_MATERIAL3D_H_
#define MARBLEEDITOR_MATERIALS_MATERIAL3D_H_

#include <ui_Material3D.h>

#include <map>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class Material3D
 * \brief
 *
 *
 */
class Material3D : public QWidget, private Ui::Material3D {
    Q_OBJECT

    public:
        explicit Material3D(Engine* engine, irr::video::SMaterial* s,
                            int num, QWidget* parent = 0);
        virtual ~Material3D() {}

        irr::video::SMaterial* getMaterial() const;
        void setMaterial(irr::video::SMaterial* mat);
        irr::video::SMaterial getSaveMaterial() const;

        bool readXML(tinyxml2::XMLElement* elem);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

        void updateElementEditor();

    private slots:
        void chooseNewTexture();
        void chooseTexture(QString text = "");
        void chooseTextureLevel(int level);
        void chooseAmbiantColor();
        void chooseSpecularColor();
        void chooseDiffuseColor();
        void chooseEmissiveColor();
        void setWireframe(bool wireframe = false);
        void setPointCloud(bool pointCloud = false);
        void setGouraudShading(bool gouraudShading = true);
        void setLighting(bool lighting = true);
        void setZWriteEnable(bool zwriteEnable = true);
        void setBackfaceCulling(bool backfaceCulling = true);
        void setFrontfaceCulling(bool frontfaceCulling = false);
        void setFogEnable(bool fogEnable = false);
        void setNormalizeNormals(bool normalizeNormals = false);
        void setTexture(irr::video::ITexture* texture = NULL);
        void setMaterialType(QString materialType = "EMT_SOLID");
        void setAmbientColor(irr::video::SColor ambiant =
                irr::video::SColor(255, 255, 255, 255));
        void setDiffuseColor(irr::video::SColor diffuse =
                irr::video::SColor(255, 255, 255, 255));
        void setEmissiveColor(irr::video::SColor emissive =
                irr::video::SColor(0, 0, 0, 0));
        void setSpecularColor(irr::video::SColor specular =
                irr::video::SColor(255, 255, 255, 255));
        void setShininess(double shininess = 0);
        void setZBuffer(QString zbuffer = "ECFN_LESSEQUAL");
        void setAntiAliasing(QString antiAliasing = "EAAM_SIMPLE");
        void setColorMask(QString colorMask = "ECP_ALL");
        void setColorMaterial(QString colorMaterial = "ECM_DIFFUSE");
        void loadAvailableShaders();
        void setCurrentMaterialType();
        void switchShader(bool value);
        void setShader(QString shader);

    signals:
        void modify(int num);

    private:
        Engine* engine;
        irr::video::SMaterial* material;
        irr::video::SMaterial save;
        int id;
        std::map<int, QString> textures;

    private:
        DISALLOW_COPY_AND_ASSIGN(Material3D);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_MATERIALS_MATERIAL3D_H_
