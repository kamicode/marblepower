/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_MaterialPhys.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_MATERIALS_MATERIALPHYS_H_
#define MARBLEEDITOR_MATERIALS_MATERIALPHYS_H_

#include <ui_MaterialPhys.h>

#include <set>
#include <vector>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

struct Mat {
    QString mat1, mat2, sound;
};

/*! \class MaterialPhys
 * \brief
 *
 *
 */
class MaterialPhys : public QDialog, private Ui::MaterialPhys {
    Q_OBJECT

    public:
        explicit MaterialPhys(Engine* engine, QWidget* parent = 0);
        virtual ~MaterialPhys();

        Mat* getMat(QString mat1, QString mat2);
        std::set<QString> getNameMats() { return nameMats; }

        bool readXML(tinyxml2::XMLElement* elem);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

        void unLoadMap();

    private slots:
        void addNewMat();
        void addMat(QString name = "");
        void majGUI();
        void chooseSoundGUI();

    private:
        Engine* engine;
        std::vector<Mat*> mats;
        std::set<QString> nameMats;

    private:
        DISALLOW_COPY_AND_ASSIGN(MaterialPhys);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_MATERIALS_MATERIALPHYS_H_
