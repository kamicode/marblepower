/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Materials/MaterialPhys.h"

#include <set>
#include <vector>

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Engines/FileChooser.h"

namespace MarbleEditor {

MaterialPhys::MaterialPhys(Engine* engine, QWidget* parent)
    : QDialog(parent) {
    setupUi(this);

    this->engine = engine;
    majGUI();

    connect(addMaterial, &QPushButton::clicked,
            this, &MaterialPhys::addNewMat);

    connect(selectMaterial1, static_cast<void (QComboBox::*)(int)>
                                        (&QComboBox::currentIndexChanged),
            this, &MaterialPhys::majGUI);

    connect(selectMaterial2, static_cast<void (QComboBox::*)(int)>
                                        (&QComboBox::currentIndexChanged),
            this, &MaterialPhys::majGUI);

    connect(chooseSound, &QPushButton::clicked,
            this, &MaterialPhys::chooseSoundGUI);
}

MaterialPhys::~MaterialPhys() {
    unLoadMap();
}

void MaterialPhys::unLoadMap() {
    std::vector<Mat*>::const_iterator it, end = mats.end();
    for (it = mats.begin(); it != end; ++it) {
        delete (*it);
    }
    mats.clear();
    nameMats.clear();

    while (materialConf->rowCount() > 0) {
        materialConf->removeRow(0);
    }

    selectMaterial1->clear();
    selectMaterial2->clear();
    sound->clear();
    newMatName->clear();
}

Mat* MaterialPhys::getMat(QString mat1, QString mat2) {
    for (size_t i = 0; i < mats.size(); ++i) {
        if ((mats.at(i)->mat1 == mat1 && mats.at(i)->mat2 == mat2) ||
                (mats.at(i)->mat2 == mat1 && mats.at(i)->mat1 == mat2)) {
            return mats.at(i);
        }
    }
    return NULL;
}

void MaterialPhys::addNewMat() {
    addMat();
}

void MaterialPhys::addMat(QString name) {
    if (name == "") {
        name = newMatName->text();
        newMatName->setText("");
    }

    if (name != "") {
        if (nameMats.find(name) != nameMats.end())
            return;

        nameMats.insert(name);
        selectMaterial1->addItem(name);
        selectMaterial2->addItem(name);
        materialConf->insertRow(materialConf->rowCount());

        QTableWidgetItem* newItem = new QTableWidgetItem();
        materialConf->setItem(materialConf->rowCount() - 1, 0, newItem);
        newItem->setText(name);
        newItem->setFlags(newItem->flags() & ~Qt::ItemIsEditable);

        for (int i = 1; i < materialConf->columnCount(); ++i) {
            QDoubleSpinBox *dSpinBoxDistance = new QDoubleSpinBox;
            dSpinBoxDistance->setMinimum(0);
            dSpinBoxDistance->setMaximum(9999);
            dSpinBoxDistance->setAccelerated(true);
            materialConf->setCellWidget(
                        materialConf->rowCount() - 1, i, dSpinBoxDistance);
        }

        std::set<QString>::const_iterator it, end = nameMats.end();
        for (it = nameMats.begin(); it != end; ++it) {
            Mat* mat = new Mat();
            mat->mat1 = name;
            mat->mat2 = *it;
            mat->sound = "";
            mats.push_back(mat);
        }
    }

    engine->setSelectedElement(engine->getSelectedElement());
}

void MaterialPhys::majGUI() {
    Mat* m = getMat(selectMaterial1->currentText(),
                    selectMaterial2->currentText());
    if (m != NULL) {
        sound->setText(m->sound);
    }
}

void MaterialPhys::chooseSoundGUI() {
    FileChooser* dialog = new FileChooser(
                engine->getDirMedias() + "Musics/", false, this);
    dialog->exec();
    if (dialog->result() == QDialog::Accepted) {
        QString text = "Musics/" + dialog->getSelectedRelativeFilePath();
        sound->setText(text);
        Mat* m = getMat(selectMaterial1->currentText(),
                        selectMaterial2->currentText());
        m->sound = text;
    }
}

bool MaterialPhys::readXML(tinyxml2::XMLElement* elem) {
    if (QString(elem->Value()) == "Material") {
        addMat(elem->Attribute("name"));

        QDoubleSpinBox* restitution = static_cast<QDoubleSpinBox*>
                (materialConf->cellWidget(materialConf->rowCount() - 1, 1));
        restitution->setValue(elem->FloatAttribute("restitution"));

        QDoubleSpinBox* friction = static_cast<QDoubleSpinBox*>
                (materialConf->cellWidget(materialConf->rowCount() - 1, 2));
        friction->setValue(elem->FloatAttribute("friction"));

        QDoubleSpinBox* lin_damping = static_cast<QDoubleSpinBox*>
                (materialConf->cellWidget(materialConf->rowCount() - 1, 3));
        lin_damping->setValue(elem->FloatAttribute("lin_damping"));

        QDoubleSpinBox* ang_damping = static_cast<QDoubleSpinBox*>
                (materialConf->cellWidget(materialConf->rowCount() - 1, 4));
        ang_damping->setValue(elem->FloatAttribute("ang_damping"));
    } else {
        if (elem->Attribute("mat1") && elem->Attribute("mat2")) {
            Mat* m = getMat(elem->Attribute("mat1"), elem->Attribute("mat2"));

            if (elem->Attribute("sound")) {
                m->sound = elem->Attribute("sound");
            }
        }
    }
    majGUI();
    return true;
}

bool MaterialPhys::writeXML(tinyxml2::XMLDocument* xmlDoc,
                            tinyxml2::XMLElement* elem) {
    if (engine->getPhysicsEngine()->isPhysicEnabled()) {
        tinyxml2::XMLElement* element;

        for (int i = 0; i < materialConf->rowCount(); ++i) {
            element = xmlDoc->NewElement("Material");
            element->SetAttribute(
                        "name",
                        materialConf->item(i, 0)->text().toUtf8().data());

            QDoubleSpinBox* restitution = static_cast<QDoubleSpinBox*>
                    (materialConf->cellWidget(i, 1));
            element->SetAttribute("restitution", restitution->value());

            QDoubleSpinBox* friction = static_cast<QDoubleSpinBox*>
                    (materialConf->cellWidget(i, 2));
            element->SetAttribute("friction", friction->value());

            QDoubleSpinBox* lin_damping = static_cast<QDoubleSpinBox*>
                    (materialConf->cellWidget(i, 3));
            element->SetAttribute("lin_damping", lin_damping->value());

            QDoubleSpinBox* ang_damping = static_cast<QDoubleSpinBox*>
                    (materialConf->cellWidget(i, 4));
            element->SetAttribute("ang_damping", ang_damping->value());
            elem->LinkEndChild(element);
        }

        for (unsigned int i = 0; i < mats.size(); ++i) {
            element = xmlDoc->NewElement("ConfigMaterial");
            element->SetAttribute("mat1",
                                  ((mats.at(i))->mat1).toUtf8().data());
            element->SetAttribute("mat2",
                                  ((mats.at(i))->mat2).toUtf8().data());
            element->SetAttribute("sound",
                                  ((mats.at(i))->sound).toUtf8().data());
            elem->LinkEndChild(element);
        }
    }

    return true;
}

}  // namespace MarbleEditor
