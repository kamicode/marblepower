<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Editor</name>
    <message>
        <location filename="../../Engines/Editor.ui" line="14"/>
        <source>Script Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="34"/>
        <source>Fichiers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="42"/>
        <source>Edition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="56"/>
        <location filename="../../Engines/Editor.ui" line="164"/>
        <location filename="../../Engines/Editor.ui" line="167"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="66"/>
        <source>Quitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="69"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="77"/>
        <source>Couper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="80"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="88"/>
        <source>Copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="91"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="96"/>
        <source>Coller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="99"/>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="104"/>
        <source>Supprimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="107"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="112"/>
        <source>Sélectionner tout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="115"/>
        <source>Ctrl+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="123"/>
        <source>Annuler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="126"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="134"/>
        <source>Retablir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="137"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="142"/>
        <source>Autocompletion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="145"/>
        <source>Ctrl+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="150"/>
        <location filename="../../Engines/Editor.ui" line="153"/>
        <source>Sauver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="156"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Editor.ui" line="170"/>
        <source>Ctrl+H</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ElementEditor</name>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="27"/>
        <source>ElementEditor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="53"/>
        <source>Element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="59"/>
        <location filename="../../Engines/ElementEditor.ui" line="62"/>
        <location filename="../../Engines/ElementEditor.ui" line="65"/>
        <location filename="../../Engines/ElementEditor.ui" line="1699"/>
        <location filename="../../Engines/ElementEditor.ui" line="1702"/>
        <location filename="../../Engines/ElementEditor.ui" line="1705"/>
        <source>Position de l&apos;Element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="68"/>
        <location filename="../../Engines/ElementEditor.ui" line="1420"/>
        <location filename="../../Engines/ElementEditor.ui" line="1708"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="77"/>
        <location filename="../../Engines/ElementEditor.ui" line="636"/>
        <location filename="../../Engines/ElementEditor.ui" line="1429"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; background-color:#ff0000;&quot;&gt;       &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Pos X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="113"/>
        <location filename="../../Engines/ElementEditor.ui" line="672"/>
        <location filename="../../Engines/ElementEditor.ui" line="1465"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; background-color:#00ff00;&quot;&gt;       &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Pos Y&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="149"/>
        <location filename="../../Engines/ElementEditor.ui" line="708"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; background-color:#0000ff;&quot;&gt;       &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Pos Z&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="188"/>
        <location filename="../../Engines/ElementEditor.ui" line="191"/>
        <location filename="../../Engines/ElementEditor.ui" line="194"/>
        <source>Rotation de l&apos;Element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="197"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="203"/>
        <location filename="../../Engines/ElementEditor.ui" line="765"/>
        <source>Rot Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="235"/>
        <location filename="../../Engines/ElementEditor.ui" line="797"/>
        <source>Rot Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="289"/>
        <location filename="../../Engines/ElementEditor.ui" line="851"/>
        <source>Rot X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="302"/>
        <location filename="../../Engines/ElementEditor.ui" line="305"/>
        <location filename="../../Engines/ElementEditor.ui" line="308"/>
        <source>Echelle de l&apos;Element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="311"/>
        <location filename="../../Engines/ElementEditor.ui" line="1101"/>
        <source>Echelle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="317"/>
        <location filename="../../Engines/ElementEditor.ui" line="1107"/>
        <source>Echelle X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="352"/>
        <location filename="../../Engines/ElementEditor.ui" line="1142"/>
        <source>Echelle Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="387"/>
        <location filename="../../Engines/ElementEditor.ui" line="1177"/>
        <source>Echelle Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="451"/>
        <source>Supprimer l&apos;Element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="474"/>
        <source>Nom :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="484"/>
        <source>Editer le nom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="495"/>
        <source>Scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="553"/>
        <source>Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="558"/>
        <location filename="../../Engines/ElementEditor.ui" line="2664"/>
        <location filename="../../Engines/ElementEditor.ui" line="4186"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="563"/>
        <source>Valeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="571"/>
        <source>Ajouter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="578"/>
        <source>Update Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="585"/>
        <source>Supprimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="593"/>
        <source>Physique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="599"/>
        <source>IsPhysic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="608"/>
        <source>ShowDebug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="618"/>
        <location filename="../../Engines/ElementEditor.ui" line="621"/>
        <location filename="../../Engines/ElementEditor.ui" line="624"/>
        <location filename="../../Engines/ElementEditor.ui" line="1411"/>
        <location filename="../../Engines/ElementEditor.ui" line="1414"/>
        <location filename="../../Engines/ElementEditor.ui" line="1417"/>
        <source>Position de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="627"/>
        <source>Delta Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="750"/>
        <location filename="../../Engines/ElementEditor.ui" line="753"/>
        <location filename="../../Engines/ElementEditor.ui" line="756"/>
        <location filename="../../Engines/ElementEditor.ui" line="2310"/>
        <location filename="../../Engines/ElementEditor.ui" line="2313"/>
        <location filename="../../Engines/ElementEditor.ui" line="2316"/>
        <source>Rotation de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="759"/>
        <source>Delta Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="864"/>
        <location filename="../../Engines/ElementEditor.ui" line="867"/>
        <location filename="../../Engines/ElementEditor.ui" line="870"/>
        <source>Défini la position depuis laquelle l&apos;impulsion est appliquée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="873"/>
        <source>Impulsion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="879"/>
        <source>Pos Impulsion X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="914"/>
        <source>Pos Impulsion Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="949"/>
        <source>Pos Impulsion Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="984"/>
        <source>Impulsion X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1019"/>
        <source>Impulsion Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1054"/>
        <source>Impulsion Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1092"/>
        <location filename="../../Engines/ElementEditor.ui" line="1095"/>
        <location filename="../../Engines/ElementEditor.ui" line="1098"/>
        <location filename="../../Engines/ElementEditor.ui" line="4039"/>
        <location filename="../../Engines/ElementEditor.ui" line="4042"/>
        <location filename="../../Engines/ElementEditor.ui" line="4045"/>
        <source>Echelle de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1218"/>
        <source>Type :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1228"/>
        <location filename="../../Engines/ElementEditor.ui" line="1237"/>
        <source>Type de structure :
ConvexeHull -&gt; enveloppe convexe
Compound -&gt; ensemble de ConvexHull
Tree -&gt; Structure collée à l&apos;ob( (l&apos;objet ne peut pas etre mobile pour des raisons de calcul)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1234"/>
        <source>Type de structure :\nConvexeHull -&gt; enveloppe convexe\nCompound -&gt; ensemble de ConvexHull\nTree -&gt; Structure collée à l&apos;ob( (l&apos;objet ne peut pas etre mobile pour des raisons de calcul)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1283"/>
        <location filename="../../Engines/ElementEditor.ui" line="1286"/>
        <location filename="../../Engines/ElementEditor.ui" line="1289"/>
        <location filename="../../Engines/ElementEditor.ui" line="1296"/>
        <location filename="../../Engines/ElementEditor.ui" line="1299"/>
        <location filename="../../Engines/ElementEditor.ui" line="1302"/>
        <source>Materiau de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1305"/>
        <source>Materiau :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1321"/>
        <location filename="../../Engines/ElementEditor.ui" line="1324"/>
        <location filename="../../Engines/ElementEditor.ui" line="1327"/>
        <location filename="../../Engines/ElementEditor.ui" line="1337"/>
        <location filename="../../Engines/ElementEditor.ui" line="1340"/>
        <location filename="../../Engines/ElementEditor.ui" line="1343"/>
        <source>Mass de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1330"/>
        <source>Masse :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1405"/>
        <source>Video2D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1504"/>
        <location filename="../../Engines/ElementEditor.ui" line="1636"/>
        <source>Detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1510"/>
        <source>FilePath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1520"/>
        <source>ScaleX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1543"/>
        <source>ScaleY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1566"/>
        <source>repeatPlayback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1573"/>
        <source>startPlayback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1580"/>
        <source>preloadIntoRAM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1630"/>
        <source>Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1642"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1652"/>
        <source>pitch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1672"/>
        <source>gain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1692"/>
        <source>loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1717"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; background-color:#ff0000;&quot;&gt;       &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Velocity X&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1753"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; background-color:#00ff00;&quot;&gt;       &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Velocity&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Y&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1789"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt; background-color:#0000ff;&quot;&gt;       &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; &lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Velocity&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt; Z&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1871"/>
        <source>Mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1877"/>
        <location filename="../../Engines/ElementEditor.ui" line="1880"/>
        <location filename="../../Engines/ElementEditor.ui" line="1883"/>
        <location filename="../../Engines/ElementEditor.ui" line="4206"/>
        <source>Génère des ombres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1886"/>
        <source>Ombre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1898"/>
        <source>Methode de calcul des ombres </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1901"/>
        <source>Methode Z-Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1908"/>
        <source>Infinité :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1934"/>
        <location filename="../../Engines/ElementEditor.ui" line="1937"/>
        <location filename="../../Engines/ElementEditor.ui" line="1940"/>
        <source>Animation de l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1943"/>
        <source>Animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1955"/>
        <location filename="../../Engines/ElementEditor.ui" line="1958"/>
        <location filename="../../Engines/ElementEditor.ui" line="1961"/>
        <location filename="../../Engines/ElementEditor.ui" line="1974"/>
        <location filename="../../Engines/ElementEditor.ui" line="1977"/>
        <location filename="../../Engines/ElementEditor.ui" line="1980"/>
        <source>Vitesse de changement de frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1964"/>
        <source>Vitesse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="1996"/>
        <location filename="../../Engines/ElementEditor.ui" line="1999"/>
        <location filename="../../Engines/ElementEditor.ui" line="2002"/>
        <location filename="../../Engines/ElementEditor.ui" line="2005"/>
        <location filename="../../Engines/ElementEditor.ui" line="2015"/>
        <location filename="../../Engines/ElementEditor.ui" line="2018"/>
        <location filename="../../Engines/ElementEditor.ui" line="2021"/>
        <source>Frame courante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2037"/>
        <location filename="../../Engines/ElementEditor.ui" line="2040"/>
        <location filename="../../Engines/ElementEditor.ui" line="2043"/>
        <location filename="../../Engines/ElementEditor.ui" line="2046"/>
        <location filename="../../Engines/ElementEditor.ui" line="2056"/>
        <location filename="../../Engines/ElementEditor.ui" line="2059"/>
        <location filename="../../Engines/ElementEditor.ui" line="2062"/>
        <source>Frame de départ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2078"/>
        <location filename="../../Engines/ElementEditor.ui" line="2081"/>
        <location filename="../../Engines/ElementEditor.ui" line="2084"/>
        <location filename="../../Engines/ElementEditor.ui" line="2087"/>
        <location filename="../../Engines/ElementEditor.ui" line="2097"/>
        <location filename="../../Engines/ElementEditor.ui" line="2100"/>
        <location filename="../../Engines/ElementEditor.ui" line="2103"/>
        <source>Frame de fin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2119"/>
        <location filename="../../Engines/ElementEditor.ui" line="2122"/>
        <location filename="../../Engines/ElementEditor.ui" line="2125"/>
        <source>Recommence l&apos;animation infiniment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2128"/>
        <source>Mode Boucle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2178"/>
        <source>Textures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2187"/>
        <location filename="../../Engines/ElementEditor.ui" line="2190"/>
        <location filename="../../Engines/ElementEditor.ui" line="2193"/>
        <location filename="../../Engines/ElementEditor.ui" line="2196"/>
        <source>Supprimer les Materiaux 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2203"/>
        <location filename="../../Engines/ElementEditor.ui" line="2206"/>
        <location filename="../../Engines/ElementEditor.ui" line="2209"/>
        <location filename="../../Engines/ElementEditor.ui" line="2212"/>
        <source>Ajouter des Materiaux 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2262"/>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2271"/>
        <source>Near Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2319"/>
        <source>Target</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2325"/>
        <source>Target Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2354"/>
        <source>Target Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2402"/>
        <source>Target X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2418"/>
        <source>FOV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2450"/>
        <source>Far Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2476"/>
        <source>Viewport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2482"/>
        <source>Up / Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2488"/>
        <location filename="../../Engines/ElementEditor.ui" line="2543"/>
        <source>X : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2511"/>
        <location filename="../../Engines/ElementEditor.ui" line="2569"/>
        <source>Y : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2537"/>
        <source>Down / Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2628"/>
        <source>Constraints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2647"/>
        <location filename="../../Engines/ElementEditor.ui" line="2657"/>
        <source>Type de liaison :
Ball -&gt; &quot;genou&quot;
Slider -&gt; déplacement que selon un axe
Hinge -&gt; &quot;charniere de porte&quot;
Corkscrew -&gt; &quot;tir bouchon&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2654"/>
        <source>Type de liaison :\nBall -&gt; &quot;genou&quot;\nSlider -&gt; déplacement que selon un axe\nHinge -&gt; &quot;charniere de porte&quot;\nCorkscrew -&gt; &quot;tir bouchon&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2691"/>
        <source>Universal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2715"/>
        <location filename="../../Engines/ElementEditor.ui" line="2718"/>
        <location filename="../../Engines/ElementEditor.ui" line="2721"/>
        <source>Objet Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2724"/>
        <source>Element A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2733"/>
        <location filename="../../Engines/ElementEditor.ui" line="2736"/>
        <location filename="../../Engines/ElementEditor.ui" line="2739"/>
        <location filename="../../Engines/ElementEditor.ui" line="2859"/>
        <location filename="../../Engines/ElementEditor.ui" line="2862"/>
        <location filename="../../Engines/ElementEditor.ui" line="2865"/>
        <location filename="../../Engines/ElementEditor.ui" line="3006"/>
        <location filename="../../Engines/ElementEditor.ui" line="3009"/>
        <location filename="../../Engines/ElementEditor.ui" line="3012"/>
        <location filename="../../Engines/ElementEditor.ui" line="3132"/>
        <location filename="../../Engines/ElementEditor.ui" line="3135"/>
        <location filename="../../Engines/ElementEditor.ui" line="3138"/>
        <source>Axe de la jointure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2745"/>
        <location filename="../../Engines/ElementEditor.ui" line="3018"/>
        <source>Pivot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2754"/>
        <location filename="../../Engines/ElementEditor.ui" line="3027"/>
        <source>Pivot Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2767"/>
        <location filename="../../Engines/ElementEditor.ui" line="3040"/>
        <source>Pivot Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2824"/>
        <location filename="../../Engines/ElementEditor.ui" line="3097"/>
        <source>Pivot X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2868"/>
        <location filename="../../Engines/ElementEditor.ui" line="3141"/>
        <source>Axe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2880"/>
        <location filename="../../Engines/ElementEditor.ui" line="3153"/>
        <source>Axe Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2893"/>
        <location filename="../../Engines/ElementEditor.ui" line="3166"/>
        <source>Axe Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2950"/>
        <location filename="../../Engines/ElementEditor.ui" line="3223"/>
        <source>Axe X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2988"/>
        <location filename="../../Engines/ElementEditor.ui" line="2991"/>
        <location filename="../../Engines/ElementEditor.ui" line="2994"/>
        <source>Objet Mobile ou fixe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="2997"/>
        <source>Element B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3275"/>
        <source>Particules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3294"/>
        <source>Taille Particule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3300"/>
        <location filename="../../Engines/ElementEditor.ui" line="3413"/>
        <location filename="../../Engines/ElementEditor.ui" line="3475"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3306"/>
        <location filename="../../Engines/ElementEditor.ui" line="3355"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3326"/>
        <location filename="../../Engines/ElementEditor.ui" line="3375"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3349"/>
        <location filename="../../Engines/ElementEditor.ui" line="3420"/>
        <location filename="../../Engines/ElementEditor.ui" line="3482"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3407"/>
        <source>Nombre de particules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3469"/>
        <source>Temps de vie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3518"/>
        <location filename="../../Engines/ElementEditor.ui" line="3886"/>
        <source>Couleur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3524"/>
        <source>MinColorStart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3530"/>
        <location filename="../../Engines/ElementEditor.ui" line="3568"/>
        <location filename="../../Engines/ElementEditor.ui" line="3892"/>
        <source>Choisir Couleur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3562"/>
        <source>MaxColorStart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3603"/>
        <source>Minimum Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3612"/>
        <source>Min X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3647"/>
        <source>Min Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3682"/>
        <source>Min Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3720"/>
        <source>Maximum Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3729"/>
        <source>Max X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3764"/>
        <source>Max Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3799"/>
        <source>Max Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3837"/>
        <source>Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3873"/>
        <source>Terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3879"/>
        <source>Rebuilt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3924"/>
        <source>patchSize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3934"/>
        <source>ETPS_9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3939"/>
        <source>ETPS_17</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3944"/>
        <source>ETPS_33</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3949"/>
        <source>ETPS_65</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3954"/>
        <source>ETPS_129</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="3968"/>
        <source>Max LOD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4000"/>
        <source>Smooth Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4029"/>
        <source>addAlsoIfHeightmapEmpty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4048"/>
        <source>Echelle Texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4104"/>
        <source>Echelle Texture 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4114"/>
        <source>Echelle Texture 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4167"/>
        <source>Lumiere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4173"/>
        <location filename="../../Engines/ElementEditor.ui" line="4181"/>
        <source>Type de lumiere :
point -&gt; dans toutes les direction
spot -&gt; dans une direction donnée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4178"/>
        <source>Type de lumiere :\npoint -&gt; dans toutes les direction\nspot -&gt; dans une direction donnée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4216"/>
        <location filename="../../Engines/ElementEditor.ui" line="4219"/>
        <location filename="../../Engines/ElementEditor.ui" line="4222"/>
        <source>choix de la couleur diffuse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4225"/>
        <source>Couleur diffuse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4231"/>
        <location filename="../../Engines/ElementEditor.ui" line="4317"/>
        <location filename="../../Engines/ElementEditor.ui" line="4364"/>
        <source>Choisir couleur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4302"/>
        <location filename="../../Engines/ElementEditor.ui" line="4305"/>
        <location filename="../../Engines/ElementEditor.ui" line="4308"/>
        <source>choix de la couleur spéculaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4311"/>
        <source>Couleur spéculaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4349"/>
        <location filename="../../Engines/ElementEditor.ui" line="4352"/>
        <location filename="../../Engines/ElementEditor.ui" line="4355"/>
        <source>choix de la couleur ambiante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4358"/>
        <source>Couleur ambiante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4396"/>
        <source>Attenuation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4402"/>
        <source>Constante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4431"/>
        <source>Lineaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4460"/>
        <source>Quadratique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4492"/>
        <location filename="../../Engines/ElementEditor.ui" line="4495"/>
        <location filename="../../Engines/ElementEditor.ui" line="4498"/>
        <source>Défini le cone d&apos;action de la lumiere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4501"/>
        <source>Cone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4507"/>
        <source>Exterieur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4530"/>
        <source>Interieur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ElementEditor.ui" line="4553"/>
        <source>différence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Engine</name>
    <message>
        <location filename="../../Engines/Engine.ui" line="39"/>
        <location filename="../../Engines/Engine.ui" line="42"/>
        <location filename="../../Engines/Engine.ui" line="45"/>
        <source>Menu de gestion de projet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="48"/>
        <location filename="../../Engines/Engine.ui" line="145"/>
        <location filename="../../Engines/Engine.ui" line="148"/>
        <location filename="../../Engines/Engine.ui" line="151"/>
        <location filename="../../Engines/Engine.ui" line="154"/>
        <source>Fichier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="57"/>
        <location filename="../../Engines/Engine.ui" line="60"/>
        <location filename="../../Engines/Engine.ui" line="63"/>
        <source>Menu d&apos;ajout d&apos;objet dans la scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="66"/>
        <source>Elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="81"/>
        <location filename="../../Engines/Engine.ui" line="84"/>
        <location filename="../../Engines/Engine.ui" line="87"/>
        <source>Menu de gestion du monde physique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="90"/>
        <location filename="../../Engines/Engine.ui" line="310"/>
        <location filename="../../Engines/Engine.ui" line="313"/>
        <location filename="../../Engines/Engine.ui" line="316"/>
        <location filename="../../Engines/Engine.ui" line="319"/>
        <source>Physique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="97"/>
        <location filename="../../Engines/Engine.ui" line="100"/>
        <location filename="../../Engines/Engine.ui" line="103"/>
        <source>Menu de gestion du fond de scene</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="106"/>
        <source>Divers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="114"/>
        <location filename="../../Engines/Engine.ui" line="117"/>
        <location filename="../../Engines/Engine.ui" line="120"/>
        <source>Menu de copier/coller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="123"/>
        <location filename="../../Engines/Engine.ui" line="287"/>
        <location filename="../../Engines/Engine.ui" line="290"/>
        <location filename="../../Engines/Engine.ui" line="293"/>
        <location filename="../../Engines/Engine.ui" line="296"/>
        <source>Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="131"/>
        <source>Aide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="172"/>
        <location filename="../../Engines/Engine.ui" line="175"/>
        <location filename="../../Engines/Engine.ui" line="178"/>
        <location filename="../../Engines/Engine.ui" line="187"/>
        <source>Liste des Maps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="210"/>
        <source>Manage Maps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="219"/>
        <location filename="../../Engines/Engine.ui" line="222"/>
        <location filename="../../Engines/Engine.ui" line="225"/>
        <source>Liste des Elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="234"/>
        <source>Liste Elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="256"/>
        <location filename="../../Engines/Engine.ui" line="259"/>
        <location filename="../../Engines/Engine.ui" line="262"/>
        <location filename="../../Engines/Engine.ui" line="265"/>
        <source>Ajouter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="332"/>
        <location filename="../../Engines/Engine.ui" line="335"/>
        <location filename="../../Engines/Engine.ui" line="338"/>
        <location filename="../../Engines/Engine.ui" line="341"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="359"/>
        <location filename="../../Engines/Engine.ui" line="362"/>
        <location filename="../../Engines/Engine.ui" line="365"/>
        <source>Mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="368"/>
        <source>Ajouter un Mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="371"/>
        <source>Shift+M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="380"/>
        <location filename="../../Engines/Engine.ui" line="383"/>
        <location filename="../../Engines/Engine.ui" line="386"/>
        <source>Ouvrir Projet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="389"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="398"/>
        <location filename="../../Engines/Engine.ui" line="401"/>
        <location filename="../../Engines/Engine.ui" line="404"/>
        <source>Sauver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="407"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="419"/>
        <location filename="../../Engines/Engine.ui" line="422"/>
        <location filename="../../Engines/Engine.ui" line="425"/>
        <source>Reglage Général</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="434"/>
        <location filename="../../Engines/Engine.ui" line="437"/>
        <source>Regler Materiaux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="440"/>
        <source>Regler Materiaux physiques</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="449"/>
        <location filename="../../Engines/Engine.ui" line="452"/>
        <source>Brouillard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="455"/>
        <source>Reglage Brouillard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="458"/>
        <source>Shift+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="467"/>
        <location filename="../../Engines/Engine.ui" line="470"/>
        <source>Ciel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="473"/>
        <source>Reglage Ciel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="476"/>
        <source>Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="485"/>
        <location filename="../../Engines/Engine.ui" line="488"/>
        <source>Lumiere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="491"/>
        <source>Ajouter une lumiere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="494"/>
        <source>Shift+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="503"/>
        <location filename="../../Engines/Engine.ui" line="506"/>
        <source>Contrainte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="509"/>
        <source>Ajouter une Contrainte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="512"/>
        <source>Shift+J</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="521"/>
        <location filename="../../Engines/Engine.ui" line="524"/>
        <source>Particules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="527"/>
        <source>Ajouter un générateur de particules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="530"/>
        <source>Shift+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="539"/>
        <location filename="../../Engines/Engine.ui" line="542"/>
        <location filename="../../Engines/Engine.ui" line="545"/>
        <source>Copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="548"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="557"/>
        <location filename="../../Engines/Engine.ui" line="560"/>
        <location filename="../../Engines/Engine.ui" line="563"/>
        <source>Coller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="566"/>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="575"/>
        <location filename="../../Engines/Engine.ui" line="578"/>
        <location filename="../../Engines/Engine.ui" line="581"/>
        <source>Supprimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="584"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="593"/>
        <location filename="../../Engines/Engine.ui" line="596"/>
        <location filename="../../Engines/Engine.ui" line="599"/>
        <location filename="../../Engines/Engine.ui" line="602"/>
        <source>Generer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="611"/>
        <location filename="../../Engines/Engine.ui" line="614"/>
        <location filename="../../Engines/Engine.ui" line="617"/>
        <source>Lancer la carte</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="620"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="629"/>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="632"/>
        <source>Shift+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="637"/>
        <location filename="../../Engines/Engine.ui" line="640"/>
        <location filename="../../Engines/Engine.ui" line="643"/>
        <source>Scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="648"/>
        <location filename="../../Engines/Engine.ui" line="651"/>
        <location filename="../../Engines/Engine.ui" line="654"/>
        <source>A propos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="663"/>
        <location filename="../../Engines/Engine.ui" line="666"/>
        <location filename="../../Engines/Engine.ui" line="669"/>
        <source>Open Medias</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="678"/>
        <location filename="../../Engines/Engine.ui" line="681"/>
        <source>Terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="684"/>
        <source>Ajouter un Terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="687"/>
        <source>Shift+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="696"/>
        <location filename="../../Engines/Engine.ui" line="699"/>
        <source>Element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="702"/>
        <source>Ajouter un Element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="705"/>
        <source>Shift+E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="714"/>
        <location filename="../../Engines/Engine.ui" line="717"/>
        <location filename="../../Engines/Engine.ui" line="720"/>
        <source>GUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="723"/>
        <source>Reglage GUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="726"/>
        <source>Shift+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="735"/>
        <location filename="../../Engines/Engine.ui" line="738"/>
        <location filename="../../Engines/Engine.ui" line="741"/>
        <source>Switch Cameras</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="744"/>
        <source>Ctrl+Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="753"/>
        <location filename="../../Engines/Engine.ui" line="756"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="759"/>
        <source>Ajouter une Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="768"/>
        <location filename="../../Engines/Engine.ui" line="771"/>
        <source>Sound</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.ui" line="774"/>
        <source>Ajouter Un Sound</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <location filename="../../Engines/FileChooser.ui" line="14"/>
        <source>FileChooser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fog</name>
    <message>
        <location filename="../../Elements/Fog.ui" line="14"/>
        <source>Brouillard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="24"/>
        <location filename="../../Elements/Fog.ui" line="27"/>
        <location filename="../../Elements/Fog.ui" line="30"/>
        <source>Choisir Couleur du brouillard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="33"/>
        <source>Couleur du brouillard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="39"/>
        <source>Choisir Couleur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="73"/>
        <location filename="../../Elements/Fog.ui" line="76"/>
        <location filename="../../Elements/Fog.ui" line="79"/>
        <source>Brouillard calculé au Pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="82"/>
        <source>Brouillard &quot;Pixel&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="89"/>
        <location filename="../../Elements/Fog.ui" line="92"/>
        <location filename="../../Elements/Fog.ui" line="95"/>
        <source>Brouillard calculé à l&apos;objet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="98"/>
        <source>Brouillard &quot;Range&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="107"/>
        <location filename="../../Elements/Fog.ui" line="114"/>
        <source>méthode de calcul du brouillard :
Linéaire, exponentiel, exponentiel²</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="111"/>
        <source>méthode de calcul du brouillard :\nLinéaire, exponentiel, exponentiel²</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="118"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="152"/>
        <location filename="../../Elements/Fog.ui" line="155"/>
        <location filename="../../Elements/Fog.ui" line="158"/>
        <source>Début du brouillard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="161"/>
        <source>Debut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="186"/>
        <location filename="../../Elements/Fog.ui" line="189"/>
        <location filename="../../Elements/Fog.ui" line="192"/>
        <source>Fin du brouillard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="195"/>
        <source>Fin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="223"/>
        <location filename="../../Elements/Fog.ui" line="226"/>
        <location filename="../../Elements/Fog.ui" line="229"/>
        <source>Densité du brouillard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Fog.ui" line="232"/>
        <source>Densité</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuiEngine</name>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="14"/>
        <source>CEGUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="24"/>
        <source>update IHM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="31"/>
        <source>Update CEGUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="42"/>
        <source>Paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="48"/>
        <source>Open Schemes Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="55"/>
        <source>Open imagesets Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="62"/>
        <source>Open fonts Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="69"/>
        <source>Open layouts Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="76"/>
        <source>Open schemas Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="83"/>
        <source>Open animations Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="90"/>
        <source>Open looknfeels Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="98"/>
        <source>Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="104"/>
        <source>DefaultFont</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="111"/>
        <source>DejaVuSans-10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="118"/>
        <source>DefaultMouseImageSet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="125"/>
        <source>WindowsLook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="132"/>
        <source>DefaultMouseCursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="139"/>
        <source>MouseArrow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="146"/>
        <source>DefaultTooltip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="153"/>
        <source>WindowsLook/Tooltip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="161"/>
        <source>FontManager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="170"/>
        <location filename="../../Engines/GuiEngine.ui" line="197"/>
        <location filename="../../Engines/GuiEngine.ui" line="224"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="180"/>
        <location filename="../../Engines/GuiEngine.ui" line="207"/>
        <location filename="../../Engines/GuiEngine.ui" line="234"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="188"/>
        <source>SchemeManager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="215"/>
        <source>ImageSetManager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="242"/>
        <location filename="../../Engines/GuiEngine.ui" line="252"/>
        <source>Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="264"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="276"/>
        <source>Add Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="281"/>
        <source>Remove Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="305"/>
        <source>widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="310"/>
        <source>event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="315"/>
        <source>ElementId</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="320"/>
        <source>script</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/GuiEngine.ui" line="325"/>
        <source>code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpWindow</name>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="14"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="31"/>
        <source>Engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="37"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Section Engine(nommé engine dans les scripts) :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Divers :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setNextMap(String nextMap);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Declare la prochaine map (il faut appeler setEtat(&amp;quot;changeMap&amp;quot;) pour réelement changer de map&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setEtat(String etat);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie l&apos;état du moteur : launch, changeMap, quit&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void log(String priority, String s);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Ajoute une ligne dans le log en fonction de la prioritée : Error, Warning, Debug, Info, Config&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setSharedPool(String name, String value);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Crée une variable accéssible par tout les scripts&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    String getSharedPoolVariable(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Recupere la valeur d&apos;une variable crée par &amp;quot;setSharedPool&amp;quot;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Aleatoire :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    int random(int min = 0, int max = RAND_MAX);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois un nombre entier aléatoir((un srand(time(null)) est fait au lancement du moteur)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    double frandom(double min = 0, double max = 1);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois un nombre flottant aléatoire&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Gestion d&apos;Elements :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getElement(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne l&apos;element qui possede le nom &amp;quot;name&amp;quot;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void destroyElement(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Supprime l&apos;element qui possede le nom &amp;quot;name&amp;quot;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue createMesh(String name, QScriptValue pos, QScriptValue rot, QScriptValue scale, String mesh, String type, QScriptValue size, String material, float masse);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Crée un mesh 3D&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue createLumiere(String name, String type, bool shadow, QScriptValue pos, QScriptValue rot, float outerCone, float innerCone, float falloff, int ambiantA, int ambiantR, int ambiantG, int ambiantB, int diffuseA, int diffuseR, int diffuseG, int diffuseB, int specularA, int specularR, int specularG, int specularB, float attenuationConst, float attenuationLin, float attenuationQuad);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Crée une Lumiere    &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue createParticules(String name, QScriptValue pos, QScriptValue rot, QScriptValue scale, QScriptValue boxMin, QScriptValue boxMax, QScriptValue dir, int minParticlesPerSecond, int maxParticlesPerSecond, int lifeTimeMin, int lifeTimeMax, int maxAngleDegrees, int minStartColorA, int minStartColorR, int minStartColorG, int minStartColorB, int maxStartColorA, int maxStartColorR, int maxStartColorG, int maxStartColorB, float minStartWidth, float minStartHeight, float maxStartWidth, float maxStartHeight);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Crée un générateur de particules&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Musique :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    int playSound(String sound);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Joue un son &amp;quot;court&amp;quot; et retourne son identifiant (appelé &amp;quot;source&amp;quot;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setSoundProperties(int source, float pitch, float gain, QScriptValue position, QScriptValue velocity, bool loop);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Applique certaines propriétés sur un son&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setSoundPitch(int source, float pitch);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Pitch&amp;quot; sur un son&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setSoundGain(int source, float gain);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Gain&amp;quot; sur un son&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setSoundPos(int source, QScriptValue position);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Position&amp;quot; sur un son&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setSoundVelocity(int source, QScriptValue velocity);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Velocity&amp;quot; sur un son&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setSoundLoop(int source, bool loop);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Loop&amp;quot; sur un son&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void playMusic(String name, String music);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Joue un son &amp;quot;long&amp;quot; identifié par un nom&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    bool musicFinished(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne vrais si la musique est finie (elle n&apos;existe plus)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setMusicProperties(String name, float pitch, float gain, QScriptValue position, QScriptValue velocity, bool loop);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Applique certaines propriétés sur une musique&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        &lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setMusicPitch(String name, float pitch);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Pitch&amp;quot; sur une musique&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setMusicGain(String name, float gain);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Gain&amp;quot; sur une musique&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setMusicPos(String name, QScriptValue position);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Position&amp;quot; sur une musique&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setMusicVelocity(String name, QScriptValue velocity);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Velocity&amp;quot; sur une musique&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setMusicLoop(String name, bool loop);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Modifie la propriété &amp;quot;Loop&amp;quot; sur une musique&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void stopMusic(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Arrete une musique&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;GUI:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    String getGuiValue(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne la valeur d&apos;un element de GUI&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void GuiSetEnable(String name, bool enable);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Active/Desactive un element de GUI&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    bool GuiIsVisible(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne vrai si un element de GUI est visible&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void GuiSetVisible(String name, bool visible);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Active/Desactive la visibilité d&apos;un element de GUI&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    String GuiComboGetSelected(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne la valeur d&apos;un element de GUI de type &amp;quot;ComboBox&amp;quot;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    float GuiSpinnerGetValue(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne la valeur d&apos;un element de GUI de type &amp;quot;Spinner&amp;quot;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Vecteurs :(QScriptValue = objet avec les attributs &amp;quot;X, Y et Z&amp;quot;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue normalizeVector(QScriptValue pos)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue crossProductVector(QScriptValue vect1, QScriptValue vect2)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue rotateXZBy(float degrees, QScriptValue vect1, QScriptValue vect2)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue rotateYZBy(float degrees, QScriptValue vect1, QScriptValue vect2)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue rotateXYBy(float degrees, QScriptValue vect1, QScriptValue vect2)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Conversions :(QScriptValue = objet avec les attributs &amp;quot;X, Y et Z&amp;quot;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    float radToDeg(float rad);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    float degToRad(float deg);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue cartesianToSpheric(QScriptValue cartesian);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue sphericToCartesian(QScriptValue spheric);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="173"/>
        <source>EventEngine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="179"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Section EventEngine(nommé engine dans les scripts) :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;bool isKeyDown(QString keyCode);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    Renvois si la touche &amp;quot;key&amp;quot; est appuyée&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;bool isJustKeyDown(QString keyCode);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    Renvois si la touche &amp;quot;key&amp;quot; viens juste d&apos;etre appuyée&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;void setMousePos(const QScriptValue &amp;amp;pos);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    Modifie la position de la souris(pos est un objet avec les attribut &amp;quot;X et Y&amp;quot;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;void setMouseVisible(bool visible);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;QScriptValue getMousePos();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    Renvoi la position de la souris&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;int getWheel();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    Renvois la valeur de la molette, et la remet à zero&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;&amp;quot;keyCode&amp;quot; peux valoir :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;&amp;quot;KEY_LBUTTON&amp;quot;, &amp;quot;KEY_RBUTTON&amp;quot;, &amp;quot;KEY_CANCEL&amp;quot;, &amp;quot;KEY_MBUTTON&amp;quot;, &amp;quot;KEY_XBUTTON1&amp;quot;, &amp;quot;KEY_XBUTTON2&amp;quot;, &amp;quot;KEY_BACK&amp;quot;, &amp;quot;KEY_TAB&amp;quot;, &amp;quot;KEY_CLEAR&amp;quot;, &amp;quot;KEY_RETURN&amp;quot;, &amp;quot;KEY_SHIFT&amp;quot;, &amp;quot;KEY_CONTROL&amp;quot;, &amp;quot;KEY_MENU&amp;quot;, &amp;quot;KEY_PAUSE&amp;quot;, &amp;quot;KEY_CAPITAL&amp;quot;, &amp;quot;KEY_KANA&amp;quot;, &amp;quot;KEY_HANGUEL&amp;quot;, &amp;quot;KEY_HANGUL&amp;quot;, &amp;quot;KEY_JUNJA&amp;quot;, &amp;quot;KEY_FINAL&amp;quot;, &amp;quot;KEY_HANJA&amp;quot;, &amp;quot;KEY_KANJI&amp;quot;, &amp;quot;KEY_ESCAPE&amp;quot;, &amp;quot;KEY_CONVERT&amp;quot;, &amp;quot;KEY_NONCONVERT&amp;quot;, &amp;quot;KEY_ACCEPT&amp;quot;, &amp;quot;KEY_MODECHANGE&amp;quot;, &amp;quot;KEY_SPACE&amp;quot;, &amp;quot;KEY_PRIOR&amp;quot;, &amp;quot;KEY_NEXT&amp;quot;, &amp;quot;KEY_END&amp;quot;, &amp;quot;KEY_HOME&amp;quot;, &amp;quot;KEY_LEFT&amp;quot;, &amp;quot;KEY_UP&amp;quot;, &amp;quot;KEY_RIGHT&amp;quot;, &amp;quot;KEY_DOWN&amp;quot;, &amp;quot;KEY_SELECT&amp;quot;, &amp;quot;KEY_PRINT&amp;quot;, &amp;quot;KEY_EXECUT&amp;quot;, &amp;quot;KEY_SNAPSHOT&amp;quot;, &amp;quot;KEY_INSERT&amp;quot;, &amp;quot;KEY_DELETE&amp;quot;, &amp;quot;KEY_HELP&amp;quot;, &amp;quot;KEY_KEY_0&amp;quot;, &amp;quot;KEY_KEY_1&amp;quot;, &amp;quot;KEY_KEY_2&amp;quot;, &amp;quot;KEY_KEY_3&amp;quot;, &amp;quot;KEY_KEY_4&amp;quot;, &amp;quot;KEY_KEY_5&amp;quot;, &amp;quot;KEY_KEY_6&amp;quot;, &amp;quot;KEY_KEY_7&amp;quot;, &amp;quot;KEY_KEY_8&amp;quot;, &amp;quot;KEY_KEY_9&amp;quot;, &amp;quot;KEY_KEY_A&amp;quot;, &amp;quot;KEY_KEY_B&amp;quot;, &amp;quot;KEY_KEY_C&amp;quot;, &amp;quot;KEY_KEY_D&amp;quot;, &amp;quot;KEY_KEY_E&amp;quot;, &amp;quot;KEY_KEY_F&amp;quot;, &amp;quot;KEY_KEY_G&amp;quot;, &amp;quot;KEY_KEY_H&amp;quot;, &amp;quot;KEY_KEY_I&amp;quot;, &amp;quot;KEY_KEY_J&amp;quot;, &amp;quot;KEY_KEY_K&amp;quot;, &amp;quot;KEY_KEY_L&amp;quot;, &amp;quot;KEY_KEY_M&amp;quot;, &amp;quot;KEY_KEY_N&amp;quot;, &amp;quot;KEY_KEY_O&amp;quot;, &amp;quot;KEY_KEY_P&amp;quot;, &amp;quot;KEY_KEY_Q&amp;quot;, &amp;quot;KEY_KEY_R&amp;quot;, &amp;quot;KEY_KEY_S&amp;quot;, &amp;quot;KEY_KEY_T&amp;quot;, &amp;quot;KEY_KEY_U&amp;quot;, &amp;quot;KEY_KEY_V&amp;quot;, &amp;quot;KEY_KEY_W&amp;quot;, &amp;quot;KEY_KEY_X&amp;quot;, &amp;quot;KEY_KEY_Y&amp;quot;, &amp;quot;KEY_KEY_Z&amp;quot;, &amp;quot;KEY_LWIN&amp;quot;, &amp;quot;KEY_RWIN&amp;quot;, &amp;quot;KEY_APPS&amp;quot;, &amp;quot;KEY_SLEEP&amp;quot;, &amp;quot;KEY_NUMPAD0&amp;quot;, &amp;quot;KEY_NUMPAD1&amp;quot;, &amp;quot;KEY_NUMPAD2&amp;quot;, &amp;quot;KEY_NUMPAD3&amp;quot;, &amp;quot;KEY_NUMPAD4&amp;quot;, &amp;quot;KEY_NUMPAD5&amp;quot;, &amp;quot;KEY_NUMPAD6&amp;quot;, &amp;quot;KEY_NUMPAD7&amp;quot;, &amp;quot;KEY_NUMPAD8&amp;quot;, &amp;quot;KEY_NUMPAD9&amp;quot;, &amp;quot;KEY_MULTIPLY&amp;quot;, &amp;quot;KEY_ADD&amp;quot;, &amp;quot;KEY_SEPARATOR&amp;quot;, &amp;quot;KEY_SUBTRACT&amp;quot;, &amp;quot;KEY_DECIMAL&amp;quot;, &amp;quot;KEY_DIVIDE&amp;quot;, &amp;quot;KEY_F1&amp;quot;, &amp;quot;KEY_F2&amp;quot;, &amp;quot;KEY_F3&amp;quot;, &amp;quot;KEY_F4&amp;quot;, &amp;quot;KEY_F5&amp;quot;, &amp;quot;KEY_F6&amp;quot;, &amp;quot;KEY_F7&amp;quot;, &amp;quot;KEY_F8&amp;quot;, &amp;quot;KEY_F9&amp;quot;, &amp;quot;KEY_F10&amp;quot;, &amp;quot;KEY_F11&amp;quot;, &amp;quot;KEY_F12&amp;quot;, &amp;quot;KEY_F13&amp;quot;, &amp;quot;KEY_F14&amp;quot;, &amp;quot;KEY_F15&amp;quot;, &amp;quot;KEY_F16&amp;quot;, &amp;quot;KEY_F17&amp;quot;, &amp;quot;KEY_F18&amp;quot;, &amp;quot;KEY_F19&amp;quot;, &amp;quot;KEY_F20&amp;quot;, &amp;quot;KEY_F21&amp;quot;, &amp;quot;KEY_F22&amp;quot;, &amp;quot;KEY_F23&amp;quot;, &amp;quot;KEY_F24&amp;quot;, &amp;quot;KEY_NUMLOCK&amp;quot;, &amp;quot;KEY_SCROLL&amp;quot;, &amp;quot;KEY_LSHIFT&amp;quot;, &amp;quot;KEY_RSHIFT&amp;quot;, &amp;quot;KEY_LCONTROL&amp;quot;, &amp;quot;KEY_RCONTROL&amp;quot;, &amp;quot;KEY_LMENU&amp;quot;, &amp;quot;KEY_RMENU&amp;quot;, &amp;quot;KEY_PLUS&amp;quot;, &amp;quot;KEY_COMMA&amp;quot;, &amp;quot;KEY_MINUS&amp;quot;, &amp;quot;KEY_PERIOD&amp;quot;, &amp;quot;KEY_ATTN&amp;quot;, &amp;quot;KEY_CRSEL&amp;quot;, &amp;quot;KEY_EXSEL&amp;quot;, &amp;quot;KEY_EREOF&amp;quot;, &amp;quot;KEY_PLAY&amp;quot;, &amp;quot;KEY_ZOOM&amp;quot;, &amp;quot;KEY_PA1&amp;quot;, &amp;quot;KEY_OEM_CLEAR&amp;quot;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="214"/>
        <source>Element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="220"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;L&apos;Element qui possede le script se nomme &amp;quot;self&amp;quot;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Quel que soit le type de l&apos;Element, on peux appeller ces methodes :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    int getName()	&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne l&apos;identifiant unique de l&apos;Element&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    String getTypeName()&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne le type de l&apos;Element&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void execute(String code, QString script)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Permet d&apos;éxecuter du code dans le script d&apos;un Element&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Si le script n&apos;est pas donné, le code s&apos;execute sur tout les scripts de l&apos;element&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="248"/>
        <source>Mesh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="254"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Un Mesh est un Element qui possede une représentation 3D&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Il possede donc les memes fonctions qu&apos;un Element, plus :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    (QScriptValue = objet avec les attributs &amp;quot;X, Y et Z&amp;quot;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Positions :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getPosition();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la position de l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setPosition(QScriptValue pos);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Deplace l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getRotation();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la rotation de l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setRotation(QScriptValue pos);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Tourne l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getScale();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la taille de l&apos;Objet(dépend de la taille du mesh importé)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setScale(QScriptValue pos);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Change la taille de l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setTarget(QScriptValue target);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Tourne l&apos;Objet vers un point donné&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Physic:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setPhysicScale(QScriptValue scale);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Change la taille de l&apos;Objet physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addForce(float x, float y, float z);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Ajoute une force a l&apos;Objet Physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addForce(QScriptValue pos);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Ajoute une force a l&apos;Objet Physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addTorque(float x, float y, float z);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Ajoute une force de rotation a l&apos;Objet Physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addTorque(QScriptValue pos);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Ajoute une force de rotation a l&apos;Objet Physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addImpulse(float pointDeltaVelocX, float pointDeltaVelocY, float pointDeltaVelocZ, float pointPositX, float pointPositY, float pointPositZ);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Ajoute une impulsion a l&apos;Objet Physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addImpulse(QScriptValue pointDeltaVeloc, QScriptValue pointPosit);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Ajoute une impulsion a l&apos;Objet Physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    float getMasse() const;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne la masse a l&apos;Objet Physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setMasse(float masse);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Affecte une masse a l&apos;Objet Physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    int nbCollisions(String name);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne le nombre de collision avec l&apos;Element &amp;quot;name&amp;quot;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    bool isInCollisionWithMap();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Retourne vrais si le Mesh est en contact avec un autre Element&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void showPhysic(bool value);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Active / Desactive la visualisation de l&apos;Objet Physique associé&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Animation :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setAnimationSpeed(int speed);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Change la vitesse de l&apos;annimation&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setAnimationFrameLoop(int begin, int end);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Change l&apos;intervalle des frame de l&apos;annimation&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setAnimationCurrentFrame(int current);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Change la frame courante de l&apos;annimation&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setAnimationLoopMode(bool loop);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Rejoue l&apos;annimation à l&apos;infini&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Ombres :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setShadow(String mesh, bool zfailmethod, float infinity);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Mise en place d&apos;une ombre&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Textures :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setMaterial3D(int num, bool wireframe, bool pointCloud, bool gouraudShading, bool lighting, bool zwriteEnable, bool backfaceCulling, bool frontfaceCulling, bool fogEnable, bool normalizeNormals, float shininess, String materialTypeParam, String materialTypeParam2, String materialType, String zBuffer, String antiAliasing, String colorMask, String colorMaterial, String texture, int ambiantAlpha, int ambiantRed, int ambiantGreen, int ambiantBlue, int diffuseAlpha, int diffuseRed, int diffuseGreen, int diffuseBlue, int emissiveAlpha, int emissiveRed, int emissiveGreen, int emissiveBlue, int specularAlpha, int specularRed, int specularGreen, int specularBlue);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Mise en place d&apos;une texture&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="352"/>
        <source>Terrain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="358"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Un Terrain est un Mesh dessiné grace à une image.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Il possede donc les memes fonctions qu&apos;un Mesh, plus :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setLODOfPatch(int patchX, int patchZ, int lod)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Affectation du LOD&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setScaleTexture(float scaleTexture1, float scaleTexture2);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Redimentionnement des deux textures&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="381"/>
        <source>Particules</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="387"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Un générateur de particules est un Element.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Il possede donc les memes fonctions qu&apos;un Element, plus :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    (QScriptValue = objet avec les attributs &amp;quot;X, Y et Z&amp;quot;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Positions :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getPosition()&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la position de l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setPosition(QScriptValue pos)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Deplace l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getRotation()&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la rotation de l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setRotation(QScriptValue pos)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Tourne l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getScale()&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la taille de l&apos;Objet(dépend de la taille du mesh importé)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setScale(QScriptValue pos)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Change la taille de l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setTarget(QScriptValue target)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Tourne l&apos;Objet vers un point donné&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Affectors :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void removeAllAffectors()&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Supprime tous les effets&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addFadeOutAffector(int alpha, int red, int green, int blue, int timeNeededToFadeOut)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Ajoute un changement de couleur des particules.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addAttractionAffector(float posX, float posY, float posZ, float speed, bool attract, bool affectX, bool affectY, bool affectZ)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Les particules sont attirées/repoussée par un point donné.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addGravityAffector(float posX, float posY, float posZ, int timeForceLost)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Les particules sont affectées par une gravité.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void addRotationAffector(float pivotX, float pivotY, float pivotZ, float speedX, float speedY, float speedZ)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Les particules subissent une rotation autours d&apos;un point donné.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Textures :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setMaterial3D(int num, bool wireframe, bool pointCloud, bool gouraudShading, bool lighting, bool zwriteEnable, bool backfaceCulling, bool frontfaceCulling, bool fogEnable, bool normalizeNormals, float shininess, String materialTypeParam, String materialTypeParam2, String materialType, String zBuffer, String antiAliasing, String colorMask, String colorMaterial, String texture, int ambiantAlpha, int ambiantRed, int ambiantGreen, int ambiantBlue, int diffuseAlpha, int diffuseRed, int diffuseGreen, int diffuseBlue, int emissiveAlpha, int emissiveRed, int emissiveGreen, int emissiveBlue, int specularAlpha, int specularRed, int specularGreen, int specularBlue);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Mise en place d&apos;une texture&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="447"/>
        <source>Lumieres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="453"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Une Lumiere est un Element.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Elle possede donc les memes fonctions qu&apos;un Element, plus :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    (QScriptValue = objet avec les attributs &amp;quot;X, Y et Z&amp;quot;)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Positions :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getPosition()&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la position de la Lumiere&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setPosition(QScriptValue pos)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Deplace la Lumiere&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getRotation()&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la rotation de la Lumiere&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setRotation(QScriptValue pos)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Tourne la Lumiere&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setTarget(QScriptValue target)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Tourne la Lumiere vers un point donné&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Rayon :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setRadius(float radius)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Definis le rayon de la lumière&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setCone(float outerCone, float innerCone, float falloff)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Definis le rayon de la lumiere de maniere plus precise que &amp;quot;setRadius&amp;quot;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Couleurs :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setAmbiantColor(int alpha, int red, int green, int blue)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setDiffuseColor(int alpha, int red, int green, int blue)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setSpecularColor(int alpha, int red, int green, int blue)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Divers :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setType(String type)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Definis le type de lumière : &amp;quot;ELT_POINT&amp;quot;, &amp;quot;ELT_SPOT&amp;quot; ou &amp;quot;ELT_DIRECTIONAL&amp;quot;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void enableCastShadow(bool enable)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Définis si la lumiere génère des ombres&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setAttenuation(float attenuationConst, float attenuationLin, float attenuationQuad)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Définis l&apos;atenuation de la lumiere&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="509"/>
        <source>Contraintes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="515"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Une Contrainte est un Element qui relie un Mesh à un autre Mesh.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Elle possede donc les memes fonctions qu&apos;un Element.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="531"/>
        <source>Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="537"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Une Camera est un Element qui permet d&apos;afficher la scene.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Elle possede donc les memes fonctions qu&apos;un Element, plus :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getPosition();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la position de l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setPosition(QScriptValue pos);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Deplace l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getRotation();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvois la rotation de l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setRotation(QScriptValue pos);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Tourne l&apos;Objet&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setTarget(QScriptValue target);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Tourne l&apos;Objet vers un point donné&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getUpVector();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;        Renvoi le vecteur qui point vers le &amp;quot;haut&amp;quot; de la camera&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setAspectRatio(float aspectRatio)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setAspectRatio(int width, int height)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setFarValue(float farValue)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setNearValue(float nearValue)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setFOV(float fov)&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    void setCameraViewport(float x1, float y1, float x2, float y2);&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;    QScriptValue getCameraViewport();&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="586"/>
        <source>Video2D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/HelpWindow.ui" line="592"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Une Video2D est un Element qui permet d&apos;afficher la scene.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:11pt;&quot;&gt;Elle possede donc les memes fonctions qu&apos;un Element.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapsManager</name>
    <message>
        <location filename="../../Engines/MapsManager.ui" line="14"/>
        <source>Maps Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/MapsManager.ui" line="27"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/MapsManager.ui" line="34"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/MapsManager.ui" line="67"/>
        <source>MainMap :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/MapsManager.ui" line="113"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/MapsManager.ui" line="120"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::Engine</name>
    <message>
        <location filename="../../Engines/Engine.cpp" line="517"/>
        <source>Generer le Jeu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.cpp" line="531"/>
        <location filename="../../Engines/Engine.cpp" line="546"/>
        <location filename="../../Engines/Engine.cpp" line="557"/>
        <location filename="../../Engines/Engine.cpp" line="850"/>
        <location filename="../../Engines/Engine.cpp" line="862"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.cpp" line="676"/>
        <location filename="../../Engines/Engine.cpp" line="913"/>
        <source>Element &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.cpp" line="677"/>
        <source>&quot; sélectioné</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.cpp" line="812"/>
        <source>Voulez vous sauvegarder ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.cpp" line="850"/>
        <location filename="../../Engines/Engine.cpp" line="862"/>
        <source>Save Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.cpp" line="860"/>
        <source>Le fichier a été sauvegardé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Engine.cpp" line="914"/>
        <source>&quot; copié</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::Fog</name>
    <message>
        <location filename="../../Elements/Fog.cpp" line="73"/>
        <source>Couleur</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::Light</name>
    <message>
        <location filename="../../Elements/Light.cpp" line="224"/>
        <source>Couleur ambiante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Light.cpp" line="235"/>
        <source>Couleur speculaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Light.cpp" line="246"/>
        <source>Couleur diffuse</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::MapsManager</name>
    <message>
        <location filename="../../Engines/MapsManager.cpp" line="50"/>
        <source>Add Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/MapsManager.cpp" line="50"/>
        <source>Map Name : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/MapsManager.cpp" line="85"/>
        <location filename="../../Engines/MapsManager.cpp" line="119"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/MapsManager.cpp" line="86"/>
        <location filename="../../Engines/MapsManager.cpp" line="120"/>
        <source>Project not loaded</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::Material3D</name>
    <message>
        <location filename="../../Materials/Material3D.cpp" line="410"/>
        <source>Selection texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.cpp" line="452"/>
        <source>Couleur ambiante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.cpp" line="463"/>
        <source>Couleur speculaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.cpp" line="474"/>
        <source>Couleur diffuse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.cpp" line="485"/>
        <source>Couleur emissive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::Medias</name>
    <message>
        <location filename="../../Engines/Medias.cpp" line="238"/>
        <location filename="../../Engines/Medias.cpp" line="297"/>
        <source>Add Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.cpp" line="239"/>
        <location filename="../../Engines/Medias.cpp" line="298"/>
        <source>Folder Name : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.cpp" line="256"/>
        <location filename="../../Engines/Medias.cpp" line="313"/>
        <location filename="../../Engines/Medias.cpp" line="334"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.cpp" line="257"/>
        <source>Shader Already exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.cpp" line="307"/>
        <source>Add File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.cpp" line="308"/>
        <source>File Name : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.cpp" line="314"/>
        <location filename="../../Engines/Medias.cpp" line="335"/>
        <source>File Already exist</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::Particules</name>
    <message>
        <location filename="../../Elements/Particules.cpp" line="515"/>
        <location filename="../../Elements/Particules.cpp" line="534"/>
        <source>Couleur min</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::ProjectChooser</name>
    <message>
        <location filename="../../Engines/ProjectChooser.cpp" line="98"/>
        <source>Creer un projet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ProjectChooser.cpp" line="106"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ProjectChooser.cpp" line="120"/>
        <source>Ouvrir un Projet existant</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::ScriptEditor</name>
    <message>
        <location filename="../../Editors/ScriptEditor.cpp" line="197"/>
        <location filename="../../Editors/ScriptEditor.cpp" line="247"/>
        <location filename="../../Editors/ScriptEditor.cpp" line="272"/>
        <location filename="../../Editors/ScriptEditor.cpp" line="292"/>
        <source>Script Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/ScriptEditor.cpp" line="198"/>
        <source>Impossible de lire l&apos;aide&apos;:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/ScriptEditor.cpp" line="248"/>
        <source>Le fichier a  ete modifie.
Voulez vous sauvegarder?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/ScriptEditor.cpp" line="273"/>
        <source>Impossible de lire le fichier %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/ScriptEditor.cpp" line="285"/>
        <source>File loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/ScriptEditor.cpp" line="293"/>
        <source>Impossible d&apos;ecrire le fichier %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/ScriptEditor.cpp" line="304"/>
        <source>File saved</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::Terrain</name>
    <message>
        <location filename="../../Elements/Terrain.cpp" line="467"/>
        <source>Couleur</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MarbleEditor::XMLEditor</name>
    <message>
        <location filename="../../Editors/XMLEditor.cpp" line="122"/>
        <location filename="../../Editors/XMLEditor.cpp" line="147"/>
        <location filename="../../Editors/XMLEditor.cpp" line="167"/>
        <source>XML Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/XMLEditor.cpp" line="123"/>
        <source>Le fichier a ete modifie.
Voulez vous sauvegarder?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/XMLEditor.cpp" line="148"/>
        <source>Impossible de lire le fichier %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/XMLEditor.cpp" line="160"/>
        <source>File loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/XMLEditor.cpp" line="168"/>
        <source>Impossible d&apos;ecrire le fichier %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Editors/XMLEditor.cpp" line="179"/>
        <source>File saved</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Material3D</name>
    <message>
        <location filename="../../Materials/Material3D.ui" line="14"/>
        <source>Material 3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="65"/>
        <location filename="../../Materials/Material3D.ui" line="68"/>
        <location filename="../../Materials/Material3D.ui" line="71"/>
        <source>Choix de la couleur ambiante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="74"/>
        <source>Couleur Ambiante</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="80"/>
        <location filename="../../Materials/Material3D.ui" line="127"/>
        <location filename="../../Materials/Material3D.ui" line="219"/>
        <location filename="../../Materials/Material3D.ui" line="266"/>
        <source>Choisir Couleur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="112"/>
        <location filename="../../Materials/Material3D.ui" line="115"/>
        <location filename="../../Materials/Material3D.ui" line="118"/>
        <source>Choix de la couleur spéculaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="121"/>
        <source>Couleur Speculaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="161"/>
        <location filename="../../Materials/Material3D.ui" line="164"/>
        <location filename="../../Materials/Material3D.ui" line="167"/>
        <location filename="../../Materials/Material3D.ui" line="177"/>
        <location filename="../../Materials/Material3D.ui" line="180"/>
        <location filename="../../Materials/Material3D.ui" line="183"/>
        <source>Taille de la réflexion Spéculaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="170"/>
        <source>Taille de la réflexion Spéculaire :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="204"/>
        <location filename="../../Materials/Material3D.ui" line="207"/>
        <location filename="../../Materials/Material3D.ui" line="210"/>
        <source>Choix de la couleur émissive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="213"/>
        <source>Couleur Emissive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="251"/>
        <location filename="../../Materials/Material3D.ui" line="254"/>
        <location filename="../../Materials/Material3D.ui" line="257"/>
        <source>Choix de la couleur diffuse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="260"/>
        <source>Couleur Diffuse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="304"/>
        <location filename="../../Materials/Material3D.ui" line="307"/>
        <location filename="../../Materials/Material3D.ui" line="310"/>
        <source>Ajout d&apos;un masque de couleur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="313"/>
        <source>Masque de couleur :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="363"/>
        <location filename="../../Materials/Material3D.ui" line="366"/>
        <location filename="../../Materials/Material3D.ui" line="369"/>
        <location filename="../../Materials/Material3D.ui" line="379"/>
        <location filename="../../Materials/Material3D.ui" line="382"/>
        <location filename="../../Materials/Material3D.ui" line="385"/>
        <source>Methode de calcul de l&apos;anti aliasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="372"/>
        <source>Anti Aliasing :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="431"/>
        <location filename="../../Materials/Material3D.ui" line="434"/>
        <location filename="../../Materials/Material3D.ui" line="437"/>
        <location filename="../../Materials/Material3D.ui" line="447"/>
        <location filename="../../Materials/Material3D.ui" line="450"/>
        <location filename="../../Materials/Material3D.ui" line="453"/>
        <source>Methode de calcul de la profondeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="440"/>
        <source>Zbuffer :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="504"/>
        <location filename="../../Materials/Material3D.ui" line="507"/>
        <location filename="../../Materials/Material3D.ui" line="510"/>
        <location filename="../../Materials/Material3D.ui" line="520"/>
        <location filename="../../Materials/Material3D.ui" line="523"/>
        <location filename="../../Materials/Material3D.ui" line="526"/>
        <source>Methode de calcul de la reflexion de la lumiere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="513"/>
        <source>Calcul de la couleur :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="567"/>
        <source>Reglage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="573"/>
        <source>brouillard activé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="580"/>
        <source>normalise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="587"/>
        <source>rendu goureaud</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="594"/>
        <source>Masque Face de devant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="601"/>
        <source>Fil de fer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="608"/>
        <source>Masque Face de derriere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="615"/>
        <source>éclairé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="622"/>
        <source>active zBuffer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="629"/>
        <source>pointillé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="639"/>
        <source>Material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="645"/>
        <location filename="../../Materials/Material3D.ui" line="654"/>
        <source>Type de texture :
Pour les Objet3D,le plus utilisé est EMT_SOLID
 pour les particules, c&apos;est EMT_TRANSPARENT_ADD_COLOR
Si c&apos;est deux la ne conviennent pas, il faut tester les autres un par un...(dsl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="651"/>
        <source>Type de texture :\nPour les Objet3D,le plus utilisé est EMT_SOLID\n pour les particules, c&apos;est EMT_TRANSPARENT_ADD_COLOR\nSi c&apos;est deux la ne conviennent pas, il faut tester les autres un par un..((dsl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="792"/>
        <source>Shader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="810"/>
        <location filename="../../Materials/Material3D.ui" line="813"/>
        <location filename="../../Materials/Material3D.ui" line="816"/>
        <source>Sélection d&apos;une image de texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="819"/>
        <location filename="../../Materials/Material3D.ui" line="825"/>
        <source>Texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="872"/>
        <source>Selection texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/Material3D.ui" line="915"/>
        <source>Level :</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MaterialPhys</name>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="14"/>
        <source>Materiaux physique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="24"/>
        <location filename="../../Materials/MaterialPhys.ui" line="27"/>
        <location filename="../../Materials/MaterialPhys.ui" line="30"/>
        <source>Nouveau Materiau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="33"/>
        <source>Materiaux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="39"/>
        <source>Nouveau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="58"/>
        <location filename="../../Materials/MaterialPhys.ui" line="61"/>
        <location filename="../../Materials/MaterialPhys.ui" line="64"/>
        <source>Ajouter Nouveau Materiau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="67"/>
        <source>Ajouter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="87"/>
        <location filename="../../Materials/MaterialPhys.ui" line="90"/>
        <location filename="../../Materials/MaterialPhys.ui" line="93"/>
        <source>Nom du Nouveau Materiau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="104"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="109"/>
        <source>restitution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="114"/>
        <source>friction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="119"/>
        <source>lin_damping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="124"/>
        <source>ang_damping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="135"/>
        <location filename="../../Materials/MaterialPhys.ui" line="138"/>
        <location filename="../../Materials/MaterialPhys.ui" line="141"/>
        <location filename="../../Materials/MaterialPhys.ui" line="144"/>
        <source>Interactions entre deux materiaux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="150"/>
        <location filename="../../Materials/MaterialPhys.ui" line="153"/>
        <location filename="../../Materials/MaterialPhys.ui" line="156"/>
        <location filename="../../Materials/MaterialPhys.ui" line="159"/>
        <source>Materiau 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="171"/>
        <location filename="../../Materials/MaterialPhys.ui" line="174"/>
        <location filename="../../Materials/MaterialPhys.ui" line="177"/>
        <location filename="../../Materials/MaterialPhys.ui" line="180"/>
        <source>Materiau 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="192"/>
        <location filename="../../Materials/MaterialPhys.ui" line="195"/>
        <location filename="../../Materials/MaterialPhys.ui" line="198"/>
        <source>Son joué lors de la collision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="201"/>
        <source>Son</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Materials/MaterialPhys.ui" line="220"/>
        <source>Choisir le son</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Medias</name>
    <message>
        <location filename="../../Engines/Medias.ui" line="18"/>
        <source>Medias</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="29"/>
        <source>Models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="35"/>
        <location filename="../../Engines/Medias.ui" line="59"/>
        <location filename="../../Engines/Medias.ui" line="83"/>
        <location filename="../../Engines/Medias.ui" line="107"/>
        <location filename="../../Engines/Medias.ui" line="155"/>
        <source>Add Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="42"/>
        <location filename="../../Engines/Medias.ui" line="66"/>
        <location filename="../../Engines/Medias.ui" line="90"/>
        <location filename="../../Engines/Medias.ui" line="114"/>
        <location filename="../../Engines/Medias.ui" line="138"/>
        <location filename="../../Engines/Medias.ui" line="169"/>
        <source>Add Existing File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="53"/>
        <source>Musics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="77"/>
        <source>Textures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="101"/>
        <source>Videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="125"/>
        <source>Gui</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="131"/>
        <location filename="../../Engines/Medias.ui" line="162"/>
        <source>Create New File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="149"/>
        <source>Scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="180"/>
        <source>Shaders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/Medias.ui" line="186"/>
        <source>Create New Shader</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PhysicEngine</name>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="14"/>
        <source>Reglage Physique</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="24"/>
        <source>Physic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="36"/>
        <source>Maximum Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="45"/>
        <source>Max X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="80"/>
        <source>Max Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="115"/>
        <source>Max Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="153"/>
        <source>Minimum Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="162"/>
        <source>Min X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="197"/>
        <source>Min Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="232"/>
        <source>Min Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="270"/>
        <source>Gravite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="279"/>
        <source>Gravite X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="314"/>
        <source>Gravite Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="349"/>
        <source>Gravite Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/PhysicEngine.ui" line="390"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProjectChooser</name>
    <message>
        <location filename="../../Engines/ProjectChooser.ui" line="14"/>
        <source>ProjectChooser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ProjectChooser.ui" line="27"/>
        <source>Nouveau projet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ProjectChooser.ui" line="34"/>
        <source>Liste des projets recents :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ProjectChooser.ui" line="43"/>
        <source>Ouvrir Projet Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Engines/ProjectChooser.ui" line="50"/>
        <source>Selectionner un projet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Sky</name>
    <message>
        <location filename="../../Elements/Sky.ui" line="14"/>
        <source>Ciel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="24"/>
        <source>Enable Sky</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="36"/>
        <location filename="../../Elements/Sky.ui" line="54"/>
        <source>Dome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="45"/>
        <location filename="../../Elements/Sky.ui" line="48"/>
        <location filename="../../Elements/Sky.ui" line="51"/>
        <location filename="../../Elements/Sky.ui" line="130"/>
        <location filename="../../Elements/Sky.ui" line="133"/>
        <location filename="../../Elements/Sky.ui" line="136"/>
        <location filename="../../Elements/Sky.ui" line="146"/>
        <location filename="../../Elements/Sky.ui" line="149"/>
        <location filename="../../Elements/Sky.ui" line="152"/>
        <location filename="../../Elements/Sky.ui" line="162"/>
        <location filename="../../Elements/Sky.ui" line="165"/>
        <location filename="../../Elements/Sky.ui" line="168"/>
        <location filename="../../Elements/Sky.ui" line="178"/>
        <location filename="../../Elements/Sky.ui" line="181"/>
        <location filename="../../Elements/Sky.ui" line="184"/>
        <location filename="../../Elements/Sky.ui" line="194"/>
        <location filename="../../Elements/Sky.ui" line="197"/>
        <location filename="../../Elements/Sky.ui" line="200"/>
        <location filename="../../Elements/Sky.ui" line="210"/>
        <location filename="../../Elements/Sky.ui" line="213"/>
        <location filename="../../Elements/Sky.ui" line="216"/>
        <source>Choix de l&apos;image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="118"/>
        <source>Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="139"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="155"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="171"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="187"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="203"/>
        <source>Front</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../Elements/Sky.ui" line="219"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
