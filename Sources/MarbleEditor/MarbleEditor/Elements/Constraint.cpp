/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Constraint.h"

#include <map>

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Mesh.h"
#include "MarbleEditor/Elements/Scripts.h"

namespace MarbleEditor {

Constraint::Constraint(QString name, Engine* engine) : Element(name, engine) {
    constraintType = "Ball";

    node = graphicsEngine->getSceneManager()->addBillboardSceneNode(
                0, irr::core::dimension2d<irr::f32>(5, 5));
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
    node->setMaterialTexture(
                0,
                graphicsEngine->getDriver()->getTexture("Gismos/joint.bmp"));
}

Constraint::~Constraint() {
}

bool Constraint::init() {
    return true;
}

void Constraint::setSelected(bool value) {
    Element::setSelected(value);


    if (node != NULL) {
        node->setDebugDataVisible(value);
    }

    QTabWidget* tab = edit->tabWidget;
    if (value) {
        int index = tab->addTab(edit->Constraints, "Constraints");
        if (edit->getCurrentTab() == "Constraints")
            tab->setCurrentIndex(index);

        edit->PositionBox->hide();
        edit->scaleBox->hide();
        edit->rotationBox->hide();

        updateElementEditor();

        connect(edit->jointTypeBox, static_cast<void (QComboBox::*)(int)>
                (&QComboBox::activated),
                this, &Constraint::change);

        connect(edit->nameA, static_cast<void (QComboBox::*)(int)>
                (&QComboBox::activated),
                this, &Constraint::change);

        connect(edit->nameB, static_cast<void (QComboBox::*)(int)>
                (&QComboBox::activated),
                this, &Constraint::change);

        connect(edit->pivotInA_X, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->pivotInA_Y, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->pivotInA_Z, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->pivotInB_X, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->pivotInB_Y, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->pivotInB_Z, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->axisInA_X, static_cast<void (QDoubleSpinBox::*)(double)>
                                            (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->axisInA_Y, static_cast<void (QDoubleSpinBox::*)(double)>
                                            (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->axisInA_Z, static_cast<void (QDoubleSpinBox::*)(double)>
                                            (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->axisInB_X, static_cast<void (QDoubleSpinBox::*)(double)>
                                            (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->axisInB_Y, static_cast<void (QDoubleSpinBox::*)(double)>
                                            (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);

        connect(edit->axisInB_Z, static_cast<void (QDoubleSpinBox::*)(double)>
                                            (&QDoubleSpinBox::valueChanged),
                this, &Constraint::change);
    } else {
        edit->jointTypeBox->disconnect();

        edit->nameA->disconnect();
        edit->nameB->disconnect();

        edit->pivotInA_X->disconnect();
        edit->pivotInA_Y->disconnect();
        edit->pivotInA_Z->disconnect();

        edit->pivotInB_X->disconnect();
        edit->pivotInB_Y->disconnect();
        edit->pivotInB_Z->disconnect();

        edit->axisInA_X->disconnect();
        edit->axisInA_Y->disconnect();
        edit->axisInA_Z->disconnect();

        edit->axisInB_X->disconnect();
        edit->axisInB_Y->disconnect();
        edit->axisInB_Z->disconnect();

        edit->PositionBox->show();
        edit->scaleBox->show();
        edit->rotationBox->show();

        tab->removeTab(tab->indexOf(edit->Constraints));
    }
}

void Constraint::updateElementEditor() {
    QStringList elements;
    std::map<QString, Element*> liste = engine->getElements();

    std::map<QString, Element*>::const_iterator it, end = liste.end();
    for (it = liste.begin(); it != end; ++it) {
        if ((it->second)->isPhysic()) {
            QString id = it->first;
            elements.append(id);
        }
    }

    elements.sort();

    edit->nameA->clear();
    edit->nameB->clear();
    edit->nameA->addItems(elements);
    elements.append("");
    elements.sort();
    edit->nameB->addItems(elements);

    edit->nameA->setCurrentIndex(edit->nameA->findText(nameA));
    edit->nameB->setCurrentIndex(edit->nameB->findText(nameB));
    edit->jointTypeBox->setCurrentIndex(
                edit->jointTypeBox->findText(constraintType));

    edit->pivotInA_X->setValue(pivotInA.x());
    edit->pivotInA_Y->setValue(pivotInA.y());
    edit->pivotInA_Z->setValue(pivotInA.z());

    edit->pivotInB_X->setValue(pivotInB.x());
    edit->pivotInB_Y->setValue(pivotInB.y());
    edit->pivotInB_Z->setValue(pivotInB.z());

    edit->axisInA_X->setValue(axisInA.x());
    edit->axisInA_Y->setValue(axisInA.y());
    edit->axisInA_Z->setValue(axisInA.z());

    edit->axisInB_X->setValue(axisInB.x());
    edit->axisInB_Y->setValue(axisInB.y());
    edit->axisInB_Z->setValue(axisInB.z());
}

bool Constraint::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("type")) {
        constraintType = elem->Attribute("type");
    }

    if (elem->Attribute("nameA")) {
        nameA = elem->Attribute("nameA");
    }

    if (elem->Attribute("nameB")) {
        nameB = elem->Attribute("nameB");
    }

    if (elem->Attribute("pivotInA_X") && elem->Attribute("pivotInA_Y")
            && elem->Attribute("pivotInA_Z")) {
        pivotInA.setX(elem->FloatAttribute("pivotInA_X"));
        pivotInA.setY(elem->FloatAttribute("pivotInA_Y"));
        pivotInA.setZ(elem->FloatAttribute("pivotInA_Z"));
    }

    if (elem->Attribute("pivotInB_X") && elem->Attribute("pivotInB_Y")
            && elem->Attribute("pivotInB_Z")) {
        pivotInB.setX(elem->FloatAttribute("pivotInB_X"));
        pivotInB.setY(elem->FloatAttribute("pivotInB_Y"));
        pivotInB.setZ(elem->FloatAttribute("pivotInB_Z"));
    }

    if (elem->Attribute("axisInA_X") && elem->Attribute("axisInA_Y")
            && elem->Attribute("axisInA_Z")) {
        axisInA.setX(elem->FloatAttribute("axisInA_X"));
        axisInA.setY(elem->FloatAttribute("axisInA_Y"));
        axisInA.setZ(elem->FloatAttribute("axisInA_Z"));
    }

    if (elem->Attribute("axisInB_X") && elem->Attribute("axisInB_Y")
            && elem->Attribute("axisInB_Z")) {
        axisInB.setX(elem->FloatAttribute("axisInB_X"));
        axisInB.setY(elem->FloatAttribute("axisInB_Y"));
        axisInB.setZ(elem->FloatAttribute("axisInB_Z"));
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            scripts->readXML(elem);
        }
        elem = elem->NextSiblingElement();
    }

    return true;
}

bool Constraint::writeXML(tinyxml2::XMLDocument* xmlDoc,
                          tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Constraint");
    element->SetAttribute("name", name.toUtf8().data());

    element->SetAttribute("type", constraintType.toUtf8().data());

    element->SetAttribute("nameA", nameA.toUtf8().data());
    element->SetAttribute("nameB", nameB.toUtf8().data());

    element->SetAttribute("pivotInA_X", pivotInA.x());
    element->SetAttribute("pivotInA_Y", pivotInA.y());
    element->SetAttribute("pivotInA_Z", pivotInA.z());

    element->SetAttribute("pivotInB_X", pivotInB.x());
    element->SetAttribute("pivotInB_Y", pivotInB.y());
    element->SetAttribute("pivotInB_Z", pivotInB.z());

    element->SetAttribute("axisInA_X", axisInA.x());
    element->SetAttribute("axisInA_Y", axisInA.y());
    element->SetAttribute("axisInA_Z", axisInA.z());

    element->SetAttribute("axisInB_X", axisInB.x());
    element->SetAttribute("axisInB_Y", axisInB.y());
    element->SetAttribute("axisInB_Z", axisInB.z());

    scripts->writeXML(xmlDoc, element);

    elem->LinkEndChild(element);
    return true;
}

void Constraint::specificRender() {
    irr::video::IVideoDriver* driver = graphicsEngine->getDriver();
    irr::core::vector3df posNode;

    if (nameA != "") {
        Element* eltA = engine->getElement(nameA);
        if (eltA) {
            irr::core::vector3df posA = eltA->getNode()->getPosition();
            posA.X += pivotInA.x();
            posA.Y += pivotInA.y();
            posA.Z += pivotInA.z();

            posNode = posA;

            if (isSelected) {
                irr::core::vector3df axe = irr::core::vector3df(
                            axisInA.x(), axisInA.y(), axisInA.z());
                axe.normalize();
                axe *= 4;
                driver->draw3DLine(
                            posA + axe, posA - axe,
                            irr::video::SColor(255, 255, 255, 0));
            }
        }
    }
    if (nameA != "" && nameB != "") {
        Element* eltA = engine->getElement(nameA);
        Element* eltB = engine->getElement(nameB);
        if (eltA && eltB) {
            irr::core::vector3df posA = eltA->getNode()->getPosition();
            posA.X += pivotInA.x();
            posA.Y += pivotInA.y();
            posA.Z += pivotInA.z();

            irr::core::vector3df posB = eltB->getNode()->getPosition();
            posB.X += pivotInB.x();
            posB.Y += pivotInB.y();
            posB.Z += pivotInB.z();

            posNode = (posB + posA) / 2;

            if (isSelected) {
                driver->draw3DLine(posA, posB,
                                   irr::video::SColor(255, 0, 0, 255));

                irr::core::vector3df axe = irr::core::vector3df(
                            axisInB.x(), axisInB.y(), axisInB.z());
                axe.normalize();
                axe *= 4;
                driver->draw3DLine(posB + axe, posB - axe,
                                   irr::video::SColor(255, 255, 255, 0));
            }
        }
    }
    node->setPosition(posNode);
}

void Constraint::change() {
    constraintType = edit->jointTypeBox->currentText();

    nameA = edit->nameA->currentText();
    nameB = edit->nameB->currentText();

    pivotInA.setX(edit->pivotInA_X->value());
    pivotInA.setY(edit->pivotInA_Y->value());
    pivotInA.setZ(edit->pivotInA_Z->value());

    pivotInB.setX(edit->pivotInB_X->value());
    pivotInB.setY(edit->pivotInB_Y->value());
    pivotInB.setZ(edit->pivotInB_Z->value());

    axisInA.setX(edit->axisInA_X->value());
    axisInA.setY(edit->axisInA_Y->value());
    axisInA.setZ(edit->axisInA_Z->value());

    axisInB.setX(edit->axisInB_X->value());
    axisInB.setY(edit->axisInB_Y->value());
    axisInB.setZ(edit->axisInB_Z->value());

    if (constraintType == "Ball") {
        edit->axisInA->setEnabled(false);
        edit->axisInB->setEnabled(false);
    } else {
        edit->axisInA->setEnabled(true);
        edit->axisInB->setEnabled(true);
    }

    if (isSelected) {
        updateElementEditor();
    }
}

void Constraint::updateNameElements(QString before, QString after) {
    if (nameA == before)
        nameA = after;
    if (nameB == before)
        nameB = after;
    if (isSelected) {
        updateElementEditor();
    }
}

}  // namespace MarbleEditor
