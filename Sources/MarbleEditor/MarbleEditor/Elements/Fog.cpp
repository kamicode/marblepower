/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Fog.h"

#include "MarbleEditor/Engines/Engine.h"

namespace MarbleEditor {

Fog::Fog(Engine* engine, QWidget* parent): QDialog(parent) {
    setupUi(this);

    this->engine = engine;
    couleur = QColor(Qt::white);
    densityWidget->hide();

    connect(typeBox, &QComboBox::currentTextChanged,
            this, &Fog::chooseType);

    connect(colorButton, &QPushButton::clicked,
            this, &Fog::chooseColor);

    connect(pixelFog, &QPushButton::clicked,
            this, &Fog::activeFog);

    connect(rangeFog, &QPushButton::clicked,
            this, &Fog::activeFog);

    connect(start, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
            this, &Fog::activeFog);

    connect(end, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
            this, &Fog::activeFog);

    connect(density, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
            this, &Fog::activeFog);
}

Fog::~Fog() {
}

void Fog::chooseType() {
    if (typeBox->currentText() == "EFT_FOG_EXP"
            || typeBox->currentText() == "EFT_FOG_EXP2") {
        densityWidget->show();
        linearWidget->hide();
    } else if (typeBox->currentText() == "EFT_FOG_LINEAR") {
        densityWidget->hide();
        linearWidget->show();
    }
    activeFog();
}

void Fog::chooseColor() {
    couleur = QColorDialog::getColor(couleur, this, tr("Couleur"),
                                     QColorDialog::ShowAlphaChannel);
    if (couleur.isValid()) {
        QPalette palette = fogColor->palette();
        palette.setColor(QPalette::Window, couleur);
        fogColor->setPalette(palette);
    } else {
        couleur = QColor(Qt::white);
    }
    activeFog();
}

bool Fog::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("alpha") && elem->Attribute("red")
            && elem->Attribute("green") && elem->Attribute("blue")) {
        couleur.setRed(elem->IntAttribute("red"));
        couleur.setGreen(elem->IntAttribute("green"));
        couleur.setBlue(elem->IntAttribute("blue"));
        couleur.setAlpha(elem->IntAttribute("alpha"));
        if (couleur.isValid()) {
            QPalette palette = fogColor->palette();
            palette.setColor(QPalette::Window, couleur);
            fogColor->setPalette(palette);
        }
    }

    if (elem->Attribute("type")) {
        typeBox->setCurrentIndex(typeBox->findText(elem->Attribute("type")));
    }

    if (elem->Attribute("start")) {
        start->setValue(elem->FloatAttribute("start"));
    }

    if (elem->Attribute("end")) {
        end->setValue(elem->FloatAttribute("end"));
    }

    if (elem->Attribute("density")) {
        density->setValue(elem->FloatAttribute("density"));
    }

    if (elem->Attribute("pixelFog")) {
        pixelFog->setChecked(elem->BoolAttribute("pixelFog"));
    }

    if (elem->Attribute("rangeFog")) {
        rangeFog->setChecked(elem->BoolAttribute("rangeFog"));
    }
    activeFog();
    return true;
}

bool Fog::writeXML(tinyxml2::XMLDocument* xmlDoc, tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Fog");

    element->SetAttribute("alpha", couleur.alpha());
    element->SetAttribute("red", couleur.red());
    element->SetAttribute("green", couleur.green());
    element->SetAttribute("blue", couleur.blue());
    element->SetAttribute("type", typeBox->currentText().toUtf8().data());
    element->SetAttribute("start", start->value());
    element->SetAttribute("end", end->value());
    element->SetAttribute("density", density->value());
    element->SetAttribute("pixelFog", pixelFog->isChecked());
    element->SetAttribute("rangeFog", rangeFog->isChecked());

    elem->LinkEndChild(element);
    return true;
}

void Fog::activeFog() {
    irr::video::SColor color(couleur.alpha(), couleur.red(),
                             couleur.green(), couleur.blue());
    engine->getGraphicsEngine()->getDriver()->setFog(
                color, Convert::stringFogType(typeBox->currentText()),
                start->value(), end->value(), density->value(),
                pixelFog->isChecked(), rangeFog->isChecked());
}

}  // namespace MarbleEditor
