/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Sound.h"

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Scripts.h"

namespace MarbleEditor {

Sound::Sound(QString name, Engine* engine) : Element(name, engine) {
    file = "";
    pitch = 1;
    gain = 1;
    velocity[0] = velocity[1] = velocity[2] = 0;
    loop = true;

    node = graphicsEngine->getSceneManager()->addBillboardSceneNode(
                0, irr::core::dimension2df(5, 5));
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
    node->setMaterialTexture(0,
                graphicsEngine->getDriver()->getTexture("Gismos/sound.png"));
}

bool Sound::init() {
    irr::core::vector3df pos = graphicsEngine->getCamera()->getTarget();
    pos.normalize();
    pos *= 5;
    pos += graphicsEngine->getCamera()->getPosition();

    node->setPosition(pos);

    return true;
}

void Sound::setSelected(bool value) {
    Element::setSelected(value);

    if (node != NULL) {
        node->setDebugDataVisible(value);
    }

    QTabWidget* tab = edit->tabWidget;
    if (value) {
        int index = tab->addTab(edit->Sound, "Sound");
        if (edit->getCurrentTab() == "Sound")
            tab->setCurrentIndex(index);

        edit->scaleBox->hide();
        edit->rotationBox->hide();

        updateElementEditor();

        connect(edit->file, static_cast<void (QComboBox::*)(int)>
                (&QComboBox::currentIndexChanged),
                this, &Sound::setDetails);

        connect(edit->pitch, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Sound::setDetails);

        connect(edit->gain, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Sound::setDetails);

        connect(edit->posX, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Sound::setDetails);

        connect(edit->posY, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Sound::setDetails);

        connect(edit->posZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Sound::setDetails);

        connect(edit->velocityX, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Sound::setDetails);

        connect(edit->velocityY, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Sound::setDetails);

        connect(edit->velocityZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Sound::setDetails);

        connect(edit->loop, &QPushButton::clicked,
                this, &Sound::setDetails);
    } else {
        edit->file->disconnect();
        edit->pitch->disconnect();
        edit->gain->disconnect();
        edit->velocityX->disconnect();
        edit->velocityY->disconnect();
        edit->velocityZ->disconnect();
        edit->loop->disconnect();

        edit->scaleBox->show();
        edit->rotationBox->show();

        tab->removeTab(tab->indexOf(edit->Sound));
    }
}

void Sound::updateElementEditor() {
    QDir d(engine->getDirMedias() + "Musics");
    QFileInfoList infoList = d.entryInfoList();
    for (int i = 0; i < infoList.count(); ++i) {
        QFileInfo f = infoList.at(i);
        if (f.isFile()) {
            edit->file->addItem("Musics/" + f.fileName());
        }
    }

    edit->file->setCurrentIndex(edit->file->findText(file));
    edit->posX->setValue(node->getPosition().X);
    edit->posY->setValue(node->getPosition().Y);
    edit->posZ->setValue(node->getPosition().Z);
    edit->velocityX->setValue(velocity[0]);
    edit->velocityY->setValue(velocity[1]);
    edit->velocityZ->setValue(velocity[2]);
    edit->pitch->setValue(pitch);
    edit->gain->setValue(gain);
    edit->loop->setChecked(loop);
}

bool Sound::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("file")) {
        file = elem->Attribute("file");
    }
    if (elem->Attribute("pitch")) {
        pitch = elem->FloatAttribute("pitch");
    }
    if (elem->Attribute("gain")) {
        gain = elem->FloatAttribute("gain");
    }
    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        irr::core::vector3df pos;
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
        node->setPosition(pos);
    }
    if (elem->Attribute("velocityX") && elem->Attribute("velocityY")
            && elem->Attribute("velocityZ")) {
        velocity[0] = elem->FloatAttribute("velocityX");
        velocity[1] = elem->FloatAttribute("velocityY");
        velocity[2] = elem->FloatAttribute("velocityZ");
    }
    if (elem->Attribute("loop")) {
        loop = elem->BoolAttribute("loop");
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            scripts->readXML(elem);
        }
        elem = elem->NextSiblingElement();
    }

    return true;
}

bool Sound::writeXML(tinyxml2::XMLDocument* xmlDoc,
                     tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Sound");
    element->SetAttribute("name", name.toUtf8().data());

    element->SetAttribute("file", file.toUtf8().data());

    element->SetAttribute("pitch", pitch);
    element->SetAttribute("gain", gain);

    element->SetAttribute("posX", node->getPosition().X);
    element->SetAttribute("posY", node->getPosition().Y);
    element->SetAttribute("posZ", node->getPosition().Z);

    element->SetAttribute("velocityX", velocity[0]);
    element->SetAttribute("velocityY", velocity[1]);
    element->SetAttribute("velocityZ", velocity[2]);

    element->SetAttribute("loop", loop);

    scripts->writeXML(xmlDoc, element);

    elem->LinkEndChild(element);

    return true;
}

void Sound::setDetails() {
    file = edit->file->currentText();

    irr::core::vector3df pos;
    pos.X = edit->posX->value();
    pos.Y = edit->posY->value();
    pos.Z = edit->posZ->value();
    node->setPosition(pos);

    velocity[0] = edit->velocityX->value();
    velocity[1] = edit->velocityY->value();
    velocity[2] = edit->velocityZ->value();

    pitch = edit->pitch->value();
    gain = edit->gain->value();
    loop = edit->loop->isChecked();
}

}  // namespace MarbleEditor
