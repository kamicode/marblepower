/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Element.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_ELEMENT_H_
#define MARBLEEDITOR_ELEMENTS_ELEMENT_H_

#include "MarbleEditor/Engines/Engine.h"

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Scripts;

/*! \class Element
 * \brief
 *
 *
 */
class Element: public QObject {
    Q_OBJECT

    public:
        explicit Element(QString name, Engine* engine);
        virtual ~Element();

        virtual bool init();

        Engine* getEngine();

        QString getName() const;
        void setName(QString name);
        virtual void updateNameElements(QString /*before*/,
                                        QString /*after*/) {}

        virtual void setSelected(bool value);
        virtual void updateElementEditor() { }

        virtual bool readXML(tinyxml2::XMLElement* elem);
        virtual bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                              tinyxml2::XMLElement* elem);

        irr::scene::ISceneNode* getNode() const;
        void setNode(irr::scene::ISceneNode* newNode);
        virtual void specificRender() {}
        virtual bool isPhysic() { return false; }

        QStringList getScripts();

    public slots:
        virtual void setPosX(double value);
        virtual void setPosY(double value);
        virtual void setPosZ(double value);

        virtual void setRotX(double value);
        virtual void setRotY(double value);
        virtual void setRotZ(double value);

        virtual void setScaleX(double value);
        virtual void setScaleY(double value);
        virtual void setScaleZ(double value);

    private slots:
        void editName();

    protected:
        irr::scene::ISceneNode* node;
        QString name;
        Engine* engine;
        GraphicsEngine* graphicsEngine;
        Scripts* scripts;
        ElementEditor* edit;
        bool isSelected;

    private:
        PhysicsEngine* physicsEngine;

    private:
        DISALLOW_COPY_AND_ASSIGN(Element);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_ELEMENT_H_
