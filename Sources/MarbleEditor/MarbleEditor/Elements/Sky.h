/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Sky.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_SKY_H_
#define MARBLEEDITOR_ELEMENTS_SKY_H_

#include <ui_Sky.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class Sky
 * \brief
 *
 *
 */
class Sky : public QDialog, private Ui::Sky {
    Q_OBJECT

    public:
        explicit Sky(Engine* engine, QWidget* parent = 0);
        virtual ~Sky();

        void unloadMap();

        QString chooseFile();
        void updateIrrlicht();
        bool readXML(tinyxml2::XMLElement* elem);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

    private slots:
        void switchDome(bool check);
        void switchBox(bool check);
        void chooseDome();
        void changeDome(QString file );
        void chooseBottom();
        void changeBottom(QString file);
        void chooseTop();
        void changeTop(QString file);
        void chooseLeft();
        void changeLeft(QString file);
        void chooseRight();
        void changeRight(QString file);
        void chooseFront();
        void changeFront(QString file);
        void chooseBack();
        void changeBack(QString file);

    private:
        Engine* engine;
        irr::scene::ISceneNode* node;

    private:
        DISALLOW_COPY_AND_ASSIGN(Sky);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_SKY_H_
