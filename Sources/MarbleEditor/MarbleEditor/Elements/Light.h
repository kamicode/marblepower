/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Light.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_LIGHT_H_
#define MARBLEEDITOR_ELEMENTS_LIGHT_H_

#include "MarbleEditor/Elements/Element.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

/*! \class Light
 * \brief
 *
 *
 */
class Light : public Element {
    Q_OBJECT

    public:
        explicit Light(QString name, Engine* engine);
        virtual ~Light();

        bool init();

        bool readXML(tinyxml2::XMLElement* elem);
        void setSelected(bool value);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

        void updateElementEditor();

        void setAmbientColor(irr::video::SColor ambiant =
                irr::video::SColor(255, 255, 255, 255));
        void setDiffuseColor(irr::video::SColor diffuse =
                irr::video::SColor(255, 255, 255, 255));
        void setSpecularColor(irr::video::SColor specular =
                irr::video::SColor(255, 255, 255, 255));

    private slots:
        void setPosX(double value);
        void setPosY(double value);
        void setPosZ(double value);

        void setRotX(double value);
        void setRotY(double value);
        void setRotZ(double value);

        void setType(QString value);
        void setShadow(bool value);

        void setConstant(double value);
        void setLineaire(double value);
        void setQuadratique(double value);

        void chooseAmbiantColor();
        void chooseSpecularColor();
        void chooseDiffuseColor();

        void setOuter(double value);
        void setInner(double value);
        void setFalloff(double value);

    private:
        irr::video::SLight light;
        irr::scene::ILightSceneNode* lightNode;

    private:
        DISALLOW_COPY_AND_ASSIGN(Light);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_LIGHT_H_
