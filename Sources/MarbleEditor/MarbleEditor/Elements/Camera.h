/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Camera.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_CAMERA_H_
#define MARBLEEDITOR_ELEMENTS_CAMERA_H_

#include "MarbleEditor/Elements/Element.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

/*! \class Camera
 * \brief
 *
 *
 */
class Camera: public Element {
    Q_OBJECT

    public:
        explicit Camera(QString name, Engine* engine);
        virtual ~Camera();

        bool init();

        bool readXML(tinyxml2::XMLElement* elem);

        void setSelected(bool value);
        void updateElementEditor();

        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

        irr::scene::ICameraSceneNode* getCamera();

        irr::core::rect<irr::f32> getViewport();
        irr::core::rect<irr::s32> getAbsoluteViewport(int width, int height);

    private slots:
        void setPosX(double value);
        void setPosY(double value);
        void setPosZ(double value);

        void setTargetX(double value);
        void setTargetY(double value);
        void setTargetZ(double value);

        void setFarValue(double value);
        void setNearValue(double value);
        void setFov(double value);
        void setViewportX1(double value);
        void setViewportX2(double value);
        void setViewportY1(double value);
        void setViewportY2(double value);

    private:
        irr::scene::ICameraSceneNode* cameraNode;
        float farValue, nearValue, fov;
        float viewportX1, viewportX2, viewportY1, viewportY2;

    private:
        DISALLOW_COPY_AND_ASSIGN(Camera);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_CAMERA_H_
