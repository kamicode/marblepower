/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Fog.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_FOG_H_
#define MARBLEEDITOR_ELEMENTS_FOG_H_

#include <ui_Fog.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;

/*! \class Fog
 * \brief
 *
 *
 */
class Fog: public QDialog, private Ui::Fog {
    Q_OBJECT

    public:
        explicit Fog(Engine* engine, QWidget* parent = 0);
        virtual ~Fog();

        bool readXML(tinyxml2::XMLElement* elem);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

    private slots:
        void chooseType();
        void chooseColor();
        void activeFog();

    private:
        Engine* engine;
        QColor couleur;

    private:
        DISALLOW_COPY_AND_ASSIGN(Fog);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_FOG_H_
