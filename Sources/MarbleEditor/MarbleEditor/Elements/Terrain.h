/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Terrain.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_TERRAIN_H_
#define MARBLEEDITOR_ELEMENTS_TERRAIN_H_

#include "MarbleEditor/Elements/Element.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Material3D;

/*! \class Terrain
 * \brief
 *
 *
 */
class Terrain : public Element {
    Q_OBJECT

    public:
        explicit Terrain(QString name, Engine* engine);
        virtual ~Terrain();

        bool init(QString heightMapFile);

        bool readXML(tinyxml2::XMLElement* elem);
        void physic(tinyxml2::XMLElement* elem);

        void setSelected(bool value);
        void updateElementEditor();

        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

    private slots:
        void chooseColor();
        void rebuiltTerrain();

        void setPatchSize(QString patch);
        void setMaxLod(int value);
        void setSmoothFactor(int value);
        void setAlsoIfHeightmapEmpty(bool value);
        void setScaleText1(double value);
        void setScaleText2(double value);

        void setPosX(double value);
        void setPosY(double value);
        void setPosZ(double value);

        void setRotX(double value);
        void setRotY(double value);
        void setRotZ(double value);

        void setScaleX(double value);
        void setScaleY(double value);
        void setScaleZ(double value);

        void setScalePhysX(double value);
        void setScalePhysY(double value);
        void setScalePhysZ(double value);
        void setMaterialPhys(QString value);

        bool loadBody();
        void setDebugPhysic(bool value);

    private:
        QString heightMapFile;
        irr::video::SColor color;
        int maxLOD, smoothFactor;
        bool addAlsoIfHeightmapEmpty;
        QString patchSize;
        float scaleText1, scaleText2;

        Material3D* material;

        bool showDebugPhysic;
        QString materialPhy;
        irr::core::vector3df scalePhys;
        btCollisionShape* m_collisionShape;
        btMotionState* m_motionState;
        btRigidBody* m_rigidBody;
        irr::core::matrix4 mat;

    private:
        DISALLOW_COPY_AND_ASSIGN(Terrain);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_TERRAIN_H_
