/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Mesh.h"

#include <vector>
#include <set>

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Scripts.h"
#include "MarbleEditor/Materials/Material3D.h"
#include "MarbleEditor/Materials/MaterialPhys.h"

namespace MarbleEditor {

Mesh::Mesh(QString name, Engine* engine) : Element(name, engine) {
    material3ds.clear();

    mesh = "";

    shadowNode = NULL;
    zfailmethod = false;
    infinity = 10000;

    animationSpeed = 0;
    animationBegin = 0;
    animationEnd = 0;
    animationCurrentFrame = 0;
    animationLoop = false;
    animate = false;

    m_rigidBody = NULL;
    meshBuf = NULL;
    m_collisionShape = NULL;
    m_motionState = NULL;
    showDebugPhysic = false;
    scalePhys =  irr::core::vector3df(1, 1, 1);
    typeBody = None;
    mass = 0;
    impulse = irr::core::vector3df(0, 0, 0);
    posImpulse = irr::core::vector3df(0, 0, 0);
}

Mesh::~Mesh() {
    std::vector<Material3D*>::const_iterator it, end = material3ds.end();
    for (it = material3ds.begin(); it != end; ++it) {
        delete (*it);
    }
    material3ds.clear();

    if (m_rigidBody) {
        engine->getPhysicsEngine()->getWorld()->removeRigidBody(m_rigidBody);
        delete m_motionState;
        delete m_collisionShape;
        delete m_rigidBody;
        m_rigidBody = NULL;
    }
}

bool Mesh::init(QString mesh) {
    irr::core::vector3df pos = graphicsEngine->getCamera()->getTarget();
    pos.normalize();
    pos *= 5;
    pos += graphicsEngine->getCamera()->getPosition();

    this->mesh = mesh;

    meshBuf = graphicsEngine->getSceneManager()->getMesh(
                (engine->getDirMedias() + mesh).toUtf8().data());
    if (meshBuf) {
        meshBuf->setHardwareMappingHint(irr::scene::EHM_STATIC);
        node = graphicsEngine->getSceneManager()->addAnimatedMeshSceneNode(
                    meshBuf, 0, -1, pos);
        return true;
    }

    return false;
}

void Mesh::setSelected(bool value) {
    Element::setSelected(value);

    if (node != NULL) {
        node->setDebugDataVisible(value);
    }

    QTabWidget* tab = edit->tabWidget;
    if (value) {
        int index = tab->addTab(edit->Mesh, "Mesh");
        if (edit->getCurrentTab() == "Mesh")
            tab->setCurrentIndex(index);

        index = tab->addTab(edit->Textures, "Textures");
        if (edit->getCurrentTab() == "Textures")
            tab->setCurrentIndex(index);

        index = tab->addTab(edit->Physique, "Physique");
        if (edit->getCurrentTab() == "Physique")
            tab->setCurrentIndex(index);

        updateElementEditor();

        connect(edit->addMat, &QPushButton::clicked,
                this, &Mesh::addMaterial);

        connect(edit->removeMats, &QPushButton::clicked,
                this, &Mesh::restoreMaterials);

        connect(edit->animation, &QGroupBox::clicked,
                this, &Mesh::activeAnimation);

        connect(edit->speed, static_cast<void (QSpinBox::*)(int)>
                                        (&QSpinBox::valueChanged),
                this, &Mesh::setAnimationSpeed);

        connect(edit->frameLoopBegin, static_cast<void (QSpinBox::*)(int)>
                                                 (&QSpinBox::valueChanged),
                this, &Mesh::setAnimationBegin);

        connect(edit->frameLoopEnd, static_cast<void (QSpinBox::*)(int)>
                                               (&QSpinBox::valueChanged),
                this, &Mesh::setAnimationEnd);

        connect(edit->currentFrame, static_cast<void (QSpinBox::*)(int)>
                                               (&QSpinBox::valueChanged),
                this, &Mesh::setAnimationCurrentFrame);

        connect(edit->loopMode, &QPushButton::clicked,
                this, &Mesh::setAnimationLoop);

        connect(edit->shadowBox, &QGroupBox::clicked,
                this, &Mesh::setShadow);

        connect(edit->infinity, static_cast<void (QDoubleSpinBox::*)(double)>
                                           (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setShadow);

        connect(edit->zfailmethod, &QPushButton::clicked,
                this, &Mesh::setShadow);

        connect(edit->physicBox, &QGroupBox::clicked,
                this, &Mesh::setPhysique);

        connect(edit->showDebugCheckBox, &QCheckBox::clicked,
                this, &Mesh::setDebugPhysic);

        connect(edit->scaleX_Phy, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setScalePhysX);

        connect(edit->scaleY_Phy, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setScalePhysY);

        connect(edit->scaleZ_Phy, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setScalePhysZ);

        connect(edit->physicTypeBox,
                static_cast<void (QComboBox::*)(const QString &)>
                (&QComboBox::activated),
                this, &Mesh::setTypeBody);

        connect(edit->materialBox, &QComboBox::currentTextChanged,
                this, &Mesh::setMaterialPhys);

        connect(edit->masseBox, static_cast<void (QDoubleSpinBox::*)(double)>
                                           (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setMasse);

        connect(edit->impulseX, static_cast<void (QDoubleSpinBox::*)(double)>
                                           (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setImpulseX);

        connect(edit->impulseY, static_cast<void (QDoubleSpinBox::*)(double)>
                                           (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setImpulseY);

        connect(edit->impulseZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                           (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setImpulseZ);

        connect(edit->posImpulseX, static_cast<void (QDoubleSpinBox::*)(double)>
                                              (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setPosImpulseX);

        connect(edit->posImpulseY, static_cast<void (QDoubleSpinBox::*)(double)>
                                              (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setPosImpulseY);

        connect(edit->posImpulseZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                              (&QDoubleSpinBox::valueChanged),
                this, &Mesh::setPosImpulseZ);
    } else {
        edit->addMat->disconnect();
        edit->removeMats->disconnect();

        edit->animation->disconnect();
        edit->speed->disconnect();
        edit->frameLoopBegin->disconnect();
        edit->frameLoopEnd->disconnect();
        edit->currentFrame->disconnect();
        edit->loopMode->disconnect();

        edit->shadowBox->disconnect();
        edit->infinity->disconnect();
        edit->zfailmethod->disconnect();

        edit->showDebugCheckBox->disconnect();

        edit->physicBox->disconnect();
        edit->showDebugCheckBox->disconnect();
        edit->scaleX_Phy->disconnect();
        edit->scaleY_Phy->disconnect();
        edit->scaleZ_Phy->disconnect();
        edit->physicTypeBox->disconnect();
        edit->materialBox->disconnect();
        edit->masseBox->disconnect();
        edit->impulseX->disconnect();
        edit->impulseY->disconnect();
        edit->impulseZ->disconnect();
        edit->posImpulseX->disconnect();
        edit->posImpulseY->disconnect();
        edit->posImpulseZ->disconnect();

        tab->removeTab(tab->indexOf(edit->Mesh));
        tab->removeTab(tab->indexOf(edit->Textures));
        tab->removeTab(tab->indexOf(edit->Physique));
    }

    if (material3ds.size() > 0) {
        edit->material3DTab->show();
        edit->removeMats->show();
        edit->addMat->hide();
    } else {
        edit->material3DTab->hide();
        edit->removeMats->hide();
        edit->addMat->show();
    }
}

void Mesh::updateElementEditor() {
    while (edit->material3DTab->count() > 0) {
        edit->material3DTab->removeTab(0);
    }

    if (isSelected) {
        // Texture
        {
            std::vector<Material3D*>::const_iterator it,
                    end = material3ds.end();
            int i = 0;
            for (it = material3ds.begin(); it != end; ++it) {
                edit->material3DTab->addTab(
                            *it, "Texture " + QString::number(++i));
                (*it)->updateElementEditor();
            }
        }

        // Shadow
        if (shadowNode != NULL) {
            edit->shadowBox->setChecked(true);
            edit->infinity->setValue(infinity);
            edit->zfailmethod->setChecked(zfailmethod);
        } else {
            edit->shadowBox->setChecked(false);
        }

        // Animation
        edit->animation->setChecked(animate);
        edit->speed->setValue(animationSpeed);
        edit->frameLoopBegin->setValue(animationBegin);
        edit->frameLoopEnd->setValue(animationEnd);
        edit->currentFrame->setValue(animationCurrentFrame);
        edit->loopMode->setChecked(animationLoop);

        // Physic
        edit->physicBox->setChecked(m_rigidBody != NULL);
        edit->showDebugCheckBox->setChecked(showDebugPhysic);
        edit->scaleX_Phy->setValue(scalePhys.X);
        edit->scaleY_Phy->setValue(scalePhys.Y);
        edit->scaleZ_Phy->setValue(scalePhys.Z);
        QString type = Convert::bodyString(typeBody);
        edit->physicTypeBox->setCurrentIndex(
                    edit->physicTypeBox->findText(type));
        edit->masseBox->setValue(mass);
        edit->impulseX->setValue(impulse.X);
        edit->impulseY->setValue(impulse.Y);
        edit->impulseZ->setValue(impulse.Z);
        edit->posImpulseX->setValue(posImpulse.X);
        edit->posImpulseY->setValue(posImpulse.Y);
        edit->posImpulseZ->setValue(posImpulse.Z);

        // Material Physic
        {
            edit->materialBox->clear();
            std::set<QString> nameMats =
                    engine->getMaterialPhysWidget()->getNameMats();
            std::set<QString>::const_iterator it, end = nameMats.end();
            for (it = nameMats.begin(); it != end; ++it) {
                edit->materialBox->addItem(*it);
            }
            materialPhy = edit->materialBox->currentText();
        }
    }
}

bool Mesh::readXML(tinyxml2::XMLElement* elem) {
    bool ret = false;

    irr::core::vector3df scale(1, 1, 1);
    irr::core::vector3df pos(0, 0, 0);
    irr::core::vector3df rot(0, 0, 0);

    if (elem->Attribute("scaleX") && elem->Attribute("scaleY")
            && elem->Attribute("scaleZ")) {
        scale.X = elem->FloatAttribute("scaleX");
        scale.Y = elem->FloatAttribute("scaleY");
        scale.Z = elem->FloatAttribute("scaleZ");
    }

    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
    }

    if (elem->Attribute("rotX") && elem->Attribute("rotY")
            && elem->Attribute("rotZ")) {
        rot.X = elem->FloatAttribute("rotX");
        rot.Y = elem->FloatAttribute("rotY");
        rot.Z = elem->FloatAttribute("rotZ");
    }

    if (elem->Attribute("mesh")) {
        mesh = elem->Attribute("mesh");
    }

    meshBuf = graphicsEngine->getSceneManager()->getMesh(
                (engine->getDirMedias() +  mesh).toUtf8().data());
    if (meshBuf) {
        node = graphicsEngine->getSceneManager()->addAnimatedMeshSceneNode(
                    meshBuf);

        if (node) {
            node->setScale(scale);
            node->setPosition(pos);
            node->setRotation(rot);

            ret = true;
        }
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (ret) {
            if (QString(elem->Value()) == "Material3D") {
                int num = elem->IntAttribute("num");
                if (num == 0) {
                    addMaterial();
                }
                material3ds.at(num)->readXML(elem);
            } else if (QString(elem->Value()) == "Animation") {
                animation(elem);
            } else if (QString(elem->Value()) == "Physic") {
                physic(elem);
            } else if (QString(elem->Value()) == "Shadow") {
                shadow(elem);
            } else if (QString(elem->Value()) == "Script") {
                scripts->readXML(elem);
            }
        }

        elem = elem->NextSiblingElement();
    }

    return ret;
}

btConvexHullShape* Mesh::createConvexHullShape(
        irr::scene::IMesh *mesh, const irr::core::vector3df& _scaling) {
    btConvexHullShape *convexHullShape = new btConvexHullShape();
    // Get mesh buffers and copy vertices
    for (irr::u32 i = 0; i < mesh->getMeshBufferCount(); ++i) {
        irr::scene::IMeshBuffer* currentMB = mesh->getMeshBuffer(i);
        if (currentMB->getVertexType() == irr::video::EVT_STANDARD) {
            irr::video::S3DVertex *tmpVertices =
                    static_cast<irr::video::S3DVertex*>
                    (currentMB->getVertices());
            // Add point to the convex hull shape
            for (irr::u32 j = 0; j < currentMB->getVertexCount(); ++j) {
                convexHullShape->addPoint(
                            btVector3(tmpVertices[j].Pos.X * _scaling.X,
                                      tmpVertices[j].Pos.Y * _scaling.Y,
                                      tmpVertices[j].Pos.Z * _scaling.Z));
            }
        } else if (currentMB->getVertexType() == irr::video::EVT_2TCOORDS) {
            irr::video::S3DVertex2TCoords *tmpVertices =
                    static_cast<irr::video::S3DVertex2TCoords*>
                    (currentMB->getVertices());
            // Add point to the convex hull shape
            for (irr::u32 i = 0; i < currentMB->getVertexCount(); ++i) {
                convexHullShape->addPoint(
                            btVector3(tmpVertices[i].Pos.X * _scaling.X,
                                      tmpVertices[i].Pos.Y * _scaling.Y,
                                      tmpVertices[i].Pos.Z * _scaling.Z));
            }
        }
    }
    return convexHullShape;
}

void Mesh::physic(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("type")) {
        typeBody = Convert::stringBody(elem->Attribute("type"));
    }

    if (elem->Attribute("sizeX") && elem->Attribute("sizeY")
            && elem->Attribute("sizeZ")) {
        scalePhys.X = elem->FloatAttribute("sizeX");
        scalePhys.Y = elem->FloatAttribute("sizeY");
        scalePhys.Z = elem->FloatAttribute("sizeZ");
    } else {
        scalePhys = node->getScale();
    }

    bool ret = loadBody();

    if (ret) {
        if (elem->Attribute("material")) {
            materialPhy = elem->Attribute("material");
        }

        if (elem->Attribute("masse")) {
            mass = elem->FloatAttribute("masse");
        }

        if (elem->Attribute("impulseX")
                && elem->Attribute("impulseY")
                && elem->Attribute("impulseZ")
                && elem->Attribute("posImpulseX")
                && elem->Attribute("posImpulseY")
                && elem->Attribute("posImpulseZ")) {
            irr::core::vector3df pointDeltaVeloc;
            pointDeltaVeloc.X = elem->FloatAttribute("impulseX");
            pointDeltaVeloc.Y = elem->FloatAttribute("impulseY");
            pointDeltaVeloc.Z = elem->FloatAttribute("impulseZ");

            irr::core::vector3df pointPosit;
            pointPosit.X = elem->FloatAttribute("posImpulseX");
            pointPosit.Y = elem->FloatAttribute("posImpulseY");
            pointPosit.Z = elem->FloatAttribute("posImpulseZ");
        }
    }
}

void Mesh::animation(tinyxml2::XMLElement* elem) {
    animate = true;

    if (elem->Attribute("speed")) {
        animationSpeed = elem->IntAttribute("speed");
        static_cast<irr::scene::IAnimatedMeshSceneNode*>
                (node)->setAnimationSpeed(animationSpeed);
    }

    if (elem->Attribute("frameLoopBegin") && elem->Attribute("frameLoopEnd")) {
        animationBegin = elem->IntAttribute("frameLoopBegin");
        animationEnd = elem->IntAttribute("frameLoopEnd");
        static_cast<irr::scene::IAnimatedMeshSceneNode*>
                (node)->setFrameLoop(animationBegin, animationEnd);
    }

    if (elem->Attribute("currentFrame")) {
        animationCurrentFrame = elem->IntAttribute("currentFrame");
        static_cast<irr::scene::IAnimatedMeshSceneNode*>
                (node)->setCurrentFrame(animationCurrentFrame);
    }

    if (elem->Attribute("loopMode")) {
        animationLoop = elem->BoolAttribute("loopMode");
        static_cast<irr::scene::IAnimatedMeshSceneNode*>
                (node)->setLoopMode(animationLoop);
    }
}

void Mesh::shadow(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("zfailmethod")) {
        zfailmethod = elem->BoolAttribute("zfailmethod");
    }
    if (elem->Attribute("infinity")) {
        infinity = elem->FloatAttribute("infinity");
    }
    setShadow(true);
}

bool Mesh::writeXML(tinyxml2::XMLDocument* xmlDoc,
                    tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Mesh");
    element->SetAttribute("name", name.toUtf8().data());

    element->SetAttribute("scaleX", node->getScale().X);
    element->SetAttribute("scaleY", node->getScale().Y);
    element->SetAttribute("scaleZ", node->getScale().Z);

    element->SetAttribute("posX", node->getPosition().X);
    element->SetAttribute("posY", node->getPosition().Y);
    element->SetAttribute("posZ", node->getPosition().Z);

    element->SetAttribute("rotX", node->getRotation().X);
    element->SetAttribute("rotY", node->getRotation().Y);
    element->SetAttribute("rotZ", node->getRotation().Z);

    element->SetAttribute("mesh", mesh.toUtf8().data());

    writePhysic(xmlDoc, element);
    writeAnimation(xmlDoc, element);
    writeShadow(xmlDoc, element);

    irr::u32 num = material3ds.size();
    for (irr::u32 i = 0; i < num; ++i) {
        Material3D* mat = material3ds.at(i);
        mat->writeXML(xmlDoc, element);
    }

    scripts->writeXML(xmlDoc, element);

    elem->LinkEndChild(element);

    return true;
}

void Mesh::writePhysic(tinyxml2::XMLDocument* xmlDoc,
                       tinyxml2::XMLElement* elem) {
    if (m_rigidBody) {
        tinyxml2::XMLElement* element = xmlDoc->NewElement("Physic");
        if (mass != 0) {
            element->SetAttribute("masse", mass);
        }

        element->SetAttribute("type",
                              Convert::bodyString(typeBody).toUtf8().data());
        element->SetAttribute("sizeX", scalePhys.X);
        element->SetAttribute("sizeY", scalePhys.Y);
        element->SetAttribute("sizeZ", scalePhys.Z);

        if (materialPhy != "") {
            element->SetAttribute("material", materialPhy.toUtf8().data());
        }

        if (impulse.X != 0 || impulse.Y != 0 || impulse.Z != 0) {
            element->SetAttribute("impulseX", impulse.X);
            element->SetAttribute("impulseY", impulse.Y);
            element->SetAttribute("impulseZ", impulse.Z);
            element->SetAttribute("posImpulseX", posImpulse.X);
            element->SetAttribute("posImpulseY", posImpulse.Y);
            element->SetAttribute("posImpulseZ", posImpulse.Z);
        }

        elem->LinkEndChild(element);
    }
}

void Mesh::writeAnimation(tinyxml2::XMLDocument* xmlDoc,
                          tinyxml2::XMLElement* elem) {
    if (animate) {
        tinyxml2::XMLElement* element = xmlDoc->NewElement("Animation");
        element->SetAttribute("speed", animationSpeed);
        element->SetAttribute("frameLoopBegin", animationBegin);
        element->SetAttribute("frameLoopEnd", animationEnd);
        element->SetAttribute("currentFrame", animationCurrentFrame);
        element->SetAttribute("loopMode", animationLoop);
        elem->LinkEndChild(element);
    }
}

void Mesh::writeShadow(tinyxml2::XMLDocument* xmlDoc,
                       tinyxml2::XMLElement* elem) {
    if (shadowNode != NULL) {
        tinyxml2::XMLElement* element = xmlDoc->NewElement("Shadow");
        element->SetAttribute("infinity", infinity);
        element->SetAttribute("zfailmethod", zfailmethod);
        elem->LinkEndChild(element);
    }
}

void Mesh::setPosX(double value) {
    Element::setPosX(value);
    updatePhysicBody();
}

void  Mesh::setPosY(double value) {
    Element::setPosY(value);
    updatePhysicBody();
}

void  Mesh::setPosZ(double value) {
    Element::setPosZ(value);
    updatePhysicBody();
}

void  Mesh::setRotX(double value) {
    Element::setRotX(value);
    updatePhysicBody();
}

void  Mesh::setRotY(double value) {
    Element::setRotY(value);
    updatePhysicBody();
}

void Mesh::setRotZ(double value) {
    Element::setRotZ(value);
    updatePhysicBody();
}

void Mesh::setScaleX(double value) {
    Element::setScaleX(value);
}

void Mesh::setScaleY(double value) {
    Element::setScaleY(value);
}

void Mesh::setScaleZ(double value) {
    Element::setScaleZ(value);
}

void Mesh::addMaterial() {
    edit->material3DTab->show();
    edit->removeMats->show();
    edit->addMat->hide();
    for (irr::u32 i = 0; i < node->getMaterialCount(); ++i) {
        Material3D* mat2 = new Material3D(engine, &(node->getMaterial(i)), i);
        material3ds.push_back(mat2);
    }
    updateElementEditor();
}

void Mesh::restoreMaterials() {
    edit->material3DTab->hide();
    edit->removeMats->hide();
    edit->addMat->show();
    irr::u32 num = material3ds.size();
    for (irr::u32 i = 0; i < num; ++i) {
        Material3D* mat = material3ds.at(i);
        node->getMaterial(i) = mat->getSaveMaterial();
        delete mat;
    }
    material3ds.clear();
    updateElementEditor();
}

void Mesh::activeAnimation(bool value) {
    animate = value;
}

void Mesh::setAnimationSpeed(int value) {
    static_cast<irr::scene::IAnimatedMeshSceneNode*>
            (node)->setAnimationSpeed(value);
    animationSpeed = value;
}

void Mesh::setAnimationBegin(int value) {
    static_cast<irr::scene::IAnimatedMeshSceneNode*>
            (node)->setFrameLoop(value, animationEnd);
    animationBegin = value;
}

void Mesh::setAnimationEnd(int value) {
    static_cast<irr::scene::IAnimatedMeshSceneNode*>
            (node)->setFrameLoop(animationBegin, value);
    animationEnd = value;
}

void Mesh::setAnimationCurrentFrame(int value) {
    static_cast<irr::scene::IAnimatedMeshSceneNode*>
            (node)->setCurrentFrame(value);
    animationCurrentFrame = value;
}

void Mesh::setAnimationLoop(bool value) {
    static_cast<irr::scene::IAnimatedMeshSceneNode*>
            (node)->setLoopMode(value);
    animationLoop = value;
}

void Mesh::setShadow(bool value) {
    if (shadowNode != NULL) {
        shadowNode->remove();
        shadowNode = NULL;
    }

    if (value) {
        shadowNode =static_cast<irr::scene::IAnimatedMeshSceneNode*>
                (node)->addShadowVolumeSceneNode(
                    NULL, -1, zfailmethod, infinity);
    }
}

void Mesh::setPhysique(bool value) {
    if (value) {
        loadBody();
    } else {
        typeBody = None;
        loadBody();
    }
}

bool Mesh::loadBody() {
    if (m_rigidBody) {
        engine->getPhysicsEngine()->getWorld()->removeRigidBody(m_rigidBody);
        delete m_motionState;
        delete m_collisionShape;
        delete m_rigidBody;
        m_rigidBody = NULL;
    }

    switch (typeBody) {
    case Box: {
        m_collisionShape = new btBoxShape(
                    btVector3(scalePhys.X / 2.0,
                              scalePhys.Y / 2.0,
                              scalePhys.Z / 2.0));
        break;
    }
    case Sphere: {
        m_collisionShape = new btSphereShape(scalePhys.X);
        break;
    }
    case Cone: {
        m_collisionShape = new btConeShape(scalePhys.X, scalePhys.Y);
        break;
    }
    case Capsule: {
        m_collisionShape = new btCapsuleShape(scalePhys.X, scalePhys.Y);
        break;
    }
    case Cylinder: {
        m_collisionShape = new btCylinderShape(
                    btVector3(scalePhys.X / 2.0,
                              scalePhys.Y / 2.0,
                              scalePhys.Z / 2.0));
        break;
    }
    case ConvexHull: {
        m_collisionShape = createConvexHullShape(meshBuf, scalePhys);
        break;
    }
    default:
        return false;
    }

    btTransform trans;
    trans.setIdentity();
    trans.setOrigin(btVector3(
                        node->getPosition().X,
                        node->getPosition().Y,
                        node->getPosition().Z));

    btQuaternion btq;
    Convert::EulerToQuaternion(btVector3(
                                   node->getRotation().X * 0.0174532925f,
                                   node->getRotation().Y * 0.0174532925f,
                                   node->getRotation().Z * 0.0174532925f),
                               btq);

    trans.setRotation(btq);

    m_motionState = new btDefaultMotionState(trans);
    btVector3 inertia(0, 0, 0);
    m_collisionShape->calculateLocalInertia(0, inertia);
    btRigidBody::btRigidBodyConstructionInfo rigidBodyInfo(
                0, m_motionState, m_collisionShape, inertia);
    m_rigidBody = new btRigidBody(rigidBodyInfo);
    m_collisionShape->setUserPointer(this);

    engine->getPhysicsEngine()->getWorld()->addRigidBody(m_rigidBody);

    if (isSelected) {
        setDebugPhysic(edit->showDebugCheckBox->isChecked());
    } else {
        setDebugPhysic(false);
    }

    return true;
}

bool Mesh::setTypeBody(QString type) {
    typeBody = Convert::stringBody(type);
    return loadBody();
}

void Mesh::setScalePhysX(double value) {
    scalePhys.X = value;
    loadBody();
}

void Mesh::setScalePhysY(double value) {
    scalePhys.Y = value;
    loadBody();
}

void Mesh::setScalePhysZ(double value) {
    scalePhys.Z = value;
    loadBody();
}

void Mesh::setMaterialPhys(QString value) {
    materialPhy = value;
}

void Mesh::setMasse(double value) {
    mass = value;
}

void Mesh::setImpulseX(double value) {
    impulse.X = value;
}

void Mesh::setImpulseY(double value) {
    impulse.Y = value;
}

void Mesh::setImpulseZ(double value) {
    impulse.Z = value;
}

void Mesh::setPosImpulseX(double value) {
    posImpulse.X = value;
}

void Mesh::setPosImpulseY(double value) {
    posImpulse.Y = value;
}

void Mesh::setPosImpulseZ(double value) {
    posImpulse.Z = value;
}

void Mesh::setDebugPhysic(bool value) {
    showDebugPhysic = value;
    if (m_rigidBody) {
        if (value) {
            m_rigidBody->setCollisionFlags(
                        m_rigidBody->getCollisionFlags()
                        & ~btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);
        } else {
            m_rigidBody->setCollisionFlags(
                        m_rigidBody->getCollisionFlags()
                        | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);
        }
    }
}

void Mesh::updatePhysicBody() {
    if (m_rigidBody) {
        loadBody();
    }
}

float Mesh::getMass() {
    return mass;
}

bool Mesh::isPhysic() {
    return m_rigidBody != NULL;
}

}  // namespace MarbleEditor
