/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Scripts.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_SCRIPTS_H_
#define MARBLEEDITOR_ELEMENTS_SCRIPTS_H_

#include <map>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Engine;
class ElementEditor;

/*! \class Scripts
 * \brief
 *
 *
 */
class Scripts : public QObject {
    Q_OBJECT

    public:
        explicit Scripts(Engine* engine);
        virtual ~Scripts();

        bool readXML(tinyxml2::XMLElement* elem);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);
        void setSelected(bool value);

        void loadAvailableScripts();

        QStringList getScripts();

    private slots:
        void addScript();
        void removeScript();
        void modifyScript();
        void detailScript();
        void updateModelDetails();
        void updateVariables();

    private:
        /*! \struct ParamScript
         * \brief
         *
         */
        struct ParamScript {
            QString type;
            QString value;
        };

        /*! \struct Script
         * \brief
         *
         */
        struct Script {
            std::map<QString, ParamScript> params;
        };

    private:
        Engine* engine;
        ElementEditor* edit;

        std::map<QString, Script> scripts;

    private:
        DISALLOW_COPY_AND_ASSIGN(Scripts);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_SCRIPTS_H_
