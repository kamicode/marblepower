/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Video2D.h"

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Scripts.h"

namespace MarbleEditor {

Video2D::Video2D(QString name, Engine* engine) : Element(name, engine) {
    filePath = "";
    scaleX = 1;
    scaleY = 1;
    posX = 0;
    posY = 0;
    repeatPlayback = false;
    startPlayback = true;
    preloadIntoRAM = true;

    node = graphicsEngine->getSceneManager()->addBillboardSceneNode(
                0, irr::core::dimension2df(5, 5));
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
    node->setMaterialTexture(
                0,
                graphicsEngine->getDriver()->getTexture("Gismos/video.png"));
}

Video2D::~Video2D() {
}

bool Video2D::init() {
    irr::core::vector3df pos = graphicsEngine->getCamera()->getTarget();
    pos.normalize();
    pos *= 5;
    pos += graphicsEngine->getCamera()->getPosition();

    node->setPosition(pos);

    return true;
}

void Video2D::setSelected(bool value) {
    Element::setSelected(value);

    if (node != NULL) {
        node->setDebugDataVisible(value);
    }

    QTabWidget* tab = edit->tabWidget;
    if (value) {
        int index = tab->addTab(edit->Video2D, "Video2D");
        if (edit->getCurrentTab() == "Video2D")
            tab->setCurrentIndex(index);

        edit->scaleBox->hide();
        edit->rotationBox->hide();

        updateElementEditor();

        connect(edit->filePath, static_cast<void (QComboBox::*)(int)>
                (&QComboBox::currentIndexChanged),
                this, &Video2D::setDetails);

        connect(edit->posX_2, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Video2D::setDetails);

        connect(edit->posY_2, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Video2D::setDetails);

        connect(edit->scaleX_2, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Video2D::setDetails);

        connect(edit->scaleY_2, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Video2D::setDetails);

        connect(edit->preloadIntoRAM, &QPushButton::clicked,
                this, &Video2D::setDetails);

        connect(edit->repeatPlayback, &QPushButton::clicked,
                this, &Video2D::setDetails);

        connect(edit->startPlayback, &QPushButton::clicked,
                this, &Video2D::setDetails);
    } else {
        edit->filePath->disconnect();
        edit->posX_2->disconnect();
        edit->posY_2->disconnect();
        edit->scaleX_2->disconnect();
        edit->scaleY_2->disconnect();
        edit->preloadIntoRAM->disconnect();
        edit->repeatPlayback->disconnect();
        edit->startPlayback->disconnect();

        edit->scaleBox->show();
        edit->rotationBox->show();

        edit->filePath->clear();
        tab->removeTab(tab->indexOf(edit->Video2D));
    }
}

void Video2D::updateElementEditor() {
    QDir d(engine->getDirMedias() + "Videos");
    QFileInfoList infoList = d.entryInfoList();
    for (int i = 0; i < infoList.count(); ++i) {
        QFileInfo f = infoList.at(i);
        if (f.isFile()) {
            edit->filePath->addItem("Videos/" + f.fileName());
        }
    }

    edit->filePath->setCurrentIndex(edit->filePath->findText(filePath));
    edit->posX_2->setValue(posX);
    edit->posY_2->setValue(posY);
    edit->scaleX_2->setValue(scaleX);
    edit->scaleY_2->setValue(scaleY);

    edit->preloadIntoRAM->setChecked(preloadIntoRAM);
    edit->repeatPlayback->setChecked(repeatPlayback);
    edit->startPlayback->setChecked(startPlayback);
}

bool Video2D::readXML(tinyxml2::XMLElement* elem) {
    filePath = elem->Attribute("file");
    scaleX = elem->FloatAttribute("scaleX");
    scaleY = elem->FloatAttribute("scaleY");
    posX = elem->FloatAttribute("posX");
    posY = elem->FloatAttribute("posY");
    preloadIntoRAM = elem->BoolAttribute("preloadIntoRAM");
    repeatPlayback = elem->BoolAttribute("repeatPlayback");
    startPlayback = elem->BoolAttribute("startPlayback");

    if (elem->Attribute("posX-Editor") && elem->Attribute("posY-Editor")
            && elem->Attribute("posZ-Editor")) {
        irr::core::vector3df pos;
        pos.X = elem->FloatAttribute("posX-Editor");
        pos.Y = elem->FloatAttribute("posY-Editor");
        pos.Z = elem->FloatAttribute("posZ-Editor");
        node->setPosition(pos);
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            scripts->readXML(elem);
        }
        elem = elem->NextSiblingElement();
    }

    return true;
}

bool Video2D::writeXML(tinyxml2::XMLDocument* xmlDoc,
                       tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Video2D");
    element->SetAttribute("name", name.toUtf8().data());
    element->SetAttribute("file", filePath.toUtf8().data());
    element->SetAttribute("posX", posX);
    element->SetAttribute("posY", posY);
    element->SetAttribute("scaleX", scaleX);
    element->SetAttribute("scaleY", scaleY);
    element->SetAttribute("preloadIntoRAM", preloadIntoRAM);
    element->SetAttribute("repeatPlayback", repeatPlayback);
    element->SetAttribute("startPlayback", startPlayback);

    element->SetAttribute("posX-Editor", node->getPosition().X);
    element->SetAttribute("posY-Editor", node->getPosition().Y);
    element->SetAttribute("posZ-Editor", node->getPosition().Z);

    scripts->writeXML(xmlDoc, element);

    elem->LinkEndChild(element);

    return true;
}

void Video2D::setDetails() {
    filePath = edit->filePath->currentText();
    scaleX = edit->scaleX_2->value();
    scaleY = edit->scaleY_2->value();
    posX = edit->posX_2->value();
    posY = edit->posY_2->value();
    preloadIntoRAM = edit->preloadIntoRAM->isChecked();
    repeatPlayback = edit->repeatPlayback->isChecked();
    startPlayback = edit->startPlayback->isChecked();
}

}  // namespace MarbleEditor
