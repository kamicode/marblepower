/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Terrain.h"

#include <set>

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Scripts.h"
#include "MarbleEditor/Materials/Material3D.h"
#include "MarbleEditor/Materials/MaterialPhys.h"

namespace MarbleEditor {

Terrain::Terrain(QString name, Engine* engine) : Element(name, engine) {
    material = NULL;

    maxLOD = 5;
    patchSize = "ETPS_17";
    smoothFactor = 0;
    addAlsoIfHeightmapEmpty = false;
    color = irr::video::SColor(255, 255, 255, 255);
    scaleText1 = 1;
    scaleText2 = 0;

    m_rigidBody = NULL;
    m_collisionShape = NULL;
    m_motionState = NULL;
    showDebugPhysic = false;
    scalePhys =  irr::core::vector3df(1, 1, 1);
}

Terrain::~Terrain() {
    if (material) {
        delete material;
        material = NULL;
    }

    if (m_rigidBody) {
        engine->getPhysicsEngine()->getWorld()->removeRigidBody(m_rigidBody);
        delete m_motionState;
        delete m_collisionShape;
        delete m_rigidBody;
        m_rigidBody = NULL;
    }
}

bool Terrain::init(QString heightMapFile) {
    this->heightMapFile = heightMapFile;

    irr::core::vector3df pos = graphicsEngine->getCamera()->getTarget();
    pos.normalize();
    pos *= 5;
    pos += graphicsEngine->getCamera()->getPosition();

    irr::core::vector3df rot = irr::core::vector3df(0, 0, 0);
    irr::core::vector3df scale = irr::core::vector3df(1, 1, 1);

    node = graphicsEngine->getSceneManager()->addTerrainSceneNode(
                (engine->getDirMedias() + heightMapFile).toUtf8().data(),
                0, -1, pos, rot, scale, color, maxLOD,
                Convert::stringTerrainPatch(patchSize), smoothFactor,
                addAlsoIfHeightmapEmpty);

    material = new Material3D(engine, &(node->getMaterial(0)), 0);

    return true;
}

void Terrain::setSelected(bool value) {
    Element::setSelected(value);

    if (node != NULL) {
        node->setDebugDataVisible(value);
    }

    QTabWidget* tab = edit->tabWidget;
    if (value) {
        int index = tab->addTab(edit->Terrain, "Terrain");
        if (edit->getCurrentTab() == "Terrain")
            tab->setCurrentIndex(index);

        index = tab->addTab(edit->Textures, "Textures");
        if (edit->getCurrentTab() == "Textures")
            tab->setCurrentIndex(index);

        index = tab->addTab(edit->Physique, "Physique");
        if (edit->getCurrentTab() == "Physique")
            tab->setCurrentIndex(index);

        edit->removeMats->hide();
        edit->addMat->hide();
        edit->physicTypeBox->setEnabled(false);
        edit->impulseBox->setEnabled(false);
        edit->masseBox->setEnabled(false);

        updateElementEditor();

        connect(edit->colorButton, &QPushButton::clicked,
                this, &Terrain::chooseColor);

        connect(edit->rebuilt, &QPushButton::clicked,
                this, &Terrain::rebuiltTerrain);

        connect(edit->physicBox, &QGroupBox::toggled,
                this, &Terrain::loadBody);

        connect(edit->showDebugCheckBox, &QPushButton::clicked,
                this, &Terrain::setDebugPhysic);

        connect(edit->scaleX_Phy,  static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Terrain::setScalePhysX);

        connect(edit->scaleY_Phy,  static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Terrain::setScalePhysY);

        connect(edit->scaleZ_Phy,  static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Terrain::setScalePhysZ);

        connect(edit->materialBox,  &QComboBox::currentTextChanged,
                this, &Terrain::setMaterialPhys);

        connect(edit->patchSize, &QComboBox::currentTextChanged,
                this, &Terrain::setPatchSize);

        connect(edit->maxLod, static_cast<void (QSpinBox::*)(int)>
                                             (&QSpinBox::valueChanged),
                this, &Terrain::setMaxLod);

        connect(edit->smoothFactor, static_cast<void (QSpinBox::*)(int)>
                                             (&QSpinBox::valueChanged),
                this, &Terrain::setSmoothFactor);

        connect(edit->addAlsoIfHeightmapEmpty, &QCheckBox::clicked,
                this, &Terrain::setAlsoIfHeightmapEmpty);

        connect(edit->scaleText1,  static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Terrain::setScaleText1);

        connect(edit->scaleText2,  static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Terrain::setScaleText2);
    } else {
        edit->colorButton->disconnect();
        edit->rebuilt->disconnect();

        edit->physicBox->disconnect();
        edit->showDebugCheckBox->disconnect();
        edit->materialBox->disconnect();
        edit->scaleX_Phy->disconnect();
        edit->scaleY_Phy->disconnect();
        edit->scaleZ_Phy->disconnect();

        edit->patchSize->disconnect();
        edit->maxLod->disconnect();
        edit->smoothFactor->disconnect();
        edit->addAlsoIfHeightmapEmpty->disconnect();
        edit->scaleText1->disconnect();
        edit->scaleText2->disconnect();

        edit->physicTypeBox->setEnabled(true);
        edit->impulseBox->setEnabled(true);
        edit->masseBox->setEnabled(true);
        edit->removeMats->show();
        edit->addMat->show();

        tab->removeTab(tab->indexOf(edit->Terrain));
        tab->removeTab(tab->indexOf(edit->Textures));
        tab->removeTab(tab->indexOf(edit->Physique));
    }
}

void Terrain::updateElementEditor() {
    while (edit->material3DTab->count() > 0) {
        edit->material3DTab->removeTab(0);
    }

    if (isSelected) {
        // Texture
        edit->material3DTab->addTab(material, "Texture");
        material->updateElementEditor();

        // Couleur Terrain
        QColor couleur(color.getRed(), color.getGreen(),
                       color.getBlue(), color.getAlpha());
        QPalette palette = edit->terrainColor->palette();
        palette.setColor(QPalette::Window, couleur);
        edit->terrainColor->setPalette(palette);

        // Terrain
        edit->patchSize->setCurrentIndex(edit->patchSize->findText(patchSize));
        edit->maxLod->setValue(maxLOD);
        edit->smoothFactor->setValue(smoothFactor);
        edit->addAlsoIfHeightmapEmpty->setChecked(addAlsoIfHeightmapEmpty);
        edit->scaleText1->setValue(scaleText1);
        edit->scaleText2->setValue(scaleText2);

        // Physic
        edit->physicBox->setChecked(m_rigidBody != NULL);
        edit->showDebugCheckBox->setChecked(showDebugPhysic);
        edit->scaleX_Phy->setValue(scalePhys.X);
        edit->scaleY_Phy->setValue(scalePhys.Y);
        edit->scaleZ_Phy->setValue(scalePhys.Z);

        // Material Physic
        edit->materialBox->clear();
        std::set<QString> nameMats =
                engine->getMaterialPhysWidget()->getNameMats();
        std::set<QString>::const_iterator it, end = nameMats.end();
        for (it = nameMats.begin(); it != end; ++it) {
            edit->materialBox->addItem(*it);
        }
        materialPhy = edit->materialBox->currentText();
    }
}

bool Terrain::readXML(tinyxml2::XMLElement* elem) {
    bool ret = true;
    heightMapFile = "";

    if (elem->Attribute("heightMapFile")) {
        heightMapFile = elem->Attribute("heightMapFile");
    }

    if (elem->Attribute("patchSize")) {
        patchSize = elem->Attribute("patchSize");
    }

    if (elem->Attribute("maxLOD")) {
        maxLOD = elem->IntAttribute("maxLOD");
    }

    if (elem->Attribute("smoothFactor")) {
        smoothFactor = elem->IntAttribute("smoothFactor");
    }

    if (elem->Attribute("addAlsoIfHeightmapEmpty")) {
        addAlsoIfHeightmapEmpty =
                elem->BoolAttribute("addAlsoIfHeightmapEmpty");
    }

    if (elem->Attribute("alpha") && elem->Attribute("red")
            && elem->Attribute("green") && elem->Attribute("blue")) {
        color.setRed(elem->IntAttribute("red"));
        color.setGreen(elem->IntAttribute("green"));
        color.setBlue(elem->IntAttribute("blue"));
        color.setAlpha(elem->IntAttribute("alpha"));
    }

    irr::core::vector3df scale(1, 1, 1);
    irr::core::vector3df pos(0, 0, 0);
    irr::core::vector3df rot(0, 0, 0);

    if (elem->Attribute("scaleX") && elem->Attribute("scaleY")
            && elem->Attribute("scaleZ")) {
        scale.X = elem->FloatAttribute("scaleX");
        scale.Y = elem->FloatAttribute("scaleY");
        scale.Z = elem->FloatAttribute("scaleZ");
    }

    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
    }

    if (elem->Attribute("rotX") && elem->Attribute("rotY")
            && elem->Attribute("rotZ")) {
        rot.X = elem->FloatAttribute("rotX");
        rot.Y = elem->FloatAttribute("rotY");
        rot.Z = elem->FloatAttribute("rotZ");
    }

    node = graphicsEngine->getSceneManager()->addTerrainSceneNode(
                (engine->getDirMedias() + heightMapFile).toUtf8().data(),
                0, -1, pos, rot, scale, color, maxLOD,
                Convert::stringTerrainPatch(patchSize), smoothFactor,
                addAlsoIfHeightmapEmpty);

    material = new Material3D(engine, &(node->getMaterial(0)), 0);

    elem = elem->FirstChildElement();
    while (elem) {
        if (ret) {
            if (QString(elem->Value()) == "TerrainMaterial3D") {
                material->readXML(elem->FirstChildElement());
                if (elem->Attribute("scaleTexture1")
                        && elem->Attribute("scaleTexture2")) {
                    scaleText1 = elem->FloatAttribute("scaleTexture1");
                    scaleText2 = elem->FloatAttribute("scaleTexture2");
                }
                static_cast<irr::scene::ITerrainSceneNode*>
                        (node)->scaleTexture(scaleText1, scaleText2);
            } else if (QString(elem->Value()) == "Script") {
                scripts->readXML(elem);
            } else if (QString(elem->Value()) == "Physic") {
                physic(elem);
            }
        }

        elem = elem->NextSiblingElement();
    }

    setSelected(false);

    return ret;
}

void Terrain::physic(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("sizeX") && elem->Attribute("sizeY")
            && elem->Attribute("sizeZ")) {
        scalePhys.X = elem->FloatAttribute("sizeX");
        scalePhys.Y = elem->FloatAttribute("sizeY");
        scalePhys.Z = elem->FloatAttribute("sizeZ");
    } else {
        scalePhys = node->getScale();
    }

    bool ret = loadBody();

    if (ret) {
        if (elem->Attribute("material")) {
            materialPhy = elem->Attribute("material");
        }
    }
    setDebugPhysic(false);
}

bool Terrain::writeXML(tinyxml2::XMLDocument* xmlDoc,
                       tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Terrain");
    element->SetAttribute("name", name.toUtf8().data());

    element->SetAttribute("heightMapFile", heightMapFile.toUtf8().data());

    element->SetAttribute("scaleX", node->getScale().X);
    element->SetAttribute("scaleY", node->getScale().Y);
    element->SetAttribute("scaleZ", node->getScale().Z);
    element->SetAttribute("posX", node->getPosition().X);
    element->SetAttribute("posY", node->getPosition().Y);
    element->SetAttribute("posZ", node->getPosition().Z);
    element->SetAttribute("rotX", node->getRotation().X);
    element->SetAttribute("rotY", node->getRotation().Y);
    element->SetAttribute("rotZ", node->getRotation().Z);

    element->SetAttribute("alpha", color.getAlpha());
    element->SetAttribute("red", color.getRed());
    element->SetAttribute("green", color.getGreen());
    element->SetAttribute("blue", color.getBlue());

    element->SetAttribute("maxLOD", maxLOD);
    element->SetAttribute("patchSize", patchSize.toUtf8().data());
    element->SetAttribute("smoothFactor", smoothFactor);
    element->SetAttribute("addAlsoIfHeightmapEmpty", addAlsoIfHeightmapEmpty);

    tinyxml2::XMLElement* el = xmlDoc->NewElement("TerrainMaterial3D");
    el->SetAttribute("scaleTexture1", scaleText1);
    el->SetAttribute("scaleTexture2", scaleText2);
    material->writeXML(xmlDoc, el);
    element->LinkEndChild(el);

    if (m_rigidBody) {
        el = xmlDoc->NewElement("Physic");
        el->SetAttribute("sizeX", scalePhys.X);
        el->SetAttribute("sizeY", scalePhys.Y);
        el->SetAttribute("sizeZ", scalePhys.Z);
        el->SetAttribute("material", materialPhy.toUtf8().data());
        element->LinkEndChild(el);
    }

    scripts->writeXML(xmlDoc, element);

    elem->LinkEndChild(element);

    return true;
}

void Terrain::rebuiltTerrain() {
    irr::video::SMaterial mat = node->getMaterial(0);

    irr::core::vector3df pos;
    pos.X = node->getPosition().X;
    pos.Y = node->getPosition().Y;
    pos.Z = node->getPosition().Z;

    irr::core::vector3df rot;
    rot.X = node->getRotation().X;
    rot.Y = node->getRotation().Y;
    rot.Z = node->getRotation().Z;

    irr::core::vector3df scale;
    scale.X = node->getScale().X;
    scale.Y = node->getScale().Y;
    scale.Z = node->getScale().Z;

    if (node != NULL) {
        node->remove();
        node = NULL;
    }

    node = graphicsEngine->getSceneManager()->addTerrainSceneNode(
                (engine->getDirMedias() + heightMapFile).toUtf8().data(),
                0, -1, pos, rot, scale, color, maxLOD,
                Convert::stringTerrainPatch(patchSize), smoothFactor,
                addAlsoIfHeightmapEmpty);

    node->getMaterial(0) = mat;
    material->setMaterial(&(node->getMaterial(0)));

    static_cast<irr::scene::ITerrainSceneNode*>
            (node)->scaleTexture(scaleText1, scaleText2);

    if (m_rigidBody != NULL) {
        loadBody();
    }
}

void Terrain::setPatchSize(QString patch) {
    if (edit->patchSize->findText(patch) != -1) {
        patchSize = patch;
    }
}

void Terrain::setMaxLod(int value) {
    maxLOD = value;
}

void Terrain::setSmoothFactor(int value) {
    smoothFactor = value;
}

void Terrain::setAlsoIfHeightmapEmpty(bool value) {
    addAlsoIfHeightmapEmpty = value;
}

void Terrain::setScaleText1(double value) {
    scaleText1 = value;
}

void Terrain::setScaleText2(double value) {
    scaleText2 = value;
}

void Terrain::chooseColor() {
    QColor couleur(color.getRed(), color.getGreen(),
                   color.getBlue(), color.getAlpha());
    couleur = QColorDialog::getColor(couleur, engine, tr("Couleur"),
                                     QColorDialog::ShowAlphaChannel);
    if (couleur.isValid()) {
        color.setAlpha(couleur.alpha());
        color.setBlue(couleur.blue());
        color.setRed(couleur.red());
        color.setGreen(couleur.green());

        QPalette palette = edit->terrainColor->palette();
        palette.setColor(QPalette::Window, couleur);
        edit->terrainColor->setPalette(palette);
    }
}

void Terrain::setPosX(double value) {
    Element::setPosX(value);
    loadBody();
}

void  Terrain::setPosY(double value) {
    Element::setPosY(value);
    loadBody();
}

void  Terrain::setPosZ(double value) {
    Element::setPosZ(value);
    loadBody();
}

void  Terrain::setRotX(double value) {
    Element::setRotX(value);
    loadBody();
}

void  Terrain::setRotY(double value) {
    Element::setRotY(value);
    loadBody();
}

void  Terrain::setRotZ(double value) {
    Element::setRotZ(value);
    loadBody();
}

void  Terrain::setScaleX(double value) {
    Element::setScaleX(value);
    loadBody();
}

void  Terrain::setScaleY(double value) {
    Element::setScaleY(value);
    loadBody();
}

void  Terrain::setScaleZ(double value) {
    Element::setScaleZ(value);
    loadBody();
}

void Terrain::setScalePhysX(double value) {
    scalePhys.X = value;
    loadBody();
}

void Terrain::setScalePhysY(double value) {
    scalePhys.Y = value;
    loadBody();
}

void Terrain::setScalePhysZ(double value) {
    scalePhys.Z = value;
    loadBody();
}

void Terrain::setMaterialPhys(QString value) {
    materialPhy = value;
}

bool Terrain::loadBody() {
    if (m_rigidBody) {
        engine->getPhysicsEngine()->getWorld()->removeRigidBody(m_rigidBody);
        delete m_motionState;
        delete m_collisionShape;
        delete m_rigidBody;
        m_rigidBody = NULL;
    }

    btVector3 vertices[3];
    irr::u32 j, k, index;
    btTriangleMesh* mTriMesh = new btTriangleMesh();

    irr::scene::CDynamicMeshBuffer* buffer = 0;

    irr::scene::IMeshBuffer* b = static_cast<irr::scene::ITerrainSceneNode*>
            (node)->getMesh()->getMeshBuffer(0);
    buffer = new irr::scene::CDynamicMeshBuffer(
                b->getVertexType(), b->getIndexType());
    static_cast<irr::scene::ITerrainSceneNode*>
                (node)->getMeshBufferForLOD(*buffer, maxLOD);

    const irr::u32 indexCount = buffer->getIndexCount();

    irr::video::S3DVertex2TCoords* mb_vertices =
            static_cast<irr::video::S3DVertex2TCoords*>
            (buffer->getVertexBuffer().getData());

    irr::u16* mb_indices = buffer->getIndices();

    for (j = 0; j < indexCount; j+=3) {
        for (k = 0; k < 3; ++k) {
            index = mb_indices[j+k];
            vertices[k] = btVector3(mb_vertices[index].Pos.X * scalePhys.X,
                                    mb_vertices[index].Pos.Y * scalePhys.Y,
                                    mb_vertices[index].Pos.Z * scalePhys.Z);
        }
        mTriMesh->addTriangle(vertices[0], vertices[1], vertices[2]);
    }

    buffer->drop();

    m_collisionShape = new btBvhTriangleMeshShape(mTriMesh, true);

    btTransform trans;
    trans.setIdentity();
    trans.setOrigin(btVector3(node->getPosition().X, node->getPosition().Y,
                              node->getPosition().Z));
    btQuaternion btq;
    Convert::EulerToQuaternion(btVector3(node->getRotation().X * 0.0174532925f,
                                         node->getRotation().Y * 0.0174532925f,
                                         node->getRotation().Z * 0.0174532925f),
                               btq);
    trans.setRotation(btq);

    m_motionState = new btDefaultMotionState(trans);
    btVector3 inertia(0, 0, 0);
    btRigidBody::btRigidBodyConstructionInfo rigidBodyInfo(0, m_motionState,
                                                           m_collisionShape,
                                                           inertia);
    m_rigidBody = new btRigidBody(rigidBodyInfo);
    m_collisionShape->setUserPointer(this);

    engine->getPhysicsEngine()->getWorld()->addRigidBody(m_rigidBody);

    if (isSelected) {
        setDebugPhysic(edit->showDebugCheckBox->isChecked());
    } else {
        setDebugPhysic(false);
    }

    return true;
}

void Terrain::setDebugPhysic(bool value) {
    if (m_rigidBody) {
        if (value) {
            m_rigidBody->setCollisionFlags(
                        m_rigidBody->getCollisionFlags()
                        & ~btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);
        } else {
            m_rigidBody->setCollisionFlags(
                        m_rigidBody->getCollisionFlags()
                        | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);
        }
    }
}

}  // namespace MarbleEditor
