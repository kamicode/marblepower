/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Sky.h"

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Engines/FileChooser.h"

namespace MarbleEditor {

Sky::Sky(Engine* engine, QWidget* parent): QDialog(parent) {
    setupUi(this);

    node = NULL;

    this->engine = engine;
    adjustSize();

    connect(domeCheckBox, &QGroupBox::toggled,
            this, &Sky::switchDome);

    connect(boxCheckBox, &QGroupBox::toggled,
            this, &Sky::switchBox);

    connect(domeButton, &QPushButton::clicked,
            this, &Sky::chooseDome);

    connect(bottomButton, &QPushButton::clicked,
            this, &Sky::chooseBottom);

    connect(topButton, &QPushButton::clicked,
            this, &Sky::chooseTop);

    connect(leftButton, &QPushButton::clicked,
            this, &Sky::chooseLeft);

    connect(rightButton, &QPushButton::clicked,
            this, &Sky::chooseRight);

    connect(frontButton, &QPushButton::clicked,
            this, &Sky::chooseFront);

    connect(backButton, &QPushButton::clicked,
            this, &Sky::chooseBack);
}

Sky::~Sky() {
    if (node) {
        node->remove();
        node = NULL;
    }
}

void Sky::unloadMap() {
    enableSky->setChecked(false);

    boxCheckBox->blockSignals(true);
    boxCheckBox->setChecked(true);
    boxCheckBox->blockSignals(false);

    domeCheckBox->blockSignals(true);
    domeCheckBox->setChecked(true);
    domeCheckBox->blockSignals(false);

    dome->setText("");
    domeSkin->setPixmap(QPixmap::fromImage(QImage()));

    bottom->setText("");
    bottomSkin->setPixmap(QPixmap::fromImage(QImage()));

    top->setText("");
    topSkin->setPixmap(QPixmap::fromImage(QImage()));

    left->setText("");
    leftSkin->setPixmap(QPixmap::fromImage(QImage()));

    right->setText("");
    rightSkin->setPixmap(QPixmap::fromImage(QImage()));

    front->setText("");
    frontSkin->setPixmap(QPixmap::fromImage(QImage()));

    back->setText("");
    backSkin->setPixmap(QPixmap::fromImage(QImage()));

    if (node) {
        node->remove();
        node = NULL;
    }
}

void Sky::switchDome(bool check) {
    boxCheckBox->blockSignals(true);
    boxCheckBox->setChecked(!check);
    boxCheckBox->blockSignals(false);
    updateIrrlicht();
}

void Sky::switchBox(bool check) {
    domeCheckBox->blockSignals(true);
    domeCheckBox->setChecked(!check);
    domeCheckBox->blockSignals(false);
    updateIrrlicht();
}

QString Sky::chooseFile() {
    FileChooser* dialog = new FileChooser(
                engine->getDirMedias() + "Textures/", true, this);
    dialog->exec();
    if (dialog->result() == QDialog::Accepted) {
        return "Textures/" + dialog->getSelectedRelativeFilePath();
    }
    return "";
}

void Sky::chooseDome() {
    QString file = chooseFile();
    changeDome(file);
}

void Sky::changeDome(QString file) {
    dome->setText(file);
    file.prepend(engine->getDirMedias());
    if (QFileInfo(file).isFile()) {
        QImage image(file);
        image.scaled(100, 100);
        domeSkin->setPixmap(QPixmap::fromImage(image));
    }
    updateIrrlicht();
}

void Sky::chooseBottom() {
    QString file = chooseFile();
    changeBottom(file);
}

void Sky::changeBottom(QString file) {
    bottom->setText(file);
    file.prepend(engine->getDirMedias());
    if (QFileInfo(file).isFile()) {
        QImage image(file);
        image.scaled(100, 100);
        bottomSkin->setPixmap(QPixmap::fromImage(image));
    }
    updateIrrlicht();
}

void Sky::chooseTop() {
    QString file = chooseFile();
    changeTop(file);
}

void Sky::changeTop(QString file) {
    top->setText(file);
    file.prepend(engine->getDirMedias());
    if (QFileInfo(file).isFile()) {
        QImage image(file);
        image.scaled(100, 100);
        topSkin->setPixmap(QPixmap::fromImage(image));
    }
    updateIrrlicht();
}

void Sky::chooseLeft() {
    QString file = chooseFile();
    changeLeft(file);
}

void Sky::changeLeft(QString file) {
    left->setText(file);
    file.prepend(engine->getDirMedias());
    if (QFileInfo(file).isFile()) {
        QImage image(file);
        image.scaled(100, 100);
        leftSkin->setPixmap(QPixmap::fromImage(image));
    }
    updateIrrlicht();
}

void Sky::chooseRight() {
    QString file = chooseFile();
    changeRight(file);
}

void Sky::changeRight(QString file) {
    right->setText(file);
    file.prepend(engine->getDirMedias());
    if (QFileInfo(file).isFile()) {
        QImage image(file);
        image.scaled(100, 100);
        rightSkin->setPixmap(QPixmap::fromImage(image));
    }
    updateIrrlicht();
}

void Sky::chooseFront() {
    QString file = chooseFile();
    changeFront(file);
}

void Sky::changeFront(QString file) {
    front->setText(file);
    file.prepend(engine->getDirMedias());
    if (QFileInfo(file).isFile()) {
        QImage image(file);
        image.scaled(100, 100);
        frontSkin->setPixmap(QPixmap::fromImage(image));
    }
    updateIrrlicht();
}

void Sky::chooseBack() {
    QString file = chooseFile();
    changeBack(file);
}

void Sky::changeBack(QString file) {
    back->setText(file);
    file.prepend(engine->getDirMedias());
    if (QFileInfo(file).isFile()) {
        QImage image(file);
        image.scaled(100, 100);
        backSkin->setPixmap(QPixmap::fromImage(image));
    }
    updateIrrlicht();
}

void Sky::updateIrrlicht() {
    irr::scene::ISceneManager* sceneManager =
            engine->getGraphicsEngine()->getSceneManager();
    irr::video::IVideoDriver* driver =
            engine->getGraphicsEngine()->getDriver();

    if (node) {
        node->remove();
        node = NULL;
    }

    if (enableSky->isChecked()) {
        if (domeCheckBox->isChecked()) {
            if (!dome->text().isEmpty()) {
                irr::io::path dome2 = (engine->getDirMedias()
                                       + dome->text()).toUtf8().data();
                driver->setTextureCreationFlag(
                            irr::video::ETCF_CREATE_MIP_MAPS, false);
                node = sceneManager->addSkyDomeSceneNode(
                            driver->getTexture(dome2));
                driver->setTextureCreationFlag(
                            irr::video::ETCF_CREATE_MIP_MAPS, true);
            }
        } else {
            if (!top->text().isEmpty() && !bottom->text().isEmpty() &&
                    !left->text().isEmpty() && !right->text().isEmpty() &&
                    !front->text().isEmpty() && !back->text().isEmpty()) {
                irr::io::path top2 = (engine->getDirMedias()
                                      + top->text()).toUtf8().data();
                irr::io::path bottom2 = (engine->getDirMedias()
                                         + bottom->text()).toUtf8().data();
                irr::io::path left2 = (engine->getDirMedias()
                                       + left->text()).toUtf8().data();
                irr::io::path right2 = (engine->getDirMedias()
                                        + right->text()).toUtf8().data();
                irr::io::path front2 = (engine->getDirMedias()
                                        + front->text()).toUtf8().data();
                irr::io::path back2 = (engine->getDirMedias()
                                       + back->text()).toUtf8().data();

                driver->setTextureCreationFlag(
                            irr::video::ETCF_CREATE_MIP_MAPS, false);
                node = sceneManager->addSkyBoxSceneNode(
                            driver->getTexture(top2),
                            driver->getTexture(bottom2),
                            driver->getTexture(left2),
                            driver->getTexture(right2),
                            driver->getTexture(front2),
                            driver->getTexture(back2));
                driver->setTextureCreationFlag(
                            irr::video::ETCF_CREATE_MIP_MAPS, true);
            }
        }
    }
}

bool Sky::readXML(tinyxml2::XMLElement* elem) {
    enableSky->setChecked(true);
    if (QString(elem->Value()) == "SkyDome") {
        domeCheckBox->setChecked(true);
        boxCheckBox->setChecked(false);
        changeDome(elem->Attribute("dome"));
    } else if (QString(elem->Value()) == "SkyBox") {
        boxCheckBox->setChecked(true);
        domeCheckBox->setChecked(false);
        changeTop(elem->Attribute("top"));
        changeBottom(elem->Attribute("bottom"));
        changeLeft(elem->Attribute("left"));
        changeRight(elem->Attribute("right"));
        changeFront(elem->Attribute("front"));
        changeBack(elem->Attribute("back"));
    }
    updateIrrlicht();
    return true;
}

bool Sky::writeXML(tinyxml2::XMLDocument* xmlDoc, tinyxml2::XMLElement* elem) {
    if (enableSky->isChecked()) {
        if (domeCheckBox->isChecked()) {
            if (!dome->text().isEmpty()) {
                tinyxml2::XMLElement* element = xmlDoc->NewElement("SkyDome");
                element->SetAttribute("dome", dome->text().toUtf8().data());
                elem->LinkEndChild(element);
            }
        } else {
            if (!top->text().isEmpty() && !bottom->text().isEmpty() &&
                    !left->text().isEmpty() && !right->text().isEmpty() &&
                    !front->text().isEmpty() && !back->text().isEmpty()) {
                tinyxml2::XMLElement* element = xmlDoc->NewElement("SkyBox");

                element->SetAttribute("top", top->text().toUtf8().data());
                element->SetAttribute("bottom", bottom->text().toUtf8().data());
                element->SetAttribute("left", left->text().toUtf8().data());
                element->SetAttribute("right", right->text().toUtf8().data());
                element->SetAttribute("front", front->text().toUtf8().data());
                element->SetAttribute("back", back->text().toUtf8().data());

                elem->LinkEndChild(element);
            }
        }
    }
    return true;
}

}  // namespace MarbleEditor
