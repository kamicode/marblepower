/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Video2D.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_VIDEO2D_H_
#define MARBLEEDITOR_ELEMENTS_VIDEO2D_H_

#include "MarbleEditor/Elements/Element.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

/*! \class Video2D
 * \brief
 *
 *
 */
class Video2D : public Element {
    Q_OBJECT

    public:
        explicit Video2D(QString name, Engine* engine);
        virtual ~Video2D();

        bool init();

        void updateElementEditor();

        void setSelected(bool value);

        bool readXML(tinyxml2::XMLElement* elem);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

    private slots:
        void setDetails();

    private:
        QString filePath;
        float scaleX;
        float scaleY;
        float posX;
        float posY;
        bool repeatPlayback;
        bool startPlayback;
        bool preloadIntoRAM;

    private:
        DISALLOW_COPY_AND_ASSIGN(Video2D);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_VIDEO2D_H_
