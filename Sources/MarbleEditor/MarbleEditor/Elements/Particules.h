/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Particules.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_PARTICULES_H_
#define MARBLEEDITOR_ELEMENTS_PARTICULES_H_

#include "MarbleEditor/Elements/Element.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Material3D;

/*! \class Particules
 * \brief
 *
 *
 */
class Particules : public Element {
    Q_OBJECT

    public:
        explicit Particules(QString name, Engine* engine);
        virtual ~Particules();

        bool init();

        void setSelected(bool value);

        bool readXML(tinyxml2::XMLElement* elem);
        void boxEmetteur(tinyxml2::XMLElement* elem);

        void updateElementEditor();

        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);
        void  writeBoxEmetteur(tinyxml2::XMLDocument* xmlDoc,
                               tinyxml2::XMLElement* elem);

    private slots:
        void maj();
        void chooseMinColorStart();
        void chooseMaxColorStart();

    private:
        Material3D* material;
        irr::core::aabbox3d<irr::f32> box;
        int lifeTimeMin, lifeTimeMax, maxAngleDegrees;

    private:
        DISALLOW_COPY_AND_ASSIGN(Particules);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_PARTICULES_H_
