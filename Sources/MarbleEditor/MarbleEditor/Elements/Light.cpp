/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Light.h"

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Scripts.h"

namespace MarbleEditor {

Light::Light(QString name, Engine* engine) : Element(name, engine) {
    lightNode = graphicsEngine->getSceneManager()->addLightSceneNode(0);
    light = lightNode->getLightData();

    node = graphicsEngine->getSceneManager()->addBillboardSceneNode(
                0, irr::core::dimension2d<irr::f32>(5, 5));
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
    node->setMaterialTexture(
                0,
                graphicsEngine->getDriver()->getTexture("Gismos/light.bmp"));
}

Light::~Light() {
    lightNode->remove();
}

bool Light::init() {
    irr::core::vector3df pos = graphicsEngine->getCamera()->getTarget();
    pos.normalize();
    pos *= 5;
    pos += graphicsEngine->getCamera()->getPosition();

    lightNode->setPosition(pos);
    node->setPosition(pos);

    return true;
}

void Light::setSelected(bool value) {
    Element::setSelected(value);

    if (node != NULL) {
        node->setDebugDataVisible(value);
    }

    QTabWidget* tab = edit->tabWidget;
    if (value) {
        int index = tab->addTab(edit->Lumiere, "Lumiere");
        if (edit->getCurrentTab() == "Lumiere")
            tab->setCurrentIndex(index);

        edit->scaleBox->hide();

        updateElementEditor();

        connect(edit->lightTypeBox, &QComboBox::currentTextChanged,
                this, &Light::setType);

        connect(edit->lightSshadow, &QPushButton::clicked,
                this, &Light::setShadow);

        connect(edit->constant, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Light::setConstant);

        connect(edit->lineaire, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Light::setLineaire);

        connect(edit->quadratique, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Light::setQuadratique);

        connect(edit->ambiantColorButton, &QPushButton::clicked,
                this, &Light::chooseAmbiantColor);

        connect(edit->specularColorButton, &QPushButton::clicked,
                this, &Light::chooseSpecularColor);

        connect(edit->diffuseColorButton, &QPushButton::clicked,
                this, &Light::chooseDiffuseColor);

        connect(edit->outer, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Light::setOuter);

        connect(edit->inner, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Light::setInner);

        connect(edit->falloff, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Light::setFalloff);
    } else {
        edit->lightTypeBox->disconnect();
        edit->lightSshadow->disconnect();
        edit->constant->disconnect();
        edit->lineaire->disconnect();
        edit->quadratique->disconnect();
        edit->ambiantColorButton->disconnect();
        edit->specularColorButton->disconnect();
        edit->diffuseColorButton->disconnect();
        edit->outer->disconnect();
        edit->inner->disconnect();
        edit->falloff->disconnect();

        edit->scaleBox->show();

        tab->removeTab(tab->indexOf(edit->Lumiere));
    }
}

void Light::updateElementEditor() {
    QString type = Convert::lightTypeString(lightNode->getLightType());
    edit->lightTypeBox->setCurrentIndex(edit->lightTypeBox->findText(type));
    edit->coneBox->setVisible(type == "ELT_SPOT");
    edit->lightSshadow->setChecked(lightNode->getCastShadow());

    edit->constant->setValue(light.Attenuation.X);
    edit->lineaire->setValue(light.Attenuation.Y);
    edit->quadratique->setValue(light.Attenuation.Z);

    QColor couleur;
    couleur.setAlpha(light.AmbientColor.toSColor().getAlpha());
    couleur.setRed(light.AmbientColor.toSColor().getRed());
    couleur.setGreen(light.AmbientColor.toSColor().getGreen());
    couleur.setBlue(light.AmbientColor.toSColor().getBlue());
    QPalette palette = edit->ambiantColor->palette();
    palette.setColor(QPalette::Window, couleur);
    edit->ambiantColor->setPalette(palette);

    couleur.setAlpha(light.SpecularColor.toSColor().getAlpha());
    couleur.setRed(light.SpecularColor.toSColor().getRed());
    couleur.setGreen(light.SpecularColor.toSColor().getGreen());
    couleur.setBlue(light.SpecularColor.toSColor().getBlue());
    palette = edit->specularColor->palette();
    palette.setColor(QPalette::Window, couleur);
    edit->specularColor->setPalette(palette);

    couleur.setAlpha(light.DiffuseColor.toSColor().getAlpha());
    couleur.setRed(light.DiffuseColor.toSColor().getRed());
    couleur.setGreen(light.DiffuseColor.toSColor().getGreen());
    couleur.setBlue(light.DiffuseColor.toSColor().getBlue());
    palette = edit->diffuseColor->palette();
    palette.setColor(QPalette::Window, couleur);
    edit->diffuseColor->setPalette(palette);
}

void Light::setPosX(double value) {
    Element::setPosX(value);
    lightNode->setPosition(node->getPosition());
    light = lightNode->getLightData();
    if (isSelected) {
        updateElementEditor();
    }
}

void  Light::setPosY(double value) {
    Element::setPosY(value);
    lightNode->setPosition(node->getPosition());
    light = lightNode->getLightData();
    if (isSelected) {
        updateElementEditor();
    }
}

void  Light::setPosZ(double value) {
    Element::setPosZ(value);
    lightNode->setPosition(node->getPosition());
    light = lightNode->getLightData();
    if (isSelected) {
        updateElementEditor();
    }
}

void  Light::setRotX(double value) {
    Element::setRotX(value);
    lightNode->setRotation(node->getRotation());
    light = lightNode->getLightData();
    if (isSelected) {
        updateElementEditor();
    }
}

void  Light::setRotY(double value) {
    Element::setRotY(value);
    lightNode->setRotation(node->getRotation());
    light = lightNode->getLightData();
    if (isSelected) {
        updateElementEditor();
    }
}

void  Light::setRotZ(double value) {
    Element::setRotZ(value);
    lightNode->setRotation(node->getRotation());
    light = lightNode->getLightData();
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::chooseAmbiantColor() {
    irr::video::SColor col = light.AmbientColor.toSColor();
    QColor couleur(col.getRed(), col.getGreen(),
                   col.getBlue(), col.getAlpha());
    couleur = QColorDialog::getColor(couleur, engine,
                                     tr("Couleur ambiante"),
                                     QColorDialog::ShowAlphaChannel);
    if (couleur.isValid()) {
        setAmbientColor(irr::video::SColor(couleur.alpha(), couleur.red(),
                                           couleur.green(), couleur.blue()));
    }
}

void Light::chooseSpecularColor() {
    irr::video::SColor col = light.SpecularColor.toSColor();
    QColor couleur(col.getRed(), col.getGreen(), col.getBlue(), col.getAlpha());
    couleur = QColorDialog::getColor(couleur, engine, tr("Couleur speculaire"),
                                     QColorDialog::ShowAlphaChannel);
    if (couleur.isValid()) {
        setSpecularColor(irr::video::SColor(couleur.alpha(), couleur.red(),
                                            couleur.green(), couleur.blue()));
    }
}

void Light::chooseDiffuseColor() {
    irr::video::SColor col = light.DiffuseColor.toSColor();
    QColor couleur(col.getRed(), col.getGreen(), col.getBlue(), col.getAlpha());
    couleur = QColorDialog::getColor(couleur, engine, tr("Couleur diffuse"),
                                     QColorDialog::ShowAlphaChannel);
    if (couleur.isValid()) {
        setDiffuseColor(irr::video::SColor(couleur.alpha(), couleur.red(),
                                           couleur.green(), couleur.blue()));
    }
}

void Light::setAmbientColor(irr::video::SColor ambiant) {
    light.AmbientColor = irr::video::SColorf(ambiant);
    lightNode->setLightData(light);
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setDiffuseColor(irr::video::SColor diffuse) {
    light.DiffuseColor = irr::video::SColorf(diffuse);
    lightNode->setLightData(light);
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setSpecularColor(irr::video::SColor specular) {
    light.SpecularColor = irr::video::SColorf(specular);
    lightNode->setLightData(light);
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setOuter(double value) {
    light.OuterCone = value;
    lightNode->setLightData(light);
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setInner(double value) {
    light.InnerCone = value;
    lightNode->setLightData(light);
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setFalloff(double value) {
    light.Falloff = value;
    lightNode->setLightData(light);
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setType(QString  value) {
    irr::video::E_LIGHT_TYPE type = Convert::stringLightType(value);
    lightNode->setLightType(type);
    light = lightNode->getLightData();
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setShadow(bool value) {
    lightNode->enableCastShadow(value);
    light = lightNode->getLightData();
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setConstant(double value) {
    light.Attenuation.X = value;
    lightNode->setLightData(light);
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setLineaire(double value) {
    light.Attenuation.Y = value;
    lightNode->setLightData(light);
    if (isSelected) {
        updateElementEditor();
    }
}

void Light::setQuadratique(double value) {
    light.Attenuation.Z = value;
    lightNode->setLightData(light);
    if (isSelected) {
        updateElementEditor();
    }
}

bool Light::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("type")) {
        lightNode->setLightType(
                    Convert::stringLightType(elem->Attribute("type")));
    }

    if (elem->Attribute("castShadow")) {
        lightNode->enableCastShadow(elem->BoolAttribute("castShadow"));
    }

    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        irr::core::vector3df pos;
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
        node->setPosition(pos);
        lightNode->setPosition(pos);
    }

    if (elem->Attribute("rotX") && elem->Attribute("rotY")
            && elem->Attribute("rotZ")) {
        irr::core::vector3df rot;
        rot.X = elem->FloatAttribute("rotX");
        rot.Y = elem->FloatAttribute("rotY");
        rot.Z = elem->FloatAttribute("rotZ");
        node->setRotation(rot);
        lightNode->setRotation(rot);
    }

    light = lightNode->getLightData();

    if (elem->Attribute("outerCone")) {
        light.OuterCone = elem->FloatAttribute("outerCone");
    }

    if (elem->Attribute("innerCone")) {
        light.InnerCone = elem->FloatAttribute("innerCone");
    }

    if (elem->Attribute("falloff")) {
        light.Falloff = elem->FloatAttribute("falloff");
    }

    if (elem->Attribute("ambiantR") && elem->Attribute("ambiantG")
            && elem->Attribute("ambiantB") && elem->Attribute("ambiantA")) {
        irr::video::SColor color(0, 0, 0, 0);
        color.setRed(elem->IntAttribute("ambiantR"));
        color.setGreen(elem->IntAttribute("ambiantG"));
        color.setBlue(elem->IntAttribute("ambiantB"));
        color.setAlpha(elem->IntAttribute("ambiantA"));
        light.AmbientColor = irr::video::SColorf(color);
    }

    if (elem->Attribute("diffuseR") && elem->Attribute("diffuseG")
            && elem->Attribute("diffuseB") && elem->Attribute("diffuseA")) {
        irr::video::SColor color(0, 0, 0, 0);
        color.setRed(elem->IntAttribute("diffuseR"));
        color.setGreen(elem->IntAttribute("diffuseG"));
        color.setBlue(elem->IntAttribute("diffuseB"));
        color.setAlpha(elem->IntAttribute("diffuseA"));
        light.DiffuseColor = irr::video::SColorf(color);
    }

    if (elem->Attribute("specularR") && elem->Attribute("specularG")
            && elem->Attribute("specularB") && elem->Attribute("specularA")) {
        irr::video::SColor color(0, 0, 0, 0);
        color.setRed(elem->IntAttribute("specularR"));
        color.setGreen(elem->IntAttribute("specularG"));
        color.setBlue(elem->IntAttribute("specularB"));
        color.setAlpha(elem->IntAttribute("specularA"));
         light.SpecularColor = irr::video::SColorf(color);
    }

    if (elem->Attribute("attenuationConst")
            && elem->Attribute("attenuationLin")
            && elem->Attribute("attenuationQuad")) {
        irr::core::vector3df attenuation;
        attenuation.X = elem->FloatAttribute("attenuationConst");
        attenuation.Y = elem->FloatAttribute("attenuationLin");
        attenuation.Z = elem->FloatAttribute("attenuationQuad");
        light.Attenuation = attenuation;
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            scripts->readXML(elem);
        }

        elem = elem->NextSiblingElement();
    }

    lightNode->setLightData(light);

    return true;
}

bool Light::writeXML(tinyxml2::XMLDocument* xmlDoc,
                     tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Light");
    element->SetAttribute("name", name.toUtf8().data());

    QString type = Convert::lightTypeString(lightNode->getLightType());
    element->SetAttribute("type", type.toUtf8().data());

    element->SetAttribute("posX", node->getPosition().X);
    element->SetAttribute("posY", node->getPosition().Y);
    element->SetAttribute("posZ", node->getPosition().Z);
    element->SetAttribute("rotX", node->getRotation().X);
    element->SetAttribute("rotY", node->getRotation().Y);
    element->SetAttribute("rotZ", node->getRotation().Z);
    element->SetAttribute("castShadow", lightNode->getCastShadow());

    element->SetAttribute("attenuationConst", light.Attenuation.X);
    element->SetAttribute("attenuationLin", light.Attenuation.Y);
    element->SetAttribute("attenuationQuad", light.Attenuation.Z);

    element->SetAttribute("ambiantA",
                          light.AmbientColor.toSColor().getAlpha());
    element->SetAttribute("ambiantR",
                          light.AmbientColor.toSColor().getRed());
    element->SetAttribute("ambiantG",
                          light.AmbientColor.toSColor().getGreen());
    element->SetAttribute("ambiantB",
                          light.AmbientColor.toSColor().getBlue());

    element->SetAttribute("diffuseA",
                          light.DiffuseColor.toSColor().getAlpha());
    element->SetAttribute("diffuseR",
                          light.DiffuseColor.toSColor().getRed());
    element->SetAttribute("diffuseG",
                          light.DiffuseColor.toSColor().getGreen());
    element->SetAttribute("diffuseB",
                          light.DiffuseColor.toSColor().getBlue());

    element->SetAttribute("specularA",
                          light.SpecularColor.toSColor().getAlpha());
    element->SetAttribute("specularR",
                          light.SpecularColor.toSColor().getRed());
    element->SetAttribute("specularG",
                          light.SpecularColor.toSColor().getGreen());
    element->SetAttribute("specularB",
                          light.SpecularColor.toSColor().getBlue());

    if (type == "ELT_SPOT") {
        element->SetAttribute("outerCone", light.OuterCone);
        element->SetAttribute("innerCone", light.InnerCone);
        element->SetAttribute("falloff", light.Falloff);
    }

    scripts->writeXML(xmlDoc, element);

    elem->LinkEndChild(element);
    return true;
}

}  // namespace MarbleEditor
