/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Scripts.h"

#include <map>

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Editors/ScriptEditor.h"

namespace MarbleEditor {

Scripts::Scripts(Engine* engine) : QObject(engine) {
    this->engine = engine;
    edit = engine->getElementEditor();

    scripts.clear();
}

Scripts::~Scripts() {
}

QStringList Scripts::getScripts() {
    QStringList ret;
    std::map<QString, Script>::const_iterator it, end = scripts.end();
    for (it = scripts.begin(); it != end; ++it) {
        ret.append(it->first);
    }
    return ret;
}

bool Scripts::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("script")) {
        QString name = elem->Attribute("script");
        Script script;
        elem = elem->FirstChildElement();
        while (elem) {
            if (QString(elem->Value()) == "Variable") {
                ParamScript param;
                param.type = elem->Attribute("type");
                param.value = elem->Attribute("value");
                script.params.insert(std::make_pair(
                                         elem->Attribute("name"), param));
            }

            elem = elem->NextSiblingElement();
        }

        scripts.insert(std::make_pair(name, script));
    }
    return true;
}

bool Scripts::writeXML(tinyxml2::XMLDocument* xmlDoc,
                       tinyxml2::XMLElement* elem) {
    std::map<QString, Script>::const_iterator it, end = scripts.end();
    for (it = scripts.begin(); it != end; ++it) {
        Script script = it->second;
        QString name = it->first;
        tinyxml2::XMLElement* element = xmlDoc->NewElement("Script");
        element->SetAttribute("script", name.toUtf8().data());

        std::map<QString, ParamScript>::const_iterator iterParam,
                endParam = script.params.end();
        for (iterParam = script.params.begin();
             iterParam != endParam; ++iterParam) {
            tinyxml2::XMLElement* elt = xmlDoc->NewElement("Variable");
            ParamScript param = iterParam->second;
            QString name = iterParam->first;
            elt->SetAttribute("name", name.toUtf8().data());
            elt->SetAttribute("type", param.type.toUtf8().data());
            elt->SetAttribute("value", param.value.toUtf8().data());
            element->LinkEndChild(elt);
        }
        elem->LinkEndChild(element);
    }
    return true;
}

void Scripts::loadAvailableScripts() {
    QDirIterator dirIterator(engine->getDirMedias() + "Scripts",
                             QDir::Files | QDir::NoSymLinks,
                             QDirIterator::Subdirectories);

    edit->listScripts->clear();
    QDir dir(engine->getDirMedias());
    while (dirIterator.hasNext()) {
        edit->listScripts->addItem(dir.relativeFilePath(dirIterator.next()));
    }
}

void Scripts::setSelected(bool value) {
    QTabWidget* tab = edit->tabWidget;
    if (value) {
        int index = tab->addTab(edit->Scripts, "Scripts");
        if (edit->getCurrentTab() == "Scripts")
            tab->setCurrentIndex(index);

        loadAvailableScripts();

        std::map<QString, Script>::const_iterator it, end = scripts.end();
        edit->listScriptActivated->clear();
        for (it = scripts.begin(); it != end;) {
            QString name = it->first;
            if (edit->listScripts->findText(name) != -1) {
                edit->listScriptActivated->addItem(name);
                ++it;
            } else {
                it = scripts.erase(it);
            }
        }

        connect(edit->addItemScript, &QPushButton::clicked,
                this, &Scripts::addScript);

        connect(edit->delItemScript, &QPushButton::clicked,
                this, &Scripts::removeScript);

        connect(edit->listScriptActivated, &QListWidget::clicked,
                this, &Scripts::detailScript);

        connect(edit->listScriptActivated, &QListWidget::doubleClicked,
                this, &Scripts::modifyScript);

        connect(edit->updateItemScript, &QPushButton::clicked,
                this, &Scripts::updateVariables);
    } else {
        while (edit->scriptEdition->rowCount()) {
            edit->scriptEdition->removeRow(0);
        }

        edit->addItemScript->disconnect();
        edit->delItemScript->disconnect();
        edit->listScriptActivated->disconnect();
        edit->updateItemScript->disconnect();

        tab->removeTab(tab->indexOf(edit->Scripts));
    }
}

void Scripts::addScript() {
    QString newScript = edit->listScripts->currentText();
    if (scripts.find(newScript) == scripts.end()) {
        Script script;
        scripts.insert(std::make_pair(newScript, script));
        edit->listScriptActivated->addItem(newScript);
    }
}

void Scripts::removeScript() {
    QList<QListWidgetItem*> selectedFiles =
            edit->listScriptActivated->selectedItems();
    if (selectedFiles.size()) {
        QListWidgetItem* i = selectedFiles.first();
        scripts.erase(i->text());
        delete i;
        edit->scriptEdition->clear();
    }
}

void Scripts::modifyScript() {
    QString scriptFile;
    if (edit->listScriptActivated->currentItem() != NULL) {
        scriptFile = engine->getDirMedias()
                + edit->listScriptActivated->currentItem()->text();
    }

    if (scriptFile != "") {
        ScriptEditor* editeur = new ScriptEditor();
        editeur->load(scriptFile);
    }

    edit->listScriptActivated->setCurrentItem(NULL);
    while (edit->scriptEdition->rowCount()) {
        edit->scriptEdition->removeRow(0);
    }
}

void Scripts::detailScript() {
    while (edit->scriptEdition->rowCount()) {
        edit->scriptEdition->removeRow(0);
    }

    QList<QListWidgetItem* > selectedFiles =
            edit->listScriptActivated->selectedItems();
    if (selectedFiles.size()) {
        QListWidgetItem* i = selectedFiles.first();

        updateModelDetails();

        Script script = scripts.at(i->text());

        std::map<QString, ParamScript>::const_iterator it,
                end = script.params.end();
        for (it = script.params.begin(); it != end; ++it) {
            ParamScript param = it->second;
            QString name = it->first;

            edit->scriptEdition->insertRow(edit->scriptEdition->rowCount());

            QTableWidgetItem* newVariable = new QTableWidgetItem();
            newVariable->setText(name);
            newVariable->setFlags(newVariable->flags() & ~Qt::ItemIsEditable);
            QTableWidgetItem* newType = new QTableWidgetItem();
            newType->setText(param.type);
            QTableWidgetItem* newValue = new QTableWidgetItem();
            newValue->setText(param.value);

            edit->scriptEdition->setItem(
                        edit->scriptEdition->rowCount() - 1, 0, newVariable);
            edit->scriptEdition->setItem(
                        edit->scriptEdition->rowCount() - 1, 1, newType);
            edit->scriptEdition->setItem(
                        edit->scriptEdition->rowCount() - 1, 2, newValue);
        }
    }
}

void Scripts::updateModelDetails() {
    QList<QListWidgetItem* > selectedFiles =
            edit->listScriptActivated->selectedItems();
    if (selectedFiles.size()) {
        QListWidgetItem* i = selectedFiles.first();

        QString scriptFile = engine->getDirMedias() + i->text();

        QFile file(scriptFile);
        if (file.open(QIODevice::ReadOnly)) {
            QString content = file.readAll();
            file.close();

            Script script;
            Script old;

            if (scripts.find(i->text()) != scripts.end())
                old = scripts.at(i->text());

            QRegExp exp("(var\\s*(\\w+)\\s*=\\s*\\$\\$\\s*;)");
            while (content.contains(exp)) {
                QString var = exp.cap(2);
                ParamScript p;
                if (old.params.find(var) != old.params.end()) {
                    p.type = old.params.at(var).type;
                    p.value = old.params.at(var).value;
                }
                script.params.insert(std::make_pair(var, p));
                content.remove(exp.cap(1));
            }
            scripts.erase(i->text());
            scripts.insert(std::make_pair(i->text(), script));
        }
    }
}

void Scripts::updateVariables() {
    QList<QListWidgetItem* > selectedFiles =
            edit->listScriptActivated->selectedItems();

    if (selectedFiles.size()) {
        int iRows = edit->scriptEdition->rowCount();

        QListWidgetItem* item = selectedFiles.first();

        Script script;

        for (int i = 0; i < iRows; ++i) {
            QTableWidgetItem* newVariable = edit->scriptEdition->item(i, 0);
            QTableWidgetItem* newType = edit->scriptEdition->item(i, 1);
            QTableWidgetItem* newValue = edit->scriptEdition->item(i, 2);

            ParamScript param;
            param.type = newType->text();
            param.value = newValue->text();
            script.params.insert(std::make_pair(newVariable->text(), param));
        }
        scripts.erase(item->text());
        scripts.insert(std::make_pair(item->text(), script));
    }
}

}  // namespace MarbleEditor
