/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Mesh.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_MESH_H_
#define MARBLEEDITOR_ELEMENTS_MESH_H_

#include <vector>

#include "MarbleEditor/Elements/Element.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

class Material3D;

/*! \class Mesh
 * \brief
 *
 *
 */
class Mesh : public Element {
    Q_OBJECT

    public:
        explicit Mesh(QString name, Engine* engine);
        virtual ~Mesh();

        bool init(QString mesh);

        bool readXML(tinyxml2::XMLElement* elem);
        void physic(tinyxml2::XMLElement* elem);
        void animation(tinyxml2::XMLElement* elem);
        void shadow(tinyxml2::XMLElement* elem);

        btConvexHullShape* createConvexHullShape(
                irr::scene::IMesh* mesh, const irr::core::vector3df& _scaling);

        void setSelected(bool value);
        void updateElementEditor();

        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);
        void writePhysic(tinyxml2::XMLDocument* xmlDoc,
                         tinyxml2::XMLElement* elem);
        void writeAnimation(tinyxml2::XMLDocument* xmlDoc,
                            tinyxml2::XMLElement* elem);
        void writeShadow(tinyxml2::XMLDocument* xmlDoc,
                         tinyxml2::XMLElement* elem);
        bool isPhysic();

    private slots:
        void setPosX(double value);
        void setPosY(double value);
        void setPosZ(double value);

        void setRotX(double value);
        void setRotY(double value);
        void setRotZ(double value);

        void setScaleX(double value);
        void setScaleY(double value);
        void setScaleZ(double value);

        void activeAnimation(bool value);
        void setAnimationSpeed(int value);
        void setAnimationBegin(int value);
        void setAnimationEnd(int value);
        void setAnimationCurrentFrame(int value);
        void setAnimationLoop(bool value);

        void setShadow(bool value = true);

        void setPhysique(bool value);
        bool loadBody();
        bool setTypeBody(QString type);

        void setScalePhysX(double value);
        void setScalePhysY(double value);
        void setScalePhysZ(double value);

        void setMaterialPhys(QString value);
        void setMasse(double value);

        void setImpulseX(double value);
        void setImpulseY(double value);
        void setImpulseZ(double value);

        void setPosImpulseX(double value);
        void setPosImpulseY(double value);
        void setPosImpulseZ(double value);

        void addMaterial();
        void restoreMaterials();

        void setDebugPhysic(bool value);
        void updatePhysicBody();

        float getMass();

    private:
        QString mesh;
        irr::scene::IAnimatedMesh* meshBuf;
        std::vector<Material3D*> material3ds;

        irr::scene::IShadowVolumeSceneNode* shadowNode;
        bool zfailmethod;
        float infinity;

        int animationSpeed, animationBegin, animationEnd, animationCurrentFrame;
        bool animate, animationLoop;

        bool showDebugPhysic;
        TypeBody typeBody;
        QString materialPhy;
        irr::core::vector3df scalePhys, impulse, posImpulse;
        float mass;
        btCollisionShape* m_collisionShape;
        btMotionState* m_motionState;
        btRigidBody* m_rigidBody;
        irr::core::matrix4 mat;

    private:
        DISALLOW_COPY_AND_ASSIGN(Mesh);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_MESH_H_
