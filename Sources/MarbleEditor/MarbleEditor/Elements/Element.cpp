/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Element.h"

#include <map>

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Scripts.h"
#include "MarbleEditor/Elements/Constraint.h"

namespace MarbleEditor {

Element::Element(QString name, Engine* engine) : QObject(engine) {
    this->name = name;
    isSelected = false;
    node = NULL;

    this->engine = engine;
    graphicsEngine = engine->getGraphicsEngine();
    physicsEngine = engine->getPhysicsEngine();
    edit = engine->getElementEditor();

    scripts = new Scripts(engine);
}

Element::~Element() {
    if (node != NULL) {
        node->remove();
        node = NULL;
    }
    if (scripts != NULL) {
        delete scripts;
        scripts = NULL;
    }
}

bool Element::init() {
    irr::core::vector3df pos = graphicsEngine->getCamera()->getTarget();
    pos.normalize();
    pos *= 5;
    pos += graphicsEngine->getCamera()->getPosition();

    node = graphicsEngine->getSceneManager()->addBillboardSceneNode(
                0,
                irr::core::dimension2d<irr::f32>(2, 2), pos);
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
    node->setMaterialTexture(
                0,
                graphicsEngine->getDriver()->getTexture("Gismos/scripts.png"));

    return true;
}

Engine* Element::getEngine() {
    return engine;
}

QString Element::getName() const {
    return name;
}

bool Element::readXML(tinyxml2::XMLElement* elem) {
    init();

    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        irr::core::vector3df pos;
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
        node->setPosition(pos);
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            scripts->readXML(elem);
        }

        elem = elem->NextSiblingElement();
    }

    return true;
}

bool Element::writeXML(tinyxml2::XMLDocument* xmlDoc,
                       tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Element");
    element->SetAttribute("name", name.toUtf8().data());
    element->SetAttribute("posX", node->getPosition().X);
    element->SetAttribute("posY", node->getPosition().Y);
    element->SetAttribute("posZ", node->getPosition().Z);

    scripts->writeXML(xmlDoc, element);
    elem->LinkEndChild(element);
    return true;
}

irr::scene::ISceneNode* Element::getNode() const {
    return node;
}

void Element::setNode(irr::scene::ISceneNode* newNode) {
    node = newNode;
}

void Element::setSelected(bool value) {
    edit->saveCurrentTab();
    edit->clean();
    QTabWidget* tab = edit->tabWidget;
    isSelected = value;
    if (value) {
        int index = tab->addTab(edit->Element, "Element");
        if (edit->getCurrentTab() == "Element")
            tab->setCurrentIndex(index);

        edit->nameEdit->setText(name);

        edit->posX->setValue(node->getPosition().X);
        edit->posY->setValue(node->getPosition().Y);
        edit->posZ->setValue(node->getPosition().Z);

        edit->rotX->setValue(node->getRotation().X);
        edit->rotY->setValue(node->getRotation().Y);
        edit->rotZ->setValue(node->getRotation().Z);

        edit->scaleX->setValue(node->getScale().X);
        edit->scaleY->setValue(node->getScale().Y);
        edit->scaleZ->setValue(node->getScale().Z);

        connect(edit->nameEditButton, &QPushButton::clicked,
                this, &Element::editName);

        connect(edit->posX, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Element::setPosX);

        connect(edit->posY, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Element::setPosY);

        connect(edit->posZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Element::setPosZ);

        connect(edit->rotX, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Element::setRotX);

        connect(edit->rotY, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Element::setRotY);

        connect(edit->rotZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Element::setRotZ);

        connect(edit->scaleX, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Element::setScaleX);

        connect(edit->scaleY, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Element::setScaleY);

        connect(edit->scaleZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Element::setScaleZ);
    } else {
        edit->nameEditButton->disconnect();
        edit->posX->disconnect();
        edit->posY->disconnect();
        edit->posZ->disconnect();
        edit->rotX->disconnect();
        edit->rotY->disconnect();
        edit->rotZ->disconnect();
        edit->scaleX->disconnect();
        edit->scaleY->disconnect();
        edit->scaleZ->disconnect();
        tab->removeTab(tab->indexOf(edit->Element));
    }
    scripts->setSelected(value);
}

void Element::setName(QString name) {
    if (engine->getElement(name) == NULL) {
        std::map<QString, Element* > elements = engine->getElements();
        std::map<QString, Element*>::const_iterator it, end = elements.end();
        for (it = elements.begin(); it != end; ++it) {
            (it->second)->updateNameElements(this->name, name);
        }

        engine->getElements().erase(this->name);
        this->name = name;
        engine->getElements().insert(std::make_pair(name, this));
    }
    if (isSelected) {
        edit->nameEdit->setText(name);
    }
    engine->updateList();
}

void Element::editName() {
    if (!edit->nameEdit->text().isEmpty())
        setName(edit->nameEdit->text());
}

void Element::setPosX(double value) {
    irr::core::vector3df pos = node->getPosition();
    pos.X = value;
    node->setPosition(pos);
    if (isSelected)
        edit->posX->setValue(value);
}

void Element::setPosY(double value) {
    irr::core::vector3df pos = node->getPosition();
    pos.Y = value;
    node->setPosition(pos);
    if (isSelected)
        edit->posY->setValue(value);
}

void Element::setPosZ(double value) {
    irr::core::vector3df pos = node->getPosition();
    pos.Z = value;
    node->setPosition(pos);
    if (isSelected)
        edit->posZ->setValue(value);
}

void  Element::setRotX(double value) {
    irr::core::vector3df rot = node->getRotation();
    rot.X = value;
    node->setRotation(rot);
    if (isSelected)
        edit->rotX->setValue(value);
}

void  Element::setRotY(double value) {
    irr::core::vector3df rot = node->getRotation();
    rot.Y = value;
    node->setRotation(rot);
    if (isSelected)
        edit->rotY->setValue(value);
}

void  Element::setRotZ(double value) {
    irr::core::vector3df rot = node->getRotation();
    rot.Z = value;
    node->setRotation(rot);
    if (isSelected)
        edit->rotZ->setValue(value);
}

void  Element::setScaleX(double value) {
    irr::core::vector3df scale = node->getScale();
    scale.X = value;
    node->setScale(scale);
    if (isSelected)
        edit->scaleX->setValue(value);
}

void  Element::setScaleY(double value) {
    irr::core::vector3df scale = node->getScale();
    scale.Y = value;
    node->setScale(scale);
    if (isSelected)
        edit->scaleY->setValue(value);
}

void  Element::setScaleZ(double value) {
    irr::core::vector3df scale = node->getScale();
    scale.Z = value;
    node->setScale(scale);
    if (isSelected)
        edit->scaleZ->setValue(value);
}

QStringList Element::getScripts() {
    return scripts->getScripts();
}

}  // namespace MarbleEditor
