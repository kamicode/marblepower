/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Camera.h"

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Elements/Scripts.h"

namespace MarbleEditor {

Camera::Camera(QString name, Engine* engine) : Element(name, engine) {
    cameraNode = graphicsEngine->getSceneManager()->addCameraSceneNode();
    cameraNode->bindTargetAndRotation(true);

    farValue = 2000;
    nearValue = 1;
    fov = 1.256f;
    viewportX1 = 0;
    viewportX2 = 1;
    viewportY1 = 0;
    viewportY2 = 1;

    node = graphicsEngine->getSceneManager()->addBillboardSceneNode(
                0, irr::core::dimension2d<irr::f32>(2, 2));
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);
    node->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
    node->setMaterialTexture(
                0,
                graphicsEngine->getDriver()->getTexture("Gismos/camera.png"));
}

Camera::~Camera() {
    cameraNode->remove();
}

irr::scene::ICameraSceneNode* Camera::getCamera() {
    return cameraNode;
}

bool Camera::init() {
    irr::core::vector3df pos = graphicsEngine->getCamera()->getTarget();
    pos.normalize();
    pos *= 5;
    pos += graphicsEngine->getCamera()->getPosition();

    node->setPosition(pos);
    cameraNode->setPosition(pos);

    return true;
}

void Camera::setSelected(bool value) {
    Element::setSelected(value);

    if (node != NULL) {
        node->setDebugDataVisible(value);
    }

    QTabWidget* tab = edit->tabWidget;
    if (value) {
        int index = tab->addTab(edit->Camera, "Camera");
        if (edit->getCurrentTab() == "Camera")
            tab->setCurrentIndex(index);

        edit->scaleBox->hide();
        edit->rotationBox->hide();

        updateElementEditor();

        connect(edit->targetX, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setTargetX);

        connect(edit->targetY, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setTargetY);

        connect(edit->targetZ, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setTargetZ);

        connect(edit->farValueSpinBox,
                static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setFarValue);

        connect(edit->nearValueSpinBox,
                static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setNearValue);

        connect(edit->fovSpinBox, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setFov);

        connect(edit->viewportX1, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setViewportX1);

        connect(edit->viewportX2, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setViewportX2);

        connect(edit->viewportY1, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setViewportY1);

        connect(edit->viewportY2, static_cast<void (QDoubleSpinBox::*)(double)>
                (&QDoubleSpinBox::valueChanged),
                this, &Camera::setViewportY2);
    } else {
        edit->targetX->disconnect();
        edit->targetY->disconnect();
        edit->targetZ->disconnect();

        edit->farValueSpinBox->disconnect();
        edit->nearValueSpinBox->disconnect();
        edit->fovSpinBox->disconnect();
        edit->viewportX1->disconnect();
        edit->viewportX2->disconnect();
        edit->viewportY1->disconnect();
        edit->viewportY2->disconnect();

        edit->scaleBox->show();
        edit->rotationBox->show();

        tab->removeTab(tab->indexOf(edit->Lumiere));
    }
}

void Camera::updateElementEditor() {
    edit->farValueSpinBox->setValue(farValue);
    edit->nearValueSpinBox->setValue(nearValue);
    edit->fovSpinBox->setValue(fov);
    edit->viewportX1->setValue(viewportX1);
    edit->viewportX2->setValue(viewportX2);
    edit->viewportY1->setValue(viewportY1);
    edit->viewportY2->setValue(viewportY2);

    edit->targetX->setValue(cameraNode->getTarget().X);
    edit->targetY->setValue(cameraNode->getTarget().Y);
    edit->targetZ->setValue(cameraNode->getTarget().Z);
}

void Camera::setPosX(double value) {
    Element::setPosX(value);
    cameraNode->setPosition(node->getPosition());
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setPosY(double value) {
    Element::setPosY(value);
    cameraNode->setPosition(node->getPosition());
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setPosZ(double value) {
    Element::setPosZ(value);
    cameraNode->setPosition(node->getPosition());
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setTargetX(double value) {
    irr::core::vector3df target = cameraNode->getTarget();
    target.X = value;
    cameraNode->setTarget(target);
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setTargetY(double value) {
    irr::core::vector3df target = cameraNode->getTarget();
    target.Y = value;
    cameraNode->setTarget(target);
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setTargetZ(double value) {
    irr::core::vector3df target = cameraNode->getTarget();
    target.Z = value;
    cameraNode->setTarget(target);
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setFarValue(double value) {
    farValue = value;
    cameraNode->setFarValue(value);
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setNearValue(double value) {
    nearValue = value;
    cameraNode->setNearValue(value);
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setFov(double value) {
    fov = value;
    cameraNode->setFOV(value);
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setViewportX1(double value) {
    if (value < viewportX2) {
        viewportX1 = value;
    }

    irr::core::dimension2d<irr::u32> size =
            engine->getGraphicsEngine()->getDriver()->getScreenSize();
    irr::core::rect<irr::s32> view =
            getAbsoluteViewport(size.Width, size.Height);
    cameraNode->setAspectRatio(
                (irr::f32)view.getWidth() / (irr::f32)view.getHeight());

    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setViewportX2(double value) {
    if (value > viewportX1) {
        viewportX2 = value;
    }
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setViewportY1(double value) {
    if (value < viewportY2) {
        viewportY1 = value;
    }
    if (isSelected) {
        updateElementEditor();
    }
}

void Camera::setViewportY2(double value) {
    if (value > viewportY1) {
        viewportY2 = value;
    }
    if (isSelected) {
        updateElementEditor();
    }
}

bool Camera::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        irr::core::vector3df pos;
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
        node->setPosition(pos);
        cameraNode->setPosition(pos);
    }

    if (elem->Attribute("targetX") && elem->Attribute("targetY")
            && elem->Attribute("targetZ")) {
        irr::core::vector3df target;
        target.X = elem->FloatAttribute("targetX");
        target.Y = elem->FloatAttribute("targetY");
        target.Z = elem->FloatAttribute("targetZ");
        cameraNode->setTarget(target);
    }

    if (elem->Attribute("farValue")) {
        farValue = elem->FloatAttribute("farValue");
    }

    if (elem->Attribute("nearValue")) {
        nearValue = elem->FloatAttribute("nearValue");
    }

    if (elem->Attribute("fov")) {
        fov = elem->FloatAttribute("fov");
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Script") {
            scripts->readXML(elem);
        } else if (QString(elem->Value()) == "Viewport") {
            viewportX1 = elem->FloatAttribute("x1");
            viewportX2 = elem->FloatAttribute("x2");
            viewportY1 = elem->FloatAttribute("y1");
            viewportY2 = elem->FloatAttribute("y2");
        }
        elem = elem->NextSiblingElement();
    }

    return true;
}

bool Camera::writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Camera");

    element->SetAttribute("name", name.toUtf8().data());
    element->SetAttribute("posX", node->getPosition().X);
    element->SetAttribute("posY", node->getPosition().Y);
    element->SetAttribute("posZ", node->getPosition().Z);
    element->SetAttribute("targetX", cameraNode->getTarget().X);
    element->SetAttribute("targetY", cameraNode->getTarget().Y);
    element->SetAttribute("targetZ", cameraNode->getTarget().Z);
    element->SetAttribute("farValue", farValue);
    element->SetAttribute("nearValue", nearValue);
    element->SetAttribute("fov", fov);

    tinyxml2::XMLElement* el = xmlDoc->NewElement("Viewport");
    el->SetAttribute("x1", viewportX1);
    el->SetAttribute("y1", viewportY1);
    el->SetAttribute("x2", viewportX2);
    el->SetAttribute("y2", viewportY2);
    element->LinkEndChild(el);

    scripts->writeXML(xmlDoc, element);

    elem->LinkEndChild(element);
    return true;
}

irr::core::rect<irr::f32> Camera::getViewport() {
    return irr::core::rect<irr::f32>(viewportX1, viewportY1,
                                     viewportX2, viewportY2);
}

irr::core::rect<irr::s32> Camera::getAbsoluteViewport(int width, int height) {
    return irr::core::rect<irr::s32>(viewportX1 * width,
                                     viewportY1 * height,
                                     viewportX2 * width,
                                     viewportY2 * height);
}

}  // namespace MarbleEditor
