/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Elements/Particules.h"

#include "MarbleEditor/Engines/Engine.h"
#include "MarbleEditor/Materials/Material3D.h"
#include "MarbleEditor/Elements/Scripts.h"

namespace MarbleEditor {

Particules::Particules(QString name, Engine* engine) : Element(name, engine) {
    node = graphicsEngine->getSceneManager()->addParticleSystemSceneNode(true);
    material = new Material3D(engine, &(node->getMaterial(0)), 0);
    box = irr::core::aabbox3d<irr::f32>(-1, -1, -1, 1, 1, 1);
    lifeTimeMin = 2000;
    lifeTimeMax = 4000;
    maxAngleDegrees = 0;
}

Particules::~Particules() {
    if (material) {
        delete material;
        material = NULL;
    }
}

bool Particules::init() {
    irr::core::vector3df pos = graphicsEngine->getCamera()->getTarget();
    pos.normalize();
    pos *= 5;
    pos += graphicsEngine->getCamera()->getPosition();

    node->setPosition(pos);

    return true;
}

void Particules::setSelected(bool value) {
    Element::setSelected(value);

    if (node != NULL) {
        node->setDebugDataVisible(value);
    }

    QTabWidget* tab = edit->tabWidget;
    if (value) {
        int index = tab->addTab(edit->Particules, "Particules");
        if (edit->getCurrentTab() == "Particules")
            tab->setCurrentIndex(index);

        index = tab->addTab(edit->Textures, "Textures");
        if (edit->getCurrentTab() == "Textures")
            tab->setCurrentIndex(index);

        edit->removeMats->hide();
        edit->addMat->hide();
        edit->scaleBox->hide();

        updateElementEditor();

        edit->posX->disconnect();
        edit->posY->disconnect();
        edit->posZ->disconnect();
        edit->rotX->disconnect();
        edit->rotY->disconnect();
        edit->rotZ->disconnect();

        connect(edit->posX, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->posY, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->posZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->rotX, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->rotY, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->rotZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->minX, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->minY, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->minZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->maxX, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->maxY, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->maxZ, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->maxAngle, static_cast<void (QSpinBox::*)(int)>
                                             (&QSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->minStartWidth, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->maxStartWidth, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->minStartHeight, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->maxStartHeight, static_cast<void (QDoubleSpinBox::*)(double)>
                                             (&QDoubleSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->numMin, static_cast<void (QSpinBox::*)(int)>
                                             (&QSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->numMax, static_cast<void (QSpinBox::*)(int)>
                                             (&QSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->lifeMax, static_cast<void (QSpinBox::*)(int)>
                                             (&QSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->lifeMin, static_cast<void (QSpinBox::*)(int)>
                                             (&QSpinBox::valueChanged),
                this, &Particules::maj);

        connect(edit->minColorStartButton, &QPushButton::clicked,
                this, &Particules::chooseMinColorStart);

        connect(edit->maxColorStartButton, &QPushButton::clicked,
                this, &Particules::chooseMaxColorStart);
    } else {
        edit->posX->disconnect();
        edit->posY->disconnect();
        edit->posZ->disconnect();
        edit->rotX->disconnect();
        edit->rotX->disconnect();
        edit->rotX->disconnect();
        edit->minX->disconnect();
        edit->minY->disconnect();
        edit->minZ->disconnect();
        edit->maxX->disconnect();
        edit->maxY->disconnect();
        edit->maxZ->disconnect();
        edit->maxAngle->disconnect();
        edit->minStartWidth->disconnect();
        edit->maxStartWidth->disconnect();
        edit->minStartHeight->disconnect();
        edit->maxStartHeight->disconnect();
        edit->numMin->disconnect();
        edit->numMax->disconnect();
        edit->lifeMax->disconnect();
        edit->lifeMin->disconnect();
        edit->minColorStartButton->disconnect();
        edit->maxColorStartButton->disconnect();

        edit->removeMats->show();
        edit->addMat->show();
        edit->scaleBox->show();

        tab->removeTab(tab->indexOf(edit->Particules));
        tab->removeTab(tab->indexOf(edit->Textures));
    }
}

void Particules::updateElementEditor() {
    irr::scene::IParticleSystemSceneNode* buf =
            static_cast<irr::scene::IParticleSystemSceneNode*>(node);
    irr::scene::IParticleEmitter* em = buf->getEmitter();

    while (edit->material3DTab->count() > 0) {
        edit->material3DTab->removeTab(0);
    }
    edit->material3DTab->addTab(material, "Texture");

    material->updateElementEditor();

    edit->minStartWidth->setValue(em->getMinStartSize().Width);
    edit->minStartHeight->setValue(em->getMinStartSize().Height);

    edit->maxStartWidth->setValue(em->getMaxStartSize().Width);
    edit->maxStartHeight->setValue(em->getMaxStartSize().Height);

    edit->numMax->setValue(em->getMaxParticlesPerSecond());
    edit->numMin->setValue(em->getMinParticlesPerSecond());

    edit->minX->setValue(box.MinEdge.X);
    edit->minY->setValue(box.MinEdge.Y);
    edit->minZ->setValue(box.MinEdge.Z);
    edit->maxX->setValue(box.MaxEdge.X);
    edit->maxY->setValue(box.MaxEdge.Y);
    edit->maxZ->setValue(box.MaxEdge.Z);

    edit->lifeMin->setValue(lifeTimeMin);
    edit->lifeMax->setValue(lifeTimeMax);
    edit->maxAngle->setValue(maxAngleDegrees);

    QColor couleur(em->getMaxStartColor().getRed(),
                   em->getMaxStartColor().getGreen(),
                   em->getMaxStartColor().getBlue(),
                   em->getMaxStartColor().getAlpha());
    if (couleur.isValid()) {
        QPalette palette = edit->maxColorStart->palette();
        palette.setColor(QPalette::Window, couleur);
        edit->maxColorStart->setPalette(palette);
    }

    couleur = QColor(em->getMinStartColor().getRed(),
                     em->getMinStartColor().getGreen(),
                     em->getMinStartColor().getBlue(),
                     em->getMinStartColor().getAlpha());
    if (couleur.isValid()) {
        QPalette palette = edit->minColorStart->palette();
        palette.setColor(QPalette::Window, couleur);
        edit->minColorStart->setPalette(palette);
    }
}

void Particules::maj() {
    irr::video::SMaterial mat = node->getMaterial(0);
    if (node != NULL) {
        node->remove();
        node = NULL;
    }
    node = graphicsEngine->getSceneManager()->addParticleSystemSceneNode(true);
    node->getMaterial(0) = mat;
    material->setMaterial(&(node->getMaterial(0)));
    node->setPosition(irr::core::vector3df(edit->posX->value(),
                                           edit->posY->value(),
                                           edit->posZ->value()));
    node->setRotation(irr::core::vector3df(edit->rotX->value(),
                                           edit->rotY->value(),
                                           edit->rotZ->value()));

    box.MinEdge.X = edit->minX->value();
    box.MinEdge.Y = edit->minY->value();
    box.MinEdge.Z = edit->minZ->value();
    box.MaxEdge.X = edit->maxX->value();
    box.MaxEdge.Y = edit->maxY->value();
    box.MaxEdge.Z = edit->maxZ->value();

    irr::core::vector3df dir(edit->rotX->value(),
                             edit->rotY->value(),
                             edit->rotZ->value());

    irr::u32 minParticlesPerSecond = edit->numMin->value();
    irr::u32 maxParticlesPerSecond = edit->numMax->value();

    QColor col = edit->minColorStart->palette().color(QPalette::Window);
    irr::video::SColor minStartColor(
                col.alpha(), col.red(), col.green(), col.blue());
    col = edit->maxColorStart->palette().color(QPalette::Window);
    irr::video::SColor maxStartColor(
                col.alpha(), col.red(), col.green(), col.blue());

    lifeTimeMin = edit->lifeMin->value();
    lifeTimeMax = edit->lifeMax->value();

    maxAngleDegrees = edit->maxAngle->value();

    irr::core::dimension2df minStartSize(edit->minStartWidth->value(),
                                         edit->minStartHeight->value());
    irr::core::dimension2df maxStartSize(edit->maxStartWidth->value(),
                                         edit->maxStartHeight->value());

    irr::scene::IParticleSystemSceneNode* buf =
            static_cast<irr::scene::IParticleSystemSceneNode*>(node);
    irr::scene::IParticleEmitter* emitter = buf->createBoxEmitter(
                box, dir, minParticlesPerSecond, maxParticlesPerSecond,
                minStartColor, maxStartColor, lifeTimeMin, lifeTimeMax,
                maxAngleDegrees, minStartSize, maxStartSize);
    buf->setEmitter(emitter);
    emitter->drop();
}

bool Particules::readXML(tinyxml2::XMLElement* elem) {
    if (elem->Attribute("posX") && elem->Attribute("posY")
            && elem->Attribute("posZ")) {
        irr::core::vector3df pos;
        pos.X = elem->FloatAttribute("posX");
        pos.Y = elem->FloatAttribute("posY");
        pos.Z = elem->FloatAttribute("posZ");
        node->setPosition(pos);
    }

    elem = elem->FirstChildElement();
    while (elem) {
        if (QString(elem->Value()) == "Material3D") {
            material->readXML(elem);
        } else if (QString(elem->Value()) == "BoxEmetteur") {
            boxEmetteur(elem);
        } else if (QString(elem->Value()) == "Script") {
            scripts->readXML(elem);
        }
        elem = elem->NextSiblingElement();
    }
    return true;
}

void Particules::boxEmetteur(tinyxml2::XMLElement* elem) {
    irr::scene::IParticleSystemSceneNode* buf =
            static_cast<irr::scene::IParticleSystemSceneNode*>(node);
    irr::scene::IParticleEmitter* em = buf->getEmitter();

    if (elem->Attribute("boxMinX") && elem->Attribute("boxMinY")
            && elem->Attribute("boxMinZ") && elem->Attribute("boxMaxX")
            && elem->Attribute("boxMaxY") && elem->Attribute("boxMaxZ")) {
        box.MinEdge.X = elem->FloatAttribute("boxMinX");
        box.MinEdge.Y = elem->FloatAttribute("boxMinY");
        box.MinEdge.Z = elem->FloatAttribute("boxMinZ");
        box.MaxEdge.X = elem->FloatAttribute("boxMaxX");
        box.MaxEdge.Y = elem->FloatAttribute("boxMaxY");
        box.MaxEdge.Z = elem->FloatAttribute("boxMaxZ");
    }

    if (elem->Attribute("dirX") && elem->Attribute("dirY")
            && elem->Attribute("dirZ")) {
        irr::core::vector3df dirX;
        dirX.X = elem->FloatAttribute("dirX");
        dirX.Y = elem->FloatAttribute("dirY");
        dirX.Z = elem->FloatAttribute("dirZ");
        node->setRotation(dirX);
        em->setDirection(dirX);
    }

    if (elem->Attribute("minParticlesPerSecond")) {
        em->setMinParticlesPerSecond(
                    elem->IntAttribute("minParticlesPerSecond"));
    }

    if (elem->Attribute("maxParticlesPerSecond")) {
        em->setMaxParticlesPerSecond(
                    elem->IntAttribute("maxParticlesPerSecond"));
    }

    if (elem->Attribute("minStartColorA")
            && elem->Attribute("minStartColorR")
            && elem->Attribute("minStartColorG")
            && elem->Attribute("minStartColorB")) {
        irr::video::SColor color(0, 0, 0, 0);
        color.setRed(elem->IntAttribute("minStartColorR"));
        color.setGreen(elem->IntAttribute("minStartColorG"));
        color.setBlue(elem->IntAttribute("minStartColorB"));
        color.setAlpha(elem->IntAttribute("minStartColorA"));
        em->setMinStartColor(color);
    }

    if (elem->Attribute("maxStartColorA")
            && elem->Attribute("maxStartColorR")
            && elem->Attribute("maxStartColorG")
            && elem->Attribute("maxStartColorB")) {
        irr::video::SColor color(0, 0, 0, 0);
        color.setRed(elem->IntAttribute("maxStartColorR"));
        color.setGreen(elem->IntAttribute("maxStartColorG"));
        color.setBlue(elem->IntAttribute("maxStartColorB"));
        color.setAlpha(elem->IntAttribute("maxStartColorA"));
        em->setMaxStartColor(color);
    }

    if (elem->Attribute("lifeTimeMin")) {
        lifeTimeMin = elem->IntAttribute("lifeTimeMin");
    }

    if (elem->Attribute("lifeTimeMax")) {
        lifeTimeMax = elem->IntAttribute("lifeTimeMax");
    }

    if (elem->Attribute("maxAngleDegrees")) {
        maxAngleDegrees = elem->IntAttribute("maxAngleDegrees");
    }

    if (elem->Attribute("minStartWidth")
            && elem->Attribute("minStartHeight")) {
        irr::core::dimension2df size;
        size.Width = elem->FloatAttribute("minStartWidth");
        size.Height = elem->FloatAttribute("minStartHeight");
        em->setMinStartSize(size);
    }

    if (elem->Attribute("maxStartWidth")
            && elem->Attribute("maxStartHeight")) {
        irr::core::dimension2df size;
        size.Width = elem->FloatAttribute("maxStartWidth");
        size.Height = elem->FloatAttribute("maxStartHeight");
        em->setMaxStartSize(size);
    }

    irr::scene::IParticleEmitter* emitter =
            buf->createBoxEmitter(
                box, node->getRotation(),
                em->getMinParticlesPerSecond(),
                em->getMaxParticlesPerSecond(),
                em->getMinStartColor(), em->getMaxStartColor(),
                lifeTimeMin, lifeTimeMax, maxAngleDegrees,
                em->getMinStartSize(), em->getMaxStartSize());
    buf->setEmitter(emitter);
    emitter->drop();
}

bool Particules::writeXML(tinyxml2::XMLDocument* xmlDoc,
                          tinyxml2::XMLElement* elem) {
    tinyxml2::XMLElement* element = xmlDoc->NewElement("Particules");

    element->SetAttribute("name", name.toUtf8().data());

    element->SetAttribute("posX", node->getPosition().X);
    element->SetAttribute("posY", node->getPosition().Y);
    element->SetAttribute("posZ", node->getPosition().Z);

    material->writeXML(xmlDoc, element);
    writeBoxEmetteur(xmlDoc, element);

    scripts->writeXML(xmlDoc, element);

    elem->LinkEndChild(element);
    return true;
}

void Particules::writeBoxEmetteur(tinyxml2::XMLDocument* xmlDoc,
                                  tinyxml2::XMLElement* elem) {
    irr::scene::IParticleSystemSceneNode* buf =
            static_cast<irr::scene::IParticleSystemSceneNode*>(node);
    irr::scene::IParticleEmitter* em = buf->getEmitter();

    tinyxml2::XMLElement* element = xmlDoc->NewElement("BoxEmetteur");

    element->SetAttribute("boxMinX", box.MinEdge.X);
    element->SetAttribute("boxMinY", box.MinEdge.Y);
    element->SetAttribute("boxMinZ", box.MinEdge.Z);

    element->SetAttribute("boxMaxX", box.MaxEdge.X);
    element->SetAttribute("boxMaxY", box.MaxEdge.Y);
    element->SetAttribute("boxMaxZ", box.MaxEdge.Z);

    element->SetAttribute("dirX", node->getRotation().X);
    element->SetAttribute("dirY", node->getRotation().Y);
    element->SetAttribute("dirZ", node->getRotation().Z);

    element->SetAttribute("minParticlesPerSecond",
                          em->getMinParticlesPerSecond());
    element->SetAttribute("maxParticlesPerSecond",
                          em->getMaxParticlesPerSecond());

    element->SetAttribute("minStartColorA", em->getMinStartColor().getAlpha());
    element->SetAttribute("minStartColorR", em->getMinStartColor().getRed());
    element->SetAttribute("minStartColorG", em->getMinStartColor().getGreen());
    element->SetAttribute("minStartColorB", em->getMinStartColor().getBlue());

    element->SetAttribute("maxStartColorA", em->getMaxStartColor().getAlpha());
    element->SetAttribute("maxStartColorR", em->getMaxStartColor().getRed());
    element->SetAttribute("maxStartColorG", em->getMaxStartColor().getGreen());
    element->SetAttribute("maxStartColorB", em->getMaxStartColor().getBlue());

    element->SetAttribute("lifeTimeMin", lifeTimeMin);
    element->SetAttribute("lifeTimeMax", lifeTimeMax);
    element->SetAttribute("maxAngleDegrees", maxAngleDegrees);

    element->SetAttribute("minStartWidth", em->getMinStartSize().Width);
    element->SetAttribute("minStartHeight", em->getMinStartSize().Height);
    element->SetAttribute("maxStartWidth", em->getMaxStartSize().Width);
    element->SetAttribute("maxStartHeight", em->getMaxStartSize().Height);

    elem->LinkEndChild(element);
}

void Particules::chooseMinColorStart() {
    irr::scene::IParticleSystemSceneNode* buf =
            static_cast<irr::scene::IParticleSystemSceneNode*>(node);
    irr::scene::IParticleEmitter* emitter = buf->getEmitter();
    irr::video::SColor col = emitter->getMinStartColor();
    QColor couleur(col.getRed(), col.getGreen(),
                   col.getBlue(), col.getAlpha());

    couleur = QColorDialog::getColor(couleur, engine, tr("Couleur min"),
                                     QColorDialog::ShowAlphaChannel);

    if (couleur.isValid()) {
        QPalette palette = edit->minColorStart->palette();
        palette.setColor(QPalette::Window, couleur);
        edit->minColorStart->setPalette(palette);
    }
    maj();
}

void Particules::chooseMaxColorStart() {
    irr::scene::IParticleSystemSceneNode* buf =
            static_cast<irr::scene::IParticleSystemSceneNode*>(node);
    irr::scene::IParticleEmitter* emitter = buf->getEmitter();
    irr::video::SColor col = emitter->getMaxStartColor();
    QColor couleur(col.getRed(), col.getGreen(),
                   col.getBlue(), col.getAlpha());

    couleur = QColorDialog::getColor(couleur, engine, tr("Couleur min"),
                                     QColorDialog::ShowAlphaChannel);

    if (couleur.isValid()) {
        QPalette palette = edit->maxColorStart->palette();
        palette.setColor(QPalette::Window, couleur);
        edit->maxColorStart->setPalette(palette);
    }
    maj();
}

}  // namespace MarbleEditor
