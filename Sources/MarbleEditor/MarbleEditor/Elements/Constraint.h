/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Constraint.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_ELEMENTS_CONSTRAINT_H_
#define MARBLEEDITOR_ELEMENTS_CONSTRAINT_H_

#include "MarbleEditor/Elements/Element.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

/*! \class Constraint
 * \brief
 *
 *
 */
class Constraint : public Element {
    Q_OBJECT

    public:
        explicit Constraint(QString name, Engine* engine);
        virtual ~Constraint();

        bool init();

        void setSelected(bool value);

        void updateElementEditor();

        bool readXML(tinyxml2::XMLElement* elem);
        bool writeXML(tinyxml2::XMLDocument* xmlDoc,
                      tinyxml2::XMLElement* elem);

        void specificRender();

        void updateNameElements(QString before, QString after);

    private slots:
        void change();

    private:
        QString nameA;
        QString nameB;
        QString constraintType;
        btVector3 pivotInA;
        btVector3 pivotInB;
        btVector3 axisInA;
        btVector3 axisInB;

    private:
        DISALLOW_COPY_AND_ASSIGN(Constraint);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_ELEMENTS_CONSTRAINT_H_
