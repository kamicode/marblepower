/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_ScriptEditor.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_EDITORS_SCRIPTEDITOR_H_
#define MARBLEEDITOR_EDITORS_SCRIPTEDITOR_H_

#include <ui_Editor.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

/*! \class ScriptEditor
 * \brief
 *
 *
 */
class ScriptEditor : public QMainWindow, private Ui::Editor {
    Q_OBJECT

    public:
        explicit ScriptEditor(QWidget* parent = 0, bool withLexer = true,
                              bool withCustomAutocomplete = true);
        virtual ~ScriptEditor();
        void load(const QString &file);
        void closeEvent(QCloseEvent* event);

    private slots:
        bool save();
        void undo();
        void redo();
        void documentWasModified();
        void helpScript();

    private:
        bool maybeSave();
        void setCurrentFile(const QString &fileName);

    private:
        QString curFile;
        QsciScintilla* textEdit;
        QsciLexerJavaScript* lexer;

    private:
        DISALLOW_COPY_AND_ASSIGN(ScriptEditor);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_EDITORS_SCRIPTEDITOR_H_
