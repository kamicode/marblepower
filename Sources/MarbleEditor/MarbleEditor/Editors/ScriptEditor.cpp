/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Editors/ScriptEditor.h"

#include "MarbleEditor/Engines/Engine.h"

namespace MarbleEditor {

class apiPers : public QsciAPIs {
    public:
        explicit apiPers(QsciLexer* lexer) : QsciAPIs(lexer) {
            loadEngineList();
            loadEventEngineList();
            loadSelfList();
        }

        void updateAutoCompletionList(const QStringList &context,
                                      QStringList &list) {
            if (context.count() == 2 && context.contains("engine")) {
                list.append(engineList);
            } else if (context.count() == 2
                       && context.contains("eventEngine")) {
                list.append(eventEngineList);
            } else if (context.count() == 2 && context.contains("self")) {
                list.append(selfList);
            } else {
                QsciAPIs::updateAutoCompletionList(context, list);
            }
        }

    private:
        void loadEngineList() {
            QFile file(":/Resources/engine");
            if (!file.open(QIODevice::ReadOnly))
                return;

            QApplication::setOverrideCursor(Qt::WaitCursor);
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString line = in.readLine();
                engineList.append(line);
            }
            file.close();
            QApplication::restoreOverrideCursor();
        }

        void loadEventEngineList() {
            QFile file(":/Resources/eventEngine");
            if (!file.open(QIODevice::ReadOnly))
                return;

            QApplication::setOverrideCursor(Qt::WaitCursor);
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString line = in.readLine();
                eventEngineList.append(line);
            }
            file.close();
            QApplication::restoreOverrideCursor();
        }

        void loadSelfList() {
            QFile file(":/Resources/self");
            if (!file.open(QIODevice::ReadOnly))
                return;

            QApplication::setOverrideCursor(Qt::WaitCursor);
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString line = in.readLine();
                selfList.append(line);
            }
            file.close();
            QApplication::restoreOverrideCursor();
        }

    private:
        QStringList engineList;
        QStringList eventEngineList;
        QStringList selfList;
};

class Lexer: public QsciLexerJavaScript {
    public:
        Lexer() : QsciLexerJavaScript() {
        }

        QStringList autoCompletionWordSeparators() const {
            return QStringList(".");
        }
};

ScriptEditor::ScriptEditor(QWidget* parent, bool withLexer,
                           bool withCustomAutocomplete) : QMainWindow(parent) {
    setAttribute(Qt::WA_DeleteOnClose);
    setupUi(this);

    textEdit = new QsciScintilla();
    textEdit->setUtf8(true);
    textEdit->setMarginLineNumbers(1, true);
    textEdit->setMarginWidth(1, 30);
    textEdit->setFolding(QsciScintilla::BoxedFoldStyle, 2);
    textEdit->setAutoIndent(true);
    textEdit->setEdgeColumn(120);
    textEdit->setEdgeMode(QsciScintilla::EdgeLine);
    textEdit->setTabWidth(4);
    textEdit->setBraceMatching(QsciScintilla::SloppyBraceMatch);
    textEdit->setAutoIndent(true);
    textEdit->setAutoCompletionFillupsEnabled(true);

    if (withLexer) {
        lexer = new Lexer();
        textEdit->setLexer(lexer);
        textEdit->setAutoCompletionSource(QsciScintilla::AcsAPIs);
        textEdit->setAutoCompletionThreshold(3);

        if (withCustomAutocomplete) {
            apiPers* api = new apiPers(lexer);
            api->load(":/Resources/invokable");
            api->prepare();
            lexer->setAPIs(api);
        }
        connect(actionAutocompletion, &QAction::triggered,
                textEdit, &QsciScintilla::autoCompleteFromAPIs);
    } else {
        actionAutocompletion->setEnabled(false);
    }

    setCentralWidget(textEdit);

    connect(actionSauver, &QAction::triggered,
            this, &ScriptEditor::save);

    connect(actionQuitter, &QAction::triggered,
            this, &ScriptEditor::close);

    connect(actionCopier, &QAction::triggered,
            textEdit, &QsciScintilla::copy);

    connect(actionColler, &QAction::triggered,
            textEdit, &QsciScintilla::paste);

    connect(actionCouper, &QAction::triggered,
            textEdit, &QsciScintilla::cut);

    connect(actionSelectAll, &QAction::triggered,
            textEdit, &QsciScintilla::selectAll);

    connect(actionSupprimer, &QAction::triggered,
            textEdit, &QsciScintilla::removeSelectedText);

    connect(actionUndo, &QAction::triggered,
            this, &ScriptEditor::undo);

    connect(actionRedo, &QAction::triggered,
            this,&ScriptEditor::redo);

    connect(textEdit, &QsciScintilla::textChanged,
            this, &ScriptEditor::documentWasModified);

    connect(textEdit, &QsciScintilla::copyAvailable,
            actionCouper, &QAction::setEnabled);

    connect(textEdit, &QsciScintilla::copyAvailable,
            actionCopier, &QAction::setEnabled);

    connect(actionHelp, &QAction::triggered,
            this, &ScriptEditor::helpScript);
}

ScriptEditor::~ScriptEditor() {
    delete textEdit;
    textEdit = NULL;
    delete lexer;
    lexer = NULL;
}

void ScriptEditor::helpScript() {
    QFile help(":/Resources/functions.html");
    if (!help.open(QFile::ReadOnly)) {
        QMessageBox::warning(this, tr("Script Editor"),
                             tr("Impossible de lire l'aide':\n%2.")
                             .arg(help.errorString()));
        return;
    }
    QApplication::setOverrideCursor(Qt::WaitCursor);
    QTextEdit* helpWidget = new QTextEdit(this);
    helpWidget->setWindowFlags(Qt::Window);

    helpWidget->setHtml(help.readAll());
    help.close();

    helpWidget->show();
    helpWidget->resize(800, 600);
    helpWidget->move(QApplication::desktop()->screen()->rect().center()
                     - helpWidget->rect().center());
    QApplication::restoreOverrideCursor();
}

void ScriptEditor::documentWasModified() {
    if (textEdit->isModified()) {
        QFileInfo fichier(curFile);
        setWindowTitle(fichier.fileName() + " - Script Editor* ");
    } else {
        QFileInfo fichier(curFile);
        setWindowTitle(fichier.fileName() + " - Script Editor");
    }
}

void ScriptEditor::undo() {
    textEdit->undo();
    if (!textEdit->isUndoAvailable()) {
        actionUndo->setEnabled(false);
        actionRedo->setEnabled(true);
    }
    documentWasModified();
}

void ScriptEditor::redo() {
    textEdit->redo();
    if (!textEdit->isRedoAvailable()) {
        actionRedo->setEnabled(false);
        actionUndo->setEnabled(true);
    }
    documentWasModified();
}

bool ScriptEditor::maybeSave() {
    if (textEdit->isModified()) {
        int ret = QMessageBox::warning(
                    this, tr("Script Editor"),
                    tr("Le fichier a  ete modifie.\n"
                       "Voulez vous sauvegarder?"),
                    QMessageBox::Yes | QMessageBox::Default,
                    QMessageBox::No,
                    QMessageBox::Cancel | QMessageBox::Escape);
        if (ret == QMessageBox::Yes)
            return save();
        else if (ret == QMessageBox::Cancel)
            return false;
    }
    return true;
}

void ScriptEditor::closeEvent(QCloseEvent* event) {
    if (maybeSave()) {
        event->accept();
    } else {
        event->ignore();
    }
}

void ScriptEditor::load(const QString &file) {
    QFile fichier(file);
    if (!fichier.open(QFile::ReadOnly)) {
        QMessageBox::warning(this, tr("Script Editor"),
                             tr("Impossible de lire le fichier %1:\n%2.")
                             .arg(file)
                             .arg(fichier.errorString()));
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    textEdit->setText(fichier.readAll());
    fichier.close();

    setCurrentFile(file);
    show();
    statusBar()->showMessage(tr("File loaded"), 2000);
    QApplication::restoreOverrideCursor();
}

bool ScriptEditor::save() {
    QFile file(curFile);
    if (!file.open(QFile::WriteOnly)) {
        QMessageBox::warning(this, tr("Script Editor"),
                             tr("Impossible d'ecrire le fichier %1:\n%2.")
                             .arg(curFile)
                             .arg(file.errorString()));
        return false;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    file.write(textEdit->text().toUtf8());
    file.close();

    setCurrentFile(curFile);
    statusBar()->showMessage(tr("File saved"), 2000);
    QApplication::restoreOverrideCursor();
    return true;
}

void ScriptEditor::setCurrentFile(const QString &fileName) {
    curFile = fileName;
    textEdit->setModified(false);
    documentWasModified();
}

}  // namespace MarbleEditor
