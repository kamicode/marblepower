/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_ElementEditor.h
 * \brief
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_EDITORS_ELEMENTEDITOR_H_
#define MARBLEEDITOR_EDITORS_ELEMENTEDITOR_H_

#include <ui_ElementEditor.h>

#include "MarbleEditor/Divers.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

/*! \class ElementEditor
 * \brief
 *
 *
 */
class ElementEditor : public QDockWidget, public Ui::ElementEditor {
    Q_OBJECT

    public:
        explicit ElementEditor(QWidget *parent = 0);
        virtual ~ElementEditor() {}
        void clean();
        void saveCurrentTab();
        QString getCurrentTab();

    private:
        QString currentTab;

    private:
        DISALLOW_COPY_AND_ASSIGN(ElementEditor);
};

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_EDITORS_ELEMENTEDITOR_H_
