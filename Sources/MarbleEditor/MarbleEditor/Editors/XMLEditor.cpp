/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Editors/XMLEditor.h"
#include "MarbleEditor/Engines/Engine.h"

namespace MarbleEditor {

XMLEditor::XMLEditor(QWidget* parent) : QMainWindow(parent) {
    setAttribute(Qt::WA_DeleteOnClose);
    setupUi(this);

    textEdit = new QsciScintilla();
    textEdit->setUtf8(true);
    textEdit->setMarginLineNumbers(1, true);
    textEdit->setMarginWidth(1, 30);
    textEdit->setFolding(QsciScintilla::BoxedFoldStyle, 2);
    textEdit->setAutoIndent(true);
    textEdit->setEdgeColumn(120);
    textEdit->setEdgeMode(QsciScintilla::EdgeLine);
    textEdit->setTabWidth(4);
    textEdit->setBraceMatching(QsciScintilla::SloppyBraceMatch);
    textEdit->setAutoIndent(true);

    lexer = new QsciLexerXML();
    textEdit->setLexer(lexer);
    textEdit->setAutoCompletionSource(QsciScintilla::AcsAPIs);
    textEdit->setAutoCompletionThreshold(3);

    setCentralWidget(textEdit);

    connect(actionSauver, &QAction::triggered,
            this, &XMLEditor::save);

    connect(actionQuitter, &QAction::triggered,
            this, &XMLEditor::close);

    connect(actionCopier, &QAction::triggered,
            textEdit, &QsciScintilla::copy);

    connect(actionColler, &QAction::triggered,
            textEdit, &QsciScintilla::paste);

    connect(actionCouper, &QAction::triggered,
            textEdit, &QsciScintilla::cut);

    connect(actionSelectAll, &QAction::triggered,
            textEdit, &QsciScintilla::selectAll);

    connect(actionSupprimer, &QAction::triggered,
            textEdit, &QsciScintilla::removeSelectedText);

    connect(actionUndo, &QAction::triggered,
            this, &XMLEditor::undo);

    connect(actionRedo, &QAction::triggered,
            this, &XMLEditor::redo);

    connect(textEdit, &QsciScintilla::textChanged,
            this, &XMLEditor::documentWasModified);

    connect(textEdit, &QsciScintilla::copyAvailable,
            actionCouper, &QAction::setEnabled);

    connect(textEdit, &QsciScintilla::copyAvailable,
            actionCopier, &QAction::setEnabled);
}

XMLEditor::~XMLEditor() {
    delete textEdit;
    textEdit = NULL;
    delete lexer;
    lexer = NULL;
}

void XMLEditor::documentWasModified() {
    if (textEdit->isModified()) {
        QFileInfo fichier(curFile);
        setWindowTitle(fichier.fileName() + " - XML Editor* ");
    } else {
        QFileInfo fichier(curFile);
        setWindowTitle(fichier.fileName() + " - XML Editor");
    }
}

void XMLEditor::undo() {
    textEdit->undo();
    if (!textEdit->isUndoAvailable()) {
        actionUndo->setEnabled(false);
        actionRedo->setEnabled(true);
    }
    documentWasModified();
}

void XMLEditor::redo() {
    textEdit->redo();
    if (!textEdit->isRedoAvailable()) {
        actionRedo->setEnabled(false);
        actionUndo->setEnabled(true);
    }
    documentWasModified();
}

bool XMLEditor::maybeSave() {
    if (textEdit->isModified()) {
        int ret = QMessageBox::warning(
                    this, tr("XML Editor"),
                    tr("Le fichier a ete modifie.\n"
                       "Voulez vous sauvegarder?"),
                    QMessageBox::Yes | QMessageBox::Default,
                    QMessageBox::No,
                    QMessageBox::Cancel | QMessageBox::Escape);
        if (ret == QMessageBox::Yes)
            return save();
        else if (ret == QMessageBox::Cancel)
            return false;
    }
    return true;
}

void XMLEditor::closeEvent(QCloseEvent* event) {
    if (maybeSave()) {
        event->accept();
    } else {
        event->ignore();
    }
}

void XMLEditor::load(const QString &file) {
    QFile fichier(file);
    if (!fichier.open(QFile::ReadOnly)) {
        QMessageBox::warning(this, tr("XML Editor"),
                             tr("Impossible de lire le fichier %1:\n%2.")
                             .arg(file)
                             .arg(fichier.errorString()));
        return;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    textEdit->setText(fichier.readAll());
    fichier.close();

    setCurrentFile(file);
    show();
    statusBar()->showMessage(tr("File loaded"), 2000);
    QApplication::restoreOverrideCursor();
}

bool XMLEditor::save() {
    QFile file(curFile);
    if (!file.open(QFile::WriteOnly)) {
        QMessageBox::warning(this, tr("XML Editor"),
                             tr("Impossible d\'ecrire le fichier %1:\n%2.")
                             .arg(curFile)
                             .arg(file.errorString()));
        return false;
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);
    file.write(textEdit->text().toUtf8());
    file.close();

    setCurrentFile(curFile);
    statusBar()->showMessage(tr("File saved"), 2000);
    QApplication::restoreOverrideCursor();
    return true;
}

void XMLEditor::setCurrentFile(const QString &fileName) {
    curFile = fileName;
    textEdit->setModified(false);
    documentWasModified();
}

}  // namespace MarbleEditor
