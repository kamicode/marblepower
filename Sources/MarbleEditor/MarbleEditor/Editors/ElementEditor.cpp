/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include "MarbleEditor/Editors/ElementEditor.h"

namespace MarbleEditor {

ElementEditor::ElementEditor(QWidget* parent) : QDockWidget(parent) {
    setupUi(this);

    currentTab = "";
    clean();
}

void ElementEditor::clean() {
    while (tabWidget->count() > 0) {
        tabWidget->removeTab(0);
    }
}

void ElementEditor::saveCurrentTab() {
    QString current = tabWidget->tabText(tabWidget->currentIndex());
    if (!current.isEmpty())
        currentTab =current;
}

QString ElementEditor::getCurrentTab() {
    return currentTab;
}

}  // namespace MarbleEditor
