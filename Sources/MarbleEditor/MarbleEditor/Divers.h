/*
Copyright(C) 2012 Tristan Kahn

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

/*!
 * \file ED_Divers.h
 * \brief Several Utilities
 * \author Tristan Kahn
 * \version 1.0
 */

#ifndef MARBLEEDITOR_DIVERS_H_
#define MARBLEEDITOR_DIVERS_H_

#include "MarbleEditor/Precompiled.h"

/*! \namespace MarbleEditor
 *
 * Namespace of the "Editor" part
 */
namespace MarbleEditor {

/*! \brief A macro to disallow the copy constructor and operator= functions
 * This should be used in the private: declarations for a class
 *
 */
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
  TypeName(const TypeName&);               \
  void operator=(const TypeName&)

enum TypeBody {
    Box,
    Sphere,
    Cone,
    Capsule,
    Cylinder,
    ChamferCylinder,
    ConvexHull,
    Compound,
    Tree,
    TerrainTree,
    None
};

enum Priority {
    Config,
    Info,
    Debug,
    Warning,
    Error
};

/*! \class IrrlichtDebugDrawer
 * \brief
 *
 */
class IrrlichtDebugDrawer : public btIDebugDraw {
    public:
        explicit IrrlichtDebugDrawer(irr::video::IVideoDriver* driver);
        virtual ~IrrlichtDebugDrawer() {}
        void drawLine(const btVector3& from, const btVector3& to,
                      const btVector3& color);
        void drawContactPoint(const btVector3& PointOnB,
                              const btVector3& normalOnB, btScalar distance,
                              int /*lifeTime*/, const btVector3& color);
        void drawTriangle(const btVector3& v0, const btVector3& v1,
                          const btVector3& v2, const btVector3& color,
                          btScalar alpha);
        void drawSphere(btScalar radius, const btTransform& transform,
                        const btVector3& color);
        void reportErrorWarning(const char* /*warningString*/) {}
        void draw3dText(const btVector3& /*location*/,
                        const char* /*textString*/) {}
        void setDebugMode(int debugMode);
        int getDebugMode() const;

    private:
        irr::video::IVideoDriver* m_driver;
        int m_debugMode;

    private:
        DISALLOW_COPY_AND_ASSIGN(IrrlichtDebugDrawer);
};

/*! \class Divers
 * \brief
 *
 *
 */
class Divers {
    public:
        static void copyFolder(const QString& sourceFolder,
                               const QString& destFolder);
};

namespace Convert {

irr::video::E_DRIVER_TYPE stringDriver(QString s);
QString driverString(irr::video::E_DRIVER_TYPE s);

TypeBody stringBody(QString s);
QString bodyString(TypeBody s);

bool stringBool(QString s);
QString boolString(bool s);

irr::video::E_MATERIAL_TYPE stringMaterialType(QString s);
QString materialTypeString(irr::video::E_MATERIAL_TYPE s);

irr::video::E_COMPARISON_FUNC stringZBuffer(QString s);
QString zBufferString(irr::video::E_COMPARISON_FUNC s);

irr::video::E_ANTI_ALIASING_MODE stringAntiAliasing(QString s);
QString antiAliasingString(irr::video::E_ANTI_ALIASING_MODE s);

irr::video::E_COLOR_PLANE stringColorMask(QString s);
QString colorMaskString(irr::video::E_COLOR_PLANE s);

irr::video::E_COLOR_MATERIAL stringColorMaterial(QString s);
QString colorMaterialString(irr::video::E_COLOR_MATERIAL s);

irr::video::E_LIGHT_TYPE stringLightType(QString s);
QString lightTypeString(irr::video::E_LIGHT_TYPE s);

irr::video::E_FOG_TYPE stringFogType(QString s);

QString terrainPatchString(irr::scene::E_TERRAIN_PATCH_SIZE e);
irr::scene::E_TERRAIN_PATCH_SIZE stringTerrainPatch(QString s);

void EulerToQuaternion(const btVector3 &euler, btQuaternion &quat);

void QuaternionToEuler(const btQuaternion &TQuat, btVector3 &TEuler);

Priority stringPriority(QString s);

QString PriorityString(Priority s);

}  // namespace Convert

}  // namespace MarbleEditor

#endif  // MARBLEEDITOR_DIVERS_H_
