@echo off

set CppCheckOptions=-j 4 --check-config --quiet --force --std=posix --std=c++11 --xml-version=2 --inconclusive --enable=warning,style,performance,portability,information,missingInclude --suppress=missingIncludeSystem
set CppLintOptions=C:\Python27\python.exe cpplint.py --verbose=0

:Marble
cppcheck.exe %CppCheckOptions% -I ..\Sources\Marble ..\Sources\Marble 2> Results\CppCheck_Marble_Result.xml
echo CppLint > Results\CppLint_Marble_Result.txt 2>&1
set cppLintFiles=
for /r ..\Sources\Marble %%f in (*.h *.cpp *.cc) do call :CppLintMarble %%f
%CppLintOptions% --root=Sources/Marble %cppLintFiles% >> Results\CppLint_Marble_Result.txt 2>&1
goto :MarbleEditor

:CppLintMarble
set cppLintFiles=%cppLintFiles% "%1"
goto :eof

:MarbleEditor
cppcheck.exe %CppCheckOptions% -I ..\Sources\MarbleEditor ..\Sources\MarbleEditor 2> Results\CppCheck_MarbleEditor_Result.xml
echo CppLint > Results\CppLint_MarbleEditor_Result.txt 2>&1
set cppLintFiles=
for /r ..\Sources\MarbleEditor %%f in (*.h *.cpp *.cc) do call :CppLintMarbleEditor %%f
%CppLintOptions% --root=Sources/MarbleEditor %cppLintFiles% >> Results\CppLint_MarbleEditor_Result.txt 2>&1
goto :eof

:CppLintMarbleEditor
set cppLintFiles=%cppLintFiles% "%1"
goto :eof

:eof
